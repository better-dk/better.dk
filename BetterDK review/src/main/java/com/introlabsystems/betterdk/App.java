package com.introlabsystems.betterdk;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.introlabsystems.betterdk.connection.database.PostgresDBConnection;
import com.introlabsystems.betterdk.provider.PropertyLoader;
import com.introlabsystems.betterdk.scraping.DBProcessing;
import com.introlabsystems.betterdk.scraping.dto.LinkDTO;
import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.scraping.processing.FacebookScraper;
import com.introlabsystems.betterdk.scraping.processing.UrlConnectionGoogleScraper;
import com.introlabsystems.betterdk.scraping.processing.YelpScraper;
import com.introlabsystems.betterdk.scraping.processing.YelpScraperApi;
import com.introlabsystems.betterdk.search.DBManipulation;
import com.introlabsystems.betterdk.search.dto.CompanyDTO;
import com.introlabsystems.betterdk.search.dto.ReviewLinkDTO;
import com.introlabsystems.betterdk.search.site.LinkSearch;
import com.introlabsystems.betterdk.search.site.impl.LinkSearchFacebook;
import com.introlabsystems.betterdk.search.site.impl.LinkSearchGoogle;
import com.introlabsystems.betterdk.search.site.impl.LinkSearchTripadvisor;
import com.introlabsystems.betterdk.search.site.impl.LinkSearchYelp;
import com.introlabsystems.betterdk.search.site.impl.searx.LinkSearchSearxFacebook;
import com.introlabsystems.betterdk.search.site.impl.searx.LinkSearchSearxTripadvisor;
import com.introlabsystems.betterdk.search.site.impl.searx.LinkSearchSearxYelp;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static com.introlabsystems.betterdk.analytic.CustomerIoAnalytic.sendToCustomerIO;
import static com.introlabsystems.betterdk.analytic.CustomerIoAnalytic.sendToCustomerIOGoogle;
import static java.lang.Thread.sleep;

public class App {

    private static final Logger logger = Logger.getLogger(App.class);
    private static final String FACEBOOK_URL = "facebookurl";
    private static final String FACEBOOK = "facebook";
    private static final String YELP_URL = "yelpurl";
    private static final String YELP = "yelp";
    private static final String TRIPADVISOR_URL = "tripadvisorurl";
    private static final String TRIPADVISOR = "tripadvisor";
    private static final String GOOGLE = "google";

    @Parameter(names = {"-c", "--config"}, description = "Path to configuration file")
    private static String config;
    @Parameter(names = {"-h", "--help"}, help = true)
    private boolean help = false;
    @Parameter(names = {"--search"}, description = "Run Search Link")
    private static boolean search = false;
    @Parameter(names = {"--searchOnBaseSite"}, description = "Run Search Link On Base Site")
    private static boolean searchOnBaseSite = false;
    @Parameter(names = {"--scrape"}, description = "Run Scraping Link")
    private static boolean scrape = false;
    @Parameter(names = {"--scrapeWithYelpApi"}, description = "Run Scraping Link With Yelp API")
    private static boolean scrapeWithYelpApi = false;

    public static void main(String[] argv) {

        checkCommandLineArguments(argv);
        if (config == null) {
            config = "config.properties";
        }
        final PropertyLoader propertyLoader = new PropertyLoader(config);
        if (search) {
            runSearchLinkSearx(propertyLoader,458);
        } else if (searchOnBaseSite) {
            runSearchLinkOnBaseSite(propertyLoader,458);
        } else if (scrape) {
            runScraping(propertyLoader, false);
        } else if (scrapeWithYelpApi) {
            runScraping(propertyLoader, true);
        } else {
            System.out.println("Run with --help ");
        }


    }

    private static void runScraping(PropertyLoader propertyLoader, boolean scrapeWithYelpApi) {
        if (scrapeWithYelpApi) {
            scrapeFromYelpAndTripadvisor(propertyLoader, YELP_URL, YELP);
            scrapeFromYelpAndTripadvisor(propertyLoader, TRIPADVISOR_URL, TRIPADVISOR);
            scrapeGoogle(propertyLoader);
            scrapeYelpWithApi(propertyLoader);
            scrapeFacebook(propertyLoader);

        } else {
            scrapeGoogle(propertyLoader);
            scrapeFromYelpAndTripadvisor(propertyLoader, YELP_URL, YELP);
            scrapeFromYelpAndTripadvisor(propertyLoader, TRIPADVISOR_URL, TRIPADVISOR);
            scrapeFacebook(propertyLoader);
        }
    }

    private static void updateDB(DBProcessing dbProcessing, LinkDTO entry, List<Review> review) throws InterruptedException {

        dbProcessing.insertReviews(review);
        dbProcessing.updateDB(entry.getCompanyId());
        logger.info("Updated " + entry.getCompanyId());
        sleep(7000);

    }

    private static void updateReviewLinkInDB(DBManipulation dbManipulation, CompanyDTO entry, List<ReviewLinkDTO> links) throws InterruptedException {

        dbManipulation.insertLinks(links);
        dbManipulation.installProcessedSearch(links);
        logger.info("Updated " + entry.getId());
        sleep(5000);

    }

    private static void scrapeGoogle(PropertyLoader propertyLoader) {
        Runnable googleTask = () -> {
            PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
            dbConnection.open();
            DBProcessing dbProcessing = new DBProcessing(dbConnection);
            Set<Map.Entry<Integer, String>> googleLink;
            if (dbProcessing.getGoogleLinks(false).isEmpty()) {
                googleLink = dbProcessing.getGoogleLinks(true).entrySet();
                scrapeFromGoogleLink(dbProcessing, googleLink, false);
            } else {
                googleLink = dbProcessing.getGoogleLinks(false).entrySet();
                scrapeFromGoogleLink(dbProcessing, googleLink, true);
            }

            dbConnection.close();
        };

        Thread threadGoogle = new Thread(googleTask);
        threadGoogle.start();
    }

    private static void scrapeFromGoogleLink(DBProcessing dbProcessing, Set<Map.Entry<Integer, String>> googleLink, boolean isScraped) {
        UrlConnectionGoogleScraper googlePlacesScraper = new UrlConnectionGoogleScraper();
        for (Map.Entry<Integer, String> entry : googleLink) {
            logger.info("Company id: " + entry.getKey());
            List<Review> newReviews = new ArrayList<>();
            try {

                List<Review> reviews = googlePlacesScraper.getReviews(entry.getValue(), entry.getKey());
                for (Review review : reviews) {
                    if (!dbProcessing.inspectionDuplicatesGoogleAndFacebook(entry.getKey(), GOOGLE).contains(Long.valueOf(review.getId()))) {
                        newReviews.add(review);
                    }
                }
                dbProcessing.insertReviews(newReviews);
                dbProcessing.updateDB(entry.getKey());
                dbProcessing.switchIsScrape(entry.getKey(), isScraped, GOOGLE);
                sendToCustomerIOGoogle(newReviews, dbProcessing, entry, isScraped);

                logger.info("Updated " + entry.getKey());
                sleep(10000);
            } catch (Exception e) {
                logger.error("exception with companyId = " + entry.getKey() +
                        " and google url: " + entry.getValue() + "\n" + e.getMessage(), e);
            }
        }
    }

    private static void scrapeFacebook(PropertyLoader propertyLoader) {
        Runnable facebookTask = () -> {
            PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
            dbConnection.open();
            DBProcessing dbProcessing = new DBProcessing(dbConnection);
            FacebookScraper scraper = new FacebookScraper();
            try {
                scraper.loginToFacebook();
                List<LinkDTO> facebookLink;
                if (dbProcessing.getLinksFromDB(FACEBOOK_URL, FACEBOOK, false).isEmpty()) {
                    facebookLink = dbProcessing.getLinksFromDB(FACEBOOK_URL, FACEBOOK, true);
                    scrapeFromFacebookLink(dbProcessing, scraper, facebookLink, false);
                } else {
                    facebookLink = dbProcessing.getLinksFromDB(FACEBOOK_URL, FACEBOOK, false);
                    scrapeFromFacebookLink(dbProcessing, scraper, facebookLink, true);
                }

                scraper.closeWebDriver();
            } catch (IOException | InterruptedException | SQLException e) {
                e.printStackTrace();
            }
            dbConnection.close();
        };

        Thread threadFacebook = new Thread(facebookTask);
        threadFacebook.start();
    }

    private static void scrapeFromFacebookLink(DBProcessing dbProcessing, FacebookScraper scraper, List<LinkDTO> facebookLink, boolean isScrape) {
        for (LinkDTO entry : facebookLink) {
            logger.info("Company id: " + entry.getCompanyId());
            List<Review> newReviews = new ArrayList<>();
            try {
                List<Review> allScrapedReviews = scraper.getReviews(entry.getUrl(), entry.getCompanyId());
                for (Review review : allScrapedReviews) {
                    if (!dbProcessing.inspectionDuplicatesGoogleAndFacebook(entry.getCompanyId(), FACEBOOK).contains(Long.valueOf(review.getId()))) {
                        newReviews.add(review);
                    }
                }
                updateDB(dbProcessing, entry, newReviews);
                dbProcessing.switchIsScrape(entry.getCompanyId(), isScrape, FACEBOOK);
                sendToCustomerIO(newReviews, dbProcessing, entry, isScrape);
            } catch (Exception e) {
                logger.error("exception with companyId = " + entry.getCompanyId() +
                        " and facebook url: " + entry.getUrl() + "\n" + e.getMessage(), e);
            }
        }
    }

    private static void scrapeFromYelpAndTripadvisor(PropertyLoader propertyLoader,String siteUrl,String site) {
        new Thread(() -> {
            PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
            dbConnection.open();
            DBProcessing dbProcessing = new DBProcessing(dbConnection);
            scrapeYelpAndTripadvisor(dbProcessing, siteUrl, site);
            dbConnection.close();
        }).start();
    }

    private static void scrapeFromYelpAndTripadvisorLink(DBProcessing dbProcessing, List<LinkDTO> yelpLink, boolean isScraped, String site) {
        for (LinkDTO entry : yelpLink) {
            YelpScraper yelpScraper = new YelpScraper();
            logger.info("Company id: " + entry.getCompanyId());
            List<Review> newReviews = new ArrayList<>();
            try {
                List<Review> yelpScraperReviews = yelpScraper.getReviews(
                        entry.getUrl(),
                        entry.getCompanyId(),
                        dbProcessing.inspectionDuplicatesTripadvisorAndYelp(entry.getCompanyId(), site));
                for (Review review : yelpScraperReviews) {
                    if (!dbProcessing.inspectionDuplicatesGoogleAndFacebook(entry.getCompanyId(), site).contains(Long.valueOf(review.getId()))) {
                        newReviews.add(review);
                    }
                }
                updateDB(dbProcessing, entry, newReviews);
                dbProcessing.switchIsScrape(entry.getCompanyId(), isScraped, site);
                sendToCustomerIO(newReviews, dbProcessing, entry, isScraped);
            } catch (Exception e) {
                logger.error("exception with companyId = " + entry.getCompanyId() +
                        " and " + site + " url: " + entry.getUrl() + "\n" + e.getMessage(), e);
            }

        }
    }

    private static void scrapeYelpWithApi(PropertyLoader propertyLoader) {
        new Thread(() -> {
            PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
            dbConnection.open();

            DBProcessing dbProcessing = new DBProcessing(dbConnection);

            try {
                List<LinkDTO> yelpLink;
                if (dbProcessing.getLinksFromDB(YELP_URL, YELP, false).isEmpty()) {
                    yelpLink = dbProcessing.getLinksFromDB(YELP_URL, YELP, true);
                } else {
                    yelpLink = dbProcessing.getLinksFromDB(YELP_URL, YELP, false);
                }
                for (LinkDTO entry : yelpLink) {
                    YelpScraperApi yelpScraperApi = new YelpScraperApi();
                    logger.info("Company id: " + entry.getCompanyId());

                    try {

                        List<Review> yelpScraperReviews = yelpScraperApi.getReviews(
                                entry.getUrl(),
                                entry.getCompanyId()
                        );
                        updateDB(dbProcessing, entry, yelpScraperReviews);
                        dbProcessing.switchIsScrape(entry.getCompanyId(), true, YELP);
                    } catch (Exception e) {
                        logger.error("exception with companyId = " + entry.getCompanyId() +
                                " and yelp api url: " + entry.getUrl() + "\n" + e.getMessage(), e);
                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            dbConnection.close();
        }).start();
    }

    private static void scrapeYelpAndTripadvisor(DBProcessing dbProcessing, String siteUrl, String site) {
        try {
            List<LinkDTO> tripadvisorLink;
            if (dbProcessing.getLinksFromDB(siteUrl, site, false).isEmpty()) {
                tripadvisorLink = dbProcessing.getLinksFromDB(siteUrl, site, true);
                scrapeFromYelpAndTripadvisorLink(dbProcessing, tripadvisorLink, false, site);
            } else {
                tripadvisorLink = dbProcessing.getLinksFromDB(siteUrl, site, false);
                scrapeFromYelpAndTripadvisorLink(dbProcessing, tripadvisorLink, true, site);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void runSearchLinkSearx(PropertyLoader propertyLoader, int industryId) {
        PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
        dbConnection.open();

        DBManipulation dbManipulation = new DBManipulation(dbConnection);
        for (CompanyDTO entry : dbManipulation.getCompanyInfoFromDB(industryId)) {
            LinkSearchSearxTripadvisor linkSearchSearxTripadvisor = new LinkSearchSearxTripadvisor();
            LinkSearchSearxFacebook linkSearchSearxFacebook = new LinkSearchSearxFacebook();
            LinkSearchSearxYelp linkSearchSearxYelp = new LinkSearchSearxYelp();
            LinkSearchGoogle linkSearchGoogle = new LinkSearchGoogle();

            logger.info("Company id: " + entry.getId());
            setLinkToDB(dbManipulation, entry, linkSearchGoogle, linkSearchSearxFacebook, linkSearchSearxYelp, linkSearchSearxTripadvisor);
        }
        dbConnection.close();
    }

    private static void runSearchLinkOnBaseSite(PropertyLoader propertyLoader, int industryId) {

        PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
        dbConnection.open();

        DBManipulation dbManipulation = new DBManipulation(dbConnection);
        for (CompanyDTO entry : dbManipulation.getCompanyInfoFromDB(industryId)) {
            LinkSearchFacebook facebookSearch = new LinkSearchFacebook();
            LinkSearchYelp yelpSearch = new LinkSearchYelp();
            LinkSearchTripadvisor tripadvisorSearch = new LinkSearchTripadvisor();
            LinkSearchGoogle linkSearchGoogle = new LinkSearchGoogle();

            logger.info("Company id: " + entry.getId());
            setLinkToDB(dbManipulation, entry, linkSearchGoogle, facebookSearch, yelpSearch, tripadvisorSearch);
        }
        dbConnection.close();
    }

    private static void setLinkToDB(DBManipulation dbManipulation, CompanyDTO entry, LinkSearch linkSearchGoogle, LinkSearch facebookSearch, LinkSearch yelpSearch, LinkSearch tripadvisorSearch) {
        try {
            List<ReviewLinkDTO> searchLinks = new ArrayList<>();
            ReviewLinkDTO reviewLinkDTO = new ReviewLinkDTO();
            reviewLinkDTO.setCompanyId(entry.getId());
            reviewLinkDTO.setFacebookUrl(getReviewLink(facebookSearch, entry));
            reviewLinkDTO.setYelpUrl(getReviewLink(yelpSearch, entry));
            reviewLinkDTO.setTripadvisorUrl(getReviewLink(tripadvisorSearch, entry));
            reviewLinkDTO.setGooglePlaceUrl(getReviewLink(linkSearchGoogle, entry));
            reviewLinkDTO.setScrapeFacebook(false);
            reviewLinkDTO.setScrapeGoogle(false);
            reviewLinkDTO.setScrapeTripadvisor(false);
            reviewLinkDTO.setScrapeYelp(false);

            searchLinks.add(reviewLinkDTO);
            updateReviewLinkInDB(dbManipulation, entry, searchLinks);
        } catch (Exception e) {
            logger.error("exception with companyId = " + entry.getId() +
                    "\n" + e.getMessage(), e);
        }
    }

    private static String getReviewLink(LinkSearch linkSearch, CompanyDTO companyDTO) {
        try {
            return linkSearch.searchLinks(
                    companyDTO.getId(),
                    companyDTO.getName(),
                    companyDTO.getCity(),
                    companyDTO.getStreetName(),
                    companyDTO.getStreetNumber()
            );
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void checkCommandLineArguments(String[] argv) {
        App app = new App();

        try {
            JCommander jCommander = new JCommander(app, argv);
            if (app.help) {
                jCommander.usage();
                System.exit(0);
            }

        } catch (ParameterException e) {
            logger.error(e.getMessage());
            logger.info("Use -h or --help for detail info");
            System.exit(0);
        }
    }

    private static void changeExternalReviewIds(PropertyLoader propertyLoader) {
        PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
        dbConnection.open();
        DBProcessing dbProcessing = new DBProcessing(dbConnection);
        Map<Long, Review> list = new HashMap<>();
        int cont = 0;
        for (Review entry : dbProcessing.getInfo()) {

            logger.info("Company id: " + entry.getCompanyId());
            try {
                long oldId = entry.getId();
                entry.setId((long) (entry.getAuthor() + String.valueOf(entry.getCompanyId()) + entry.getLink() + entry.getText()).hashCode());
                list.put(oldId, entry);
            } catch (Exception e) {
                logger.error("exception with companyId = " + entry.getCompanyId() +
                        " and yelp url: " + entry.getCompanyId() + "\n" + e.getMessage(), e);
            }
            cont++;
        }

        try {
            dbProcessing.updateExternalReviewId(list);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        dbConnection.close();
    }

    private static void cutGoogleReview(PropertyLoader propertyLoader) {
        final String original = "(Original)";
        PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
        dbConnection.open();
        DBProcessing dbProcessing = new DBProcessing(dbConnection);
        try {
            List<Review> rewOld = dbProcessing.getRewOld();
            for (Review entry : rewOld) {
                logger.info("Company id: " + entry.getCompanyId());
                logger.info("Company getText: " + entry.getText());
                if (entry.getText().contains(original)) {
                    entry.setText(StringUtils.substringAfter(entry.getText(), original));
                }
                logger.info("Company getTextAfter: " + entry.getText());
            }
            dbProcessing.changeReview(rewOld);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        dbConnection.close();
    }

    private static void runUpdateRatingAndCount(PropertyLoader propertyLoader) {
        PostgresDBConnection dbConnection = new PostgresDBConnection(propertyLoader.load());
        dbConnection.open();
        DBProcessing dbProcessing = new DBProcessing(dbConnection);
        try {
            updateRatingAndCount(dbProcessing);
        } catch (InterruptedException | SQLException e) {
            e.printStackTrace();
        }
        dbConnection.close();
    }

    private static void updateRatingAndCount(DBProcessing dbProcessing) throws InterruptedException, SQLException {

        for (Integer id : dbProcessing.getCompanyIdsWithReviews()) {
            dbProcessing.updateDB(id);
            logger.info("Updated " + id);
        }
    }

}
