package com.introlabsystems.betterdk.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author : Vitalii M.; create date: 6/13/16.
 */
@ToString
@Getter
@Setter
public class DBProperties {

    private String database;
    private String dbUser;
    private String dbPassword;
}
