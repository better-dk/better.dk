package com.introlabsystems.betterdk.provider;

import com.introlabsystems.betterdk.domain.DBProperties;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author : Vitalii M.; create date: 6/13/16.
 */
public class PropertyLoader {

    private Properties prop = new Properties();

    public PropertyLoader(String file) {
        try (InputStream input = new FileInputStream(file)) {
            prop.load(input);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(PropertyLoader.class);
            logger.error(ex.getMessage(), ex);
        }
    }

    public DBProperties load() {

        DBProperties dbProperties = new DBProperties();
        dbProperties.setDatabase(prop.getProperty("database"));
        dbProperties.setDbUser(prop.getProperty("dbuser"));
        dbProperties.setDbPassword(prop.getProperty("dbpassword"));
        return dbProperties;
    }

}
