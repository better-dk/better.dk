package com.introlabsystems.betterdk.scraping.processing;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabsystems.betterdk.scraping.date.impl.ScraperDateImpl;
import com.introlabsystems.betterdk.scraping.dto.Review;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Liubov Zavorotna
 */
public class YelpScraperApi {

    private static final String PATTERN = "biz.*";
    private static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    private static final String END_PART_API_ADDRESS = "/reviews";
    private static final String FIRST_API_ADDRESS = "https://api.yelp.com/v3/businesses/";

    private static final String AUTHORIZATION = "Authorization";
    private static final String ACCESS_TOKEN = "Bearer ZLvThXklExvaTrTUCJ3BW8DUjI1-0aC69s9smnmxxB5E_kEtMohjtGlX8u-pbXYa8Og2ElaGXOba5v4wt4y27JKoTRzmenlTQWVud3rJ3PcQNBOOQtY8b03pAxjiWHYx";
    private static final String JSON_HEADER = "reviews";
    private static final String AUTHOR = "author";
    private static final String RATING = "rating";
    private static final String NAME = "name";
    private static final String TEXT = "text";
    private static final String SITE = "yelp";

    private String getUrlForApi(String url) {

        Matcher matcher = COMPILED_PATTERN.matcher(url);
        if (matcher.find()) {
            String cutUrl = String.valueOf(matcher.group());
            return FIRST_API_ADDRESS + cutUrl.replace("biz/", "") + END_PART_API_ADDRESS;
        }
        return url;
    }


    public List<Review> getReviews(String url, int companyId) throws IOException {
        String newUrl = getUrlForApi(url);


        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(newUrl);

        request.addHeader(AUTHORIZATION, ACCESS_TOKEN);
        HttpResponse response = client.execute(request);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        JsonObject parse = new JsonParser().parse(result.toString()).getAsJsonObject();
        List<Review> reviews = new ArrayList<>();
        for (JsonElement reviewsInJson : parse.getAsJsonArray(JSON_HEADER)) {
            Review review = new Review();
            review.setRating(
                    reviewsInJson
                            .getAsJsonObject()
                            .getAsJsonPrimitive(RATING)
                            .getAsInt());

            review.setAuthor(
                    reviewsInJson
                            .getAsJsonObject()
                            .getAsJsonObject("user")
                            .getAsJsonPrimitive(NAME)
                            .getAsString());
            review.setDate(String.valueOf(new ScraperDateImpl().getDate()));
            review.setText(
                    reviewsInJson.
                            getAsJsonObject().
                            getAsJsonPrimitive(TEXT).
                            getAsString());
            review.setSite(SITE);
            review.setId((long) (review.getAuthor() + review.getText()).hashCode());
            review.setLink(url);
            review.setCompanyId(companyId);
            review.setReviewsFromApi(true);
            reviews.add(review);
        }
        return reviews;
    }
}