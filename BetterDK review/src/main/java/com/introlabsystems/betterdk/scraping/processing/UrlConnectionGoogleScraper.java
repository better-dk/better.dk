package com.introlabsystems.betterdk.scraping.processing;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.introlabsystems.betterdk.scraping.date.impl.ScraperDateImpl;
import com.introlabsystems.betterdk.scraping.dto.Review;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

/**
 * @author vitalii.
 */
public class UrlConnectionGoogleScraper {

    public List<Review> getReviews(String link, int companyId) throws IOException, InterruptedException {
        List<Review> googleReviews = new ArrayList<>();
        List<Review> tempReviews;

        String url = makeRequestUrl(link);
        for (int i = 0; i < 100; i++) {
            String request = String.format(url, i * 10);
            URL oracle = new URL(request);
            URLConnection yc = oracle.openConnection();

            Charset charset = Charset.forName("UTF8");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream(), charset));
            StringBuilder stringBuilder = new StringBuilder();
            in.lines().forEach(stringBuilder::append);
            in.close();
            try {
                tempReviews = parseResponse(stringBuilder.toString().substring(4), companyId, link);
            } catch (Exception e) {
                break;
            }
            googleReviews.addAll(tempReviews);
            if (tempReviews.size() < 10) {
                break;
            }
            sleep(5000);
        }
        return googleReviews;
    }

    private List<Review> parseResponse(String json, int companyId, String url) throws Exception {

        List<Review> googleReviews = new ArrayList<>();
        JsonElement jsonElement = new JsonParser().parse(json);
        if (jsonElement.getAsJsonArray().get(0).isJsonNull()) {
            return Collections.emptyList();
        }
        JsonArray results = jsonElement.getAsJsonArray().get(0).getAsJsonArray();

        for (JsonElement element : results) {
            JsonArray reviewArray = element.getAsJsonArray();
            String author_name = reviewArray.get(0).getAsJsonArray().get(1).getAsJsonPrimitive().getAsString();
            String textOrigin = reviewArray.get(3).isJsonNull() ? "" : reviewArray.get(3).getAsJsonPrimitive().getAsString();
            String textSubstringAfterOrigin;
            if (textOrigin.contains("(Original)")) {
                textSubstringAfterOrigin = StringUtils.substringAfter(textOrigin, "(Original)");
            } else {
                textSubstringAfterOrigin = textOrigin;
            }
            int rating = reviewArray.get(4).getAsJsonPrimitive().getAsInt();

            Review googleReview = new Review();
            googleReview.setAuthor(author_name);
            googleReview.setText(textSubstringAfterOrigin);
            googleReview.setLink(url);
            googleReview.setRating(rating);
            googleReview.setSite("google");
            googleReview.setDate(String.valueOf(new ScraperDateImpl().getDate()));
            googleReview.setCompanyId(companyId);
            googleReview.setId((long) (googleReview.getAuthor() + String.valueOf(companyId) + googleReview.getLink()).hashCode());
            googleReviews.add(googleReview);
        }
        return googleReviews;
    }

    private String makeRequestUrl(String originUrl) {

        Pattern pattern = Pattern.compile(":(.{10,20}?)!8m2!");
        Matcher matcher = pattern.matcher(originUrl);
        String uid = null;
        if (matcher.find()) {
            uid = matcher.group(1);
        }
        return "https://www.google.com/maps/preview/reviews?" +
                "authuser=0&" +
                "hl=en&" +
                "pb=!1s0x464be4243160eeb3:" + uid + "!2i%s!3i10!4e6!7m3!2b1!5b1!6b1";

    }
}
