package com.introlabsystems.betterdk.scraping.processing;


import com.introlabsystems.betterdk.connection.web.PhantomJsConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.scraping.date.impl.ScraperDateImpl;
import com.introlabsystems.betterdk.scraping.dto.Review;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * @author vitalii.
 */
public class FacebookScraper {
    private SeleniumConnection connection = new PhantomJsConnection();

    private static final String SITE = "facebook";

    private static final String PAGES_NAVIGATION_REVIEWS_SELECTOR = "#pages_navigation a[href*=reviews]";
    private static final String PAGES_WITHOUT_NAVIGATION_REVIEWS_SELECTOR = "#page_recommendations_browse_pager .pam.uiBoxWhite.noborder.uiMorePagerPrimary";
    private static final String MORE_REVIEWS_BUTTON_SELECTOR = "#page_recommendations_browse_pager .pam.uiBoxWhite.noborder.uiMorePagerPrimary";
    private static final String PAGE_REVIEWS_TAB_LIST_SELECTOR = "#page_reviews_tab_list > li";
    private static final String AUTHOR_SELECTOR = ".clearfix strong";
    private static final String RATING_SELECTOR = ".img u";
    private static final String REVIEW_SELECTOR = "#page_reviews_tab_list ._4w5j span:not([class])";

    private static final String LOGIN_LINK = "https://www.facebook.com/login.php";
    private static final String INPUT_TEXT_SELECTOR = "input[type=text]";
    private static final String INPUT_PASSWORD_SELECTOR = "input[type=password]";
    private static final String LOGINBUTTON = "#loginbutton";

    private static final String LOGIN = "380933513231";
    private static final String PASSWORD = "g5e6g54e89";

    private static final String PATTERN = "\\d";
    private static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    private static final String REVIEW_CARD_CSS = "div[role=article]";
    private static final String RATING_CSS = ".fcg i u";//first char
    private static final String AUTHOR_CSS = ".fcg .fwb span.profileLink, .fcg .fwb .profileLink";
    private static final String REVIEW_CSS = ".userContent p";

    public List<Review> getReviews(String url, int companyId) throws IOException, InterruptedException {

        connection.get(url);
        connection.waitForMarkup(5000);

        if (connection.findElements(By.cssSelector(PAGES_NAVIGATION_REVIEWS_SELECTOR)).size() > 0) {
            connection.findElement(By.cssSelector(PAGES_NAVIGATION_REVIEWS_SELECTOR)).click();

            return getFromPageReviews(url, companyId);

        } else if (connection.findElements(By.cssSelector(PAGES_WITHOUT_NAVIGATION_REVIEWS_SELECTOR)).size() > 0) {
            if (connection.findElements(By.cssSelector(MORE_REVIEWS_BUTTON_SELECTOR)).size() > 0) {
                clickMoreReviewsButton();
            }
            return parseCompanyPage(url, companyId);

        } else {
            return Collections.emptyList();
        }


    }

    private void clickMoreReviewsButton() {
        do {
            connection.waitForMarkup(5000);
            connection.findElement(By.cssSelector(MORE_REVIEWS_BUTTON_SELECTOR)).click();
            connection.waitForMarkup(5000);
        }
        while (connection.findElements(By.cssSelector(MORE_REVIEWS_BUTTON_SELECTOR)).size() > 0);
    }

    private List<Review> getFromPageReviews(String url, int companyId) {

        connection.scrollDown(5000);
        Document document = Jsoup.parse(connection.getPageSource());

        return parseCompanyReviewPage(document, url, companyId);
    }

    private List<Review> parseCompanyReviewPage(Document document, String url, int companyId) {

        Elements elements = document.select(REVIEW_CARD_CSS);
        return elements
                .stream()
                .filter(element -> !element.select(AUTHOR_CSS).text().isEmpty())
                .map(element -> {
                    Review review = new Review();
                    try {
                        review.setRating(Integer.parseInt(String.valueOf(element.select(RATING_CSS).text().charAt(0))));
                    } catch (Exception ignored) {
                    }
                    review.setAuthor(element.select(AUTHOR_CSS).text());
                    review.setDate(String.valueOf(new ScraperDateImpl().getDate()));
                    review.setText(element.select(REVIEW_CSS).text());
                    review.setSite(SITE);
                    review.setLink(url);
                    review.setCompanyId(companyId);
                    review.setId((long) (review.getAuthor() + String.valueOf(companyId) + review.getLink()).hashCode());
                    return review;
                }).collect(Collectors.toList());
    }


    private List<Review> parseCompanyPage(String url, int companyId) {

        List<Review> reviews = new ArrayList<>();
        List<Review> reviewsOnPage = connection
                .findElements(By
                        .cssSelector(PAGE_REVIEWS_TAB_LIST_SELECTOR))
                .stream()
                .filter(element -> element.findElement(By.cssSelector(AUTHOR_SELECTOR)).isEnabled())
                .map(getElementReviewFunction(url, companyId))
                .collect(Collectors.toList());
        reviews.addAll(reviewsOnPage);
        return reviews;
    }

    private Function<WebElement, Review> getElementReviewFunction(String url, int companyId) {
        return element -> {
            Review review = parseReviewEntry(element, Jsoup.parse(element.getAttribute("innerHTML")));
            review.setLink(url);
            review.setCompanyId(companyId);
            review.setId((long) (review.getAuthor() + String.valueOf(companyId) + review.getLink()).hashCode());
            return review;

        };
    }

    private Review parseReviewEntry(WebElement element, Document document) {
        Review review = new Review();

        review.setRating(parseRating(String.valueOf(document.select(RATING_SELECTOR).text())));
        review.setAuthor(element.findElement(By.cssSelector(AUTHOR_SELECTOR)).getText());
        review.setDate(String.valueOf(new ScraperDateImpl().getDate()));
        review.setText(String.valueOf(element.findElement(By.cssSelector(REVIEW_SELECTOR)).getText()));
        review.setSite(SITE);

        return review;
    }


    public void loginToFacebook() throws IOException, InterruptedException {
        connection.get(LOGIN_LINK);
        connection.waitForMarkup(5000);
        connection.findElement(By.cssSelector(INPUT_TEXT_SELECTOR)).sendKeys(LOGIN);
        connection.waitForMarkup(3000);
        connection.findElement(By.cssSelector(INPUT_PASSWORD_SELECTOR)).sendKeys(PASSWORD);
        connection.waitForMarkup(3000);
        connection.findElement(By.cssSelector(LOGINBUTTON)).click();
        connection.waitForMarkup(3000);
    }

    private int parseRating(final String textClass) {

        Matcher matcher = COMPILED_PATTERN.matcher(textClass);
        if (matcher.find()) {
            return Integer.valueOf(matcher.group());
        }
        return 0;

    }

    public void closeWebDriver() {
        connection.close();

    }
}
