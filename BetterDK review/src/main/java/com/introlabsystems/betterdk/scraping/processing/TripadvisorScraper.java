package com.introlabsystems.betterdk.scraping.processing;

import com.introlabsystems.betterdk.connection.web.ChromeConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.scraping.date.impl.ScraperDateImpl;
import com.introlabsystems.betterdk.scraping.dto.Review;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Liubov Zavorotna
 */
public class TripadvisorScraper {

    private SeleniumConnection connection = new ChromeConnection();

    private static final String PATTERN = "\\d";
    private static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    private static final String HREF = "href";
    private static final String CLASS_ATTRIBUTE = "class";
    private static final String AUTHOR_SELECTOR = ".username.mo";

    private static final String MORE_TA_LNK_SELECTOR = ".more.taLnk";
    private static final String REVIEW_CARD_SELECTOR = ".review.basic_review";
    private static final String REVIEW_CONTAINER_SELECTOR = ".review-container";
    private static final String RATING_BUBBLE_SELECTOR = ".wrap > .rating > .ui_bubble_rating";
    private static final String RATING_S_FILL_SELECTOR = ".wrap > .rating>.rate>.rating_s_fill";
    private static final String LOC_PHOTOS_TOP_SELECTOR = "#LOC_PHOTOS_TOP";
    private static final String LISTING_MAIN_SUR_SELECTOR = "#listing_main_sur";
    private static final String RADIO_BUTTON_IS_ON_SELECTOR = ".toggle > input[data-include=\"true\"]";
    private static final String RADIO_BUTTON_IS_ON_ALL_SELECTOR = ".toggle > input[value=ALL]";
    private static final String REVIEW_SELECTOR_COMMON_HTML = ".wrap > .prw_common_html:not(.mgrRspnInline) > .entry > .partial_entry";
    private static final String LISTING_MAIN_SUR_HEADING_SELECTOR = "#listing_main_sur div .HEADING";

    private static final String REVIEW_SELECTOR_ENTRY = ".entry > .partial_entry";
    private static final String NEXT_BUTTON_SELECTOR = ".pagination > .next";
    private static final String NEXT_BUTTON_SELECTOR_EXIT = ".pagination > .next.disabled";

    private static final String SITE_NAME = "tripadvisor";


    private String getPageWithReviews(String url) throws IOException, InterruptedException {
        connection.get(url);
        connection.waitForMarkup(2000);
        if (connection.findElements(By.cssSelector(LOC_PHOTOS_TOP_SELECTOR)).size() > 0) {
            return connection.findElement(By.cssSelector(MORE_TA_LNK_SELECTOR)).getAttribute(HREF);
        } else if (connection.findElements(By.cssSelector(LISTING_MAIN_SUR_SELECTOR)).size() > 0) {
            return connection.findElement(By.cssSelector(LISTING_MAIN_SUR_HEADING_SELECTOR)).getAttribute(HREF);
        }
        return url;
    }

    public List<Review> getReviews(String url, int companyId, List<Review> reviewsFromDb) throws IOException, InterruptedException {
        connection.get(getPageWithReviews(url));
        List<Review> reviews = new ArrayList<>();
        connection.waitForMarkup(5000);
        if (connection.findElements(By.cssSelector(RADIO_BUTTON_IS_ON_ALL_SELECTOR)).size() > 0) {
            connection.findElements(By.cssSelector(RADIO_BUTTON_IS_ON_ALL_SELECTOR)).get(0).click();
        }
        do {

            connection.waitForMarkup(5000);
            List<Review> reviewsOnPage = getReviews(url, companyId);

            final long countOfUnUniqueReviews = reviewsOnPage.stream()
                    .filter(reviewsFromDb::contains)
                    .count();

            final List<Review> filteredReviews = reviewsOnPage.stream()
                    .filter(review -> !reviewsFromDb.contains(review))
                    .collect(Collectors.toList());

            reviews.addAll(filteredReviews);
            connection.waitForMarkup(5000);
            if (goToNextPage() || countOfUnUniqueReviews != 0) break;
        } while (true);
        connection.close();
        return reviews;
    }


    private List<Review> getReviews(String url, int companyId) {
        if (connection.findElements(By.cssSelector(REVIEW_CARD_SELECTOR)).size() > 0) {
            return connection
                    .findElements(By
                            .cssSelector(REVIEW_CARD_SELECTOR))

                    .stream()
                    .filter(element -> element.findElement(By.cssSelector(AUTHOR_SELECTOR)).isEnabled())
                    .map(getElementReviewFunction(url, companyId))
                    .collect(Collectors.toList());
        } else if (connection.findElements(By.cssSelector(REVIEW_CONTAINER_SELECTOR)).size() > 0) {
            return connection
                    .findElements(By
                            .cssSelector(REVIEW_CONTAINER_SELECTOR))

                    .stream()
                    .filter(element -> element.findElements(By.cssSelector(AUTHOR_SELECTOR)).size() > 0)
                    .map(getElementReviewFunction(url, companyId))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }

    }

    private boolean goToNextPage() {
        if (connection.findElements(By.cssSelector(NEXT_BUTTON_SELECTOR_EXIT)).size() > 0) {
            return true;
        } else if (connection.findElements(By.cssSelector(NEXT_BUTTON_SELECTOR)).size() > 0) {

            connection.findElements(By.cssSelector(NEXT_BUTTON_SELECTOR)).get(0).click();

        } else {
            return true;
        }
        return false;
    }

    private Function<WebElement, Review> getElementReviewFunction(String url, int companyId) {
        return element -> {
            Review review = parseReviewEntry(element);
            review.setLink(url);
            review.setCompanyId(companyId);
            review.setId((long) (review.getAuthor() + String.valueOf(companyId) + review.getLink()).hashCode());
            return review;

        };
    }

    private Review parseReviewEntry(WebElement element) {
        Review review = new Review();
        review
                .setRating(
                        parseRating(
                                String.valueOf(
                                        element.findElements(By.cssSelector(RATING_S_FILL_SELECTOR)).size() > 0 ?
                                                element.findElement(By.cssSelector(RATING_S_FILL_SELECTOR)).getAttribute(CLASS_ATTRIBUTE) :
                                                element.findElement(By.cssSelector(RATING_BUBBLE_SELECTOR)).getAttribute(CLASS_ATTRIBUTE))));

        review.setAuthor(element.findElement(By.cssSelector(AUTHOR_SELECTOR)).getText());

        review.setDate(String.valueOf(new ScraperDateImpl().getDate()));
        review.setText(String.valueOf(element.findElements(By.cssSelector(REVIEW_SELECTOR_ENTRY)).size() > 0 ?
                element.findElement(By.cssSelector(REVIEW_SELECTOR_ENTRY)).getText() :
                element.findElement(By.cssSelector(REVIEW_SELECTOR_COMMON_HTML)).getText()));

        review.setSite(SITE_NAME);

        return review;
    }


    private int parseRating(final String textClass) {

        Matcher matcher = COMPILED_PATTERN.matcher(textClass);
        if (matcher.find()) {
            return Integer.valueOf(matcher.group());
        }
        return 0;

    }
}
