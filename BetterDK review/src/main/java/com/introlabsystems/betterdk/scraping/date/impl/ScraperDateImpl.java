package com.introlabsystems.betterdk.scraping.date.impl;

import com.introlabsystems.betterdk.scraping.date.ScraperDate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Liubov Zavorotna
 */
public class ScraperDateImpl implements ScraperDate {
    private static final String DATE_TEMPLATE = "dd.MM.yy";

    public String getDate() {
        long currentDateTime = System.currentTimeMillis();
        Date currentDate = new Date(currentDateTime);
        DateFormat dateFormat = new SimpleDateFormat(DATE_TEMPLATE);

        return dateFormat.format(currentDate);

    }

}
