package com.introlabsystems.betterdk.scraping.date;

/**
 * @author Liubov Zavorotna
 */
public interface ScraperDate {
    String getDate();
}
