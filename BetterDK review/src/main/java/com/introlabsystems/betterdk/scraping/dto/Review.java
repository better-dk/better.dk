package com.introlabsystems.betterdk.scraping.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author vitalii.
 */

@ToString
@Getter
@Setter
public class Review {
    private Long id;
    private String text;
    private String author;
    private String link;
    private String site;
    private String date;
    private int rating;
    private Integer companyId;
    private boolean reviewsFromApi;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        return rating == review.rating && text.equals(review.text) && author.equals(review.author);
    }

    @Override
    public int hashCode() {
        int result = text.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + rating;
        return result;
    }
}
