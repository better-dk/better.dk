package com.introlabsystems.betterdk.scraping;

import com.introlabsystems.betterdk.connection.database.PostgresDBConnection;
import com.introlabsystems.betterdk.scraping.dto.LinkDTO;
import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.search.dto.CompanyDTO;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Vitalii M.; create date: 6/13/16.
 */
public class DBProcessing {

    private Logger logger = Logger.getLogger(DBProcessing.class);

    private PostgresDBConnection dbConnection;

    public DBProcessing(PostgresDBConnection connection) {
        this.dbConnection = connection;
    }

    public CompanyDTO getCompanyInfoFromDB(int companyId) {

        String selectWithIndustry = "SELECT name,  email FROM company WHERE id=?";
        PreparedStatement statement;
        try {
            statement = dbConnection
                    .getConnection().prepareStatement(selectWithIndustry);
            statement.setInt(1, companyId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            CompanyDTO companyDTO = new CompanyDTO();

            companyDTO.setName(resultSet.getString(1));
            companyDTO.setEmail(resultSet.getString(2));
            companyDTO.setId(companyId);
            return companyDTO;
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public List<Long> inspectionDuplicatesGoogleAndFacebook(int companyId, String site) throws IOException {
        try {
            return getReviewsFromDbForGoogleAndFacebook(companyId, site).stream().map(Review::getId).collect(Collectors.toList());
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    public List<Review> inspectionDuplicatesTripadvisorAndYelp(int companyId, String site) throws IOException {
        try {
            return getReviewsFromDbForTripadvisorAndYelp(companyId, site);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }


    private List<Review> getReviewsFromDbForGoogleAndFacebook(int companyId, String site) throws SQLException {
        String select = "SELECT id FROM external_reviews WHERE companyId = ? AND site = ?";
        PreparedStatement statement = dbConnection.getConnection().prepareStatement(select);
        statement.setInt(1, companyId);
        statement.setString(2, site);

        ResultSet resultSet = statement.executeQuery();
        List<Review> oldData = new ArrayList<>();
        while (resultSet.next()) {
            Review review = new Review();
            review.setId(resultSet.getLong("id"));
            oldData.add(review);

        }
        return oldData;
    }

    private List<Review> getReviewsFromDbForTripadvisorAndYelp(int companyId, String site) throws SQLException {
        String select = "SELECT author, review, rating FROM external_reviews WHERE companyId = ? AND site = ?";
        PreparedStatement statement = dbConnection.getConnection().prepareStatement(select);
        statement.setInt(1, companyId);
        statement.setString(2, site);

        ResultSet resultSet = statement.executeQuery();
        List<Review> oldData = new ArrayList<>();
        while (resultSet.next()) {
            Review review = new Review();
            review.setAuthor(resultSet.getString("author"));
            review.setText(resultSet.getString("review"));
            review.setRating(resultSet.getInt("rating"));
            oldData.add(review);

        }
        return oldData;
    }


    private List<Review> getAll() throws SQLException {
        String select = "SELECT author, review, link, companyid,id FROM external_reviews WHERE site='tripadvisor';";
        Statement statement = dbConnection
                .getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(select);
        List<Review> reviews = new ArrayList<>();
        while (resultSet.next()) {
            Review review = new Review();
            review.setAuthor(resultSet.getString(1));
            review.setText(resultSet.getString(2));
            review.setLink(resultSet.getString(3));
            review.setCompanyId(resultSet.getInt(4));
            review.setId(resultSet.getLong(5));
            reviews.add(review);
        }
        return reviews;


    }

    public List<Review> getRewOld() throws SQLException {
        String select = "SELECT review, id FROM external_reviews; ";
        Statement statement = dbConnection
                .getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(select);
        List<Review> reviews = new ArrayList<>();
        while (resultSet.next()) {
            Review review = new Review();

            review.setText(resultSet.getString(1));

            review.setId(resultSet.getLong(2));
            reviews.add(review);
        }
        return reviews;


    }

    public List<Review> getInfo() {

        try {
            return getAll();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    public List<Integer> getCompanyIdsWithReviews() throws SQLException {

        String select = "SELECT DISTINCT company.id FROM company " +
                "INNER JOIN external_reviews ON company.id=external_reviews.companyid; ";
        Statement statement = dbConnection
                .getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(select);
        List<Integer> ids = new ArrayList<>();
        while (resultSet.next()) {

            ids.add(resultSet.getInt(1));
        }
        return ids;
    }

    public void updateExternalReviewId(Map<Long, Review> reviews) throws IOException, SQLException {

        String query = "UPDATE review_links SET yelpurl=? WHERE companyId=?;";
        try {
            dbConnection.getConnection().setAutoCommit(false);

            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);
            for (Map.Entry<Long, Review> record : reviews.entrySet()) {
                ps.setInt(1, record.getValue().getId().intValue());
                ps.setInt(2, record.getKey().intValue());
                ps.addBatch();
            }
            ps.executeBatch();
            dbConnection.getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }
    }

    public void changeReview(List<Review> reviews) throws IOException, SQLException {

        String query = "UPDATE external_reviews SET review=? WHERE id=?;";
//    String query  = "UPDATE review_links SET yelpurl=? WHERE companyId=?;";
        try {
            dbConnection.getConnection().setAutoCommit(false);

            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);
            for (Review record : reviews) {
                ps.setString(1, record.getText());
                ps.setInt(2, record.getId().intValue());
                ps.addBatch();
            }
            ps.executeBatch();
            dbConnection.getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }
    }


    public List<LinkDTO> getLinksFromDB(String siteUrl, String site, boolean isScrape) throws SQLException {

        String selectBase = "SELECT companyid, " + siteUrl + " FROM review_links WHERE "
                + siteUrl + " IS NOT NULL AND " + siteUrl + " LIKE 'http%' and is_scrape_" + site + "=" + isScrape + "";
        PreparedStatement statement = dbConnection.getConnection().prepareStatement(selectBase);
        ResultSet resultSet = statement.executeQuery();
        List<LinkDTO> linkDTOS = new ArrayList<>();
        while (resultSet.next()) {
            LinkDTO linkDto = new LinkDTO();
            linkDto.setCompanyId(resultSet.getInt(1));
            linkDto.setUrl(resultSet.getString(2));
            linkDto.setReviewId(getAllReviews(linkDto.getCompanyId()));
            linkDTOS.add(linkDto);
        }
        return linkDTOS;


    }

    public Map<Integer, String> getGoogleLinks(boolean isScrape) {
        String selectBase = "SELECT companyid, googleplaceurl FROM review_links " +
                "WHERE googleplaceurl IS NOT NULL AND googleplaceurl LIKE 'http%' and is_scrape_google=" + isScrape + "";
        try {

            Statement statement = dbConnection
                    .getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(selectBase);
            Map<Integer, String> map = new TreeMap<>();
            while (resultSet.next()) {
                map.put(resultSet.getInt(1), resultSet.getString(2));
            }
            return map;
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return Collections.emptyMap();
    }

    private List<Integer> getAllReviews(int companyId) {
        String select = "SELECT id FROM external_reviews WHERE companyId = " + companyId;
        try {
            Statement statement = dbConnection
                    .getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(select);
            List<Integer> reviewIds = new ArrayList<>();
            while (resultSet.next()) {
                reviewIds.add(resultSet.getInt(1));
            }
            return reviewIds;
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }


    public void switchIsScrape(int companyId, boolean isScrape, String site) {
        String query = "UPDATE review_links SET is_scrape_" + site + "=? WHERE companyid=?";

        try {
            dbConnection.getConnection().setAutoCommit(false);
            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);
            ps.setBoolean(1, isScrape);
            ps.setInt(2, companyId);
            ps.addBatch();
            ps.executeBatch();
            dbConnection
                    .getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }


    }

    public void insertReviews(List<Review> reviews) {

        String query = "INSERT INTO external_reviews(id, companyId, author, rating, review, link, site, publishdate, reviews_from_api) " +
                "SELECT ?,?,?,?,?,?,?,?,? WHERE NOT EXISTS (" +
                "SELECT id FROM external_reviews WHERE id = ?) LIMIT 1;";
        try {
            dbConnection.getConnection().setAutoCommit(false);

            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);
            for (Review record : reviews) {
                ps.setInt(1, record.getId().intValue());
                ps.setInt(2, record.getCompanyId());
                ps.setString(3, record.getAuthor());
                ps.setInt(4, record.getRating());
                ps.setString(5, record.getText());
                ps.setString(6, record.getLink());
                ps.setString(7, record.getSite());
                ps.setString(8, record.getDate());
                ps.setBoolean(9, record.isReviewsFromApi());
                ps.setInt(10, record.getId().intValue());
                ps.addBatch();
            }
            ps.executeBatch();
            dbConnection
                    .getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }
    }


    public void updateDB(int companyId) {

        String updateCount = "UPDATE company SET number_of_reviews = (SELECT " +
                "                                          (SELECT count(er) AS number_ofReview " +
                "                                                FROM external_reviews er " +
                "                                                WHERE er.companyid = ?) + " +
                "                                               (SELECT count(r) AS number_ofReview " +
                "                                                FROM review AS r " +
                "                                                WHERE  r.company_id=?)) WHERE id=?";

        String updateRating = "UPDATE Company SET rating = (SELECT avg(rating) * 2 FROM (SELECT rating, companyid FROM external_reviews er WHERE er.companyid = ? UNION ALL" +
                "   SELECT rating, company_id AS companyid  FROM Review r WHERE r.company_id = ?) AS sub) WHERE id = ?";
        try {
            dbConnection.getConnection().setAutoCommit(true);

            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(updateCount);
            ps.setInt(1, companyId);
            ps.setInt(2, companyId);
            ps.setInt(3, companyId);
            ps.execute();

            ps = dbConnection
                    .getConnection().prepareStatement(updateRating);
            ps.setInt(1, companyId);
            ps.setInt(2, companyId);
            ps.setInt(3, companyId);
            ps.execute();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }
    }


}
