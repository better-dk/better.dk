package com.introlabsystems.betterdk.scraping.processing;

import com.introlabsystems.betterdk.connection.web.PhantomJsConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.scraping.date.impl.ScraperDateImpl;
import com.introlabsystems.betterdk.scraping.dto.Review;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Liubov Zavorotna
 */
public class YelpScraper {
    private SeleniumConnection connection = new PhantomJsConnection();
    private static final String PATTERN = "\\d";
    private static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    private static final String MAP = "/map";
    private static final String BIZ = "/biz";
    private static final String RATING_SELECTOR = ".i-stars";
    private static final String AUTHOR_SELECTOR = "a.user-display-name";
    private static final String REVIEW_SELECTOR = ".review-content p";
    private static final String REVIEW_CARD_SELECTOR = "ul.reviews > li .review.review--with-sidebar:not(.js-war-widget)";
    private static final String TITLE_ATTRIBUTE_SELECTOR = "title";

    private static final String SITE_NAME = "yelp";
    private static final String NEXT_BUTTON_SELECTOR = ".next.pagination-links_anchor";

    public List<Review> getReviews(String url, int companyId, List<Review> reviewsFromDb) throws IOException, InterruptedException {
        connection.get(changeUrl(url));
        List<Review> reviews = new ArrayList<>();

        do {
            connection.waitForMarkup(15000);
            Document document = Jsoup.parse(connection.getPageSource());

            List<Review> reviewsOnPage = document
                    .select(REVIEW_CARD_SELECTOR)
                    .stream()
                    .filter(element -> !element.select(AUTHOR_SELECTOR).text().isEmpty())
                    .map(element -> {
                        Review review = parseReviewCard(element);
                        review.setLink(url);
                        review.setCompanyId(companyId);
                        review.setId((long) (review.getAuthor() + String.valueOf(companyId) + review.getLink()).hashCode());
                        return review;
                    })
                    .collect(Collectors.toList());


            final long countOfUnUniqueReviews = reviewsOnPage.stream()
                    .filter(reviewsFromDb::contains)
                    .count();

            final List<Review> filteredReviews = reviewsOnPage.stream()
                    .filter(review -> !reviewsFromDb.contains(review))
                    .collect(Collectors.toList());

            reviews.addAll(filteredReviews);

            connection.waitForMarkup(3000);
            if (countOfUnUniqueReviews != 0) {
                if (connection.findElements(By.cssSelector(NEXT_BUTTON_SELECTOR)).size() > 0) {
                    connection.waitForMarkup(3000);
                    connection.findElements(By.cssSelector(NEXT_BUTTON_SELECTOR)).get(0).click();
                } else {
                    break;
                }
            } else {
                break;
            }


        } while (true);

        connection.close();
        return reviews;

    }

    private String changeUrl(String url) {
        if (url.contains(MAP)) {
            return url.replace(MAP, BIZ);
        } else {
            return url;
        }
    }

    private Review parseReviewCard(Element element) {
        Review review = new Review();
        review.setRating(
                parseRating(String.valueOf(element
                        .select(RATING_SELECTOR)
                        .attr(TITLE_ATTRIBUTE_SELECTOR).charAt(0))));
        review.setAuthor(String.valueOf(element.select(AUTHOR_SELECTOR).text()));
        review.setDate(String.valueOf(new ScraperDateImpl().getDate()));
        review.setText(String.valueOf(element.select(REVIEW_SELECTOR).text()));
        review.setSite(SITE_NAME);
        review.setReviewsFromApi(false);
        return review;
    }

    private int parseRating(final String textClass) {

        Matcher matcher = COMPILED_PATTERN.matcher(textClass);
        if (matcher.find()) {
            return Integer.valueOf(matcher.group());
        }
        return 0;

    }


}
