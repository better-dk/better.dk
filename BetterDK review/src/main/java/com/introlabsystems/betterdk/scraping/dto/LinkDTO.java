package com.introlabsystems.betterdk.scraping.dto;

import lombok.Data;

import java.util.List;

/**
 * @author Liubov Zavorotna
 */
@Data
public class LinkDTO {
    private int companyId;
    private String url;
    private List<Integer> reviewId;

}
