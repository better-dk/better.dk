package com.introlabsystems.betterdk.search.site.impl.searx;


import com.introlabsystems.betterdk.connection.web.PhantomJsConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.search.site.LinkSearch;
import org.openqa.selenium.By;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.introlabsystems.betterdk.search.site.utils.EncodeUrlUtils.encodeStringForUrl;


/**
 * @author Liubov Zavorotna
 */
public class LinkSearchSearxYelp implements LinkSearch {
    private SeleniumConnection connection = new PhantomJsConnection();

    private static final String YELP = "yelp";
    private static final String HREF = "href";
    private static final String SEARCH = "search";
    private static final String RESULT = ".col-sm-8 > div:first-of-type > .result_header > a";
    private static final String SITE_YELP = "site:yelp.dk";
    private static final String HIGHLIGHT = ".col-sm-8 > div:first-of-type > p span ";
    private static final String EXTERNAL_LINK = ".col-sm-8 > div:first-of-type > .external-link";
    private static final String LANGUAGE_SELECT = "&categories=general&language=da-DK";
    private static final String HEADER_SELECTOR = ".col-sm-8 > div:first-of-type > .result_header > a span";
    private static final String SEARX_SEARCH_LINK = "http://172.17.0.2:8888/?q=";

    @Override
    public String searchLinks(int company_id, String companyName, String city, String street, String streetNumber) throws IOException, InterruptedException {
        connection.get(SEARX_SEARCH_LINK
                + SITE_YELP + " "
                + encodeStringForUrl(companyName, city, street, streetNumber)
        +LANGUAGE_SELECT);
        connection.waitForMarkup(2000);

        if (matchHeader(city, street)) {

            if (connection.findElement(By.cssSelector(EXTERNAL_LINK)).getText().contains(YELP) &
                    !connection.findElement(By.cssSelector(EXTERNAL_LINK)).getText().contains(SEARCH)) {
                String link = connection.findElement(By.cssSelector(RESULT)).getAttribute(HREF);
                connection.waitForMarkup(2000);
                connection.close();
                return link.isEmpty() ? null : link;
            }
            connection.close();
            return null;

        }
        connection.close();
        return null;

    }


    private boolean matchHeader(String city, String street) {
        List<String> list = new ArrayList<>();
        connection.findElements(By.cssSelector(HEADER_SELECTOR))
                .forEach(webElement -> list.add(webElement.getText()));
        boolean hasCity = list.stream().anyMatch(item -> item.equals(city));
        boolean hasStreet = list.stream().anyMatch(item -> item.equals(street));
        return hasCity || hasStreet;
    }


}
