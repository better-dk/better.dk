package com.introlabsystems.betterdk.search.site.impl;

import com.introlabsystems.betterdk.connection.web.PhantomJsConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.search.site.LinkSearch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;

import java.io.IOException;

import static com.introlabsystems.betterdk.search.site.utils.EncodeUrlUtils.encodeCityAndStreetForYelpUrl;
import static com.introlabsystems.betterdk.search.site.utils.EncodeUrlUtils.encodeCompanyNameForYelpUrl;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchYelp implements LinkSearch {
    private SeleniumConnection connection = new PhantomJsConnection();

    private static final String HREF = "abs:href";
    private static final String SITE = "https://www.yelp.dk";
    private static final String FIND_LOC = "&find_loc=";
    private static final String LINK_SELECTOR = ".search-results >li:first-of-type .biz-name";
    private static final String YELP_SEARCH_LINK = "https://www.yelp.dk/search?find_desc=";
    private static final String SECONDARY_ATTRIBUTES_SELECTOR = ".search-results >li:first-of-type .secondary-attributes";


    @Override
    public String searchLinks(int company_id, String company_name, String city, String street, String streetNumber) throws IOException, InterruptedException {
        String searchUrl = replaceSpaces(YELP_SEARCH_LINK
                + encodeCompanyNameForYelpUrl(company_name)
                +FIND_LOC
                + encodeCityAndStreetForYelpUrl(city,street));
        connection.get(searchUrl);
        connection.waitForMarkup(5000);
        Document document = Jsoup.parse(connection.getPageSource(), SITE);
        if (!document.select(LINK_SELECTOR).isEmpty()) {
            if (matchBody(city,street,streetNumber)) {
                Elements select = document.select(LINK_SELECTOR);
                String link = select.attr(HREF);
                connection.close();
                return link.isEmpty() ? null : link;
            }
            connection.close();
            return null;
        }
        connection.close();
        return null;
    }

    private String replaceSpaces(String text) {
        return text.replace(" ", "+");

    }
    private boolean matchBody(String city, String street, String streetNumber) {
        System.out.println(connection.findElement(By.cssSelector(SECONDARY_ATTRIBUTES_SELECTOR)).getText());
        boolean hasStreet = connection.findElement(By.cssSelector(SECONDARY_ATTRIBUTES_SELECTOR)).getText().contains(street);
        System.out.println(hasStreet);
        boolean hasStreetNumber = connection.findElement(By.cssSelector(SECONDARY_ATTRIBUTES_SELECTOR)).getText().contains(streetNumber);
        System.out.println(hasStreetNumber);
        return hasStreet && hasStreetNumber;
    }

}
