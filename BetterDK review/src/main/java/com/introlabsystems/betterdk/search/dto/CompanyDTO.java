package com.introlabsystems.betterdk.search.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author Liubov Zavorotna
 */
@Data
@ToString
public class CompanyDTO {

    private int id;

    private String name;

    private String streetName;

    private int postalCode;

    private String postalDistrict;

    private String city;

    private String municipal;

    private String streetNumber;

    private String email;

    private boolean processedBySearch;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CompanyDTO that = (CompanyDTO) o;

        return streetName.equals(that.streetName) && city.equals(that.city) && streetNumber.equals(that.streetNumber);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + streetName.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + streetNumber.hashCode();
        return result;
    }
}
