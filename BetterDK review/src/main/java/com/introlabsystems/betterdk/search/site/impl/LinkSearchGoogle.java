package com.introlabsystems.betterdk.search.site.impl;

import com.introlabsystems.betterdk.connection.web.ChromeConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.search.site.LinkSearch;
import org.openqa.selenium.By;

import java.io.IOException;

import static com.introlabsystems.betterdk.search.site.utils.EncodeUrlUtils.encodeStringForUrl;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchGoogle implements LinkSearch {
    private SeleniumConnection connection = new ChromeConnection();
    private static final String REVIEW_SELECTOR = ".section-rating-term > span > span >.widget-pane-link";
    private static final String GOOGLE_SEARCH_LINK = "https://maps.google.com/?q=";


    @Override
    public String searchLinks(int company_id, String company_name, String city, String street, String streetNumber) throws IOException, InterruptedException {

        connection.get(GOOGLE_SEARCH_LINK + encodeStringForUrl(company_name, city, street, streetNumber));
        connection.waitForMarkup(5000);
        if (connection.findElements(By.cssSelector(REVIEW_SELECTOR)).size() > 0) {
            connection.findElement(By.cssSelector(REVIEW_SELECTOR)).click();
            connection.waitForMarkup(2000);
            String link = connection.getUrl();
            connection.waitForMarkup(1000);
            connection.close();
            return link.isEmpty() ? null : link;
        } else {
            connection.close();
            return null;
        }


    }


}
