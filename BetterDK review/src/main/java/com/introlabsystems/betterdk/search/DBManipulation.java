package com.introlabsystems.betterdk.search;

import com.introlabsystems.betterdk.connection.database.PostgresDBConnection;
import com.introlabsystems.betterdk.search.dto.CompanyDTO;
import com.introlabsystems.betterdk.search.dto.ReviewLinkDTO;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Liubov Zavorotna
 */
public class DBManipulation {
    private Logger logger = Logger.getLogger(DBManipulation.class);

    private PostgresDBConnection dbConnection;

    public DBManipulation(PostgresDBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<CompanyDTO> getCompanyInfoFromDB(int industryId) {
        String selectDefalt = "SELECT id,  name,  city_name,  street_name,  street_number " +
                "FROM company WHERE processed_by_search = FALSE ";
        String selectWithIndustry = "SELECT id,  name,  city_name,  street_name,  street_number  " +
                "FROM company INNER JOIN company_industry ON company.id = company_industry.company_id " +
                "WHERE company_industry.industry_id="+industryId+" AND company.processed_by_search = FALSE AND company.booking_url ISNULL ";
        Statement statement;
        try {
            statement = dbConnection
                    .getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(selectWithIndustry);
            List<CompanyDTO> companyDTOS = new ArrayList<>();
            while (resultSet.next()) {
                CompanyDTO companyDTO = new CompanyDTO();
                companyDTO.setId(resultSet.getInt(1));
                companyDTO.setName(resultSet.getString(2));
                companyDTO.setCity(resultSet.getString(3));
                companyDTO.setStreetName(resultSet.getString(4));
                companyDTO.setStreetNumber(resultSet.getString(5));
                companyDTOS.add(companyDTO);
            }
            return companyDTOS;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public void installProcessedSearch(List<ReviewLinkDTO> links) {
        String query = "UPDATE company SET processed_by_search = TRUE WHERE id=?";
        try {
            dbConnection.getConnection().setAutoCommit(false);
            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);

            for (ReviewLinkDTO record : links) {
                ps.setInt(1, record.getCompanyId());

                ps.addBatch();
            }
            ps.executeBatch();
            dbConnection
                    .getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }

    }

    public void insertLinks(List<ReviewLinkDTO> links) {

        String query = "INSERT INTO review_links(companyid, googleplaceurl, facebookurl,yelpurl, tripadvisorurl,is_scrape_google, is_scrape_facebook," +
                "is_scrape_yelp,is_scrape_tripadvisor)" +
                "SELECT ?, ?, ?, ?, ?,?,?,?,? WHERE NOT EXISTS (" +
                "SELECT companyId FROM review_links WHERE companyId = ?) LIMIT 1;";
        try {
            dbConnection.getConnection().setAutoCommit(false);

            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);
            for (ReviewLinkDTO record : links) {
                ps.setInt(1, record.getCompanyId());
                ps.setString(2, record.getGooglePlaceUrl());
                ps.setString(3, record.getFacebookUrl());
                ps.setString(4, record.getYelpUrl());
                ps.setString(5, record.getTripadvisorUrl());
                ps.setBoolean(6, false);
                ps.setBoolean(7, false);
                ps.setBoolean(8, false);
                ps.setBoolean(9, false);
                ps.setInt(10, record.getCompanyId());

                ps.addBatch();
            }
            ps.executeBatch();
            dbConnection
                    .getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }
    }

    public void updateYelpLinks(List<ReviewLinkDTO> links) {

        String query = "UPDATE review_links SET yelpurl=? WHERE companyId=?;";
        try {
            dbConnection.getConnection().setAutoCommit(false);

            PreparedStatement ps = dbConnection
                    .getConnection().prepareStatement(query);
            for (ReviewLinkDTO record : links) {
                ps.setString(1, record.getYelpUrl());
                ps.setInt(2, record.getCompanyId());

                ps.addBatch();
            }
            ps.executeBatch();
            dbConnection
                    .getConnection().commit();
            dbConnection.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e.getNextException());
        }
    }

}
