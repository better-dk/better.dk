package com.introlabsystems.betterdk.search.dto;

import lombok.Data;

/**
 * @author Liubov Zavorotna
 */
@Data
public class ReviewLinkDTO {
    private Long id;
    private int companyId;
    private String googlePlaceUrl;
    private String facebookUrl;
    private String yelpUrl;
    private String tripadvisorUrl;
    private boolean isScrapeGoogle;
    private boolean isScrapeFacebook;
    private boolean isScrapeYelp;
    private boolean isScrapeTripadvisor;



}
