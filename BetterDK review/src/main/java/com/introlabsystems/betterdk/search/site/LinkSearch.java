package com.introlabsystems.betterdk.search.site;



import java.io.IOException;

/**
 * @author Liubov Zavorotna
 */
public interface LinkSearch {

    String searchLinks(int company_id, String company_name, String city, String street, String streetNumber) throws IOException, InterruptedException;
}
