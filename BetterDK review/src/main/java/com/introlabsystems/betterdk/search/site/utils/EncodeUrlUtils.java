package com.introlabsystems.betterdk.search.site.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author Liubov Zavorotna
 */
public class EncodeUrlUtils {

    public static String encodeStringForUrl(String company_name, String city, String street, String streetNumber) throws UnsupportedEncodingException {
        return URLEncoder.encode(company_name + " " + city + " " +
                street + " " + streetNumber, "UTF-8");
    }
    public static String encodeCompanyNameForYelpUrl(String company_name) throws UnsupportedEncodingException {
        return URLEncoder.encode(company_name, "UTF-8");
    }
    public static String encodeCityAndStreetForYelpUrl(String city, String street) throws UnsupportedEncodingException {
        return URLEncoder.encode(city+" "+street, "UTF-8");
    }
    public static String encodeStringForFacebookUrl(String company_name, String city, String street) throws UnsupportedEncodingException {
        return URLEncoder.encode(company_name + " " + city, "UTF-8");
    }



}
