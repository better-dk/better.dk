package com.introlabsystems.betterdk.search.site.impl;

import com.introlabsystems.betterdk.connection.web.ChromeConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.search.site.LinkSearch;
import org.openqa.selenium.By;

import java.io.IOException;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchTripadvisor implements LinkSearch {

    private SeleniumConnection connection = new ChromeConnection();

    private static final String HREF = "href";
    private static final String SITE = "https://tripadvisor.dk/";
    private static final String LINK = ".ppr_rup div[id=search_result] > .all-results > .all-results > .result:first-of-type > .result_wrap > .info > .reviews >a";
    private static final String SEARCH_BUTTON = ".search_button";
    private static final String LIST_SELECTOR = ".ppr_rup div[id=search_result] > .result.LODGING:not(.all-results) > .result_wrap";
    private static final String INPUT_ID_MAIN_SEARCH = "#mainSearch";
    private static final String LINK_FROM_ALL_RESULTS = ".ppr_rup div[id=search_result] > .result.LODGING:not(.all-results) > .result_wrap >.info > .reviews >a";
    private static final String INPUT_ID_GEO_SCOPED_SEARCH_INPUT = "#GEO_SCOPED_SEARCH_INPUT";
    private static final String LIST_SELECTOR_WITH_ALL_RESULTS = ".ppr_rup div[id=search_result] > .all-results > .all-results";


    @Override
    public String searchLinks(int company_id, String company_name, String city, String street, String streetNumber) throws IOException, InterruptedException {
        getResultSearch(company_name, city);

        if (connection.findElements(By.cssSelector(LIST_SELECTOR)).size() > 0) {
            String link = connection.findElement(By.cssSelector(LINK_FROM_ALL_RESULTS)).getAttribute(HREF);
            connection.close();
            return link.isEmpty() ? null : link;
        } else if (connection.findElements(By.cssSelector(LIST_SELECTOR_WITH_ALL_RESULTS)).size() > 0) {
            if (connection.findElement(By.cssSelector(".address")).getText().contains(street)) {
                String link = connection.findElement(By.cssSelector(LINK)).getAttribute(HREF);
                connection.close();
                return link.isEmpty() ? null : link;
            } else {
                connection.close();
                return null;
            }

        }
        connection.close();
        return null;


    }


    private void getResultSearch(String company_name, String city) throws IOException, InterruptedException {
        connection.get(SITE);
        connection.waitForMarkup(5000);
        connection.findElement(By.cssSelector(INPUT_ID_MAIN_SEARCH)).sendKeys(company_name);
        connection.waitForMarkup(5000);
        connection.findElement(By.cssSelector(INPUT_ID_GEO_SCOPED_SEARCH_INPUT)).sendKeys(city);
        connection.waitForMarkup(5000);
        connection.findElement(By.cssSelector(SEARCH_BUTTON)).click();
        connection.waitForMarkup(5000);

    }


}
