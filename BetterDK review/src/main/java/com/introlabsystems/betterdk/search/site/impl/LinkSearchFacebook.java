package com.introlabsystems.betterdk.search.site.impl;

import com.introlabsystems.betterdk.connection.web.PhantomJsConnection;
import com.introlabsystems.betterdk.connection.web.SeleniumConnection;
import com.introlabsystems.betterdk.search.site.LinkSearch;
import org.openqa.selenium.By;

import java.io.IOException;

import static com.introlabsystems.betterdk.search.site.utils.EncodeUrlUtils.encodeStringForFacebookUrl;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchFacebook implements LinkSearch {
    private SeleniumConnection connection = new PhantomJsConnection();
    private static final String HREF = "href";
    private static final String LINK = "#all_search_results > div > div:first-of-type > div > div > a";
    private static final String SEARCH_QUERY = "https://www.facebook.com/search/pages/?q=";
    private static final String LIST_SELECTOR = "#all_search_results > div > div:first-of-type";

    @Override
    public String searchLinks(int company_id, String company_name, String city, String street, String streetNumber) throws IOException, InterruptedException {
        String searchUrl = SEARCH_QUERY + encodeStringForFacebookUrl(company_name, city, street);
        connection.get(searchUrl);
        connection.waitForMarkup(5000);
        if (!(connection.findElements(By.cssSelector(LIST_SELECTOR)).size() < 0)) {

            String link = connection.findElement(By.cssSelector(LINK)).getAttribute(HREF);
            connection.close();
            return link.isEmpty() ? null : link;
        }
        connection.close();
        return null;

    }


}
