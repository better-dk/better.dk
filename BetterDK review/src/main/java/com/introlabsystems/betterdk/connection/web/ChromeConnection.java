package com.introlabsystems.betterdk.connection.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.Locale;

import static org.jsoup.Jsoup.parse;

/**
 * @author Liubov Zavorotna
 */
public class ChromeConnection extends SeleniumConnection {
    private final static String CHROME_PATH = "chromedriver" +
            (System.getProperty("os.name").toLowerCase(Locale.getDefault())
                    .contains("win") ? ".exe" : "");

    static {
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
    }

    public ChromeConnection() {
        super();
    }

    @Override
    protected WebDriver configureWebDriver() {
        final ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1400,1000");
        options.addArguments("--disable-plugin",
                "--disable-notifications",
                "--ignore-certificate-errors");
        ChromeDriverService service = new ChromeDriverService.Builder()
                .usingAnyFreePort()
                .build();
        return new ChromeDriver(service, options);
    }

    @Override
    public Response get(String url) throws IOException, InterruptedException {
        waitForMarkup(3000);
        goTo(url);

        return Response.builder()
                .document(parse(getPageSource(), url))
                .contentType("text/html")
                .build();
    }
}
