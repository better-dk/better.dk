package com.introlabsystems.betterdk.connection.web;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Objects;

import static java.lang.Thread.sleep;
import static org.apache.log4j.Logger.getLogger;

/**
 * @author Liubov Zavorotna
 */
public abstract class SeleniumConnection implements WebConnection {
    private static final Logger logger = getLogger(SeleniumConnection.class);
    private static final String SCROLL_SCRIPT = "window.scrollTo(0, document.body.scrollHeight);";

    protected final WebDriverWait webDriverWait;

    abstract protected WebDriver configureWebDriver();

    protected WebDriver driver;

    public SeleniumConnection() {
        this.driver = configureWebDriver();
        this.webDriverWait = new WebDriverWait(driver, 30);
    }

    public void goTo(String url) {
        driver.get(url);
    }

    public String getPageSource() {
        return driver.getPageSource();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void waitForMarkup(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    public void click(By by) {
        click(by, 1000);
    }

    private void click(By by, long millis) {
        waitForMarkup(2000);
        findElement(by).click();
        waitForMarkup(2000);
    }

    public void scrollDown(int delay) {

        String currentBody = driver.getPageSource();
        String newBody = "";

        JavascriptExecutor js = (JavascriptExecutor) driver;
        while (!Objects.equals(newBody, currentBody)) {
            currentBody = driver.getPageSource();
            js.executeScript(SCROLL_SCRIPT);
            try {
                sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            newBody = driver.getPageSource();
        }
    }
    public void clearInput(By by) {
        final WebElement inputField = findElement(by);
        while (!inputField.getAttribute("value").equals("")) {
            inputField.sendKeys(Keys.BACK_SPACE);
            waitForMarkup(1000);
        }
    }

    public void inputText(By by, String value) {

        clearInput(by);

        final WebElement inputField = findElement(by);
        for (char c : value.toCharArray()) {
            inputField.sendKeys(Character.toString(c));
            waitForMarkup(1000);
        }
    }

    public void close() {
        driver.close();
        driver.quit();
    }

}
