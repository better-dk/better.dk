package com.introlabsystems.betterdk.connection.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.Locale;

import static org.jsoup.Jsoup.parse;

/**
 * @author Liubov Zavorotna
 */
public class PhantomJsConnection extends SeleniumConnection {

    private final static String PHANTOMJS_PATH = "phantomjs"
            + (System.getProperty("os.name")
            .toLowerCase(Locale.getDefault())
            .contains("win") ? ".exe" : "");

    private final static String[] PHANTOMJS_ARGS = {
            "--web-security=no",
            "--ssl-protocol=any",
            "--ignore-ssl-errors=yes",
            "--load-images=false"};

    public PhantomJsConnection() {
        super();
    }


    @Override
    protected WebDriver configureWebDriver() {
        String userAgent = "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0";
        final DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setJavascriptEnabled(true);

        capabilities.setCapability(
                PhantomJSDriverService.PHANTOMJS_GHOSTDRIVER_CLI_ARGS,
                PHANTOMJS_ARGS);
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent", userAgent);
        capabilities.setCapability(
                PhantomJSDriverService.PHANTOMJS_CLI_ARGS, PHANTOMJS_ARGS);

        capabilities.setCapability(
                PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                PHANTOMJS_PATH);

        return new PhantomJSDriver(capabilities);
    }

    @Override
    public Response get(String url) throws IOException, InterruptedException {

        waitForMarkup(3000);
        goTo(url);

        return Response.builder()
                .document(parse(getPageSource(), url))
                .contentType("text/html")
                .build();
    }
}
