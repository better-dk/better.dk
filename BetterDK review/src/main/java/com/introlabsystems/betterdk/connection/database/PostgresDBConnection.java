package com.introlabsystems.betterdk.connection.database;

import com.introlabsystems.betterdk.domain.DBProperties;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author : Vitalii M.; create date: 6/10/16.
 */
public class PostgresDBConnection {

    private Logger logger = Logger.getLogger(PostgresDBConnection.class);

    private Connection connection;

    private DBProperties dbProperties;

    public Connection getConnection() {
        return connection;
    }

    public PostgresDBConnection(DBProperties dbProperties) {
        this.dbProperties = dbProperties;
    }

    public void open() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://" + dbProperties.getDatabase(),
                    dbProperties.getDbUser(),
                    dbProperties.getDbPassword());
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            return;

        }

        if (connection != null) {
            logger.info("Connection established!");
        } else {
            logger.error("Failed to make connection!");
        }
    }

    public void close() {
        try {
            connection.close();
            logger.info("Connection closed!");
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
    }


}
