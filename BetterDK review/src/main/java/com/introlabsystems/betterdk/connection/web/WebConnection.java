package com.introlabsystems.betterdk.connection.web;

import lombok.Builder;
import lombok.Data;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * @author Liubov Zavorotna
 */
public interface WebConnection extends AutoCloseable {
    Response get(String url) throws IOException, InterruptedException;



    @Data
    @Builder
    class Response {
        private int status;
        private Document document;
        private String contentType;
    }
}
