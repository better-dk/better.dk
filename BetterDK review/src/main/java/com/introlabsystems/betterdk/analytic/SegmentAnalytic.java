package com.introlabsystems.betterdk.analytic;

import com.github.segmentio.AnalyticsClient;
import com.github.segmentio.models.Props;
import com.introlabsystems.betterdk.App;
import com.introlabsystems.betterdk.scraping.DBProcessing;
import com.introlabsystems.betterdk.scraping.dto.LinkDTO;
import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.search.dto.CompanyDTO;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

/**
 * @author Liubov Zavorotna
 */
public class SegmentAnalytic {
    private static final Logger logger = Logger.getLogger(App.class);
    private static final String SEGMENT_IO_KEY_PRODUCTION = "hgWHQms7okwJ5OUoEdndfoWt13QAF2p2";
    private static final String SEGMENT_IO_KEY_DEVELOPMENT = "sjJt52gu4CEsQBHoFNFKYvoMslH69vAG";

    public static void sendToSegmentIO(List<Review> reviews, DBProcessing dbProcessing, LinkDTO entry) {
        CompanyDTO companyDTO = dbProcessing.getCompanyInfoFromDB(entry.getCompanyId());
        AnalyticsClient analytics = new AnalyticsClient(SEGMENT_IO_KEY_PRODUCTION);
        putDataToSegment(reviews, companyDTO, analytics);
    }
    public static void sendToSegmentIOGoogle(List<Review> reviews, DBProcessing dbProcessing, Map.Entry<Integer, String> entry) {
        CompanyDTO companyDTO = dbProcessing.getCompanyInfoFromDB(entry.getKey());
        AnalyticsClient analytics = new AnalyticsClient(SEGMENT_IO_KEY_PRODUCTION);
        putDataToSegment(reviews, companyDTO, analytics);
    }

    private static void putDataToSegment(List<Review> reviews, CompanyDTO companyDTO, AnalyticsClient analytics) {
        try {
            for (Review review : reviews) {
                Props props = new Props();
                props.put("Company name: ", companyDTO.getName());
                props.put("Company Email: ", companyDTO.getEmail());
                props.put("Author: ", review.getAuthor());
                props.put("Date: ", review.getDate());
                props.put("Link: ", review.getLink());
                props.put("Rating: ", review.getRating());
                props.put("Review text: ", review.getText());
                analytics.track(String.valueOf(review.getCompanyId()), "Review scraped", props);
                analytics.flush();
            }
            analytics.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
