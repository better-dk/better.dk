package com.introlabsystems.betterdk.analytic;


import com.introlabsystems.betterdk.App;
import com.introlabsystems.betterdk.scraping.DBProcessing;
import com.introlabsystems.betterdk.scraping.dto.LinkDTO;
import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.search.dto.CompanyDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Liubov Zavorotna
 */
public class CustomerIoAnalytic {
    private static final Logger logger = Logger.getLogger(App.class);
    private static final String AUTHORIZATION_KEY_TEST = "Basic Mjc4YzJmMGFlMjgzYzBlOTI4NTU6YTkzNDRmOWRlZWNhOGZiZTAwNTM=";
    private static final String AUTHORIZATION_KEY_PRODUCTION = "Basic M2EzZDIzMjEyZDY2NTE4ZTJhMjI6MjY0MWFmMzBhODg3NzljNzIxYjc=";

    public static void sendToCustomerIO(List<Review> reviews, DBProcessing dbProcessing, LinkDTO entry, boolean isScraped) {
        if (!isScraped){
            CompanyDTO companyDTO = dbProcessing.getCompanyInfoFromDB(entry.getCompanyId());
            postToCustomerIO(reviews, companyDTO);
        }else {
            logger.info("Scrape all reviews: " + false);
        }

    }

    public static void sendToCustomerIOGoogle(List<Review> reviews, DBProcessing dbProcessing, Map.Entry<Integer, String> entry, boolean isScraped) {
        if (!isScraped){
            CompanyDTO companyDTO = dbProcessing.getCompanyInfoFromDB(entry.getKey());
            postToCustomerIO(reviews, companyDTO);
        }else {
            logger.info("Scrape all reviews: " + false);
        }

    }


    private static void postToCustomerIO(List<Review> reviews, CompanyDTO companyDTO) {

        try {
            for (Review review : reviews) {

                String email;
                if (companyDTO.getEmail() == null) {
                    email = "";
                } else {
                    email = companyDTO.getEmail();
                }

                Jsoup.connect("https://track.customer.io/api/v1/customers/5/events")
                        .ignoreContentType(true)
                        .header("Cache-Control", "no-cache")
                        .header("Authorization", AUTHORIZATION_KEY_PRODUCTION)
                        .data("cookieexists", "false")
                        .data("name", "Review scraped")
                        .data("data[Company name]", companyDTO.getName())
                        .data("data[Company Email]", email)
                        .data("data[Author]", review.getAuthor())
                        .data("data[Date]", review.getDate())
                        .data("data[Link]", review.getLink())
                        .data("data[Rating]", String.valueOf(review.getRating()))
                        .data("data[Review text]", review.getText())
                        .post();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
