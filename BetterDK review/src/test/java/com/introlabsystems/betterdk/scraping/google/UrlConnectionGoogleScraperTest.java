package com.introlabsystems.betterdk.scraping.google;

import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.scraping.processing.UrlConnectionGoogleScraper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class UrlConnectionGoogleScraperTest {
    private static UrlConnectionGoogleScraper urlConnectionGoogleScraper;

    @BeforeClass
    public static void init() {
        urlConnectionGoogleScraper = new UrlConnectionGoogleScraper();
    }

    @Test
    public void testParseGoogleWithoutSingleQuote() throws Exception {
        String url = "https://www.google.com/maps/place/Robertos+Pizzaria/@55.6996416,12.466631,17z/data=!4m7!3m6!1s0x4652517441de52c1:0xb36c59622de3154!8m2!3d55.6996416!4d12.4688197!9m1!1b1";
        Integer companyId = 1494927;
        List<Review> scraperReviews = urlConnectionGoogleScraper.getReviews(url, companyId);
        assertTrue(!scraperReviews.isEmpty());

    }

    @Test
    public void testParseGoogleWithoutBrackets() throws Exception {
        String url = "https://www.google.com/maps/place/Viking+kitchen+ex.+The+Black+pot/@56.1514185,10.2086351,17z/data=!4m7!3m6!1s0x464c3f902230f87b:0x68a23efa07ffe81c!8m2!3d56.1514185!4d10.2108238!9m1!1b1";
        Integer companyId = 1494927;
        List<Review> scraperReviews = urlConnectionGoogleScraper.getReviews(url, companyId);
        assertTrue(!scraperReviews.isEmpty());

    }

}
