package com.introlabsystems.betterdk.scraping.facebook;

import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.scraping.processing.FacebookScraper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class FacebookScraperTest {
    private static FacebookScraper facebookScraper;

    @BeforeClass
    public static void init() {
        facebookScraper = new FacebookScraper();
        try {
            facebookScraper.loginToFacebook();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseCompanyReviewPage() throws Exception {
        String url = "https://www.facebook.com/pages/Palermo-Pizza-Kebab/137838762941550";
        Integer companyId = 1494927;
        List<Review> scraperReviews = facebookScraper.getReviews(url, companyId);
        System.out.println(scraperReviews);
        assertTrue(!scraperReviews.isEmpty());

    }

    @Test
    public void testParseCompanyPage() throws Exception {
        String url = "https://www.facebook.com/pg/VesterbrosPizzaGrill/about/?section=hours&tab=page_inf";
        Integer companyId = 1494927;
        List<Review> scraperReviews = facebookScraper.getReviews(url, companyId);
        assertTrue(!scraperReviews.isEmpty());

    }

    @Test
    public void test(){
        List<Long> ids = Arrays.asList(5L,7L,7L,8L);

        if (ids.contains(Long.valueOf(5))){
            System.out.println(true);
        }
    }

}
