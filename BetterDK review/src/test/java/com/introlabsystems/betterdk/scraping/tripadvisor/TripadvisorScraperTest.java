package com.introlabsystems.betterdk.scraping.tripadvisor;

import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.scraping.processing.TripadvisorScraper;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class TripadvisorScraperTest {
    private static TripadvisorScraper tripadvisorScraper;
    private WebDriver webdriver;
    @BeforeClass
    public static void init() {
        tripadvisorScraper = new TripadvisorScraper();

    }

    @Test
    public void testParseTripadvisorAdvertisementPage() throws Exception {
        List<Review> reviewsFromDb = new ArrayList<>();
        String url = "https://www.tripadvisor.dk/Hotel_Review-g189541-d237644-Reviews-Citilet_Apartments-Copenhagen_Zealand.html";
        Integer companyId = 1494927;
        List<Review> tripadvisorScraperReviews = tripadvisorScraper.getReviews(url,companyId,reviewsFromDb);
        tripadvisorScraperReviews.forEach(System.out::println);
        System.out.println(tripadvisorScraperReviews.size());
        assertTrue(!tripadvisorScraperReviews.isEmpty());

    }

    @Test
    public void testParseTripadvisorReviewPage() throws Exception {
        List<Review> reviewsFromDb = new ArrayList<>();
        String url = "https://www.tripadvisor.dk/Hotel_Review-g298052-d502046-Reviews-Hotel_Ingul-Mykolayiv_Mykolaiv_Oblast.html";
        Integer companyId = 1494927;
        List<Review> tripadvisorScraperReviews = tripadvisorScraper.getReviews(url,companyId,reviewsFromDb);
        tripadvisorScraperReviews.forEach(System.out::println);
        System.out.println(tripadvisorScraperReviews.size());
        assertTrue(!tripadvisorScraperReviews.isEmpty());


    }

}
