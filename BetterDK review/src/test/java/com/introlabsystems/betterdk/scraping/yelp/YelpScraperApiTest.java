package com.introlabsystems.betterdk.scraping.yelp;

import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.scraping.processing.YelpScraperApi;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;

/**
 * @author Liubov Zavorotna
 */
public class YelpScraperApiTest {
    private static YelpScraperApi yelpScraperApi;

    @BeforeClass
    public static void init() {
        yelpScraperApi = new YelpScraperApi();
    }

    @Test
    public void fetchYelpComments() throws Exception {

        String url = "https://www.yelp.com/biz/de-grand-family-restaurant-denmark";
        Integer companyId = 1494927;
        List<Review> yelpScraperReviews = yelpScraperApi.getReviews(url, companyId);
        assertFalse(yelpScraperReviews.isEmpty());
    }

}
