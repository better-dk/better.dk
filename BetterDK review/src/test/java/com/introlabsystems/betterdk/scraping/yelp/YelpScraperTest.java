package com.introlabsystems.betterdk.scraping.yelp;

import com.introlabsystems.betterdk.scraping.dto.Review;
import com.introlabsystems.betterdk.scraping.processing.YelpScraper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class YelpScraperTest {


    private static YelpScraper yelpScraper;

    @BeforeClass
    public static void init() {
        yelpScraper = new YelpScraper();
    }

    @Test
    public void testParseYelpPage() throws Exception {
        List<Review> reviewsFromDb = new ArrayList<>();
        String url = "https://www.yelp.com/biz/de-grand-family-restaurant-denmark";
        Integer companyId = 1494927;
        List<Review> yelpScraperReviews = yelpScraper.getReviews(url, companyId, reviewsFromDb);
        yelpScraperReviews.forEach(System.out::println);
        System.out.println(yelpScraperReviews.size());
        assertTrue(!yelpScraperReviews.isEmpty());

    }

}
