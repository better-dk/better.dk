package com.introlabsystems.betterdk.search.yelp;

import com.introlabsystems.betterdk.search.site.impl.LinkSearchYelp;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchYelpTest {
    private static LinkSearchYelp linkSearchYelp;

    @BeforeClass
    public static void init() {
        linkSearchYelp = new LinkSearchYelp();
    }

    @Test
    public void fetchLink() throws IOException, InterruptedException {
        Integer companyId = 1494927;
        String company_name = "Pizza King ";
        String city = "Herning";

        String street = "";
        String street_num = "";
        String reviewLinkDTOS = linkSearchYelp.searchLinks(companyId,company_name,city,street,street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());
    }
}
