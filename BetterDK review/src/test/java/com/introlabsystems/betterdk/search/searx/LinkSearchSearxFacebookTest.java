package com.introlabsystems.betterdk.search.searx;

import com.introlabsystems.betterdk.search.site.impl.searx.LinkSearchSearxFacebook;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchSearxFacebookTest {
    private static LinkSearchSearxFacebook linkSearchSearxFacebook;

    @BeforeClass
    public static void init() {
        linkSearchSearxFacebook = new LinkSearchSearxFacebook();
    }

    @Test
    public void testSearchSearx() throws IOException, InterruptedException {

        Integer companyId = 1494927;
        String company_name = "Lucerne Pizza og Indisk Specialiteter";
        String city = "Rødovre";
        String street = "Lucernevej";
        String street_num = "58";

        String reviewLinkDTOS = linkSearchSearxFacebook.searchLinks(companyId, company_name, city, street, street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());
    }


}
