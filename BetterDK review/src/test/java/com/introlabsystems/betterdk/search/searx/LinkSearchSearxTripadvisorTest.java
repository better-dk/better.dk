package com.introlabsystems.betterdk.search.searx;

import com.introlabsystems.betterdk.search.site.impl.searx.LinkSearchSearxTripadvisor;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchSearxTripadvisorTest {
    private static LinkSearchSearxTripadvisor linkSearchSearxTripadvisor;
    @BeforeClass
    public static void init() {
        linkSearchSearxTripadvisor = new LinkSearchSearxTripadvisor();
    }
    @Test
    public void testSearchSearx() throws IOException, InterruptedException {

        Integer companyId = 1494927;
        String company_name = "Paradiso Pizza & Restaurant";
        String city = "Slangerup";
        String street = "Kongensgade";
        String street_num = "4";
        String reviewLinkDTOS = linkSearchSearxTripadvisor.searchLinks(companyId, company_name, city, street, street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());
    }
}
