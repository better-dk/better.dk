package com.introlabsystems.betterdk.search.tripadvisor;

import com.introlabsystems.betterdk.search.site.impl.LinkSearchTripadvisor;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchTripadvisorTest {
    private static LinkSearchTripadvisor linkSearchTripadvisor;

    @BeforeClass
    public static void init() {
        linkSearchTripadvisor = new
                LinkSearchTripadvisor();

    }
    @Test
    public void fetchLink() throws IOException, InterruptedException {
        Integer companyId = 1494927;
        String company_name ="Gentofte Hotel";
        String city = "København";
        String street = "";
        String street_num = "";
       String  reviewLinkDTOS = linkSearchTripadvisor.searchLinks(companyId,company_name,city,street, street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());
    }
}
