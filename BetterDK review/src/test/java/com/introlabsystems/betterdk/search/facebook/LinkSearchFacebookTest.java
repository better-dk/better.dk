package com.introlabsystems.betterdk.search.facebook;

import com.introlabsystems.betterdk.search.site.impl.LinkSearchFacebook;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchFacebookTest {

    private static LinkSearchFacebook linkSearchFacebook;

    @BeforeClass
    public static void init() {
        linkSearchFacebook = new LinkSearchFacebook();
    }

    @Test
    public void fetchLink() throws IOException, InterruptedException {

        Integer companyId = 1494927;
        String company_name = "Mama Mia Pizza Kebab House";
        String city = "Brande";
        String street = "";
        String street_num = "";
        String reviewLinkDTOS = linkSearchFacebook.searchLinks(companyId, company_name, city, street,street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());
    }
}
