package com.introlabsystems.betterdk.search.searx;


import com.introlabsystems.betterdk.search.site.impl.searx.LinkSearchSearxYelp;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchSearxYelpTest {

    private static LinkSearchSearxYelp linkSearchSearxYelp;

    @BeforeClass
    public static void init() {
        linkSearchSearxYelp = new LinkSearchSearxYelp();
    }

    @Test
    public void testSearchSearxOnCorrectResult() throws IOException, InterruptedException {

        Integer companyId = 1494927;
        String company_name = "Mors Køkken";
        String city = "Frederiksberg";

        String street = "Godthåbsvej";
        String street_num = "16";


        String reviewLinkDTOS = linkSearchSearxYelp.searchLinks(companyId, company_name, city, street, street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());

    }

    @Test
    public void testSearchSearxOnNotCorrectResult() throws IOException, InterruptedException {

        Integer companyId = 1494927;
        String company_name = "King of Chicken";
        String city = "København V";
        String street = "Oehlenschlægersgade";
        String street_num = "50";

        String reviewLinkDTOS = linkSearchSearxYelp.searchLinks(companyId, company_name, city, street, street_num);
        assertTrue(reviewLinkDTOS.isEmpty());

    }
}
