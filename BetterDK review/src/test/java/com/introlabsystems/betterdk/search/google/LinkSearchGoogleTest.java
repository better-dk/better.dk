package com.introlabsystems.betterdk.search.google;

import com.introlabsystems.betterdk.search.site.impl.LinkSearchGoogle;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Liubov Zavorotna
 */
public class LinkSearchGoogleTest {
    private static LinkSearchGoogle linkSearchGoogle;
    @BeforeClass
    public static void init(){
        linkSearchGoogle = new LinkSearchGoogle();
    }

    @Test
    public void testSearchInGoogleMaps() throws IOException, InterruptedException {
        Integer companyId = 1494927;
        String company_name ="Korsor Grill House";
        String city = "Algade";
        String street = "";
        String street_num = "";
        String reviewLinkDTOS = linkSearchGoogle.searchLinks(companyId, company_name, city, street, street_num);
        assertTrue(!reviewLinkDTOS.isEmpty());
    }
}
