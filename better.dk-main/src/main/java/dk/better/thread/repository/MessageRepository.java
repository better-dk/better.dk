package dk.better.thread.repository;

import dk.better.thread.entity.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    @Query("select m from Message m inner join m.thread t inner join fetch m.sender p left join fetch p.account left join fetch p.company c left join fetch c.subdomains where t.id = :threadId")
    Slice<Message> findByThreadId(@Param("threadId") int threadId, Pageable pageable);
}