package dk.better.thread.repository;

import dk.better.company.entity.Company;
import dk.better.thread.entity.Participant;
import dk.better.thread.entity.Thread;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface ThreadRepository extends JpaRepository<Thread, Integer>, JpaSpecificationExecutor<Thread>, ThreadRepositoryCustom {
    @Query(
            value = "select distinct t " +
                    "from Thread t " +
                    "inner join fetch t.sender s " +
                    "left join fetch s.account " +
                    "inner join fetch t.receiver r " +
                    "left join fetch r.account " +
                    "left join fetch s.company " +
                    "left join fetch r.company " +
                    "where (s.account.id = :accountId and s.company is null) " +
                    "or (r.account.id = :accountId and r.company is null)",
            countQuery = "select count(distinct t) " +
                    "from Thread t " +
                    "inner join t.sender s " +
                    "left join s.account " +
                    "inner join t.receiver r " +
                    "left join r.account " +
                    "left join s.company " +
                    "left join r.company " +
                    "where (s.account.id = :accountId and s.company is null) " +
                    "or (r.account.id = :accountId and r.company is null)"
    )
    Page<Thread> getThreadsForUserInbox(@Param("accountId") int accountId, Pageable pageable);

    @Query(
            value = "select distinct t " +
                    "from Thread t " +
                    "inner join fetch t.sender s " +
                    "left join fetch s.account " +
                    "left join fetch s.company sender_company " +
                    "inner join fetch t.receiver r " +
                    "left join fetch r.account " +
                    "left join fetch r.company receiver_company " +
                    "where sender_company.id = :companyId or receiver_company.id = :companyId",
            countQuery = "select count(distinct t) " +
                    "from Thread t " +
                    "inner join t.sender s " +
                    "left join s.account " +
                    "left join s.company sender_company " +
                    "inner join t.receiver r " +
                    "left join r.account " +
                    "left join r.company receiver_company " +
                    "where sender_company.id = :companyId or receiver_company.id = :companyId"
    )
    Page<Thread> getThreadsForCompanyInbox(@Param("companyId") int companyId, Pageable pageable);

    @Query("select distinct p " +
            "from Participant p " +
            "inner join p.account a " +
            "inner join p.thread t " +
            "where a.id = :accountId and t.id = :threadId")
    List<Participant> getThreadParticipantsFromAccount(@Param("accountId") int accountId, @Param("threadId") int threadId);

    @Query("select distinct t " +
            "from Thread t " +
            "inner join fetch t.participants p " +
            "inner join p.account a " +
            "inner join p.company c " +
            "inner join fetch t.receiver r " +
            "left join fetch r.account " +
            "left join fetch r.company receiver_company " +
            "where t.id = :threadId and a.id = :accountId and c.id = :companyId")
    Thread getThreadWithCompanyRepresentantParticipantAndReceiver(@Param("threadId") int threadId, @Param("accountId") int accountId, @Param("companyId") int companyId);

    @Query("select distinct t " +
            "from Thread t " +
            "inner join fetch t.participants p " +
            "inner join p.account a " +
            "inner join p.company c " +
            "inner join fetch t.receiver r " +
            "left join fetch r.account " +
            "left join fetch r.company receiver_company " +
            "where t.id = :threadId and a.accessToken = :accessToken and c.id = :companyId")
    Thread getThreadWithCompanyRepresentativeParticipantAndReceiver(@Param("threadId") int threadId, @Param("accessToken") String accessToken, @Param("companyId") int companyId);

    @Query("select case when count(t) > 0 then true else false end " +
            "from Thread t " +
            "left join t.sender s " +
            "left join s.company sender_company " +
            "left join sender_company.accountCompanies sender_ac " +
            "left join sender_ac.account sender_ac_account " +
            "left join t.receiver r " +
            "left join r.company receiver_company " +
            "left join receiver_company.accountCompanies receiver_ac " +
            "left join receiver_ac.account receiver_ac_account " +
            "where t.id = :threadId " +
            "and (s.account.id = :accountId or r.account.id = :accountId or (sender_ac_account.id = :accountId) or (receiver_ac_account.id = :accountId))")
    boolean hasAccessToThread(@Param("accountId") int accountId, @Param("threadId") int threadId);

    @Query("select case when count(t) > 0 then true else false end " +
            "from Thread t " +
            "left join t.receiver r " +
            "left join t.sender s " +
            "where t.id = :threadId and (" +
            "r.company.id = :companyId " +
            "or s.company.id = :companyId)")
    boolean hasCompanyAccessToThread(@Param("threadId") int threadId, @Param("companyId") int companyId);

    @Query("select rc " +
            "from Thread t " +
            "inner join t.receiver r " +
            "inner join r.company rc " +
            "where t.id = :threadId")
    Company getParticipatingCompany(@Param("threadId") int threadId); // todo: make this "generic" such that the method also supports companies starting a thread with a user

    @Query("select case when count(t) > 0 then true else false end " +
            "from Thread t " +
            "inner join t.participants p " +
            "inner join p.account a " +
            "inner join p.company c " +
            "where t.id = :threadId and a.id = :accountId and c.id = :companyId")
    boolean isParticipatingInThreadOnBehalfOfCompany(@Param("threadId") int threadId, @Param("accountId") int accountId, @Param("companyId") int companyId);

    @Query("select distinct t " +
            "from Thread t " +
            "inner join fetch t.sender s " +
            "left join fetch s.account sa " +
            "left join fetch s.company " +
            "inner join fetch t.receiver r " +
            "left join fetch r.account ra " +
            "left join fetch r.company " +
            "where t.id = :threadId and ((sa.id = :accountId and s.company is null) or (ra.id = :accountId and r.company is null))")
    Thread getThreadByIdAndAccount(@Param("threadId") int threadId, @Param("accountId") int accountId);

    @Query(
            value = "select t " +
                    "from Thread t " +
                    "inner join fetch t.sender s " +
                    "inner join fetch s.account sa " +
                    "inner join fetch t.receiver r " +
                    "inner join fetch r.company rc " +
                    "where r.lastActivity is null",

            countQuery = "select count(t) " +
                    "from Thread t " +
                    "inner join t.sender s " +
                    "inner join s.account sa " +
                    "inner join t.receiver r " +
                    "inner join r.company rc " +
                    "where r.lastActivity is null"
    )
    Page<Thread> getUnreadThreadsLeads(Pageable pageable);
}