package dk.better.thread.service;

import dk.better.account.entity.Account;
import dk.better.company.entity.Company;
import dk.better.thread.entity.Message;
import dk.better.thread.entity.Participant;
import dk.better.thread.entity.Thread;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Map;

/**
 * @author Bo Andersen
 */
public interface ThreadService {
    static final String EVENT_NEW_THREAD = "New Thread Created";
    static final String EVENT_NEW_THREAD_NO_EMAIL = "New Thread Created (no e-mail)";
    static final String EVENT_MESSAGE_SENT = "Sent Message";
    static final String EVENT_WELCOME_MESSAGE = "Sent Welcome Message";

    Thread sendMessageInNewThreadFromAccountToCompany(String subject, String messageText, int companyId, String ipAddress, String email, String firstName);

    Thread sendMessageInNewThreadFromAccountToCompany(String subject, String messageText, int companyId, String ipAddress, String email, String firstName, String lastName);

    Thread sendMessageInNewThreadFromAccountToCompany(String subject, String messageText, int companyId, String ipAddress, String email, String firstName, String middleName, String lastName);

    boolean hasAccessToThread(int threadId, int accountId);

    boolean hasAccessToThread(int threadId, Account account);

    boolean hasAccessToThread(int threadId, Long checksum, String accessToken);

    boolean hasCompanyAccessToThread(int threadId, int companyId);

    boolean hasAccessToCompanyInbox(Account account, int companyId);

    boolean isParticipatingInThreadOnBehalfOfCompany(int threadId, Account account, int companyId);

    boolean isParticipatingInThreadOnBehalfOfCompany(int threadId, int companyId, String accessToken, Long checksum);

    Participant getThreadParticipant(Account account, int threadId);

    Thread getThread(int threadId, Account account);

    /**
     * Marks a given thread as read for a regular user
     *
     * @param threadId The ID of the thread
     */
    void markAsReadForRegularUser(int threadId);

    /**
     * Marks a given thread as read for a regular user
     *
     * @param threadId    The ID of the thread
     * @param accessToken Access token for the account that is participating in the thread
     * @param checksum    A checksum to verify access
     */
    void markAsReadForRegularUser(int threadId, String accessToken, Long checksum);

    /**
     * Marks a given thread as read for a company
     *
     * @param threadId  The ID of the thread
     * @param companyId The ID of the company
     */
    void markAsReadForCompany(int threadId, int companyId);

    /**
     * Marks a given thread as read for a company
     *
     * @param threadId    The ID of the thread
     * @param companyId   The ID of the company
     * @param accessToken Access token for the company that is participating in the thread
     * @param checksum    A checksum to verify access
     */
    void markAsReadForCompany(int threadId, int companyId, String accessToken, Long checksum);

    /**
     * Marks a given thread as read for a user who is participating in the thread on behalf of a company
     *
     * @param threadId  The ID of the thread
     * @param companyId The ID of the company for which the user participates on behalf of
     * @param account   The account for which to mark the thread as read for
     */
    void markAsReadForCompanyParticipant(int threadId, int companyId, Account account);

    /**
     * Marks a given thread as read for a user who is participating in the thread on behalf of a company
     *
     * @param threadId    The ID of the thread
     * @param companyId   The ID of the company for which the user participates on behalf of
     * @param accessToken Access token for the company that is participating in the thread
     * @param checksum    A checksum to verify access
     */
    void markAsReadForCompanyParticipant(int threadId, int companyId, String accessToken, Long checksum);

    @PreAuthorize("isAuthenticated()")
    Slice<Message> getMessages(int threadId, Pageable pageable);

    Slice<Message> getMessages(int threadId, String accessToken, Long checksum, Pageable pageable);

    @PreAuthorize("isAuthenticated()")
    Message addMessageToThread(int threadId, String messageText, Account sender, String ipAddress, boolean isSentAsCompany);

    Page<Thread> getThreadsForUserInbox(int accountId, Pageable pageable);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Page<Thread> getUnreadThreadsLeads(Pageable pageable);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void sendEmailToReceiver(int threadId);

    /**
     * Gets the number of unread messages for each thread that has unread messages for the account
     *
     * @param accountId The ID of the account
     * @return A map with K = thread ID and V = number of unread messages
     */
    Map<Integer, Long> getUnreadMessagesCountPerThread(int accountId);

    Map<Integer, Map<Integer, Long>> getUnreadMessagesCountPerThreadForClaimedCompanies(int accountId);

    Company getParticipatingCompany(int threadId);

    Thread sendWelcomeMessageToCompany(int companyId, int fromAccountId);

    Page<Thread> getThreadsForCompanyInbox(int companyId, Pageable pageable);

    Page<Thread> getThreadsForCompanyInbox(int companyId, Long checksum, String accessToken, Pageable pageable);
}