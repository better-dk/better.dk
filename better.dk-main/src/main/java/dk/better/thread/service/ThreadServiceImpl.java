package dk.better.thread.service;

import com.github.segmentio.models.Props;
import com.github.segmentio.models.Traits;
import dk.better.account.entity.Account;
import dk.better.account.entity.Role;
import dk.better.account.service.AccountService;
import dk.better.account.util.AccountUtils;
import dk.better.api.converter.MessageConverter;
import dk.better.api.resource.MessageResource;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.service.PushService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.DateUtils;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.service.CompanyService;
import dk.better.company.util.CompanyUtils;
import dk.better.thread.entity.Message;
import dk.better.thread.entity.Participant;
import dk.better.thread.entity.Thread;
import dk.better.thread.repository.MessageRepository;
import dk.better.thread.repository.ParticipantRepository;
import dk.better.thread.repository.ThreadRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class ThreadServiceImpl implements ThreadService {
    private static Logger logger = LogManager.getLogger(ThreadService.class);

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private PushService pushService;


    @Override
    public Thread sendMessageInNewThreadFromAccountToCompany(String subject, String messageText, int companyId, String ipAddress, String email, String firstName) {
        return this.sendMessageInNewThreadFromAccountToCompany(subject, messageText, companyId, ipAddress, email, firstName, null, null);
    }

    @Override
    public Thread sendMessageInNewThreadFromAccountToCompany(String subject, String messageText, int companyId, String ipAddress, String email, String firstName, String lastName) {
        return this.sendMessageInNewThreadFromAccountToCompany(subject, messageText, companyId, ipAddress, email, firstName, null, lastName);
    }

    @Override
    @Transactional
    public Thread sendMessageInNewThreadFromAccountToCompany(String subject, String messageText, int companyId, String ipAddress, String email, String firstName, String middleName, String lastName) {
        Assert.notNull(email);
        Assert.notNull(firstName);
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("A company with ID: %d could not be found!", companyId));
        }

        logger.info(String.format("Sending message in new thread from %s (%s) to company with ID: %d. Message: %s", firstName + " " + middleName + " " + lastName, email, companyId, messageText));
        Date now = new Date();

        // Find existing account or create a new one
        Account sender;
        boolean isNewUser = false;

        if (!email.isEmpty()) {
            sender = this.accountService.findByEmail(email);

            if (sender == null) {
                isNewUser = true;

                sender = new Account();
                sender.setUuid(AccountService.generateUUID());
                sender.setAccessToken(GeneralUtils.getRandomUUID());
                sender.setEmail(email);
                sender.setFirstName(firstName);
                sender.setMiddleName(middleName);
                sender.setLastName(lastName);
                sender.setCreated(now);
                sender.setEnabled(false);
                sender.add(this.threadRepository.getEntityManager().getReference(Role.class, AccountService.ROLE_USER_ID)); // Add ROLE_USER as default
            }
        } else {
            throw new InvalidArgumentException("Cannot send message in new thread without an e-mail address");
        }

        /** Create thread **/
        Thread thread = new Thread();
        thread.setSubject(subject);
        thread.setLastActivity(now);
        thread.setCreated(now);
        Thread savedThread = this.threadRepository.save(thread);

        /** Save participants **/
        Participant senderParticipant = new Participant();
        senderParticipant.setAccount(sender);
        senderParticipant.setThread(savedThread);
        senderParticipant.setLastActivity(now);

        Participant companyParticipant = new Participant();
        companyParticipant.setCompany(company);
        companyParticipant.setThread(savedThread);

        this.participantRepository.save(senderParticipant);
        this.participantRepository.save(companyParticipant);

        /** Save message **/
        Message message = new Message();
        message.setText(messageText);
        message.setThread(savedThread);
        message.setIpAddress(ipAddress);
        message.setSender(senderParticipant);
        message.setCreated(now);
        message = this.messageRepository.save(message);

        /** Add participants to thread **/
        savedThread.setSender(senderParticipant);
        savedThread.setReceiver(companyParticipant);

        Set<Participant> participants = new HashSet<>(2);
        participants.add(senderParticipant);
        participants.add(companyParticipant);
        savedThread.setParticipants(participants);
        this.threadRepository.save(savedThread);

        // Ignore analytics error
        try {
            // Add new user to group
            if (isNewUser) {
                String analyticsFirstName = ((sender.getMiddleName() != null) ? sender.getFirstName() + " " + sender.getMiddleName() : sender.getFirstName());

                this.analyticsService.identify(sender.getUuid(), new Traits()
                        .put("firstName", analyticsFirstName)
                        .put("lastName", sender.getLastName())
                        .put("email", sender.getEmail())
                        .put("createdAt", sender.getCreated().getTime()));

                Traits traits = new Traits();
                traits.put("name", "Partial Users");
                this.analyticsService.group(sender.getUuid(), String.valueOf(AnalyticsService.GROUP_PARTIAL_USER_ID), traits);
            }

            // Trigger new thread event
            String companyEmail = company.getEmail();
            Props props = new Props();
            props.put("thread_id", savedThread.getId());
            props.put("thread_subject", subject);
            props.put("message", messageText);
            props.put("sender", new Props()
                    .put("firstName", firstName)
                    .put("middleName", middleName)
                    .put("lastName", lastName)
                    .put("fullName", AccountUtils.getFullName(firstName, middleName, lastName)));
            Collection<String> industries = CompanyUtils.getIndustryNames(company.getIndustries());

            if (companyEmail == null || companyEmail.isEmpty()) {
                props.put("company", new Props()
                        .put("id", companyId)
                        .put("name", company.getName())
                        .put("profile_url", company.getProfileUrl())
                        .put("city", company.getCity())
                        .put("postal_code", company.getPostalCode())
                        .put("phone_number", company.getPhoneNumber())
                        .put("call_tracking_number", company.getCallTrackingNumber())
                        .put("accessToken", company.getAccessToken())
                        .put("checksum", CompanyService.generateChecksum(companyId))
                        .put("industries", industries)
                        .put("is_claimed", company.getAccountCompanies().size() > 0)
                        .put("founding_date", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

                this.analyticsService.track(sender.getUuid(), EVENT_NEW_THREAD_NO_EMAIL, props);
            } else {
                props.put("company", new Props()
                        .put("id", companyId)
                        .put("name", company.getName())
                        .put("profile_url", company.getProfileUrl())
                        .put("city", company.getCity())
                        .put("postal_code", company.getPostalCode())
                        .put("phone_number", company.getPhoneNumber())
                        .put("call_tracking_number", company.getCallTrackingNumber())
                        .put("accessToken", company.getAccessToken())
                        .put("checksum", CompanyService.generateChecksum(companyId))
                        .put("email", companyEmail)
                        .put("industries", industries)
                        .put("is_claimed", company.getAccountCompanies().size() > 0)
                        .put("founding_date", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

                this.analyticsService.track(sender.getUuid(), EVENT_NEW_THREAD, props);
            }
        } catch (Exception e) {
            logger.fatal(String.format("Error triggering events when sending message in new thread for user (ID: %d)", sender.getId()), e);
        }

        // Trigger push event
        Converter<Message, MessageResource> converter = new MessageConverter();
        Map<String, Object> pushData = new HashMap<>();
        pushData.put("threadId", savedThread.getId());
        pushData.put("companyId", companyId);
        pushData.put("subject", subject);
        pushData.put("timeSent", savedThread.getCreated().getTime());
        pushData.put("message", converter.convert(message));
        this.pushService.push("private-company-notifications-" + companyId, "new-thread", pushData);

        return savedThread;
    }

    @Override
    public Page<Thread> getThreadsForUserInbox(int accountId, Pageable pageable) {
        return this.threadRepository.getThreadsForUserInbox(accountId, pageable);
    }

    @Override
    public Page<Thread> getThreadsForCompanyInbox(int companyId, Pageable pageable) {
        return this.getThreadsForCompanyInbox(companyId, null, null, pageable);
    }

    @Override
    public Page<Thread> getThreadsForCompanyInbox(int companyId, Long checksum, String accessToken, Pageable pageable) {
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("Company with identifier %d not found!", companyId));
        }

        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            throw new AccessDeniedException("User is not allowed to view this inbox!");
        }

        return this.threadRepository.getThreadsForCompanyInbox(companyId, pageable);
    }

    @Override
    @Transactional
    public Slice<Message> getMessages(int threadId, Pageable pageable) {
        if (!this.hasAccessToThread(threadId, AuthUtils.getPrincipal())) {
            throw new AccessDeniedException(String.format("The user does not have access to this thread (ID: %d)!", threadId));
        }

        return this.messageRepository.findByThreadId(threadId, pageable);
    }

    @Override
    @Transactional
    public Slice<Message> getMessages(int threadId, String accessToken, Long checksum, Pageable pageable) {
        if (!this.hasAccessToThread(threadId, checksum, accessToken)) {
            throw new AccessDeniedException(String.format("The user does not have access to this thread (ID: %d)!", threadId));
        }

        return this.messageRepository.findByThreadId(threadId, pageable);
    }

    @Override
    @Transactional
    public Message addMessageToThread(int threadId, String messageText, Account sender, String ipAddress, boolean isSentAsCompany) {
        if (!this.hasAccessToThread(threadId, sender)) {
            throw new AccessDeniedException("Thread does not exist, or the account does not have access to it.");
        }

        Thread thread = this.threadRepository.findOne(threadId);
        List<Participant> participants = this.threadRepository.getThreadParticipantsFromAccount(sender.getId(), threadId);
        Participant senderParticipant = null;
        Date now = new Date();
        Company company;

        // Sending messages between two companies is currently not supported, so it is safe to do this
        if (thread.getReceiver().getCompany() != null) {
            company = thread.getReceiver().getCompany();
        } else {
            company = thread.getSender().getCompany();
        }

        if (participants.size() == 0) { // Account is not yet a participant in this thread
            if (isSentAsCompany) {
                senderParticipant = new Participant();
                senderParticipant.setAccount(this.threadRepository.getEntityManager().getReference(Account.class, sender.getId()));
                senderParticipant.setCompany(company);
            } else {
                // There is no participant for the user. Therefore the user has not sent a message in the thread.
                // Therefore the sender is not the user; the user must be the receiver
                senderParticipant = thread.getReceiver();
            }

            // Add thread participant
            senderParticipant.setLastActivity(now);
            thread.getParticipants().add(senderParticipant);
        } else if (participants.size() == 1) {
            Participant currentParticipant = participants.get(0);

            if (isSentAsCompany) {
                if (currentParticipant.getCompany() != null) {
                    // The current participant is the user sending on behalf on the company; reuse participant
                    senderParticipant = currentParticipant;
                } else {
                    // The current participant is the user, but the message was sent as a user;
                    // create new participant for the user messaging on behalf of the company
                    senderParticipant = new Participant();
                    senderParticipant.setAccount(this.threadRepository.getEntityManager().getReference(Account.class, sender.getId()));
                    senderParticipant.setCompany(company);

                    // Add thread participant
                    senderParticipant.setLastActivity(now);
                    thread.getParticipants().add(senderParticipant);
                }
            } else {
                if (currentParticipant.getCompany() != null) {
                    // The participant is the user, but on behalf of the company. So create a new participant with "only the user"
                    senderParticipant = new Participant();
                    senderParticipant.setAccount(this.threadRepository.getEntityManager().getReference(Account.class, sender.getId()));

                    // Add thread participant
                    senderParticipant.setLastActivity(now);
                    thread.getParticipants().add(senderParticipant);
                } else {
                    senderParticipant = currentParticipant; // The participant is the user (and not on behalf of the company)
                }
            }
        } else {
            // Account is participating both as a user and as a company; find the appropriate participant and use it
            int i = 0;
            boolean found = false;

            while (i < participants.size() && !found) {
                Participant temp = participants.get(i);

                if ((isSentAsCompany && temp.getCompany() != null) || (!isSentAsCompany && temp.getCompany() == null)) {
                    senderParticipant = temp;
                    found = true;
                }

                i++;
            }

            if (!found) {
                throw new RuntimeException("Could not find thread participant!");
            }
        }

        senderParticipant.setThread(thread);
        senderParticipant.setLastActivity(now);
        Message message = new Message();
        message.setText(messageText);
        message.setIpAddress(ipAddress);
        message.setThread(thread);
        message.setSender(senderParticipant);
        message.setCreated(now);
        thread.setLastActivity(now);

        // We know that the receiver is a company
        if (isSentAsCompany) {
            thread.getReceiver().setLastActivity(now);
        }

        Message saved = this.messageRepository.save(message);
        this.threadRepository.save(thread);

        // Trigger push message
        String channelName;
        Participant receiverParticipant;
        Converter<Message, MessageResource> converter = new MessageConverter();
        Map<String, Object> pushData = new HashMap<>();
        pushData.put("threadId", threadId);
        pushData.put("message", converter.convert(saved));

        if (isSentAsCompany) { // Company sending to user
            if (thread.getReceiver().getCompany() == null) {
                receiverParticipant = thread.getReceiver();
            } else {
                receiverParticipant = thread.getSender();
            }

            channelName = "private-user-notifications-" + receiverParticipant.getAccount().getUuid();
        } else { // User sending to company
            if (thread.getReceiver().getCompany() != null) {
                receiverParticipant = thread.getReceiver();
            } else {
                receiverParticipant = thread.getSender();
            }

            channelName = "private-company-notifications-" + company.getId();
            pushData.put("companyId", company.getId());
        }

        this.pushService.push(channelName, "new-message", pushData);

        // Trigger sent message event
        Props senderProps = new Props();
        Props receiverProps = new Props();

        if (senderParticipant.getCompany() != null) {
            senderProps.put("name", senderParticipant.getCompany().getName());
        } else {
            Account senderAccount = senderParticipant.getAccount();
            senderProps.put("name", AccountUtils.getFullName(senderAccount.getFirstName(), senderAccount.getMiddleName(), senderAccount.getLastName()));
        }

        if (receiverParticipant.getCompany() != null) {
            Company receiverCompany = receiverParticipant.getCompany();
            receiverProps.put("id", receiverCompany.getId());
            receiverProps.put("name", receiverCompany.getName());
            receiverProps.put("email", receiverCompany.getEmail());
            receiverProps.put("accessToken", receiverCompany.getAccessToken());
            receiverProps.put("checksum", CompanyService.generateChecksum(receiverCompany.getId()));
        } else {
            Account receiverAccount = receiverParticipant.getAccount();
            receiverProps.put("id", receiverAccount.getId());
            receiverProps.put("name", AccountUtils.getFullName(receiverAccount.getFirstName(), receiverAccount.getMiddleName(), receiverAccount.getLastName()));
            receiverProps.put("email", receiverAccount.getEmail());
            receiverProps.put("accessToken", receiverAccount.getAccessToken());
            receiverProps.put("checksum", AccountService.generateChecksum(receiverAccount.getId()));
        }

        Props props = new Props();
        props.put("threadId", threadId);
        props.put("message", messageText);
        props.put("isCompany", isSentAsCompany);
        props.put("sender", senderProps);
        props.put("receiver", receiverProps);
        this.analyticsService.track(senderParticipant.getAccount().getUuid(), EVENT_MESSAGE_SENT, props);

        return saved;
    }

    @Override
    public boolean hasAccessToThread(int threadId, Account account) {
        Assert.notNull(account);
        return this.hasAccessToThread(threadId, account.getId());
    }

    @Override
    public boolean hasAccessToThread(int threadId, int accountId) {
        return (AuthUtils.isAdmin() || this.threadRepository.hasAccessToThread(accountId, threadId));
    }

    @Override
    @Transactional
    public boolean hasAccessToThread(int threadId, Long checksum, String accessToken) {
        Assert.notNull(checksum);
        Assert.notNull(accessToken);

        if (AuthUtils.isAdmin()) {
            return true;
        }

        Thread thread = this.threadRepository.findOne(threadId);

        if (thread == null) {
            return false;
        }

        Company receiver = thread.getReceiver().getCompany();
        Account sender = thread.getSender().getAccount();

        return ((CompanyService.generateChecksum(receiver.getId()) == checksum && accessToken.equals(receiver.getAccessToken()))
                || (AccountService.generateChecksum(sender.getId()) == checksum && accessToken.equals(sender.getAccessToken())));
    }

    @Override
    public boolean hasCompanyAccessToThread(int threadId, int companyId) {
        return this.threadRepository.hasCompanyAccessToThread(threadId, companyId);
    }

    @Override
    public boolean hasAccessToCompanyInbox(Account account, int companyId) {
        Assert.notNull(account);
        return (CompanyUtils.isCompanyOwner(account, companyId) || AuthUtils.isAdmin());
    }

    @Override
    public boolean isParticipatingInThreadOnBehalfOfCompany(int threadId, Account account, int companyId) {
        Assert.notNull(account);
        return this.threadRepository.isParticipatingInThreadOnBehalfOfCompany(threadId, account.getId(), companyId);
    }

    @Override
    @Transactional
    public boolean isParticipatingInThreadOnBehalfOfCompany(int threadId, int companyId, String accessToken, Long checksum) {
        Assert.notNull(accessToken);
        Assert.notNull(checksum);
        Thread thread = this.threadRepository.findOne(threadId);

        if (thread == null) {
            return false;
        }

        for (Participant participant : thread.getParticipants()) {
            boolean isMatchingAccessToken = (participant.getAccount() != null && accessToken.equals(participant.getAccount().getAccessToken()));
            boolean isMatchingChecksum = (participant.getAccount() != null && AccountService.generateChecksum(participant.getAccount().getId()) == checksum);

            if (participant.getCompany() != null && participant.getCompany().getId() == companyId && isMatchingAccessToken && isMatchingChecksum) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Participant getThreadParticipant(Account account, int threadId) {
        return this.participantRepository.getThreadParticipant(account.getId(), threadId);
    }

    @Override
    @Transactional
    public void markAsReadForRegularUser(int threadId) {
        this.markAsReadForRegularUser(threadId, null, null);
    }

    @Override
    @Transactional
    public void markAsReadForRegularUser(int threadId, String accessToken, Long checksum) {
        if (AuthUtils.isAuthenticated() && (accessToken == null && checksum == null)) {
            if (!this.hasAccessToThread(threadId, AuthUtils.getPrincipal().getId())) {
                throw new AccessDeniedException(String.format("User is not allowed to access thread (ID: %d)", threadId));
            }
        } else {
            if (accessToken == null || checksum == null || !this.hasAccessToThread(threadId, checksum, accessToken)) {
                throw new AccessDeniedException(String.format("User is not allowed to access thread (ID: %d)", threadId));
            }
        }

        Thread thread = this.threadRepository.findOne(threadId);

        if (thread != null) {
            // We know that either the sender or receiver is the user participant (the participant without a company)
            if (thread.getSender().getCompany() == null) {
                thread.getSender().setLastActivity(new Date());
            } else {
                thread.getReceiver().setLastActivity(new Date());
            }

            this.threadRepository.save(thread);
        }
    }

    @Override
    @Transactional
    public void markAsReadForCompany(int threadId, int companyId) {
        this.markAsReadForCompany(threadId, companyId, null, null);
    }

    @Override
    @Transactional
    public void markAsReadForCompany(int threadId, int companyId, String accessToken, Long checksum) {
        // Verify that the company is participating in the thread
        if (!this.hasCompanyAccessToThread(threadId, companyId)) {
            throw new AccessDeniedException(String.format("This company (ID: %d) does not have access to this thread (ID: %d)", companyId, threadId));
        }

        boolean isAuthenticated = AuthUtils.isAuthenticated();

        if (!isAuthenticated && (accessToken == null || checksum == null)) {
            throw new AccessDeniedException("Access token and/or checksum not provided");
        }

        Thread thread = this.threadRepository.findOne(threadId);

        // Logged in with no tokens
        if (isAuthenticated && (accessToken == null || checksum == null)) {
            boolean isCompanyOwner = CompanyUtils.isCompanyOwner(AuthUtils.getPrincipal(), companyId);

            // Don't mark as read for admins, unless they own the company
            if (!isCompanyOwner && AuthUtils.isAdmin()) {
                return;
            }

            if (!isCompanyOwner) {
                throw new AccessDeniedException(String.format("User does not have access to company (ID: %d). Attempted to view thread with ID: %d", companyId, threadId));
            }
        } else {
            if (AuthUtils.isAdmin()) {
                return; // Don't mark as read if admin
            }

            // Verify access token and checksum for company
            Company company = thread.getReceiver().getCompany(); // Currently, the receiver is always a company

            if ((CompanyService.generateChecksum(company.getId()) != checksum || !accessToken.equals(company.getAccessToken()))) {
                throw new AccessDeniedException("Incorrect checksum and/or access token");
            }
        }

        Participant receiver = thread.getReceiver();

        if (receiver.getCompany() != null && receiver.getCompany().getId() == companyId) {
            Date now = new Date();
            receiver.setLastActivity(now);
            this.threadRepository.save(thread);
        }
    }

    @Override
    @Transactional
    public void markAsReadForCompanyParticipant(int threadId, int companyId, String accessToken, Long checksum) {
        Assert.notNull(accessToken);
        Assert.notNull(checksum);
        Thread thread = this.threadRepository.getThreadWithCompanyRepresentativeParticipantAndReceiver(threadId, accessToken, companyId);

        if (thread != null) {
            Participant participant = thread.getParticipants().iterator().next();

            // Ensure that the checksum matches
            if (AccountService.generateChecksum(participant.getAccount().getId()) != checksum) {
                throw new AccessDeniedException("Invalid checksum");
            }

            Date now = new Date();
            participant.setLastActivity(now);
            Participant receiver = thread.getReceiver();

            // The user acts on behalf of the company, and the company is the receiver
            if (receiver.getCompany() != null && receiver.getCompany().getId() == companyId) {
                receiver.setLastActivity(now);
            }

            this.threadRepository.save(thread);
        }
    }

    @Override
    @Transactional
    public void markAsReadForCompanyParticipant(int threadId, int companyId, Account account) {
        Assert.notNull(account);

        if (CompanyUtils.isCompanyOwner(account, companyId)) {
            Thread thread = this.threadRepository.getThreadWithCompanyRepresentantParticipantAndReceiver(threadId, account.getId(), companyId);

            if (thread != null) {
                Date now = new Date();
                Participant participant = thread.getParticipants().iterator().next();
                participant.setLastActivity(now);
                Participant receiver = thread.getReceiver();

                // The user acts on behalf of the company, and the company is the receiver
                if (receiver.getCompany() != null && receiver.getCompany().getId() == companyId) {
                    receiver.setLastActivity(now);
                }

                this.threadRepository.save(thread);
            }
        }
    }

    @Override
    public Thread getThread(int threadId, Account account) {
        Assert.notNull(account);
        return this.threadRepository.getThreadByIdAndAccount(threadId, account.getId());
    }

    @Override
    public Map<Integer, Long> getUnreadMessagesCountPerThread(int accountId) {
        return this.threadRepository.getUnreadMessagesCountInUnreadThreads(accountId);
    }

    @Override
    public Map<Integer, Map<Integer, Long>> getUnreadMessagesCountPerThreadForClaimedCompanies(int accountId) {
        return this.threadRepository.getUnreadMessagesCountInUnreadThreadsForClaimedCompanies(accountId);
    }

    @Override
    public Company getParticipatingCompany(int threadId) {
        return this.threadRepository.getParticipatingCompany(threadId);
    }

    @Override
    public Page<Thread> getUnreadThreadsLeads(Pageable pageable) {
        return this.threadRepository.getUnreadThreadsLeads(pageable);
    }

    @Override
    @Transactional
    public void sendEmailToReceiver(int threadId) {
        Thread thread = this.threadRepository.findOne(threadId);

        if (thread == null) {
            throw new RuntimeException(String.format("Thread not found (ID: %d)", threadId));
        }

        if (thread.getReceiver().getLastActivity() != null) {
            throw new RuntimeException(String.format("The thread has already been seen by the receiver (ID: %d)", threadId));
        }

        // Trigger new thread event
        Account sender = thread.getSender().getAccount(); // Sender is currently always an account
        Company receiver = thread.getReceiver().getCompany(); // Receiver is currently always a company
        String companyEmail = receiver.getEmail();

        if (companyEmail == null || companyEmail.isEmpty()) {
            throw new RuntimeException(String.format("The receiver does not have an e-mail address (thread ID: %d)", threadId));
        }

        Props props = new Props();
        props.put("thread", new Props()
                .put("id", thread.getId())
                .put("subject", thread.getSubject()));

        props.put("receiver", new Props()
                .put("id", receiver.getId())
                .put("name", receiver.getName())
                .put("profileUrl", receiver.getProfileUrl())
                .put("city", receiver.getCity())
                .put("postalCode", receiver.getPostalCode())
                .put("phoneNumber", receiver.getPhoneNumber())
                .put("callTrackingNumber", receiver.getCallTrackingNumber())
                .put("email", companyEmail)
                .put("accessToken", receiver.getAccessToken())
                .put("checksum", CompanyUtils.generateChecksum(receiver.getId()))
                .put("industries", CompanyUtils.getIndustryNames(receiver.getIndustries()))
                .put("isClaimed", receiver.getAccountCompanies().size() > 0)
                .put("foundingDate", (receiver.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", receiver.getFoundingDate()))));

        props.put("sender", new Props()
                .put("email", sender.getEmail())
                .put("firstName", sender.getFirstName())
                .put("middleName", sender.getMiddleName())
                .put("lastName", sender.getLastName())
                .put("fullName", AccountUtils.getFullName(sender.getFirstName(), sender.getMiddleName(), sender.getLastName())));

        this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Lead Message", props);
    }

    @Override
    @Transactional
    public Thread sendWelcomeMessageToCompany(int companyId, int fromAccountId) {
        Account account = this.accountService.find(fromAccountId);

        if (account == null) {
            throw new InvalidArgumentException(String.format("Invalid account ID: %d", fromAccountId));
        }

        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("A company with ID: %d could not be found!", companyId));
        }

        Date now = new Date();
        String subject = "Velkommen til Better";
        String messageBody = "Hej, og hjertelig velkommen til Better!\n\n" +
                "På Better.dk, får du en helt gratis platform til at kommunikere med dine kunder, markedsføre " +
                "din virksomhed i dit lokalområde, og meget mere! Vi arbejder på en række værktøjer som hjælper din " +
                "virksomhed med at vokse og få øget online tilstedeværelse, så hold øje med nye funktioner her i " +
                "dit kontrolpanel.\n\n" +
                "Skulle du have nogle spørgsmål, er du altid mere end velkommen til at kontakte os her i denne samtale. " +
                "Det er i øvrigt også her, at du vil kunne se dine kundehenvendelser. Endnu en gang velkommen, og vi glæder os " +
                "til at vækste din virksomhed sammen med dig!\n\n" +
                "Med venlig hilsen\n" +
                "Better Teamet";

        /** Create thread **/
        Thread thread = new Thread();
        thread.setSubject(subject);
        thread.setLastActivity(now);
        thread.setCreated(now);
        Thread savedThread = this.threadRepository.save(thread);

        /** Save participants **/
        Participant senderParticipant = new Participant();
        senderParticipant.setAccount(account);
        senderParticipant.setThread(savedThread);
        senderParticipant.setLastActivity(now);

        Participant companyParticipant = new Participant();
        companyParticipant.setCompany(company);
        companyParticipant.setThread(savedThread);

        this.participantRepository.save(senderParticipant);
        this.participantRepository.save(companyParticipant);

        /** Save message **/
        Message message = new Message();
        message.setText(messageBody);
        message.setThread(savedThread);
        message.setSender(senderParticipant);
        message.setCreated(now);
        message = this.messageRepository.save(message);

        /** Add participants to thread **/
        savedThread.setSender(senderParticipant);
        savedThread.setReceiver(companyParticipant);

        Set<Participant> participants = new HashSet<>(2);
        participants.add(senderParticipant);
        participants.add(companyParticipant);
        savedThread.setParticipants(participants);
        this.threadRepository.save(savedThread);

        // Ignore analytics error
        try {
            // Trigger new thread event
            Props props = new Props();
            props.put("thread_id", savedThread.getId());
            props.put("thread_subject", subject);
            props.put("message", messageBody);
            props.put("sender", new Props()
                    .put("firstName", account.getFirstName())
                    .put("middleName", account.getMiddleName())
                    .put("lastName", account.getLastName())
                    .put("fullName", AccountUtils.getFullName(account.getFirstName(), account.getMiddleName(), account.getLastName())));

            props.put("company", new Props()
                    .put("id", companyId)
                    .put("name", company.getName())
                    .put("profile_url", company.getProfileUrl())
                    .put("city", company.getCity())
                    .put("postal_code", company.getPostalCode())
                    .put("phone_number", company.getPhoneNumber())
                    .put("call_tracking_number", company.getCallTrackingNumber())
                    .put("accessToken", company.getAccessToken())
                    .put("industries", CompanyUtils.getIndustryNames(company.getIndustries()))
                    .put("is_claimed", company.getAccountCompanies().size() > 0)
                    .put("founding_date", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

            this.analyticsService.track(account.getUuid(), EVENT_WELCOME_MESSAGE, props);
        } catch (Exception e) {
            logger.fatal(String.format("Error triggering events when sending message in new thread for user (ID: %d)", account.getId()), e);
        }

        // Trigger push event
        Converter<Message, MessageResource> converter = new MessageConverter();
        Map<String, Object> pushData = new HashMap<>();
        pushData.put("threadId", savedThread.getId());
        pushData.put("companyId", companyId);
        pushData.put("subject", subject);
        pushData.put("timeSent", savedThread.getCreated().getTime());
        pushData.put("message", converter.convert(message));
        this.pushService.push("private-company-notifications-" + companyId, "new-thread", pushData);

        return savedThread;
    }
}