package dk.better.thread.controller;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.application.util.AuthUtils;
import dk.better.thread.entity.Thread;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Bo Andersen
 */
@Controller("threadController")
public class ThreadController {
    @Autowired
    private ThreadService threadService;

    @Autowired
    private AccountService accountService;


    @RequestMapping("/messages")
    public String viewInbox(@RequestParam(value = "token", required = false) String accessToken, @RequestParam(value = "checksum", required = false) Long checksum, @RequestParam(value = "page", defaultValue = "1") int page, Model model) {
        Account account;

        if (AuthUtils.isAuthenticated() && (accessToken == null && checksum == null)) {
            account = AuthUtils.getPrincipal();
        } else {
            if (accessToken == null || accessToken.isEmpty() || checksum == null) {
                throw new AccessDeniedException("Not permitted to access messages without access token!");
            }

            account = this.accountService.findByAccessToken(accessToken);

            if (account == null) {
                throw new AccessDeniedException("Invalid access token!");
            }

            if (checksum != AccountService.generateChecksum(account.getId())) {
                throw new AccessDeniedException("Invalid checksum provided!");
            }
        }

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "lastActivity"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Page<Thread> threads = this.threadService.getThreadsForUserInbox(account.getId(), pageable);

        model.addAttribute("title", "Beskeder");
        model.addAttribute("accessedAccount", account);
        model.addAttribute("threads", threads.getContent());
        model.addAttribute("hasMoreThreads", threads.hasNext());

        return "view-user-inbox";
    }
}