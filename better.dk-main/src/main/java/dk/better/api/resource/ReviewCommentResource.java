package dk.better.api.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewCommentResource {
    private int id;
    private String text;
    private Date created;
    private CompanyResource company;
    private AccountResource account;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public CompanyResource getCompany() {
        return company;
    }

    public void setCompany(CompanyResource company) {
        this.company = company;
    }

    public AccountResource getAccount() {
        return account;
    }

    public void setAccount(AccountResource account) {
        this.account = account;
    }
}