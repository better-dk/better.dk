package dk.better.api.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThreadResource {
    private int id;
    private String subject;
    private Date created;
    private Date lastActivity;
    private String mostRecentMessageText;
    private ParticipantResource sender;
    private ParticipantResource receiver;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public ParticipantResource getSender() {
        return sender;
    }

    public void setSender(ParticipantResource sender) {
        this.sender = sender;
    }

    public ParticipantResource getReceiver() {
        return receiver;
    }

    public void setReceiver(ParticipantResource receiver) {
        this.receiver = receiver;
    }

    public String getMostRecentMessageText() {
        return mostRecentMessageText;
    }

    public void setMostRecentMessageText(String mostRecentMessageText) {
        this.mostRecentMessageText = mostRecentMessageText;
    }
}