package dk.better.api.util;

import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.NotAuthenticatedException;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.GeneralUtils;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Bo Andersen
 * @see http://stackoverflow.com/questions/7486012/static-classes-in-java
 */
public final class ApiUtils {
    private static final String SORT_ASCENDING = "+";
    private static final String SORT_DESCENDING = "-";

    private ApiUtils() {
    }

    public static void validateAuthenticated() {
        if (!AuthUtils.isAuthenticated()) {
            throw new NotAuthenticatedException();
        }
    }

    public static void validateIsAdmin() {
        if (!AuthUtils.isAuthenticated()) {
            throw new NotAuthenticatedException();
        }

        if (!AuthUtils.isAdmin()) {
            throw new AccessDeniedException("This resource requires administrator access level");
        }
    }

    /**
     * Performs simple offset verification
     *
     * @param offset The offset at which the first item should be fetched from the collection
     * @return Whether or not the offset is valid
     */
    public static boolean isValidOffset(int offset) {
        return (offset >= 0);
    }

    // improve: document
    public static Sort getSortOrder(String sort) {
        return getSortOrder(sort, null);
    }

    // improve: document
    public static Sort getSortOrder(String sort, List<String> permittedPropertyNames) {
        return getSortOrder(sort, permittedPropertyNames, false);
    }

    // improve: document
    public static Sort getSortOrder(String sort, List<String> permittedPropertyNames, boolean ignoreErrors) {
        if (sort.isEmpty()) {
            if (ignoreErrors) {
                return null;
            } else {
                throw new InvalidArgumentException("Sort cannot be empty");
            }
        }

        String[] parts = sort.split(",");
        List<Sort.Order> orders = new ArrayList<Sort.Order>();
        String propertyName;

        for (String part : parts) {
            if (part.length() > 1) {
                propertyName = part.substring(1, part.length());

                if (!permittedPropertyNames.contains(propertyName)) {
                    if (!ignoreErrors) {
                        throw new InvalidArgumentException(String.format("%s is not a valid property", propertyName));
                    }
                } else {
                    if (part.startsWith(SORT_ASCENDING)) {
                        orders.add(new Sort.Order(Sort.Direction.ASC, propertyName));
                    } else if (part.startsWith(SORT_DESCENDING)) {
                        orders.add(new Sort.Order(Sort.Direction.DESC, propertyName));
                    }
                }
            }
        }

        if (!orders.isEmpty()) {
            return new Sort(orders);
        }

        if (ignoreErrors) {
            return null;
        } else {
            throw new InvalidArgumentException("No valid sort properties provided");
        }
    }

    // improve: document
    public static Collection<Link> getPaginationLinks(Slice slice, HttpServletRequest request, int offset, int limit) {
        return getPaginationLinks(slice, request, offset, limit, null);
    }

    // improve: document
    public static Collection<Link> getPaginationLinks(Slice slice, HttpServletRequest request, int offset, int limit, Map<String, String> additionalParameters) {
        Collection<Link> links = new ArrayList<>();

        if (slice.hasPrevious()) {
            links.add(new Link(getPaginationLink(request, (offset - limit), limit, additionalParameters), "previous"));
        }

        if (slice.hasNext()) {
            links.add(new Link(getPaginationLink(request, (offset + limit), limit, additionalParameters), "next"));
        }

        return links;
    }

    // improve: document
    private static String getPaginationLink(HttpServletRequest request, int offset, int limit, Map<String, String> additionalParameters) {
        StringBuilder stringBuilder = new StringBuilder(128);
        stringBuilder.append(request.getScheme() + "://")
                .append(GeneralUtils.getRootDomain(request))
                .append(request.getRequestURI())
                .append("?")
                .append(getQueryParameter("limit", String.valueOf(limit)))
                .append("&")
                .append(getQueryParameter("offset", String.valueOf(offset)));

        if (additionalParameters != null) {
            for (Map.Entry<String, String> entry : additionalParameters.entrySet()) {
                stringBuilder.append("&")
                        .append(getQueryParameter(entry.getKey(), entry.getValue()));
            }
        }

        return stringBuilder.toString();
    }

    // improve: document
    private static String getQueryParameter(String name, String value) {
        if (name.isEmpty() || value.isEmpty()) {
            return null;
        }

        try {
            return URLEncoder.encode(name, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return name + "=" + value; // This really shouldn't happen
        }
    }
}