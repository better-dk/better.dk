package dk.better.api.controller;

import dk.better.application.exception.InvalidArgumentException;
import dk.better.company.entity.Call;
import dk.better.company.entity.Company;
import dk.better.company.service.CallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author Bo Andersen
 */
@RestController
@RequestMapping("/api/call")
public class CallController {
    @Autowired
    private CallService callService;

    private static final String TOKEN = "9B8C466F2B838FE89B7D5F9698F51D19";


    @RequestMapping(value = "/receiver-number", method = RequestMethod.GET, produces = "text/html")
    public String getReceiverNumber(@RequestParam(value = "token", required = true) String token, @RequestParam(value = "opkaldsnummer", required = true) String fromNumber) {
        if (!token.equals(TOKEN)) {
            throw new InvalidArgumentException("Invalid token specified");
        }

        Call mostRecentCallByNumber = this.callService.getLatestCallByFromNumber(fromNumber);
        return (mostRecentCallByNumber != null ? mostRecentCallByNumber.getToNumber() : "");
    }

    @RequestMapping("/end-call")
    public void endCall(@RequestParam(value = "token", required = true) String token, @RequestParam(value = "from", required = true) String fromNumber, @RequestParam(value = "to", required = true) String toNumber, @RequestParam(value = "start", required = true) Long start, @RequestParam(value = "end", required = true) Long end, @RequestParam(value = "connect_time", required = true) Long connectTimestamp, @RequestParam(value = "identifier", required = true) String uniqueId) {
        if (!token.equals(TOKEN)) {
            throw new InvalidArgumentException("Invalid token specified");
        }

        if (fromNumber == null || fromNumber.isEmpty()) {
            return;
        }

        Date startTime = new Date(start * 1000);
        Date connectTime = (connectTimestamp != null && connectTimestamp > 0 ? new Date(connectTimestamp * 1000) : null);
        Date endTime = new Date(end * 1000);

        this.callService.endCall(fromNumber, toNumber, startTime, connectTime, endTime, uniqueId);
    }

    @RequestMapping(value = "/call-tracking/receiver-number", method = RequestMethod.GET, produces = "text/html")
    public String callTrackingReceiverNumber(@RequestParam(value = "token", required = true) String token, @RequestParam(value = "to", required = true) String callTrackingNumber, @RequestParam(value = "from", required = true) String fromNumber) {
        if (!token.equals(TOKEN)) {
            throw new InvalidArgumentException("Invalid token specified");
        }

        Company company = this.callService.initializeCallTrackingCall(fromNumber, callTrackingNumber);
        return (company != null ? company.getPhoneNumber() : "");
    }
}