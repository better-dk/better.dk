package dk.better.api.controller;

import dk.better.company.entity.SendDelay;
import dk.better.company.service.SendDelayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Liubov Zavorotna
 */
@Controller
@RequestMapping(value = "/api/send-delay")
public class SendDelayController {
    @Autowired
    private SendDelayService sendDelayService;


    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addOrders(@RequestBody SendDelay delay) throws BindException {
        sendDelayService.updateSendDelay(delay.getCompanyId(),delay.getHours());
    }
}
