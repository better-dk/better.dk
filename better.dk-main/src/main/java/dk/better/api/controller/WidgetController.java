package dk.better.api.controller;

import dk.better.api.model.widget.SocialRating;
import dk.better.api.model.widget.WidgetSimple;
import dk.better.api.model.widget.WidgetSocial;
import dk.better.company.entity.Company;
import dk.better.company.entity.ExternalReview;
import dk.better.company.repository.ExternalReviewRepositoryImpl;
import dk.better.company.service.CompanyService;
import dk.better.company.service.ExternalReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping(value = "/api/widget")
public class WidgetController {

    @Autowired
    private ExternalReviewService reviewService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ExternalReviewRepositoryImpl reviewRepository;

    @RequestMapping("/overall/{companyId}")
    public WidgetSimple getSimpleWidgetData(@PathVariable int companyId) {
        return reviewRepository.findOverall(companyId);
    }

    @RequestMapping("/social/{companyId}")
    public WidgetSocial getSocialWidgetData(@PathVariable int companyId) {
        List<SocialRating> ratings = reviewService
                .findSocialInfo(companyId)
                .stream()
                .filter(socialRating -> socialRating.getRating() != null && socialRating.getRating() > 3.5)
                .collect(Collectors.toList());
        Company company = companyService.find(companyId);
        List<ExternalReview> reviews = reviewService.findReviewsForWidget(companyId);
        WidgetSimple overall = reviewRepository.findOverall(companyId);
        return new WidgetSocial(companyId, company.getName(), overall, ratings, reviews);
    }
}
