package dk.better.api.controller;

import dk.better.api.converter.AcknowledgementConverter;
import dk.better.api.converter.ServiceConverter;
import dk.better.api.resource.AcknowledgementResource;
import dk.better.api.resource.ServiceResource;
import dk.better.company.entity.Acknowledgement;
import dk.better.company.entity.Service;
import dk.better.company.service.AcknowledgementService;
import dk.better.company.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Bo Andersen
 */
@RestController
@RequestMapping("/api/industry")
public class IndustryController {
    @Autowired
    private AcknowledgementService acknowledgementService;

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "/{industryIds}/acknowledgements", method = RequestMethod.GET)
    public ResponseEntity<Resources<AcknowledgementResource>> getAcknowledgementsForIndustry(HttpServletRequest request, @PathVariable("industryIds") Integer[] industryIds) {
        List<Acknowledgement> acknowledgements;

        if (industryIds.length == 1) {
            acknowledgements = this.acknowledgementService.findByIndustryId(industryIds[0]);
        } else {
            acknowledgements = this.acknowledgementService.findByIndustryIds(Arrays.asList(industryIds));
        }

        Converter<Acknowledgement, AcknowledgementResource> converter = new AcknowledgementConverter();
        List<AcknowledgementResource> resources = new ArrayList<>(acknowledgements.size());

        for (Acknowledgement acknowledgement : acknowledgements) {
            resources.add(converter.convert(acknowledgement));
        }

        // Links
        Collection<Link> links = new ArrayList<>();
        String requestUri = request.getRequestURI();
        String queryString = request.getQueryString();

        if (queryString == null) {
            links.add(new Link(requestUri, "self"));
        } else {
            links.add(new Link(requestUri + "?" + queryString, "self"));
        }

        Resources<AcknowledgementResource> payload = new Resources<>(resources);
        payload.add(links);

        return new ResponseEntity<Resources<AcknowledgementResource>>(payload, HttpStatus.OK);
    }

    @RequestMapping(value = "/{industryIds}/services", method = RequestMethod.GET)
    public ResponseEntity<Resources<ServiceResource>> getServicesForIndustry(HttpServletRequest request, @PathVariable("industryIds") Integer[] industryIds) {
        List<Service> services;

        if (industryIds.length == 1) {
            services = this.serviceService.findByIndustryId(industryIds[0]);
        } else {
            services = this.serviceService.findByIndustryIds(Arrays.asList(industryIds));
        }

        Converter<Service, ServiceResource> converter = new ServiceConverter();
        List<ServiceResource> resources = new ArrayList<>(services.size());

        for (Service service : services) {
            resources.add(converter.convert(service));
        }

        // Links
        Collection<Link> links = new ArrayList<>();
        String requestUri = request.getRequestURI();
        String queryString = request.getQueryString();

        if (queryString == null) {
            links.add(new Link(requestUri, "self"));
        } else {
            links.add(new Link(requestUri + "?" + queryString, "self"));
        }

        Resources<ServiceResource> payload = new Resources<>(resources);
        payload.add(links);

        return new ResponseEntity<Resources<ServiceResource>>(payload, HttpStatus.OK);
    }
}