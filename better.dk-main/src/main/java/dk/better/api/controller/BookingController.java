package dk.better.api.controller;

import dk.better.application.model.EmptyJsonResponse;
import dk.better.booking.dto.AddBookingDTO;
import dk.better.booking.service.BookingService;
import dk.better.booking.util.BookingType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bo Andersen
 */
@RestController
@RequestMapping("/api/booking")
public class BookingController {
    private static Logger logger = LogManager.getLogger(BookingController.class);

    @Autowired
    private BookingService bookingService;

    @RequestMapping(value = {"/add"}, method = {RequestMethod.POST})
    public ResponseEntity<EmptyJsonResponse> add(@Valid @RequestBody AddBookingDTO dto, BindingResult bindingResult, HttpServletRequest request) throws BindException {
        String auth = request.getHeader("Authorization");
        if (auth == null) {
            throw new AccessDeniedException("Missing Authorization header!");
        }
        String regex = "API-KEY=\"([a-z0-9]+)\"";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(auth);
        if (!matcher.find()) {
            throw new AccessDeniedException("Invalid Authorization header!");
        }
        String apiKey = matcher.group(1);
        if (bindingResult.hasErrors()) {
            BindException be = new BindException(bindingResult);
            logger.error("Validation of booking failed!", be);
            throw be;
        }

        this.bookingService.add(dto, BookingType.TREATMENT, apiKey);
        return new ResponseEntity<>(new EmptyJsonResponse(), null, HttpStatus.CREATED);
    }
}