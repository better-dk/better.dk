package dk.better.api.controller;

import dk.better.company.entity.TriggeredOrders;
import dk.better.company.service.TriggeredOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Liubov Zavorotna
 */
@Controller
@RequestMapping(value = "/api/triggered-orders")
public class TriggeredOrdersController {

    private final TriggeredOrdersService triggeredOrdersService;

    @Autowired
    public TriggeredOrdersController(TriggeredOrdersService triggeredOrdersService) {
        this.triggeredOrdersService = triggeredOrdersService;
    }


    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addOrders(@RequestBody TriggeredOrders orders) throws BindException {
        triggeredOrdersService.trackWithCustomerIO(orders);
    }


}
