package dk.better.api.controller;

import dk.better.application.dto.ExternalReviewDTO;
import dk.better.company.service.ExternalReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Liubov Zavorotna
 */
@RestController
@RequestMapping("/api/external")
public class ExternalReviewController {
    @Autowired
    private ExternalReviewService externalReviewService;


    @RequestMapping(value = "/reviews", method = RequestMethod.GET)
    public List<ExternalReviewDTO> getExternalReviews() {
        return externalReviewService.findExternalReviewsLimited();
    }




}
