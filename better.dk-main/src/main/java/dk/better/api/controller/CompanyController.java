package dk.better.api.controller;

import dk.better.account.entity.Account;
import dk.better.api.converter.LikeConverter;
import dk.better.api.converter.ReviewConverter;
import dk.better.api.resource.LikeResource;
import dk.better.api.resource.ReviewResource;
import dk.better.api.util.ApiUtils;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.application.util.AuthUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Like;
import dk.better.company.entity.Review;
import dk.better.company.service.CompanyService;
import dk.better.company.service.ReviewService;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.ReviewType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * @author Bo Andersen
 */
@RestController("apiCompanyController")
@RequestMapping("/api/company")
public class CompanyController {
    private static final String REVIEW_RATING_POSITIVE = "positive";
    private static final String REVIEW_RATING_NEGATIVE = "negative";

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ReviewService reviewService;


    @RequestMapping(value = "/{companyId}/likes", method = RequestMethod.POST)
    public ResponseEntity<LikeResource> addLike(@PathVariable int companyId) {
        ApiUtils.validateAuthenticated();

        if (companyId <= 0) {
            throw new InvalidArgumentException("Invalid company ID");
        }

        Account account = (Account) AuthUtils.getAuthentication().getPrincipal(); // At this point, we know that the user is authenticated
        Like likeEntity = this.companyService.addLike(companyId, account);
        Converter<Like, LikeResource> converter = new LikeConverter();
        LikeResource like = converter.convert(likeEntity);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(like.getId()).toUri());

        return new ResponseEntity<>(like, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{companyId}/reviews", method = RequestMethod.GET)
    public ResponseEntity<Resources<ReviewResource>> getReviews(HttpServletRequest request, @PathVariable int companyId, @RequestParam(required = false, defaultValue = "0") int offset, @RequestParam(required = false, defaultValue = "10") int limit, @RequestParam(required = false) String sort, @RequestParam(required = false) String rating) {
        // improve: Perform filtering with the JPA Specification object: http://docs.spring.io/spring-data/jpa/docs/1.5.0.RELEASE/reference/html/jpa.repositories.html#specifications

        if (companyId <= 0) {
            throw new InvalidArgumentException("Invalid company ID");
        }

        if (limit <= 0 || limit > 100) {
            throw new InvalidArgumentException("Limit must be higher than 0 and no higher than 100");
        }

        if (!ApiUtils.isValidOffset(offset)) {
            throw new InvalidArgumentException(String.format("%d is not a valid offset", offset));
        }

        int pageNumber = ((offset == 0) ? 0 : (offset / limit)); // The PageRequest object uses page numbers starting from 0
        Page<Review> slice;
        PageRequest pageRequest;

        if (sort != null) {
            Sort sortOrder = ApiUtils.getSortOrder(sort, Arrays.asList("created", "rating"), false); // improve: rename method
            pageRequest = new PageRequest(pageNumber, limit, sortOrder);
        } else {
            pageRequest = new PageRequest(pageNumber, limit);
        }

        if (rating != null) {
            // improve: Handle greater than, less than and equal to in the URL
            switch (rating) {
                case REVIEW_RATING_POSITIVE:
                    slice = this.reviewService.findByCompanyId(companyId, pageRequest, ReviewType.POSITIVE);
                    break;

                case REVIEW_RATING_NEGATIVE:
                    slice = this.reviewService.findByCompanyId(companyId, pageRequest, ReviewType.NEGATIVE);
                    break;

                default:
                    throw new InvalidArgumentException(String.format("Rating must have one of the following values: %s, %s", REVIEW_RATING_POSITIVE, REVIEW_RATING_NEGATIVE));
            }
        } else {
            slice = this.reviewService.findByCompanyId(companyId, pageRequest);
        }

        // Convert entities to resources
        List<ReviewResource> reviews = new ArrayList<>();

        if (slice.hasContent()) {
            Converter<Review, ReviewResource> converter = new ReviewConverter();

            for (Review review : slice.getContent()) {
                reviews.add(converter.convert(review));
            }
        }

        // Add links
        Map<String, String> additionalParameters = new HashMap<String, String>();

        if (sort != null) {
            additionalParameters.put("sort", sort);
        }

        if (rating != null) {
            additionalParameters.put("rating", rating);
        }

        Collection<Link> links = ApiUtils.getPaginationLinks(slice, request, offset, limit, additionalParameters);
        String requestUri = request.getRequestURI();

        // Add link to self
        String queryString = request.getQueryString();

        if (queryString == null) {
            links.add(new Link(requestUri, "self"));
        } else {
            links.add(new Link(requestUri + "?" + queryString, "self"));
        }

        Resources<ReviewResource> payload = new Resources<>(reviews);
        payload.add(links);

        return new ResponseEntity<>(payload, HttpStatus.OK);
    }

    @RequestMapping(value = "/{companyId}", method = RequestMethod.PUT)
    public ResponseEntity<EmptyJsonResponse> edit(@Valid @RequestBody Company company, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        ApiUtils.validateAuthenticated();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        // Validate that the company exists
        if (company.getId() <= 0) { // Use this company ID because the one in the URL can theoretically be manipulated
            throw new InvalidArgumentException("Invalid company ID");
        }

        Company existing = this.companyService.find(company.getId());

        if (existing == null) {
            throw new ResourceNotFoundException("Company does not exist");
        }

        // Copy new values to existing company
        existing.setTeaser(company.getTeaser());
        existing.setDescription(company.getDescription());
        existing.setPhoneNumber(company.getPhoneNumber());
        existing.setCallTrackingNumber(company.getCallTrackingNumber());
        existing.setStreetName(company.getStreetName());
        existing.setStreetNumber(company.getStreetNumber());
        existing.setAcknowledgements(company.getAcknowledgements());
        existing.setIndustries(company.getIndustries());
        existing.setCompanyServices(company.getCompanyServices());
        existing.setCiNumber(company.getCiNumber());
        existing.setCity(company.getCity());
        existing.setPostalCode(company.getPostalCode());
        existing.setName(company.getName());
        existing.setEmail(company.getEmail());
        existing.setWebsite(company.getWebsite());
        existing.setAdditionalAddressText(company.getAdditionalAddressText());
        existing.setCoName(company.getCoName());
        existing.setPostBox(company.getPostBox());
        existing.setLastModified(new Date());
        existing.setBookingUrl(company.getBookingUrl());
        existing.setMeta(company.getMeta());

        // Validate that the user is the owner of the company or is an administrator
        Authentication authentication = AuthUtils.getAuthentication(); // We know that the user is signed in because of @PreAuthorize
        Account principal = (Account) authentication.getPrincipal();

        if (!CompanyUtils.isCompanyOwner(principal, existing) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company %d or an administrator", existing.getId()));
        }

        // Save company
        if (AuthUtils.isAdmin()) {
            existing.setSubdomains(company.getSubdomains()); // Overwrite subdomains with provided subdomains
            this.companyService.adminSaveAndFlush(existing);
        } else {
            this.companyService.saveAndFlush(existing);
        }

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }
}