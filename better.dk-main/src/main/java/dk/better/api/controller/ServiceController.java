package dk.better.api.controller;

import dk.better.api.converter.ServiceConverter;
import dk.better.api.resource.ServiceResource;
import dk.better.api.util.ApiUtils;
import dk.better.company.entity.Service;
import dk.better.company.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Bo Andersen
 */
@RestController
@RequestMapping("/api/service")
public class ServiceController {
    @Autowired
    private ServiceService serviceService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ServiceResource> add(@Valid @RequestBody Service service, BindingResult bindingResult) throws BindException {
        ApiUtils.validateIsAdmin();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Converter<Service, ServiceResource> converter = new ServiceConverter();
        Service saved = this.serviceService.saveAndFlush(service);
        ServiceResource converted = converter.convert(saved);

        return new ResponseEntity<>(converted, HttpStatus.CREATED);
    }
}