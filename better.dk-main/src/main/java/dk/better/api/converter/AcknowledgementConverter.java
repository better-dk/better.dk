package dk.better.api.converter;

import dk.better.api.resource.AcknowledgementResource;
import dk.better.company.entity.Acknowledgement;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.util.HtmlUtils;

/**
 * @author Bo Andersen
 */
public class AcknowledgementConverter implements Converter<Acknowledgement, AcknowledgementResource> {
    @Override
    public AcknowledgementResource convert(Acknowledgement acknowledgement) {
        AcknowledgementResource resource = new AcknowledgementResource();
        resource.setId(acknowledgement.getId());
        resource.setName(HtmlUtils.htmlEscape(acknowledgement.getName()));
        resource.setDescription(HtmlUtils.htmlEscape(acknowledgement.getDescription()));
        resource.setImageName(HtmlUtils.htmlEscape(acknowledgement.getImageName()));

        return resource;
    }
}