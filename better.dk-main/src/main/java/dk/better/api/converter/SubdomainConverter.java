package dk.better.api.converter;

import dk.better.api.resource.CompanyResource;
import dk.better.api.resource.SubdomainResource;
import dk.better.application.util.ViewUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Subdomain;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.util.HtmlUtils;

/**
 * @author Bo Andersen
 */
public class SubdomainConverter implements Converter<Subdomain, SubdomainResource> {
    @Override
    public SubdomainResource convert(Subdomain subdomain) {
        SubdomainResource target = new SubdomainResource();
        target.setId(subdomain.getId());
        target.setName(HtmlUtils.htmlEscape(subdomain.getName()));
        target.setActive(subdomain.isActive());
        target.setTimeAdded(subdomain.getTimeAdded());

        Company company = subdomain.getCompany();
        CompanyResource companyResource = new CompanyResource();
        companyResource.setId(company.getId());
        companyResource.setProfileUrl(company.getProfileUrl());
        companyResource.setName(HtmlUtils.htmlEscape(company.getName()));
        companyResource.setCiNumber(company.getCiNumber());
        companyResource.setPhoneNumber(HtmlUtils.htmlEscape(company.getPhoneNumber()));
        companyResource.setStreetName(HtmlUtils.htmlEscape(company.getStreetName()));
        companyResource.setStreetNumber(HtmlUtils.htmlEscape(company.getStreetNumber()));
        companyResource.setTeaser(HtmlUtils.htmlEscape(company.getTeaser()));
        companyResource.setDescription(HtmlUtils.htmlEscape(company.getDescription()));
        companyResource.setRating(company.getRating());
        companyResource.setNumberOfReviews(company.getNumberOfReviews());
        String baseUrl = ViewUtils.getStaticResourceUrl();

        if (company.getLogoName() != null) {
            companyResource.setLogoUrl(baseUrl + "/" + company.getLogoName());
        } else {
            companyResource.setLogoUrl(baseUrl + "/companies/logos/default/default.png");
        }

        target.setCompany(companyResource);
        return target;
    }
}