package dk.better.api.converter;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.ajax.converter.ReviewCommentConverter;
import dk.better.api.resource.AccountResource;
import dk.better.api.resource.ReviewCommentResource;
import dk.better.api.resource.ReviewResource;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Review;
import dk.better.company.entity.ReviewComment;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bo Andersen
 */
public class ReviewConverter implements Converter<Review, ReviewResource> {
    @Override
    public ReviewResource convert(Review review) {
        // Review
        ReviewResource target = new ReviewResource();
        target.setId(review.getId());
        target.setRating(review.getRating());
        target.setTitle(HtmlUtils.htmlEscape(review.getTitle()));
        target.setText(GeneralUtils.nl2br(HtmlUtils.htmlEscape(review.getText())));
        target.setCreated(review.getCreated());

        // Account
        Account account = review.getAccount();

        if (account != null) {
            AccountResource accountResource = new AccountResource();
            accountResource.setId(account.getUuid());
            accountResource.setFirstName(HtmlUtils.htmlEscape(account.getFirstName()));
            accountResource.setMiddleName(HtmlUtils.htmlEscape(account.getMiddleName()));
            accountResource.setLastName(HtmlUtils.htmlEscape(account.getLastName()));

            if (account.getFacebookProfileId() != null) {
                accountResource.setProfilePicture(AccountService.getProfilePictureUrl(account.getFacebookProfileId()));
            } else {
                accountResource.setProfilePicture("https://better.dk/images/default-account-photo.png"); // todo: make this dynamic
            }

            target.setAuthor(accountResource);
        }

        // todo: company

        // Comments
        List<ReviewCommentResource> comments;

        if (review.getComments() != null) {
            comments = new ArrayList<>(review.getComments().size());
            Converter<ReviewComment, ReviewCommentResource> converter = new ReviewCommentConverter();

            for (ReviewComment comment : review.getComments()) {
                comments.add(converter.convert(comment));
            }
        } else {
            comments = new ArrayList<>(0);
        }

        target.setComments(comments);
        return target;
    }
}