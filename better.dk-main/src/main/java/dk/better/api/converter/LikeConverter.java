package dk.better.api.converter;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.api.resource.AccountResource;
import dk.better.api.resource.CompanyResource;
import dk.better.api.resource.LikeResource;
import dk.better.application.util.ViewUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Like;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.util.HtmlUtils;

/**
 * @author Bo Andersen
 */
public class LikeConverter implements Converter<Like, LikeResource> {
    @Override
    public LikeResource convert(Like like) {
        // Like
        LikeResource target = new LikeResource();
        target.setId(like.getId());
        target.setCreated(like.getCreated());

        // Account
        AccountResource accountResource = new AccountResource();
        Account account = like.getAccount();
        accountResource.setId(account.getUuid());
        accountResource.setFirstName(HtmlUtils.htmlEscape(account.getFirstName()));
        accountResource.setMiddleName(HtmlUtils.htmlEscape(account.getMiddleName()));
        accountResource.setLastName(HtmlUtils.htmlEscape(account.getLastName()));

        if (account.getFacebookProfileId() != null) {
            accountResource.setProfilePicture(AccountService.getProfilePictureUrl(account.getFacebookProfileId()));
        } else {
            accountResource.setProfilePicture("https://better.dk/images/default-account-photo.png"); // todo: make this dynamic
        }

        target.setAccount(accountResource);

        // Company
        Company company = like.getCompany();
        CompanyResource companyResource = new CompanyResource();
        companyResource.setId(company.getId());
        companyResource.setProfileUrl(company.getProfileUrl());
        companyResource.setName(HtmlUtils.htmlEscape(company.getName()));
        companyResource.setCiNumber(company.getCiNumber());
        companyResource.setPhoneNumber(HtmlUtils.htmlEscape(company.getPhoneNumber()));
        companyResource.setStreetName(HtmlUtils.htmlEscape(company.getStreetName()));
        companyResource.setStreetNumber(HtmlUtils.htmlEscape(company.getStreetNumber()));
        companyResource.setTeaser(HtmlUtils.htmlEscape(company.getTeaser()));
        companyResource.setDescription(HtmlUtils.htmlEscape(company.getDescription()));
        companyResource.setRating(company.getRating());
        companyResource.setNumberOfReviews(company.getNumberOfReviews());
        companyResource.setWebsite(HtmlUtils.htmlEscape(company.getWebsite()));
        companyResource.setCreated(company.getCreated());
        companyResource.setLastModified(company.getLastModified());
        String baseUrl = ViewUtils.getStaticResourceUrl();

        if (company.getLogoName() != null) {
            companyResource.setLogoUrl(baseUrl + "/" + company.getLogoName());
        } else {
            companyResource.setLogoUrl(baseUrl + "/companies/logos/default/default.png");
        }

        target.setCompany(companyResource);
        return target;
    }
}