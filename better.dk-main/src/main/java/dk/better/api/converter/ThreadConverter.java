package dk.better.api.converter;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.api.resource.AccountResource;
import dk.better.api.resource.CompanyResource;
import dk.better.api.resource.ParticipantResource;
import dk.better.api.resource.ThreadResource;
import dk.better.application.util.ViewUtils;
import dk.better.company.entity.Company;
import dk.better.thread.entity.Participant;
import dk.better.thread.entity.Thread;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Bo Andersen
 */
public class ThreadConverter implements Converter<Thread, ThreadResource> {
    @Override
    public ThreadResource convert(Thread source) {
        ThreadResource target = new ThreadResource();
        target.setId(source.getId());
        target.setSubject(source.getSubject());
        target.setCreated(source.getCreated());
        target.setLastActivity(source.getLastActivity());
        target.setMostRecentMessageText(source.getMostRecentMessageText());

        // Sender
        Participant senderParticipant = source.getSender();
        ParticipantResource senderResource = new ParticipantResource();
        Account senderAccount = senderParticipant.getAccount();
        AccountResource senderAccountResource = new AccountResource();
        senderAccountResource.setId(senderAccount.getUuid());
        senderAccountResource.setFirstName(senderAccount.getFirstName());
        senderAccountResource.setMiddleName(senderAccount.getMiddleName());
        senderAccountResource.setLastName(senderAccount.getLastName());

        if (senderAccount.getFacebookProfileId() != null) {
            senderAccountResource.setProfilePicture(AccountService.getProfilePictureUrl(senderAccount.getFacebookProfileId()));
        } else {
            senderAccountResource.setProfilePicture("https://better.dk/images/default-account-photo.png"); // todo: make this dynamic
        }

        senderResource.setAccount(senderAccountResource);

        /***** Receiver *****/
        Participant receiverParticipant = source.getReceiver();
        ParticipantResource receiverResource = new ParticipantResource();

        // Account
        if (receiverParticipant.getAccount() != null) {
            Account receiverAccount = receiverParticipant.getAccount();
            AccountResource receiverAccountResource = new AccountResource();
            receiverAccountResource.setId(receiverAccount.getUuid());
            receiverAccountResource.setFirstName(receiverAccount.getFirstName());
            receiverAccountResource.setMiddleName(receiverAccount.getMiddleName());
            receiverAccountResource.setLastName(receiverAccount.getLastName());

            if (receiverAccount.getFacebookProfileId() != null) {
                receiverAccountResource.setProfilePicture(AccountService.getProfilePictureUrl(receiverAccount.getFacebookProfileId()));
            } else {
                receiverAccountResource.setProfilePicture("https://better.dk/images/default-account-photo.png"); // todo: make this dynamic
            }

            receiverResource.setAccount(receiverAccountResource);
        }

        // Company
        Company receiverCompany = receiverParticipant.getCompany();

        if (receiverCompany != null) {
            CompanyResource companyResource = new CompanyResource();
            companyResource.setId(receiverCompany.getId());
            companyResource.setName(receiverCompany.getName());
            companyResource.setProfileUrl(receiverCompany.getProfileUrl());
            String baseUrl = ViewUtils.getStaticResourceUrl();

            if (receiverCompany.getLogoName() != null) {
                companyResource.setLogoUrl(baseUrl + "/" + receiverCompany.getLogoName());
            } else {
                companyResource.setLogoUrl(baseUrl + "/companies/logos/default/default.png");
            }

            receiverResource.setCompany(companyResource);
        }

        target.setSender(senderResource);
        target.setReceiver(receiverResource);
        return target;
    }
}