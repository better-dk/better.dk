package dk.better.api.model.widget;

/**
 * @author vitalii.
 */
public class WidgetSimple {
    private int id;
    private double rating;
    private long count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public WidgetSimple(int id, double rating, long count) {
        this.id = id;
        this.rating = rating;
        this.count = count;
    }

    public WidgetSimple() {
    }

    @Override
    public String toString() {
        return "WidgetSimple{" +
                "id=" + id +
                ", rating=" + rating +
                ", count=" + count +
                '}';
    }
}
