package dk.better.api.model.widget;

import dk.better.company.entity.ExternalReview;

import java.util.List;

/**
 * @author vitalii.
 */
public class WidgetExtended {
    private int id;
    private double rating;
    private int count;

    private List<ExternalReview> reviews;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ExternalReview> getReviews() {
        return reviews;
    }

    public void setReviews(List<ExternalReview> reviews) {
        this.reviews = reviews;
    }
}
