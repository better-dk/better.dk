package dk.better.api.model;

/**
 * @author Bo Andersen
 */
public class ValidationResponseEntry extends AbstractResponseEntity {
    private String field;
    private String message;
    private String code;
    private String value;


    public ValidationResponseEntry(String field, String message) {
        this(field, message, null);
    }

    public ValidationResponseEntry(String field, String message, String code) {
        this(field, message, code, null);
    }

    public ValidationResponseEntry(String field, String message, String code, String value) {
        this.field = field;
        this.message = message;
        this.code = code;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}