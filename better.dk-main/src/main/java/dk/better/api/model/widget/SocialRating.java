package dk.better.api.model.widget;

/**
 * @author vitalii.
 */
public class SocialRating {
    private String name;
    private Double rating;
    private Long count;
    private Integer maxRating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Integer getMaxRating() {
        return maxRating;
    }

    public void setMaxRating(Integer maxRating) {
        this.maxRating = maxRating;
    }

    public SocialRating(String name, Double rating, Long count) {
        this.name = name;
        this.rating = rating;
        this.count = count;
    }

    public SocialRating() {
    }
}
