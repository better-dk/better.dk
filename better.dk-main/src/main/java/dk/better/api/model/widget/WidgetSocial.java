package dk.better.api.model.widget;

import dk.better.company.entity.ExternalReview;

import java.util.List;

/**
 * @author vitalii.
 */
public class WidgetSocial {
    private int id;

    private String name;

    private WidgetSimple overall;

    private List<SocialRating> ratings;

    private List<ExternalReview> reviews;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public WidgetSimple getOverall() {
        return overall;
    }

    public void setOverall(WidgetSimple overall) {
        this.overall = overall;
    }

    public List<SocialRating> getRatings() {
        return ratings;
    }

    public void setRatings(List<SocialRating> ratings) {
        this.ratings = ratings;
    }

    public List<ExternalReview> getReviews() {
        return reviews;
    }

    public void setReviews(List<ExternalReview> reviews) {
        this.reviews = reviews;
    }

    public WidgetSocial(int id, String name, WidgetSimple overall, List<SocialRating> ratings, List<ExternalReview> reviews) {
        this.id = id;
        this.name = name;
        this.overall = overall;
        this.ratings = ratings;
        this.reviews = reviews;
    }
}
