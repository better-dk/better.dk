package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.api.resource.MessageResource;

import java.util.Collection;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResourceListDTO {
    private boolean hasNext;
    private Collection<MessageResource> messages;


    public MessageResourceListDTO() {
    }

    public MessageResourceListDTO(boolean hasNext, Collection<MessageResource> messages) {
        this.hasNext = hasNext;
        this.messages = messages;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public Collection<MessageResource> getMessages() {
        return messages;
    }

    public void setMessages(Collection<MessageResource> messages) {
        this.messages = messages;
    }
}