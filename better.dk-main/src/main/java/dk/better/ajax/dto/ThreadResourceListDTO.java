package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.api.resource.ThreadResource;

import java.util.Collection;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThreadResourceListDTO {
    private boolean hasNext;
    private Collection<ThreadResource> threads;


    public ThreadResourceListDTO() {
    }

    public ThreadResourceListDTO(boolean hasNext, Collection<ThreadResource> threads) {
        this.hasNext = hasNext;
        this.threads = threads;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public Collection<ThreadResource> getThreads() {
        return threads;
    }

    public void setThreads(Collection<ThreadResource> threads) {
        this.threads = threads;
    }
}