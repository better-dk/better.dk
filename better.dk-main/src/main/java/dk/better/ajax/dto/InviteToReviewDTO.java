package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteToReviewDTO {
    @Size(min = 1, max = 500)
    private List<String> emails;

    private boolean addToCustomerList;


    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public boolean isAddToCustomerList() {
        return addToCustomerList;
    }

    public void setAddToCustomerList(boolean addToCustomerList) {
        this.addToCustomerList = addToCustomerList;
    }
}