package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateEmailDTO {
    @NotEmpty
    @Email
    @Length(max = 100)
    private String email;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}