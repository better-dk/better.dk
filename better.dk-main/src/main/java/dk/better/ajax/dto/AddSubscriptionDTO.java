package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddSubscriptionDTO {
    @NotNull
    private Integer productId;

    @NotEmpty
    @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2}")
    private String startDate;

    @NotEmpty
    @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2}")
    private String endDate;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}