package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateWebsiteDTO {
    @URL
    @Length(max = 100)
    private String website;


    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        if (website != null) {
            if (website.isEmpty()) {
                website = null;
            } else if (!website.contains("http://") && !website.contains("https://")) {
                website = "http://" + website;
            }
        }

        this.website = website;
    }
}