package dk.better.ajax.dto;

/**
 * @author vitalii.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ActivateIntegrationDTO {
    private Map<String, Object> metaData;
    private int integration;

    public ActivateIntegrationDTO() {
    }

    public Map<String, Object> getMetaData() {
        return this.metaData;
    }

    public void setMetaData(Map<String, Object> metaData) {
        this.metaData = metaData;
    }

    public int getIntegration() {
        return this.integration;
    }

    public void setIntegration(int integration) {
        this.integration = integration;
    }
}
