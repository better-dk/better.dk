package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddCompanyPageDTO {
    @NotNull
    @Length(min = 5, max = 50)
    private String name;

    @NotNull
    @Length(min = 2, max = 25)
    @Pattern(regexp = "^[a-z0-9\\-]+$", message = "{subdomain.invalid.format}")
    private String slug;

    @NotNull
    @Length(min = 2, max = 50)
    private String title;

    @NotNull
    @Length(min = 25)
    private String content;

    private Integer parentId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}