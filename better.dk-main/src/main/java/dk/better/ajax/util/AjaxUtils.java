package dk.better.ajax.util;

import dk.better.application.exception.NotAuthenticatedException;
import dk.better.application.util.AuthUtils;
import org.springframework.security.access.AccessDeniedException;

/**
 * @author Bo Andersen
 * @see http://stackoverflow.com/questions/7486012/static-classes-in-java
 */
public final class AjaxUtils {
    private AjaxUtils() {
    }

    public static void validateAuthenticated() {
        if (!AuthUtils.isAuthenticated()) {
            throw new NotAuthenticatedException();
        }
    }

    public static void validateIsAdmin() {
        if (!AuthUtils.isAuthenticated()) {
            throw new NotAuthenticatedException();
        }

        if (!AuthUtils.isAdmin()) {
            throw new AccessDeniedException("This resource requires administrator access level");
        }
    }
}