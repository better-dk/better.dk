package dk.better.ajax.controller;

import dk.better.ajax.annotation.AjaxController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Bo Andersen
 */
@AjaxController("ajaxIndexController")
@RequestMapping("/ajax")
public class IndexController {
    @RequestMapping(value = "/keep-alive", method = RequestMethod.GET)
    public ResponseEntity<String> keepAlive() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}