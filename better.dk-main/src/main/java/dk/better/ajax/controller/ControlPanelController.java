package dk.better.ajax.controller;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.*;
import dk.better.ajax.util.AjaxUtils;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.application.util.AuthUtils;
import dk.better.company.entity.Company;
import dk.better.company.service.CompanyService;
import dk.better.company.service.CustomerService;
import dk.better.company.service.IntegrationService;
import dk.better.company.service.ReviewService;
import dk.better.company.util.IntegrationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@AjaxController("ajaxControlPanelController")
@RequestMapping("/ajax/cp")
public class ControlPanelController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private IntegrationService integrationService;


    @RequestMapping(value = "/company/{companyId}/choose-password", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> choosePassword(@Valid @RequestBody ControlPanelChoosePasswordDTO dto, @PathVariable("companyId") int companyId, BindingResult bindingResult) throws BindException {
        if (AuthUtils.isAuthenticated()) {
            throw new RuntimeException("Must not already be logged in");
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        if (!dto.getPassword().equals(dto.getRepeatPassword())) {
            bindingResult.rejectValue("password", null, "Kodeordene matcher ikke");
            throw new BindException(bindingResult);
        }

        if (!this.companyService.hasAccessToCompany(companyId, dto.getChecksum(), dto.getAccessToken())) {
            throw new AccessDeniedException("Not allowed to access company");
        }

        Company company = this.companyService.getForControlPanelViewById(companyId);
        Assert.notNull(company.getEmail());

        if (company.getAccountCompanies() != null && !company.getAccountCompanies().isEmpty()) {
            throw new RuntimeException("This company has already been claimed!");
        }

        Account account = this.accountService.createAndClaim(dto.getName(), company.getEmail(), dto.getPassword(), companyId);

        // Login
        Authentication auth = new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/company/{companyId}/reviews/{reviewUuid}/request-deletion", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> requestReviewDeletion(@Valid @RequestBody RequestReviewDeletionDTO dto, @PathVariable("companyId") int companyId, @PathVariable("reviewUuid") String reviewUuid, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        this.reviewService.requestDeletion(reviewUuid, dto.getReason());
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/reviews/invite", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Integer>> inviteCustomersToReview(@Valid @RequestBody InviteToReviewDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        AjaxUtils.validateAuthenticated();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Integer importedCount = this.reviewService.inviteToReview(companyId, dto.getEmails(), dto.isAddToCustomerList(), CustomerService.Type.CUSTOMER);
        Map<String, Integer> results = new HashMap<>(1);
        results.put("count", importedCount);

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/customers/import", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Integer>> importCustomers(@Valid @RequestBody ImportCustomersDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        AjaxUtils.validateAuthenticated();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        int importedCount = this.customerService.importEmails(companyId, dto.getEmails(), CustomerService.Type.CUSTOMER);
        Map<String, Integer> results = new HashMap<>(1);
        results.put("count", importedCount);

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/company/{companyId}/integrations/activate"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> activateIntegration(@Valid @RequestBody ActivateIntegrationDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        AjaxUtils.validateAuthenticated();
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.integrationService.activate(companyId, IntegrationType.valueOf(dto.getIntegration()));
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/company/{companyId}/integrations/deactivate"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> deactivateIntegration(@Valid @RequestBody DeactivateIntegrationDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        AjaxUtils.validateAuthenticated();
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.integrationService.deactivate(companyId, IntegrationType.valueOf(dto.getIntegration()));
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }
}