package dk.better.ajax.controller;

import dk.better.account.entity.Account;
import dk.better.account.util.AccountUtils;
import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.AddMessageToThreadDTO;
import dk.better.ajax.dto.CreateNewThreadDTO;
import dk.better.ajax.dto.MessageResourceListDTO;
import dk.better.ajax.dto.ThreadResourceListDTO;
import dk.better.ajax.util.AjaxUtils;
import dk.better.api.converter.MessageConverter;
import dk.better.api.converter.ThreadConverter;
import dk.better.api.resource.MessageResource;
import dk.better.api.resource.ThreadResource;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.application.util.AuthUtils;
import dk.better.thread.entity.Message;
import dk.better.thread.entity.Thread;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * @author Bo Andersen
 */
@AjaxController("ajaxThreadController")
@RequestMapping("/ajax/thread")
public class ThreadController {
    @Autowired
    private ThreadService threadService;


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Integer>> add(@Valid @RequestBody CreateNewThreadDTO dto, BindingResult bindingResult, HttpServletRequest request) throws BindException {
        // improve: check "throttle"; spam prevention, i.e. has not sent message to same company within X mins (config)

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Map<String, String> nameParts = AccountUtils.getNameParts(dto.getName());
        dk.better.thread.entity.Thread saved = this.threadService.sendMessageInNewThreadFromAccountToCompany(dto.getSubject(), dto.getMessage(), dto.getCompanyId(), request.getRemoteAddr(), dto.getEmail(), nameParts.get(AccountUtils.FIRST_NAME), nameParts.get(AccountUtils.MIDDLE_NAME), nameParts.get(AccountUtils.LAST_NAME));
        Map<String, Integer> map = new HashMap<>(1);
        map.put("id", saved.getId());

        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/fetch", method = RequestMethod.GET)
    public ResponseEntity<ThreadResourceListDTO> getThreads(@RequestParam(value = "page", required = true) int page, @RequestParam(value = "companyId", required = false) Integer companyId, @RequestParam(value = "token", required = false) String accessToken, @RequestParam(value = "checksum", required = false) Long checksum) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "lastActivity"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Page<Thread> threads;

        if (companyId != null) {
            threads = this.threadService.getThreadsForCompanyInbox(companyId, checksum, accessToken, pageable);
        } else {
            AjaxUtils.validateAuthenticated();
            threads = this.threadService.getThreadsForUserInbox(AuthUtils.getPrincipal().getId(), pageable);
        }

        Collection<Thread> threadCollection = threads.getContent();
        Collection<ThreadResource> resources = new ArrayList<>(threadCollection.size());
        Converter<Thread, ThreadResource> converter = new ThreadConverter();

        for (Thread thread : threadCollection) {
            resources.add(converter.convert(thread));
        }

        return new ResponseEntity<>(new ThreadResourceListDTO(threads.hasNext(), resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/{threadId}/message/add", method = RequestMethod.POST)
    public ResponseEntity<MessageResource> addNewMessageToThread(@Valid @RequestBody AddMessageToThreadDTO dto, BindingResult bindingResult, @PathVariable("threadId") int threadId, HttpServletRequest request) throws BindException {
        AjaxUtils.validateAuthenticated();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();
        Message message = this.threadService.addMessageToThread(threadId, dto.getMessageText(), account, request.getRemoteAddr(), dto.isCompany());
        Converter<Message, MessageResource> converter = new MessageConverter();

        return new ResponseEntity<>(converter.convert(message), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{threadId}/messages", method = RequestMethod.GET)
    public ResponseEntity<MessageResourceListDTO> getMessages(@PathVariable("threadId") int threadId, @RequestParam(value = "token", required = false) String accessToken, @RequestParam(value = "checksum", required = false) Long checksum, @RequestParam(value = "page", required = true) int page) {
        if (page < 1) {
            throw new InvalidArgumentException("Page parameter must not be less than 1");
        }

        if (accessToken != null && checksum != null) {
            if (!this.threadService.hasAccessToThread(threadId, checksum, accessToken)) {
                throw new AccessDeniedException("User is not allowed to access thread!");
            }
        } else {
            AjaxUtils.validateAuthenticated();

            if (!this.threadService.hasAccessToThread(threadId, AuthUtils.getPrincipal().getId())) {
                throw new AccessDeniedException("User is not allowed to access thread!");
            }
        }

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Slice<Message> slice;

        if (AuthUtils.isAuthenticated() && (accessToken == null && checksum == null)) {
            slice = this.threadService.getMessages(threadId, pageable);
        } else {
            slice = this.threadService.getMessages(threadId, accessToken, checksum, pageable);
        }

        List<MessageResource> resources = new ArrayList<>(slice.getNumberOfElements());
        Converter<Message, MessageResource> converter = new MessageConverter();

        for (Message message : slice.getContent()) {
            resources.add(converter.convert(message));
        }

        return new ResponseEntity<>(new MessageResourceListDTO(slice.hasNext(), resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/{threadId}/mark/read", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> markThreadAsRead(@PathVariable("threadId") int threadId, @RequestParam(value = "companyId", required = false) Integer companyId, @RequestParam(value = "token", required = false) String accessToken, @RequestParam(value = "checksum", required = false) Long checksum) {
        boolean isAuthenticated = AuthUtils.isAuthenticated();

        if (companyId == null) {
            // Mark as read for regular user
            this.threadService.markAsReadForRegularUser(threadId, accessToken, checksum);
        } else {
            boolean isParticipatingOnBehalfOfCompany;

            if (isAuthenticated) {
                isParticipatingOnBehalfOfCompany = this.threadService.isParticipatingInThreadOnBehalfOfCompany(threadId, AuthUtils.getPrincipal(), companyId);
            } else {
                if (checksum == null || accessToken == null) {
                    throw new AccessDeniedException("Must be either logged in or provide access token");
                }

                isParticipatingOnBehalfOfCompany = this.threadService.isParticipatingInThreadOnBehalfOfCompany(threadId, companyId, accessToken, checksum);
            }

            // Mark as read for company representative or the company in general
            if (isParticipatingOnBehalfOfCompany && isAuthenticated) {
                this.threadService.markAsReadForCompanyParticipant(threadId, companyId, AuthUtils.getPrincipal());
            } else if (isParticipatingOnBehalfOfCompany) { // Participating on behalf of company, but not logged in
                this.threadService.markAsReadForCompanyParticipant(threadId, companyId, accessToken, checksum);
            } else {
                this.threadService.markAsReadForCompany(threadId, companyId, accessToken, checksum);
            }
        }

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }
}