package dk.better.ajax.controller;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.CreateNewUserDTO;
import dk.better.ajax.dto.MergeAccountsDTO;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.application.util.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author Bo Andersen
 */
@AjaxController("ajaxAccountController")
@RequestMapping("/ajax/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody CreateNewUserDTO dto, BindingResult bindingResult, HttpServletRequest request) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        if (!dto.getPassword().equals(dto.getRepeatPassword())) {
            bindingResult.rejectValue("password", null, "Kodeordene matcher ikke"); // improve: translate
            throw new BindException(bindingResult);
        }

        Account account = this.accountService.create(dto.getName(), dto.getEmail(), dto.getPassword());

        Authentication auth = new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(auth);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/merge", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> merge(@Valid @RequestBody MergeAccountsDTO dto, BindingResult bindingResult) throws BindException {
        if (AuthUtils.isAuthenticated()) {
            throw new AccessDeniedException("Already logged in");
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        if (!dto.getPassword().equals(dto.getRepeatPassword())) {
            bindingResult.rejectValue("password", null, "Kodeordene matcher ikke"); // improve: translate
            throw new BindException(bindingResult);
        }

        Account account = this.accountService.merge(dto.getName(), dto.getPassword(), dto.getAccessToken(), dto.getChecksum());

        // Login
        Authentication auth = new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }
}