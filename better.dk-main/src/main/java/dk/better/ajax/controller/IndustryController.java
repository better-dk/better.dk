package dk.better.ajax.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.better.ajax.annotation.AjaxController;
import dk.better.company.entity.Industry;
import dk.better.company.service.IndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;

/**
 * @author Bo Andersen
 */
@AjaxController("ajaxIndustryController")
@RequestMapping("/ajax/industry")
public class IndustryController {
    @Autowired
    private IndustryService industryService;

    @Autowired
    private ObjectMapper objectMapper;


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<Collection<Industry>> searchIndustries(@RequestParam(value = "query", required = true) String query, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.ASC, "name"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Page<Industry> results = this.industryService.search(query, pageable);

        return new ResponseEntity<>(results.getContent(), HttpStatus.OK);
    }
}