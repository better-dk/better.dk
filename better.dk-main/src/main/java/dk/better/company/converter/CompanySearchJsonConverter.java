package dk.better.company.converter;

import dk.better.company.entity.Company;
import dk.better.company.entity.CompanyService;
import dk.better.company.entity.Industry;
import dk.better.company.model.CompanySearchJson;
import dk.better.company.service.IndustryService;
import org.springframework.core.convert.converter.Converter;

import java.util.*;

/**
 * @author Bo Andersen
 */
public class CompanySearchJsonConverter implements Converter<Company, CompanySearchJson> {
    @Override
    public CompanySearchJson convert(Company source) {
        CompanySearchJson target = new CompanySearchJson();
        target.setId(source.getId());

        MapOperations mo = (Map<String, Object> map, String key, String value) -> {
            if (value != null && !value.isEmpty()) {
                map.put(key, value.trim());
            }
        };

        Map<String, Object> fields = new HashMap<>();
        mo.addIfNotEmpty(fields, "company_name", source.getName().toLowerCase());
        mo.addIfNotEmpty(fields, "teaser", source.getTeaser());
        mo.addIfNotEmpty(fields, "ci_number", String.valueOf(source.getCiNumber()));
        mo.addIfNotEmpty(fields, "address", source.getStreetName().toLowerCase() + " " + source.getStreetNumber().toLowerCase());
        mo.addIfNotEmpty(fields, "postal_code", String.valueOf(source.getPostalCode()));
        mo.addIfNotEmpty(fields, "city", source.getCity().toLowerCase());
        mo.addIfNotEmpty(fields, "phone_number", source.getPhoneNumber());
        mo.addIfNotEmpty(fields, "postal_district", source.getPostalDistrict().toLowerCase());
        mo.addIfNotEmpty(fields, "municipal", source.getMunicipal().toLowerCase());
        fields.put("is_claimed", (source.getAccountCompanies().isEmpty() ? 0 : 1));
        fields.put("likes", source.getNumberOfLikes());
        fields.put("reviews", source.getNumberOfReviews());

        Double rating = source.getRating();
        Set<Industry> industries = source.getIndustries();
        Set<CompanyService> companyServices = source.getCompanyServices();

        if (rating != null) {
            fields.put("rating", rating);
        }

        // Industries
        if (!industries.isEmpty()) {
            List<String> industriesList = new ArrayList<>(industries.size());

            for (Industry industry : industries) {
                int industryId = industry.getId();

                if (industryId != IndustryService.EMPTY_INDUSTRY_ID
                        && industryId != IndustryService.UNDISCLOSED_INDUSTRY_ID
                        && industryId != IndustryService.UNKNOWN_INDUSTRY_ID) {
                    industriesList.add(industry.getName().toLowerCase());
                }
            }

            fields.put("industries", industriesList);
        }

        // Services
        if (!companyServices.isEmpty()) {
            List<String> servicesList = new ArrayList<>(companyServices.size());

            for (CompanyService cs : companyServices) {
                servicesList.add(cs.getService().getName().toLowerCase());
            }

            fields.put("services", servicesList);
        }

        target.setFields(fields);
        return target;
    }

    interface MapOperations {
        void addIfNotEmpty(Map<String, Object> map, String key, String value);
    }
}