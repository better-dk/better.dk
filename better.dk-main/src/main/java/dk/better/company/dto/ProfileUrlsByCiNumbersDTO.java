package dk.better.company.dto;

/**
 * @author Bo Andersen
 */
public class ProfileUrlsByCiNumbersDTO {
    private Integer ciNumber;
    private String profileUrl;


    public Integer getCiNumber() {
        return ciNumber;
    }

    public void setCiNumber(Integer ciNumber) {
        this.ciNumber = ciNumber;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
}