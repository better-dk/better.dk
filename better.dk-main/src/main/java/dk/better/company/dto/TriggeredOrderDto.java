package dk.better.company.dto;

import dk.better.company.entity.TriggeredOrders;

/**
 * @author Liubov Zavorotna
 */
public class TriggeredOrderDto {
    private TriggeredOrders order;
    private Long sendDelay;
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    private String companyEmail;

    public TriggeredOrders getOrder() {
        return order;
    }

    public void setOrder(TriggeredOrders order) {
        this.order = order;
    }

    public Long getSendDelay() {
        return sendDelay;
    }

    public void setSendDelay(Long sendDelay) {
        this.sendDelay = sendDelay;
    }

    public TriggeredOrderDto(TriggeredOrders order, Long sendDelay, String companyName, String companyEmail) {
        this.order = order;
        this.sendDelay = sendDelay;
        this.companyName = companyName;
        this.companyEmail = companyEmail;
    }
}
