package dk.better.company.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Page;
import dk.better.company.entity.Subdomain;
import dk.better.company.service.CompanyService;
import dk.better.company.service.PageService;
import dk.better.company.service.SubdomainService;
import dk.better.company.util.CompanyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trimou.Mustache;
import org.trimou.engine.MustacheEngine;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@Controller

public class PageController {
    @Autowired
    private PageService pageService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private SubdomainService subdomainService;

    @Autowired
    private MustacheEngine mustacheEngine;

    @Value("${google.api.key}")
    private String googleApiKey;

    @Value("${facebook.app.id}")
    private String facebookAppId;


    @RequestMapping("/p/{companyIdOrSlug}/page/add")
    public ModelAndView add(@PathVariable("companyIdOrSlug") String companyIdOrSlug, Model model, HttpServletRequest request) throws ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        // todo: validate access

        if (company.getNumberOfReviews() > 0) {
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

        model.addAttribute("title", "Tilføj side");
        model.addAttribute("company", company);
        model.addAttribute("googleApiKey", this.googleApiKey);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("currentPageUrl", company.getProfileUrl());
        model.addAttribute("rootPages", this.pageService.findRootPagesForCompany(company.getId()));

        return new ModelAndView("add-page-to-company");
    }

    @RequestMapping("/p/{companyIdOrSlug}/{pageSlug}")
    public ModelAndView viewRootPage(@PathVariable("companyIdOrSlug") String companyIdOrSlug, @PathVariable("pageSlug") String pageSlug, Model model, HttpServletRequest request) throws ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        if (company.getNumberOfReviews() > 0) {
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

        Page page = this.pageService.findRootPageBySlug(pageSlug, company.getId());

        if (page == null) {
            throw new ResourceNotFoundException(String.format("Page not found (slug: \"%s\", company ID: %d)!", pageSlug, company.getId()));
        }

        // todo: og tags?

        if (page.getData() != null) {
            JsonElement data = new JsonParser().parse(page.getData());
            String template = data.getAsJsonObject().get("template").getAsString();
            Mustache mustache = this.mustacheEngine.getMustache(template);
            String output = mustache.render(data);
            model.addAttribute("content", output);

            // todo: make this dynamic
            Map<String, String> metas = new HashMap<>(1);

            if (template.equals("opening-hours")) {
                metas.put("description", String.format("Se åbningstider for %s her. Vores adresse er %s %s, %d %s.", company.getName(), company.getStreetName(), company.getStreetNumber(), company.getPostalCode(), company.getCity()));
            } else if (template.equals("restaurant-menu")) {
                metas.put("description", String.format("Se menukort for %s i %s her. Bestil via telefon eller brug online bestilling.", company.getName(), company.getCity()));
            }

            model.addAttribute("metas", metas);
        }

        if (company.getBookingUrl() != null || !company.getBookingUrl().isEmpty()) {
            model.addAttribute("title", page.getName() + " - " + company.getName() + " - " + company.getCity());
        } else {
            model.addAttribute("title", page.getName());
        }
        model.addAttribute("page", page);
        model.addAttribute("company", company);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("currentPageUrl", company.getProfileUrl() + "/" + pageSlug);

        return new ModelAndView("view-root-page");
    }

    @RequestMapping("/p/{companyIdOrSlug}/{parentSlug}/{childSlug}")
    public ModelAndView viewChildPage(@PathVariable("companyIdOrSlug") String companyIdOrSlug, @PathVariable("parentSlug") String parentSlug, @PathVariable("childSlug") String childSlug, Model model, HttpServletRequest request) throws ResourceNotFoundException {

        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        if (company.getNumberOfReviews() > 0) {
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

        Page page = this.pageService.findChildPageBySlug(parentSlug, childSlug, company.getId());

        if (page == null) {
            throw new ResourceNotFoundException(String.format("Child page not found (child slug: \"%s\", parent slug: \"%s\",company ID: %d)!", childSlug, parentSlug, company.getId()));
        }

        // todo: og tags?

        if (page.getData() != null) {
            JsonElement data = new JsonParser().parse(page.getData());
            String template = data.getAsJsonObject().get("template").getAsString();
            Mustache mustache = this.mustacheEngine.getMustache(template);
            String output = mustache.render(data);
            model.addAttribute("content", output);

            // todo: make this dynamic
            Map<String, String> metas = new HashMap<>(1);

            if (template.equals("opening-hours")) {
                metas.put("description", String.format("Se åbningstider for %s her. Vores adresse er %s %s, %d %s.", company.getName(), company.getStreetName(), company.getStreetNumber(), company.getPostalCode(), company.getCity()));
            } else if (template.equals("restaurant-menu")) {
                metas.put("description", String.format("Se menukort for %s i %s her. Bestil via telefon eller brug online bestilling.", company.getName(), company.getCity()));
            }

            model.addAttribute("metas", metas);
        }
        if (company.getBookingUrl() != null && !company.getBookingUrl().isEmpty()) {
            model.addAttribute("title", page.getName() + " - " + company.getName() + " - " + company.getCity());
        } else {
            model.addAttribute("title", page.getName());
        }
        model.addAttribute("page", page);
        model.addAttribute("company", company);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("currentPageUrl", company.getProfileUrl() + "/" + parentSlug + "/" + childSlug);

        return new ModelAndView("view-root-page");
    }
}