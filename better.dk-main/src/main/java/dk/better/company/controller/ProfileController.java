package dk.better.company.controller;

import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.EventType;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.ExternalReview;
import dk.better.company.entity.Review;
import dk.better.company.entity.Subdomain;
import dk.better.company.form.SiteReviewForm;
import dk.better.company.service.*;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.ProductType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Bo Andersen
 */
@Controller
public class ProfileController {
    private static final int PROFILE_REVIEW_COUNT = 5;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private SubdomainService subdomainService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private ReviewSiteService reviewSiteService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private ExternalReviewService externalReviewService;

    @Value("${google.api.key}")
    private String googleApiKey;

    @Value("${facebook.app.id}")
    private String facebookAppId;

    @Value("${static.resource.baseurl}")
    private String staticResourcesBaseUrl;

    @Value("${application.domain.name}")
    private String domainName;


    @RequestMapping("/p/{companyIdOrSlug}")
    public ModelAndView view(@PathVariable String companyIdOrSlug, Model model, HttpServletRequest request) throws ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
//                 We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        if (company.getNumberOfReviews() > 0) {
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

        // Meta entries
        Map<String, String> metaProperties = new HashMap<>(8);
        metaProperties.put("fb:app_id", this.facebookAppId);
        metaProperties.put("og:title", String.format("%s - Hjælp os, giv os et like på vores site.", company.getName()));
        metaProperties.put("og:type", "website");
        metaProperties.put("og:url", company.getProfileUrl());
        metaProperties.put("og:description", "Kontakt & Bedøm os her - Hjælp din venner og bekendte - Vi sætter stor pris på din mening");
        metaProperties.put("og:site_name", "Better.dk - Find, Kontakt & Bedøm Lokale Virksomheder");
        metaProperties.put("og:locale", "da_DK"); // improve: define in config

        if (company.getBannerName() != null && !company.getBannerName().isEmpty()) {
            metaProperties.put("og:image", this.staticResourcesBaseUrl + "/" + company.getBannerName());
        }

        Map<String, String> metas = new HashMap<>(1);

        inspectionOnBookingUrl(model, company, metas);
        List<String> categories = companyService.findCategories(company.getId());

        model.addAttribute("metas", metas);
        model.addAttribute("metaProperties", metaProperties);

        model.addAttribute("company", company);
        model.addAttribute("categories", categories);
        model.addAttribute("googleApiKey", this.googleApiKey);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("showReviewSources", false);
        model.addAttribute("currentPageUrl", company.getProfileUrl());

        return new ModelAndView("view-company-profile");
    }

    @RequestMapping("/p/{companyIdOrSlug}/bedoemmelser")
    public ModelAndView reviews(@PathVariable String companyIdOrSlug, Model model, HttpServletRequest request) throws
            ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        if (company.getNumberOfReviews() > 0) {
            Sort sort = new Sort(Sort.Direction.DESC, "created");
            Slice<Review> slice = this.reviewService.findByCompanyId(company.getId(), new PageRequest(0, PROFILE_REVIEW_COUNT, sort));
            List<Review> reviews = null;

            if (slice.hasContent()) {
                reviews = slice.getContent();
            }

            model.addAttribute("reviews", reviews);
            model.addAttribute("hasMoreReviews", slice.hasNext());
            model.addAttribute("ratingDistribution", this.reviewService.getRatingDistribution(company.getId()));
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

//        List<ExternalReview> reviews = externalReviewService.findByCompanyId(company.getId());
        List<ExternalReview> reviewsFromDB = externalReviewService.findByCompanyId(company.getId());

        List<ExternalReview> reviews = reviewsFromDB
                .stream()
                .map((reviewObj) -> {
                    reviewObj.setReview(
                            reviewObj.getReview().contains("(Original)")
                                    ? StringUtils.substringAfter(reviewObj.getReview(), "(Original)")
                                    : reviewObj.getReview());
                    return reviewObj;
                })
                .collect(Collectors.toList());
        model.addAttribute("externalReviews", reviews);

        Map<String, String> metas = new HashMap<>(1);

        if (company.getBookingUrl() == null || company.getBookingUrl().isEmpty()) {
            metas.put("description", String.format("Se bedømmelser for %s her. Vores adresse er %s %s, %d %s. Se hvad dine venner siger.", company.getName(), company.getStreetName(), company.getStreetNumber(), company.getPostalCode(), company.getCity()));
        } else {
            metas.put("description", String.format("Se bedømmelser for %s i %s her, samt menukort. Bestil via telefon eller brug online bestilling.", company.getName(), company.getCity()));
        }

        model.addAttribute("title", "Bedømmelser af " + company.getName());
        model.addAttribute("metas", metas);
        model.addAttribute("company", company);
        model.addAttribute("googleApiKey", this.googleApiKey);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("currentPageUrl", company.getProfileUrl() + "/bedoemmelser"); // improve: make dynamic

        return new ModelAndView("view-profile-reviews");
    }

    @RequestMapping("/p/{companyIdOrSlug}/bedoemmelser/{reviewUuid}")
    public ModelAndView viewSingleReview(@PathVariable("companyIdOrSlug") String
                                                 companyIdOrSlug, @PathVariable("reviewUuid") String uuid, Model model, HttpServletRequest request) throws
            ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        Review review = this.reviewService.findByUuid(company.getId(), uuid);

        if (review == null) {
            throw new ResourceNotFoundException(String.format("Review not found (UUID: %s)!", uuid));
        }

        Map<String, String> metaProperties = new HashMap<>(2);
        metaProperties.put("fb:app_id", this.facebookAppId);
        metaProperties.put("og:title", ((review.getTitle() != null) ? review.getTitle() : String.format("Bedømmelse af %s", review.getCompany().getName())));

        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", review.getTitle());

        model.addAttribute("review", review);
        model.addAttribute("ratingDistribution", this.reviewService.getRatingDistribution(company.getId()));
        model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        model.addAttribute("title", "Bedømmelse af " + company.getName());
        model.addAttribute("metas", metas);
        model.addAttribute("metaProperties", metaProperties);
        model.addAttribute("company", company);
        model.addAttribute("googleApiKey", this.googleApiKey);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("currentPageUrl", company.getProfileUrl() + "/bedoemmelser/" + uuid); // improve: make dynamic

        return new ModelAndView("view-profile-single-review");
    }

    @RequestMapping("/p/{companyIdOrSlug}/kontakt")
    public ModelAndView contact(@PathVariable String companyIdOrSlug, Model model, HttpServletRequest request) throws
            ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        if (company.getNumberOfReviews() > 0) {
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

        Map<String, String> metas = new HashMap<>(1);

        if (company.getBookingUrl() == null || company.getBookingUrl().isEmpty()) {
            model.addAttribute("title", "Kontakt " + company.getName() + " - " + company.getCity());
            metas.put("description", String.format("Kontakt & bedøm %s her. Vores adresse er %s %s, %d %s. Se hvad dine venner siger.", company.getName(), company.getStreetName(), company.getStreetNumber(), company.getPostalCode(), company.getCity()));
        } else {
            model.addAttribute("title", "Kontaktinfo - " + company.getName() + " - " + company.getCity());
            metas.put("description", String.format("Se menukort for %s i %s her. Bestil via telefon eller brug online bestilling. Se åbningstider og bedømmelser her.", company.getName(), company.getCity()));
        }

        model.addAttribute("metas", metas);
        model.addAttribute("company", company);
        model.addAttribute("googleApiKey", this.googleApiKey);
        model.addAttribute("facebookAppId", this.facebookAppId);
        model.addAttribute("currentPageUrl", company.getProfileUrl() + "/kontakt"); // improve: make dynamic

        return new ModelAndView("view-profile-contact");
    }

    @RequestMapping("/p/{companyIdOrSlug}/bedoem")
    public ModelAndView reviewLandingPage(@PathVariable String companyIdOrSlug, Model model, HttpServletRequest
            request, RedirectAttributes redirectAttributes) throws ResourceNotFoundException {

        redirectAttributes.addAttribute("showReviewSources",true);

        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);
        Company company;
        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
//                 We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", companyIdOrSlug));
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request));
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        if (company.getNumberOfReviews() > 0) {
            model.addAttribute("formattedRating", CompanyUtils.formatRating(company.getRating()));
        }

        if (subdomains.size() == 0) {
            if (!CompanyUtils.isCompanyId(companyIdOrSlug)) {
                throw new ResourceNotFoundException(String.format("Company not found! Subdomain: %s", new Object[]{companyIdOrSlug}));
            }

            company = this.companyService.getFullWithMeta(Integer.parseInt(companyIdOrSlug));
        } else {
            Subdomain redirectUrl = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(redirectUrl);
            String isInternalRedirect = redirectUrl.getName().trim();
            if (!isInternalRedirect.equals(companyIdOrSlug)) {
                RedirectView metas1 = new RedirectView(request.getScheme() + "://" + isInternalRedirect + "." + GeneralUtils.getRootDomain(request));
                metas1.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(metas1);
            }

            company = this.companyService.getFullWithMeta(redirectUrl.getCompany().getId());
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", new Object[]{companyIdOrSlug}));
        } else {
            String redirectUrl1 = null;
            boolean isInternalRedirect1 = false;
            if (this.subscriptionService.isSubscribed(company.getId(), ProductType.TRIAL) || this.subscriptionService.isSubscribed(company.getId(), ProductType.PAID)) {
                redirectUrl1 = this.reviewService.getReviewRedirectUrl(company.getMeta(), request.getHeader("User-Agent"));
            }

            if (redirectUrl1 == null) {
                redirectUrl1 = company.getProfileUrl() + "/bedoemmelser#bedoem";
                isInternalRedirect1 = true;
            }
            Map<String, String> metaProperties = new HashMap<>(8);
            Map<String, String> metas = new HashMap<>(1);
            metaProperties.put("fb:app_id", this.facebookAppId);
            metaProperties.put("og:title", String.format("%s - Hjælp os, giv os et like på vores site.", company.getName()));
            metaProperties.put("og:type", "website");
            metaProperties.put("og:url", company.getProfileUrl());
            metaProperties.put("og:description", "Kontakt & Bedøm os her - Hjælp din venner og bekendte - Vi sætter stor pris på din mening");
            metaProperties.put("og:site_name", "Better.dk - Find, Kontakt & Bedøm Lokale Virksomheder");
            metaProperties.put("og:locale", "da_DK"); // improve: define in config
            metas.put("robots", "noindex");

            if (company.getBannerName() != null && !company.getBannerName().isEmpty()) {
                metaProperties.put("og:image", this.staticResourcesBaseUrl + "/" + company.getBannerName());
            }

            inspectionOnBookingUrl(model, company, metas);
            List<String> categories = companyService.findCategories(company.getId());
            List<SiteReviewForm> siteLinks = reviewSiteService.getReviewSitesForCompany(company);

            model.addAttribute("metaProperties", metaProperties);
            model.addAttribute("categories", categories);
            model.addAttribute("googleApiKey", this.googleApiKey);
            model.addAttribute("facebookAppId", this.facebookAppId);
            model.addAttribute("currentPageUrl", company.getProfileUrl());
            model.addAttribute("metas", metas);
            model.addAttribute("title", String.format("Bedøm %s", company.getName()));
            model.addAttribute("siteLinks", siteLinks);
            model.addAttribute("showReviewSources", true);
            model.addAttribute("company", company);
            model.addAttribute("redirectUrl", redirectUrl1);
            model.addAttribute("isInternalRedirect", isInternalRedirect1);
            model.addAttribute("eventName", EventType.REVIEW_REDIRECT.getValue());

            return new ModelAndView("view-company-profile");
        }
    }

    private void inspectionOnBookingUrl(Model model, Company company, Map<String, String> metas) {
        if (company.getBookingUrl() == null || company.getBookingUrl().isEmpty()) {
            model.addAttribute("title", company.getName() + " - " + company.getCity());
            metas.put("description", String.format("Kontakt & bedøm %s her. Vores adresse er %s %s, %d %s. Se hvad dine venner siger.", company.getName(), company.getStreetName(), company.getStreetNumber(), company.getPostalCode(), company.getCity()));
        } else {
            model.addAttribute("title", company.getName() + " " + company.getCity() + " - 10% rabat ved Online Bestilling!");
            metas.put("description", String.format("Se menukort for %s i %s her. Bestil via telefon eller brug online bestilling. Se åbningstider og bedømmelser her.", company.getName(), company.getCity()));
        }
    }

    @RequestMapping("/p/{companyIdOrSlug}/ydelse/{serviceSlug}")
    public ModelAndView viewService(@PathVariable String companyIdOrSlug, @PathVariable String serviceSlug, Model model, HttpServletRequest request) throws ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException("Company not found!");
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request) + "/ydelse/" + serviceSlug);
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        dk.better.company.entity.CompanyService companyService = this.companyService.getService(company.getId(), serviceSlug);

        // todo: handle differently, because the service may have been removed from a profile, so perhaps just redirect to the profile
        if (companyService == null) {
            throw new ResourceNotFoundException(String.format("Service %s not found for company %s", serviceSlug, companyIdOrSlug));
        }

        // Moved permanently
        RedirectView rv = new RedirectView(company.getProfileUrl());
        rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        return new ModelAndView(rv);
    }

    @RequestMapping("/p/{companyIdOrSlug}/ydelser")
    public ModelAndView viewServicesIndex(@PathVariable String companyIdOrSlug, Model model, HttpServletRequest request) throws ResourceNotFoundException {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException("Company not found!");
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request) + "/ydelser");
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        // Moved permanently
        RedirectView rv = new RedirectView(company.getProfileUrl());
        rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        return new ModelAndView(rv);
    }

    @RequestMapping("/p/{companyIdOrSlug}/om")
    public ModelAndView viewProfileDescription(@PathVariable String companyIdOrSlug, HttpServletRequest request) {
        Company company;
        List<Subdomain> subdomains = this.subdomainService.getSubdomains(companyIdOrSlug);

        if (subdomains.size() == 0) {
            if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(Integer.parseInt(companyIdOrSlug));
            } else {
                // We have a subdomain, and the subdomain does not exist
                throw new ResourceNotFoundException("Company not found!");
            }
        } else {
            Subdomain activeSubdomain = CompanyUtils.getActiveSubdomain(subdomains);
            Assert.notNull(activeSubdomain);
            String name = activeSubdomain.getName().trim();

            // If the entered subdomain is the active one, then fetch the company
            if (name.equals(companyIdOrSlug)) {
                company = this.companyService.getForProfileViewById(activeSubdomain.getCompany().getId());
            } else {
                // The active subdomain differs from the entered one, so do a redirect to the active subdomain
                RedirectView rv = new RedirectView(request.getScheme() + "://" + name + "." + GeneralUtils.getRootDomain(request) + "/om");
                rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                return new ModelAndView(rv);
            }
        }

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID or slug %s", companyIdOrSlug));
        }

        // Moved permanently
        RedirectView rv = new RedirectView(company.getProfileUrl());
        rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        return new ModelAndView(rv);
    }

    @RequestMapping("/search")
    public ModelAndView searchOld(@RequestParam(value = "q1", required = false) String
                                          q1, @RequestParam(value = "q2", required = false) String q2) throws Exception {
        String what = q1;
        String where = q2;

        boolean isWhatEmpty = (what == null || what.isEmpty());
        boolean isWhereEmpty = (where == null || where.isEmpty());
        RedirectView rv = new RedirectView();
        rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);

        if (!isWhatEmpty && !isWhereEmpty) {
            rv.setUrl("/s/" + URLEncoder.encode(what, "UTF-8") + "/" + URLEncoder.encode(where, "UTF-8"));
        } else if (isWhereEmpty && !isWhatEmpty) {
            rv.setUrl("/s/" + URLEncoder.encode(what, "UTF-8"));
        } else if (isWhatEmpty && !isWhereEmpty) {
            rv.setUrl("/sl/" + URLEncoder.encode(where, "UTF-8"));
        } else {
            // todo: empty search page (/s)
        }

        return new ModelAndView(rv);
    }

    @RequestMapping("/sl/{where}")
    public String searchWhere(@PathVariable("where") String where,
                              @RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        if (page < 1) {
            throw new InvalidArgumentException("Page must be at least 1");
        }

        String title = where.substring(0, 1).toUpperCase() + where.substring(1);
        String description = "Find, Kontakt & Bedøm lokale virksomheder på Better.dk";

        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", title + " - " + description);

        model.addAttribute("metas", metas);
        model.addAttribute("title", title + " - " + description);
        model.addAttribute("where", where);
        model.addAttribute("searchResults", this.companyService.searchByLocation(where, page));

        return "public-profile-search";
    }

    @RequestMapping("/s/{what}")
    public String searchWhat(@PathVariable("what") String what,
                             @RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        if (page < 1) {
            throw new InvalidArgumentException("Page must be at least 1");
        }

        String title = what.substring(0, 1).toUpperCase() + what.substring(1);
        String description = "Find, Kontakt & Bedøm lokale virksomheder på Better.dk";

        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", title + " - " + description);

        model.addAttribute("metas", metas);
        model.addAttribute("title", title + " - " + description);
        model.addAttribute("what", what);
        model.addAttribute("searchResults", this.companyService.search(what, page));

        return "public-profile-search";
    }

    @RequestMapping("/s/{what}/{where}")
    public String searchBoth(@PathVariable("what") String what, @PathVariable("where") String where,
                             @RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        if (page < 1) {
            throw new InvalidArgumentException("Page must be at least 1");
        }

        // "Plumber Aalborg" instead of "plumber aalborg"
        String titleWhat = what.substring(0, 1).toUpperCase() + what.substring(1);
        String titleWhere = where.substring(0, 1).toUpperCase() + where.substring(1);
        String title = titleWhat + " " + titleWhere;

        String metaDescription = title + " - Find, Kontakt & Bedøm lokale virksomheder her. Se hvad dine venner siger.";
        title += " - Find, Kontakt & Bedøm lokale virksomheder på Better.dk";
        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", metaDescription);

        model.addAttribute("metas", metas);
        model.addAttribute("title", title);
        model.addAttribute("searchResults", this.companyService.search(what, where, page));
        model.addAttribute("what", what);
        model.addAttribute("where", where);

        return "public-profile-search";
    }
}