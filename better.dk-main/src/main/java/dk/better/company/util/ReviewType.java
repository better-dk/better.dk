package dk.better.company.util;

/**
 * @author Bo Andersen
 */
public enum ReviewType {
    POSITIVE,
    NEGATIVE
}
