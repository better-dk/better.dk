package dk.better.company.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author vitalii.
 */
public enum IntegrationType {
    BILLY(1000),
    HAIRTOOLS(1001),
    ADMIND(1002),
    ONLINEBOOQ(1003),
    ECONOMIC(1004),
    KLIKBOOK(1005),
    GECKO(1006),
    NEMTILMELD(1007),
    SUPERSAAS(1008),
    DINNERBOOKING(1009),
    CBIT(1010);

    private int value;
    private static Map<Integer, IntegrationType> map;

    IntegrationType(int value) {
        this.value = value;
    }

    public static IntegrationType valueOf(int integrationType) {
        return map.get(integrationType);
    }

    public int getValue() {
        return this.value;
    }

    static {
        map = new HashMap();
        IntegrationType[] var0 = values();

        for (IntegrationType integrationType : var0) {
            map.put(integrationType.value, integrationType);
        }

    }
}
