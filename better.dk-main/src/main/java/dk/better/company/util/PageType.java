package dk.better.company.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
public enum PageType {
    REGULAR(1),
    MENU_CARD(2),
    OPENING_HOURS(3);

    private int value;
    private static Map<Integer, PageType> map = new HashMap<>();

    private PageType(int value) {
        this.value = value;
    }

    static {
        for (PageType pageType : PageType.values()) {
            map.put(pageType.value, pageType);
        }
    }

    public static PageType valueOf(int pageType) {
        return map.get(pageType);
    }

    public int getValue() {
        return value;
    }
}