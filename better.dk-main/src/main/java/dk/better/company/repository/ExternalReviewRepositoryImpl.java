package dk.better.company.repository;

import dk.better.api.model.widget.WidgetSimple;
import dk.better.application.repository.AbstractEntityManagerRepository;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

/**
 * @author vitalii.
 */
@Component
public class ExternalReviewRepositoryImpl
        extends AbstractEntityManagerRepository {

    public WidgetSimple findOverall(int companyId) {
        Query query = getEntityManager()
                .createNativeQuery("SELECT unionCompanyId as id, avg(unionRating) as rating, count(unionRating) AS count FROM " +
                        "((select e.companyId as unionCompanyId, e.rating as unionRating from external_reviews e where e.companyId=" + companyId +
                        " UNION ALL " +
                        "SELECT r.company_id as unionCompanyId, r.rating AS unionRating FROM review r WHERE r.company_id=" + companyId +
                        ")) AS united GROUP BY unionCompanyId");
        WidgetSimple overall = null;
        List resultList = query.getResultList();
        Iterator it = resultList.iterator();
        while (it.hasNext()) {
            Object[] result = (Object[]) it.next();
            overall = new WidgetSimple((int) result[0], ((BigDecimal) result[1]).doubleValue(), ((BigInteger) result[2]).longValue());
        }
        return overall;

    }
}
