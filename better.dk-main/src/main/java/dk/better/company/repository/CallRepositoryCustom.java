package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;

/**
 * @author Bo Andersen
 */
public interface CallRepositoryCustom extends EntityManagerAware {
}