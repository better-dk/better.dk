package dk.better.company.repository;

import dk.better.api.model.widget.SocialRating;
import dk.better.api.model.widget.WidgetSimple;
import dk.better.company.entity.ExternalReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface ExternalReviewRepository extends JpaRepository<ExternalReview, Long> {
    List<ExternalReview> findByCompanyId(int companyId);

    @Query(value = "select * from external_reviews where rating > 3 and length(review) > 150 and companyId=:companyId", nativeQuery = true)
    List<ExternalReview> findReviewsForWidget(@Param("companyId") int companyId);

    @Query(value = "select new dk.better.company.entity.ExternalReview('better user', e.rating, e.text, 'better.dk', 'some time ago') from dk.better.company.entity.Review e where e.rating > 3 and length(e.text) > 150 and e.company.id=:companyId")
    List<ExternalReview> findBetterReviewsForWidget(@Param("companyId") int companyId);

    @Query(value = "select new dk.better.api.model.widget.WidgetSimple(e.companyId, avg(e.rating), count(e)) from dk.better.company.entity.ExternalReview e where e.companyId=:companyId group by e.companyId")
    WidgetSimple findOverallInfo(@Param("companyId") int companyId);

    @Query(value = "select new dk.better.api.model.widget.SocialRating(e.site, avg(e.rating), count(e)) from dk.better.company.entity.ExternalReview e where e.companyId=:companyId group by e.site")
    List<SocialRating> findSocialInfo(@Param("companyId") int companyId);

    @Query(value = "select new dk.better.api.model.widget.SocialRating('better.dk', avg(e.rating), count(e)) from dk.better.company.entity.Review e where e.company.id=:companyId")
    List<SocialRating> findBetterSocialInfo(@Param("companyId") int companyId);

    @Query(value = "select DISTINCT ON (site) *  from external_reviews WHERE review>='1' ORDER BY site,create_date DESC limit 5", nativeQuery = true)
    List<ExternalReview> findExternalReviewsLimited();
    @Query(value = "SELECT * FROM external_reviews " +
            "            WHERE companyid=:companyId " +
            "                  AND create_date = " +
            "                      (SELECT MAX(create_date) FROM external_reviews " +
            "                      WHERE companyid=:companyId " +
            "                      GROUP BY companyid); ", nativeQuery = true)
    List<ExternalReview> findExternalReviewsLastByCompanyId(@Param("companyId") int companyId);

    @Query(value = "SELECT er FROM ExternalReview er WHERE er.site=:site and er.companyId=:companyId")
    List<ExternalReview> findBySiteAndCompanyId(@Param("companyId") int companyId, @Param("site") String site);

}