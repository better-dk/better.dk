package dk.better.company.repository;

import dk.better.company.entity.Subdomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface SubdomainRepository extends JpaRepository<Subdomain, Integer>, SubdomainRepositoryCustom {
    // improve: find a way to fetch company without executing another query - and without requiring an EAGER fetch type to company
    @Query(nativeQuery = true, value = "SELECT * FROM subdomain AS s INNER JOIN (SELECT name, is_active, company_id FROM subdomain WHERE name = :subdomain) AS t ON (s.company_id = t.company_id) INNER JOIN company AS c ON (c.id = s.company_id) ORDER BY s.id DESC")
    List<Subdomain> findAllCompanySubdomainsByName(@Param("subdomain") String subdomain);

    @Query("SELECT s FROM Subdomain AS s INNER JOIN FETCH s.company AS c WHERE c.id = :companyId ORDER BY s.id DESC")
    List<Subdomain> findAllCompanySubdomainsByCompanyId(@Param("companyId") int companyId);

    @Query("select case when count(s) > 0 then true else false end from Subdomain s where s.name = :subdomain")
    boolean existsByName(@Param("subdomain") String subdomain);

    @Modifying
    @Query("update Subdomain s set s.isActive = false where s.company.id = :companyId")
    void deactivateCompanySubdomains(@Param("companyId") int companyId);

    Subdomain findByName(String subdomain);
}