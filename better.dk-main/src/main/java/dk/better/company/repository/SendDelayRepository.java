package dk.better.company.repository;

import dk.better.company.entity.SendDelay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Liubov Zavorotna
 */
@Repository
public interface SendDelayRepository extends JpaRepository<SendDelay, Long> {
    SendDelay findByCompanyId(int companyId);

    @Query(value = "UPDATE SendDelay sd SET sd.hours=:hours WHERE sd.companyId=:companyId")
    @Modifying(clearAutomatically = true)
    void updateSendDelay(@Param("companyId") int companyId,
                         @Param("hours") int hours);

}
