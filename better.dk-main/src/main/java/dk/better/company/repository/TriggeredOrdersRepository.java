package dk.better.company.repository;

import dk.better.company.entity.TriggeredOrders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Liubov Zavorotna
 */
public interface TriggeredOrdersRepository extends JpaRepository<TriggeredOrders, Long> {

    List<TriggeredOrders> findByCompanyId(@Param("companyId") int companyId);

    Page<TriggeredOrders> findByCompanyId(int companyId, Pageable pageable);

    @Query(value = "INSERT INTO triggered_orders (company_id, " +
            "                              customer_name, customer_email, " +
            "                              transaction_id) " +
            "VALUES (:companyId,:customerName,:customerEmail,:transactionId);", nativeQuery = true)
    @Modifying
    void insertTriggeredOrder(@Param("companyId") int companyId,
                              @Param("customerName") String customerName,
                              @Param("customerEmail") String customerEmail,
                              @Param("transactionId") String transactionId

    );

}
