package dk.better.company.repository;

import dk.better.company.entity.ReviewLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface ReviewLinkRepository extends JpaRepository<ReviewLink, Long> {
    List<ReviewLink> findByCompanyId(int companyId);
}