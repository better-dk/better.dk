package dk.better.company.repository;

import dk.better.company.entity.Company;
import dk.better.company.entity.CompanyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer>, JpaSpecificationExecutor<Company>, CompanyRepositoryCustom {
    @EntityGraph(value = "graph.company.profile.full", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select c from Company c where c.id = :id")
        // This query is necessary to tell Spring Data JPA to not use the method name to generate a query automatically
    Company findFullProfileById(@Param("id") int id);

    List<Company> findByIdIn(Collection<Integer> ids);

    @EntityGraph(value = "graph.company.profile.full", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select c from Company c where c.id >= :firstId and c.id <= :lastId")
    List<Company> findFullProfileWithIdsBetween(@Param("firstId") int firstId, @Param("lastId") int lastId);

    @Query("select c from Company c where c.id in :companyIds")
    List<Company> findProfilesByIds(@Param("companyIds") Collection<Integer> companyIds);

    @Query("select c.id from Company c where c.municipal is null")
    List<Integer> getCompanyIdsWithEmptyMunicipal();

    @EntityGraph(value = "graph.company.profile.full", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select c from Company c where c.id = :id")
    Company findOneByIdForProfileView(@Param("id") int id);

    @EntityGraph(value = "graph.company.cp.view", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select c from Company c where c.id = :id")
    Company findOneByIdForControlPanel(@Param("id") int id);

    @Query("select c from Company c left join fetch c.industries where c.id = :id")
    Company findOneWithIndustries(@Param("id") int id);

    @Query(nativeQuery = true, value = "select hc.name from Company c, hungry as h, hungry_category as hc where c.id = :id and c.id=h.company_id and h.hungry_id = hc.hungry_id")
    List<String> findCategories(@Param("id") int id);

    @EntityGraph(value = "graph.company.admin.company.edit", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select c from Company c where c.id = :id")
        // This query is necessary to tell Spring Data JPA to not use the method name to generate a query automatically
    Company findOneByIdForAdminCompanyEdit(@Param("id") int id);

    @Query("select c from Company c left join fetch c.acknowledgements left join fetch c.industries left join fetch c.subdomains where c.id in :companyIds")
    List<Company> findByCompanyIdsForSearchResults(@Param("companyIds") Set<Integer> companyIds);

    @Query("select cs from CompanyService cs inner join cs.company c inner join fetch cs.service s where c.id = :companyId and s.slug = :serviceSlug")
    CompanyService getService(@Param("companyId") int companyId, @Param("serviceSlug") String serviceSlug);

    @Query(
            value = "select c from Company c where c.created >= :from and c.created <= :to",
            countQuery = "select count(c) from Company c where c.created >= :from and c.created <= :to"
    )
    Page<Company> getCompaniesAddedBetween(Pageable pageable, @Param("from") Date from, @Param("to") Date to);

    @Query(
            value = "select distinct c from Company c inner join c.industries i where c.created >= :from and c.created <= :to and i.id in :industryIds",
            countQuery = "select count(distinct c) from Company c inner join c.industries i where c.created >= :from and c.created <= :to and i.id in :industryIds"
    )
    Page<Company> getCompaniesAddedBetweenInIndustries(Pageable pageable, @Param("from") Date from, @Param("to") Date to, @Param("industryIds") Integer[] industryIds);

    @Query("select distinct c from Company c left join fetch c.subdomains inner join fetch c.industries inner join fetch c.accountCompanies ac where ac.created >= :from and ac.created <= :to")
    List<Company> getCompaniesClaimedBetween(@Param("from") Date from, @Param("to") Date to);

    @Query("select distinct c from Company c left join fetch c.subdomains inner join fetch c.industries i inner join fetch c.accountCompanies ac where ac.created >= :from and ac.created <= :to and i.id in :industryIds")
    List<Company> getCompaniesClaimedBetweenInIndustries(@Param("from") Date from, @Param("to") Date to, @Param("industryIds") Integer[] industryIds);

    @Query("select case when count(ac) > 0 then true else false end from AccountCompany ac inner join ac.company c where c.id = :companyId")
    boolean isCompanyClaimed(@Param("companyId") int companyId);

    @Query("select distinct c from Company c inner join c.companyKeywords ck where ck.isActive = 0")
    Page<Company> findCompaniesWithUnapprovedKeywords(Pageable pageable);

    @Modifying
    @Query("update Company c set c.website = :website where c.id = :companyId")
    void updateWebsite(@Param("companyId") int companyId, @Param("website") String website);

    @Modifying
    @Query("update Company c set c.phoneNumber = :phoneNumber where c.id = :companyId")
    void updatePhoneNumber(@Param("companyId") int companyId, @Param("phoneNumber") String phoneNumber);

    @Modifying
    @Query("update Company c set c.email = :email where c.id = :companyId")
    void updateEmail(@Param("companyId") int companyId, @Param("email") String email);

    @Modifying
    @Query("update Company c set c.teaser = :teaser where c.id = :companyId")
    void updateTeaser(@Param("companyId") int companyId, @Param("teaser") String teaser);

    @Modifying
    @Query("update Company c set c.numberOfLikes = c.numberOfLikes + 1 where c.id = :companyId")
    void incrementLikeCount(@Param("companyId") int companyId);

    @Modifying
    @Query(nativeQuery = true, value = "UPDATE company SET number_of_reviews = (SELECT " +
            "                                          (SELECT count(er) as number_ofReview " +
            "                                                FROM external_reviews er " +
            "                                                WHERE er.companyid = :companyId) + " +
            "                                               (SELECT count(r) as number_ofReview " +
            "                                                FROM review as r " +
            "                                                WHERE  r.company_id=:companyId)) WHERE id=:companyId")
    void incrementReviewCount(@Param("companyId") int companyId);

    @Modifying
    @Query(value = "update Company set rating = (SELECT avg(rating) * 2 FROM (SELECT rating, companyid FROM external_reviews er WHERE er.companyid = :companyId UNION ALL" +
            "   SELECT rating, company_id AS companyid  FROM Review r WHERE r.company_id = :companyId) AS sub) where id = :companyId", nativeQuery = true)
    void updateReviewRating(@Param("companyId") int companyId);

    @Modifying
    @Query("update Company c set c.profileUrl = CONCAT(:scheme, '://', c.id, '.', :domain) where c.profileUrl is null")
    void initializeProfileUrls(@Param("scheme") String httpScheme, @Param("domain") String domainName);

    @Modifying
    @Query(value = "update company c set municipal = (select municipal from company where city_name = c.city_name limit 1) where c.municipal is null", nativeQuery = true)
    void initializeMissingMunicipals();

    @Modifying
    @Query("update Company c set c.profileUrl = CONCAT(:scheme, '://', :subdomain, '.', :domain) where c.id = :companyId")
    void updateProfileUrl(@Param("companyId") int companyId, @Param("subdomain") String subdomain, @Param("scheme") String httpScheme, @Param("domain") String domainName);

    @Query(
            value = "select distinct c from Company c inner join c.industries i where i.id = :industryId and not exists (select 1 from Subdomain s where s.company.id = c.id)",
            countQuery = "select count(distinct c) from Company c inner join c.industries i where i.id = :industryId and not exists (select 1 from Subdomain s where s.company.id = c.id)"
    )
    Page<Company> getCompaniesInIndustryWithoutSubdomain(Pageable pageable, @Param("industryId") int industryId);

    @Query(
            value = "select distinct c from Company c inner join c.industries i where i.id = :industryId and c.bookingUrl is not null and not exists (select 1 from Subdomain s where s.company.id = c.id)",
            countQuery = "select count(distinct c) from Company c inner join c.industries i where i.id = :industryId and c.bookingUrl is not null and not exists (select 1 from Subdomain s where s.company.id = c.id)"
    )
    Page<Company> getCompaniesInIndustryWithBookingUrlAndWithoutSubdomain(Pageable pageable, @Param("industryId") int industryId);

    @Query("select case when count(c) > 0 then true else false end from Company c where c.id = :companyId and c.accessToken = :accessToken")
    boolean isValidAccessToken(@Param("companyId") int companyId, @Param("accessToken") String accessToken);

    Company findByCallTrackingNumber(String callTrackingNumber);

    List<Company> findByCiNumberAndPostalCode(long vatNumber, int postalCode);

    List<Company> findByCiNumber(long var1);

    public Company findByAccessToken(String var1);
}
