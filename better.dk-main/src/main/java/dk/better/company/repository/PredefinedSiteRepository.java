package dk.better.company.repository;

import dk.better.company.entity.PredefinedSite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii
 */
@Repository
public interface PredefinedSiteRepository extends JpaRepository<PredefinedSite, Integer> {

    PredefinedSite findByName(String name);
}