package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;
import dk.better.company.entity.Acknowledgement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface AcknowledgementRepository extends JpaRepository<Acknowledgement, Integer>, EntityManagerAware, AcknowledgementRepositoryCustom {
    @Query("select distinct a from Acknowledgement a inner join fetch a.industries i where i.id in :industryIds order by i.id")
        // ORDER BY so it is easy to group acknowledgements in view
    List<Acknowledgement> findByIndustryIds(@Param("industryIds") Collection<Integer> industryIds);

    @Query("select a from Acknowledgement a inner join fetch a.industries i where i.id = :industryId order by i.id")
        // ORDER BY so it is easy to group acknowledgements in view
    List<Acknowledgement> findByIndustryId(@Param("industryId") int industryId);
}