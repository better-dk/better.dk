package dk.better.company.repository;

import dk.better.company.entity.TextMessage;
import dk.better.company.service.TextMessageService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @author Bo Andersen
 */
@Repository
public interface TextMessageRepository extends JpaRepository<TextMessage, Integer>, JpaSpecificationExecutor<TextMessage> {
    @Query("select case when count(tm) > 0 then true else false end from TextMessage tm where tm.senderCompany.id = :companyId and tm.created > :oneDayAgo")
    boolean existsWithinOneDay(@Param("companyId") int companyId, @Param("oneDayAgo") Date oneDayAgo);

    @Query(
            value = "select tm " +
                    "from TextMessage tm " +
                    "inner join fetch tm.senderCompany " +
                    "inner join fetch tm.senderAccount " +
                    "where tm.status = " + TextMessageService.STATUS_PENDING_REVIEW,
            countQuery = "select count(tm) " +
                    "from TextMessage tm " +
                    "inner join tm.senderCompany " +
                    "inner join tm.senderAccount " +
                    "where tm.status = " + TextMessageService.STATUS_PENDING_REVIEW
    )
    Page<TextMessage> getPendingTextMessages(Pageable pageable);

    @Query(
            "select tm " +
                    "from TextMessage tm " +
                    "inner join fetch tm.textMessageCustomers tmc " +
                    "inner join fetch tmc.customer " +
                    "where tm.id = :textMessageId " +
                    "and tm.senderCompany.id = :companyId"
    )
    TextMessage findForConfirmation(@Param("companyId") int companyId, @Param("textMessageId") int textMessageId);

    @Query("select tm from TextMessage tm where tm.id = :textMessageId and tm.senderCompany.id = :companyId")
    TextMessage findForDecline(@Param("companyId") int companyId, @Param("textMessageId") int textMessageId);

    @Modifying
    @Query("update TextMessage tm set tm.text = :newText where tm.id = :textMessageId and tm.senderCompany.id = :companyId")
    void updateText(@Param("companyId") int companyId, @Param("textMessageId") int textMessageId, @Param("newText") String newText);

    @Modifying
    @Query("update TextMessageCustomer tmc set tmc.status = :status, tmc.statusUpdate = :date where tmc.uuid = :uuid")
    void updateTextMessageCustomerStatus(@Param("uuid") String uuid, @Param("status") int status, @Param("date") Date date);
}