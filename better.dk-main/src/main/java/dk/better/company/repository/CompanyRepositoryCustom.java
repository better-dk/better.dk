package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;
import dk.better.company.dto.ProfileUrlsByCiNumbersDTO;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface CompanyRepositoryCustom extends EntityManagerAware {
    List<ProfileUrlsByCiNumbersDTO> getProfileUrlsByCiNumbers(@Param("ciNumbers") Collection<Integer> ciNumbers);
}