package dk.better.company.repository;

import dk.better.application.repository.AbstractEntityManagerRepository;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bo Andersen
 */
public class ReviewRepositoryImpl extends AbstractEntityManagerRepository implements ReviewRepositoryCustom {
    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, Long> getRatingDistribution(int companyId) {
        String jpql = "SELECT rating, count(rating) " +
                "FROM (SELECT\n" +
                "      rating,\n" +
                "      companyid\n" +
                "    FROM external_reviews er\n" +
                "    WHERE er.companyid = :companyId\n" +
                "    UNION ALL\n" +
                "    SELECT\n" +
                "      rating,\n" +
                "      company_id AS companyid\n" +
                "    FROM Review r\n" +
                "    WHERE r.company_id = :companyId\n" +
                "  )\n" +
                "    AS sub group by rating";

        List<Object[]> results = this.getEntityManager()
                .createNativeQuery(jpql)
                .setParameter("companyId", companyId)
                .getResultList();

        Map<Long, Long> map = new HashMap<>(results.size());

        for (Object[] arr : results) {
            map.put(Long.valueOf(String.valueOf(arr[0])), ((BigInteger) arr[1]).longValue()); // Convert key to Long so that it can be used within JSTL
        }

        return map;
    }
}