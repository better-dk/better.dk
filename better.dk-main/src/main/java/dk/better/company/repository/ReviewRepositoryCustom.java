package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;

import java.util.Map;

/**
 * @author Bo Andersen
 */
public interface ReviewRepositoryCustom extends EntityManagerAware {
    Map<Long, Long> getRatingDistribution(int companyId);
}