package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;
import dk.better.company.entity.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface LikeRepository extends JpaRepository<Like, Integer>, EntityManagerAware {
    int countByCompanyIdAndAccountId(int companyId, int accountId);
}