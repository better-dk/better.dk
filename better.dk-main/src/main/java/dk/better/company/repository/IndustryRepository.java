package dk.better.company.repository;

import dk.better.company.entity.Industry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface IndustryRepository extends JpaRepository<Industry, Integer>, IndustryRepositoryCustom {
    @Query("select i from Industry i where lower(name) like %:query%")
    Page<Industry> findByNameContaining(@Param("query") String query, Pageable pageable);
}