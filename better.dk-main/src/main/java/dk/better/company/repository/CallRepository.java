package dk.better.company.repository;

import dk.better.company.entity.Call;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface CallRepository extends JpaRepository<Call, Integer>, CallRepositoryCustom {
    Call findTop1ByFromNumberAndEndTimeIsNullOrderByIdDesc(String fromPhoneNumber);

    Call findTop1ByFromNumberAndToNumberAndEndTimeIsNullOrderByIdDesc(String fromPhoneNumber, String toPhoneNumber);
}