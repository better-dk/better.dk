package dk.better.company.repository;

import dk.better.company.entity.SubdomainRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface SubdomainRequestRepository extends JpaRepository<SubdomainRequest, Integer>, SubdomainRequestRepositoryCustom {
    // For some reason, we need to join to RequestStatus, because otherwise the generated query is incorrect
    @EntityGraph(value = "graph.subdomain-request.display", type = EntityGraph.EntityGraphType.FETCH)
    @Query(
            value = "select sr from SubdomainRequest sr inner join fetch sr.requestStatus rs where rs.id = :requestStatusId",
            countQuery = "select count(sr) from SubdomainRequest sr inner join sr.requestStatus rs where rs.id = :requestStatusId"
    )
    Page<SubdomainRequest> findByRequestStatusId(@Param("requestStatusId") int requestStatusId, Pageable pageable);

    // For some reason, we need to join to RequestStatus, because otherwise the generated query is incorrect
    @EntityGraph(value = "graph.subdomain-request.display", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select sr from SubdomainRequest sr inner join fetch sr.requestStatus where sr.id = :id")
    SubdomainRequest findById(@Param("id") int id);
}