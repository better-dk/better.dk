package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;
import dk.better.company.entity.Industry;
import dk.better.company.entity.Service;

import java.util.Collection;

/**
 * @author Bo Andersen
 */
public interface ServiceRepositoryCustom extends EntityManagerAware {
    /**
     * Validates whether or not a collection of services are linked to a collection of industries.
     * Returns true only if all services are linked to one of the provided industries.
     *
     * @param services   The services to check against the industries
     * @param industries The industries for which the services must be linked
     * @return True if all services are linked to one of the provided industries. False otherwise.
     */
    boolean areLinkedToIndustries(Collection<Service> services, Collection<Industry> industries);
}