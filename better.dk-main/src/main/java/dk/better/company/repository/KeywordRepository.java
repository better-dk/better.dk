package dk.better.company.repository;

import dk.better.company.entity.CompanyKeyword;
import dk.better.company.entity.Keyword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * @author Bo Andersen
 */
@Repository
public interface KeywordRepository extends JpaRepository<Keyword, Integer> {
    Keyword findByName(String name);

    @Query("select case when count(ck) > 0 then true else false end from CompanyKeyword ck inner join ck.company c inner join ck.keyword k where c.id = :companyId and k.id = :keywordId")
    boolean hasCompanyKeyword(@Param("companyId") int companyId, @Param("keywordId") int keywordId);

    @Query("select ck from CompanyKeyword ck inner join ck.company c inner join fetch ck.keyword where c.id = :companyId")
    Set<CompanyKeyword> getCompanyKeywords(@Param("companyId") int companyId);
}