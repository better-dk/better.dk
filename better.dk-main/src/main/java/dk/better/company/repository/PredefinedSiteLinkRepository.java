package dk.better.company.repository;

import dk.better.company.entity.Company;
import dk.better.company.entity.PredefinedSite;
import dk.better.company.entity.PredefinedSiteLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface PredefinedSiteLinkRepository extends JpaRepository<PredefinedSiteLink, Integer> {
    List<PredefinedSiteLink> findByCompany(Company company);

    PredefinedSiteLink findByCompanyAndSite(Company company, PredefinedSite site);
}