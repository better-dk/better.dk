package dk.better.company.repository;

import dk.better.application.repository.AbstractEntityManagerRepository;
import dk.better.company.entity.Acknowledgement;
import dk.better.company.entity.Industry;
import dk.better.company.util.CompanyUtils;
import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author Bo Andersen
 */
public class AcknowledgementRepositoryImpl extends AbstractEntityManagerRepository implements AcknowledgementRepositoryCustom {
    @SuppressWarnings("unchecked")
    @Override
    public boolean areLinkedToIndustries(Collection<Acknowledgement> acknowledgements, Collection<Industry> industries) {
        String jpql = "select count(distinct a) from Acknowledgement a inner join a.industries as i where a.id in :acknowledgements and i.id in :industries";
        Collection<Integer> acknowledgementIds = (Collection<Integer>) CollectionUtils.collect(acknowledgements, new BeanToPropertyValueTransformer("id"));
        Collection<Integer> industryIds = CompanyUtils.getIndustryIds(industries);

        long result = (long) this.getEntityManager()
                .createQuery(jpql)
                .setParameter("acknowledgements", new HashSet<>(acknowledgementIds))
                .setParameter("industries", new HashSet<>(industryIds))
                .getSingleResult();

        return (result == acknowledgements.size());
    }
}