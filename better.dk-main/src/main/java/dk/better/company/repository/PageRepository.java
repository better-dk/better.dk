package dk.better.company.repository;

import dk.better.application.repository.EntityManagerAware;
import dk.better.company.entity.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface PageRepository extends JpaRepository<Page, Integer>, EntityManagerAware {
    @Query("select p from Page p where p.company.id = :companyId and p.parent is null")
    List<Page> findRootPagesForCompany(@Param("companyId") int companyId);

    @Query("select p from Page p where p.slug = :slug and p.company.id = :companyId")
    Page findRootPageBySlug(@Param("slug") String slug, @Param("companyId") int companyId);

    @Query("select p from Page p where p.slug = :childSlug and p.parent.slug = :parentSlug and p.company.id = :companyId")
    Page findChildPageBySlug(@Param("parentSlug") String parentSlug, @Param("childSlug") String childSlug, @Param("companyId") int companyId);

    @Query("select case when count(p) > 0 then true else false end from Page p where p.slug = :slug and p.parent.id = :parentId and p.company.id = :companyId")
    boolean exists(@Param("slug") String slug, @Param("companyId") int companyId, @Param("parentId") Integer parentId);
}