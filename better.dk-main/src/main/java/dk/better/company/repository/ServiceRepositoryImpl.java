package dk.better.company.repository;

import dk.better.application.repository.AbstractEntityManagerRepository;
import dk.better.company.entity.Industry;
import dk.better.company.entity.Service;
import dk.better.company.util.CompanyUtils;
import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author Bo Andersen
 */
public class ServiceRepositoryImpl extends AbstractEntityManagerRepository implements ServiceRepositoryCustom {
    @SuppressWarnings("unchecked")
    @Override
    public boolean areLinkedToIndustries(Collection<Service> services, Collection<Industry> industries) {
        String jpql = "select count(distinct s) from Service s inner join s.industries as i where s.id in :services and i.id in :industries";
        Collection<Integer> serviceIds = (Collection<Integer>) CollectionUtils.collect(services, new BeanToPropertyValueTransformer("id"));
        Collection<Integer> industryIds = CompanyUtils.getIndustryIds(industries);

        long result = (long) this.getEntityManager()
                .createQuery(jpql)
                .setParameter("services", new HashSet<>(serviceIds))
                .setParameter("industries", new HashSet<>(industryIds))
                .getSingleResult();

        return (result == services.size());
    }
}