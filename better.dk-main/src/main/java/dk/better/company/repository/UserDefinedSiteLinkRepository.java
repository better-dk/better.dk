package dk.better.company.repository;

import dk.better.company.entity.Company;
import dk.better.company.entity.UserDefinedSiteLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface UserDefinedSiteLinkRepository extends JpaRepository<UserDefinedSiteLink, Integer> {
    List<UserDefinedSiteLink> findByCompany(Company company);

    UserDefinedSiteLink findByCompanyAndName(Company company, String name);
}