package dk.better.company.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Bo Andersen
 */
public class LogoAndBannerUpload {
    private MultipartFile logo;
    private MultipartFile banner;


    public MultipartFile getLogo() {
        return logo;
    }

    public void setLogo(MultipartFile logo) {
        this.logo = logo;
    }

    public MultipartFile getBanner() {
        return banner;
    }

    public void setBanner(MultipartFile banner) {
        this.banner = banner;
    }
}