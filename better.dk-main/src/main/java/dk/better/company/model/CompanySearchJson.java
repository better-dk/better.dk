package dk.better.company.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

/**
 * @author Bo Andersen
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CompanySearchJson {
    private int id;
    private String type = "add";

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> fields;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }
}