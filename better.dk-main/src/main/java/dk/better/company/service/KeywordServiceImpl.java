package dk.better.company.service;

import dk.better.application.exception.ResourceNotFoundException;
import dk.better.company.entity.Company;
import dk.better.company.entity.CompanyKeyword;
import dk.better.company.entity.Keyword;
import dk.better.company.repository.KeywordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Service
public class KeywordServiceImpl implements KeywordService {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private KeywordRepository keywordRepository;


    @Override
    public boolean existsByName(String keyword) {
        return (this.keywordRepository.findByName(keyword) != null);
    }

    @Override
    public Keyword findByName(String keyword) {
        return this.keywordRepository.findByName(keyword);
    }

    @Override
    public boolean hasCompanyKeyword(int companyId, int keywordId) {
        return this.keywordRepository.hasCompanyKeyword(companyId, keywordId);
    }

    @Override
    public Set<CompanyKeyword> getCompanyKeywords(int companyId) {
        return this.keywordRepository.getCompanyKeywords(companyId);
    }

    @Override
    @Transactional
    public void addKeywordsToCompany(int companyId, Set<String> keywords) {
        Assert.notNull(companyId);
        Assert.notNull(keywords);
        Assert.notEmpty(keywords);

        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("A company with ID: %d was not found!", companyId));
        }

        Set<CompanyKeyword> companyKeywords = new HashSet<>(keywords.size());

        for (String keyword : keywords) {
            Keyword existingKeyword = this.findByName(keyword);

            if (existingKeyword == null) {
                // Add new keyword and link company to it
                Keyword saved = this.keywordRepository.save(new Keyword(keyword));
                companyKeywords.add(new CompanyKeyword(company, saved, false));
            } else {
                // Link company to existing keyword if not linked already
                if (!this.hasCompanyKeyword(companyId, existingKeyword.getId())) {
                    companyKeywords.add(new CompanyKeyword(company, existingKeyword, false));
                }
            }
        }

        company.setCompanyKeywords(companyKeywords);
        this.companyService.save(company);
    }
}