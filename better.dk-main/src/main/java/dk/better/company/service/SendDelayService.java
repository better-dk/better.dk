package dk.better.company.service;

import dk.better.company.entity.SendDelay;

/**
 * @author Liubov Zavorotna
 */
public interface SendDelayService {

    void save(SendDelay delay);

    SendDelay findByCompanyId(int companyId);

    void updateSendDelay(int companyId, int hours);
}
