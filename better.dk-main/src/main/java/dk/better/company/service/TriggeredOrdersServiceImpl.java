package dk.better.company.service;

import com.github.segmentio.models.Props;
import dk.better.application.service.AnalyticsService;
import dk.better.company.dto.TriggeredOrderDto;
import dk.better.company.entity.Company;
import dk.better.company.entity.SendDelay;
import dk.better.company.entity.TriggeredOrders;
import dk.better.company.repository.TriggeredOrdersRepository;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

/**
 * @author Liubov Zavorotna
 */
@Service
public class TriggeredOrdersServiceImpl implements TriggeredOrdersService {
    private static final String AUTHORIZATION_KEY_TEST = "Basic Nzk0YmE5MzFkNzgyNmUwYjFiNTU6OWY1NWMwZGVmNDNlMDJlNTAxNGM=";
    private static final String AUTHORIZATION_KEY_PRODUCTION = "Basic M2EzZDIzMjEyZDY2NTE4ZTJhMjI6MjY0MWFmMzBhODg3NzljNzIxYjc=";
    private static final int PAGE_SIZE = 10;
    @Autowired
    private TriggeredOrdersRepository triggeredOrdersRepository;
    @Autowired
    private AnalyticsService analyticsService;
    @Autowired
    private SendDelayService sendDelayService;
    @Autowired
    private CompanyService companyService;

    @Override
    public List<TriggeredOrders> findByCompanyId(int companyId) {
        return triggeredOrdersRepository.findByCompanyId(companyId);

    }


    @Override
    public Page<TriggeredOrders> findByCompanyIdPage(int companyId, int page) {
        PageRequest request = new PageRequest(page - 1, PAGE_SIZE, Sort.Direction.DESC, "id");
        return triggeredOrdersRepository.findByCompanyId(companyId, request);
    }

    @Override
    @Transactional
    public void insertTriggeredOrder(int companyId, String customerName, String customerEmail,
                                     String transactionId) {
        this.triggeredOrdersRepository.insertTriggeredOrder(companyId, customerName,
                customerEmail, transactionId);
    }

    @Transactional
    @Override
    public void save(TriggeredOrders orders) {
        this.triggeredOrdersRepository.save(orders);
    }

    @Override
    public void trackWithSegmentIO(TriggeredOrders order) {
        save(order);
        SendDelay sendDelay = sendDelayService.findByCompanyId(order.getCompanyId());
        Company company = companyService.find(order.getCompanyId());
        postToSegmentIO(new TriggeredOrderDto(order, (long) sendDelay.getHours(), company.getName(), company.getEmail()));
    }

    @Override
    public void trackWithCustomerIO(TriggeredOrders order) {
        save(order);
        SendDelay sendDelay = sendDelayService.findByCompanyId(order.getCompanyId());
        Company company = companyService.find(order.getCompanyId());
        postToCustomerIO(new TriggeredOrderDto(order, (long) sendDelay.getHours(), company.getName(), company.getEmail()));
    }

    @Override
    public void postToSegmentIO(TriggeredOrderDto dto) {

        TriggeredOrders order = dto.getOrder();
        try {
            Props props = new Props();
            props.put("Company ID: ", order.getCompanyId())
                    .put("Customer Name: ", order.getCustomerName())
                    .put("Customer Email: ", order.getCustomerEmail())
                    .put("Transaction ID: ", order.getTransactionId())
                    .put("Send Delay: ", dto.getSendDelay());

            this.analyticsService.track(String.valueOf(order.getCompanyId()), "Review Invite", props);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void postToCustomerIO(TriggeredOrderDto dto) {

        TriggeredOrders order = dto.getOrder();

        String email;
        if (dto.getCompanyEmail() == null) {
            email = "";
        } else {
            email = dto.getCompanyEmail();
        }

        try {
            Jsoup.connect("https://track.customer.io/api/v1/customers/5/events")
                    .ignoreContentType(true)
                    .header("Cache-Control", "no-cache")
                    .header("Authorization", AUTHORIZATION_KEY_PRODUCTION)
                    .data("cookieexists", "false")
                    .data("name", "Review Invite")
                    .data("data[Company ID]", String.valueOf(order.getCompanyId()))
                    .data("data[Company Name]", dto.getCompanyName())
                    .data("data[Company Email]", email)
                    .data("data[Customer Name]", order.getCustomerName())
                    .data("data[Customer Email]", order.getCustomerEmail())
                    .data("data[Transaction ID]", order.getTransactionId())
                    .data("data[Send Delay]", String.valueOf(dto.getSendDelay()))
                    .post();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
