package dk.better.company.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.github.segmentio.models.Props;
import dk.better.account.entity.Account;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.EventType;
import dk.better.company.entity.Company;
import dk.better.company.entity.Integration;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.IntegrationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author vitalii.
 */
@Service
public class IntegrationServiceImpl implements IntegrationService {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private AnalyticsService analyticsService;
    @Autowired
    private DynamoDBMapper dynamoMapper;

    @Transactional
    public void activate(int companyId, IntegrationType type) {
        this.activate(companyId, type, null);
    }

    @Transactional
    public void activate(int companyId, IntegrationType type, Map<String, Object> metaData) {
        Account account = AuthUtils.getPrincipal();
        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        } else {
            Integration integration = this.find(companyId, type);
            if (integration == null) {
                integration = new Integration();
                integration.setCreated(new Date());
                integration.setCompanyId(companyId);
                integration.setMetaData(metaData);
                integration.setIntegrationId(type.getValue());
            }

            integration.setActive(true);
            integration.setLastModified(new Date());
            this.dynamoMapper.save(integration);
            Company company = this.companyService.find(companyId);
            Props props = new Props();
            props.put("company", (new Props()).put("id", companyId).put("name", company.getName()).put("city", company.getCity()).put("postalCode", Integer.valueOf(company.getPostalCode())).put("profileUrl", company.getProfileUrl()).put("email", company.getEmail()).put("phoneNumber", company.getPhoneNumber()).put("callTrackingNumber", company.getCallTrackingNumber()).put("accessToken", company.getAccessToken()).put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));
            props.put("integration", (new Props()).put("id", type.getValue()).put("name", type.name()));
            this.analyticsService.track(account.getUuid(), EventType.INTEGRATION_ACTIVATED.getValue(), props);
        }
    }

    @Transactional
    public void deactivate(int companyId, IntegrationType type) {
        Account account = AuthUtils.getPrincipal();
        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        } else {
            Integration integration = this.find(companyId, type);
            if (integration == null) {
                throw new RuntimeException(String.format("Could not find integration for company (company ID: %d, type: %d)", companyId, type.getValue()));
            } else {
                integration.setActive(false);
                integration.setLastModified(new Date());
                this.dynamoMapper.save(integration);
                Company company = this.companyService.find(companyId);
                Props props = new Props();
                props.put("company", (new Props()).put("id", companyId).put("name", company.getName()).put("city", company.getCity()).put("postalCode", company.getPostalCode()).put("profileUrl", company.getProfileUrl()).put("email", company.getEmail()).put("phoneNumber", company.getPhoneNumber()).put("callTrackingNumber", company.getCallTrackingNumber()).put("accessToken", company.getAccessToken()).put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));
                props.put("integration", (new Props()).put("id", type.getValue()).put("name", type.name()));
                this.analyticsService.track(account.getUuid(), EventType.INTEGRATION_DEACTIVATED.getValue(), props);
            }
        }
    }

    public Integration find(int companyId, IntegrationType type) {
        Integration integrationKey = new Integration();
        integrationKey.setCompanyId(companyId);
        Condition rangeKeyCondition = (new Condition()).withComparisonOperator(ComparisonOperator.EQ.toString()).withAttributeValueList((new AttributeValue()).withN(String.valueOf(type.getValue())));
        DynamoDBQueryExpression queryExpression = (new DynamoDBQueryExpression()).withHashKeyValues(integrationKey).withRangeKeyCondition("IntegrationID", rangeKeyCondition).withConsistentRead(false);
        PaginatedQueryList integrations = this.dynamoMapper.query(Integration.class, queryExpression);
        return integrations.size() > 0 ? (Integration) integrations.get(0) : null;
    }

    public Map<IntegrationType, Boolean> getIntegrationStatuses(int companyId) {
        Account account = AuthUtils.getPrincipal();
        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        } else {
            Integration integrationKey = new Integration();
            integrationKey.setCompanyId(companyId);
            DynamoDBQueryExpression queryExpression = (new DynamoDBQueryExpression()).withHashKeyValues(integrationKey).withConsistentRead(false);
            PaginatedQueryList integrations = this.dynamoMapper.query(Integration.class, queryExpression);
            HashMap statuses = new HashMap(integrations.size());
            Iterator var7 = integrations.iterator();

            while (var7.hasNext()) {
                Integration integration = (Integration) var7.next();
                statuses.put(IntegrationType.valueOf(integration.getIntegrationId()), integration.isActive());
            }

            return statuses;
        }
    }
}

