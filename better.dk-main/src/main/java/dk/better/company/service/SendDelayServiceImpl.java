package dk.better.company.service;

import dk.better.company.entity.SendDelay;
import dk.better.company.repository.SendDelayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Liubov Zavorotna
 */
@Service
public class SendDelayServiceImpl implements SendDelayService {
    @Autowired
    private SendDelayRepository sendDelayRepository;


    @Transactional
    @Override
    public void save(SendDelay delay) {
        this.sendDelayRepository.save(delay);

    }

    @Override
    @Transactional
    public SendDelay findByCompanyId(int companyId) {
        return sendDelayRepository.findByCompanyId(companyId);
    }

    @Transactional
    @Override
    public void updateSendDelay(int companyId, int hours) {
        this.sendDelayRepository.updateSendDelay(companyId, hours);
    }
}
