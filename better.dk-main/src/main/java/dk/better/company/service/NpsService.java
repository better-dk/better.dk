package dk.better.company.service;

/**
 * @author vitalii.
 */
public interface NpsService {
    void addScore(int var1, int var2, String var3);

    void addScore(int var1, int var2, String var3, Integer var4);
}