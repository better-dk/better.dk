package dk.better.company.service;

import com.github.segmentio.models.Props;
import dk.better.account.entity.Account;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.DateUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Page;
import dk.better.company.entity.PageRevision;
import dk.better.company.repository.PageRepository;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.PageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Service
public class PageServiceImpl implements PageService {
    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AnalyticsService analyticsService;


    @Override
    public Page find(int id) {
        return this.pageRepository.findOne(id);
    }

    @Override
    public Page save(Page page) {
        return this.pageRepository.save(page);
    }

    @Override
    public Page saveAndFlush(Page page) {
        return this.pageRepository.saveAndFlush(page);
    }

    @Override
    public Page getReference(Object identifier) {
        return this.pageRepository.getEntityManager().getReference(Page.class, identifier);
    }

    @Override
    public Object getReference(Class type, Object identifier) {
        return this.pageRepository.getEntityManager().getReference(type, identifier);
    }

    @Override
    public List<Page> findRootPagesForCompany(int companyId) {
        return this.pageRepository.findRootPagesForCompany(companyId);
    }

    @Override
    @Transactional
    public Page add(Page page, int companyId) {
        return this.add(page, companyId, null);
    }

    @Override
    @Transactional
    public Page add(Page page, int companyId, Integer parentId) {
        Assert.notNull(page);
        Account account = AuthUtils.getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException("User does not have privileges to add page");
        }

        if (this.exists(page.getSlug(), companyId, parentId)) {
            throw new UniqueConstraintViolationException(String.format("Slug (\"%s\") is not unique for company (ID: %d)", page.getSlug(), companyId));
        }

        if (parentId != null) {
            Page parent = this.pageRepository.findOne(parentId);

            if (parent == null || parent.getCompany().getId() != companyId) {
                throw new AccessDeniedException("Parent page does not exist, or the company does not have access to it");
            }

            if (parent.getParent() != null) {
                throw new UnsupportedOperationException("Page hierarchy cannot be greater than two levels");
            }

            page.setParent(this.getReference(parentId));
        }

        Company company = this.companyService.getForProfileViewById(companyId);
        Date now = new Date();

        PageRevision revision = new PageRevision();
        revision.setName(page.getName());
        revision.setTitle(page.getTitle());
        revision.setSlug(page.getSlug());
        revision.setContent(page.getContent());
        revision.setData(page.getData());
        revision.setCreated(now);
        revision.setPage(page);
        revision.setAuthor(account);

        page.setPageType(PageType.REGULAR);
        page.setCreated(now);
        page.setLastModified(now);
        page.add(revision);
        page.setCompany(company);

        // Trigger event
        Props props = new Props();
        props.put("company", new Props()
                .put("id", companyId)
                .put("name", company.getName())
                .put("city", company.getCity())
                .put("postalCode", company.getPostalCode())
                .put("profileUrl", company.getProfileUrl())
                .put("email", company.getEmail())
                .put("phoneNumber", company.getPhoneNumber())
                .put("callTrackingNumber", company.getCallTrackingNumber())
                .put("accessToken", company.getAccessToken())
                .put("checksum", CompanyUtils.generateChecksum(companyId))
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries()))
                .put("foundingDate", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

        this.analyticsService.track(account.getUuid(), "Page Created", props);
        return this.pageRepository.save(page);
    }

    @Override
    public boolean exists(String slug, int companyId, Integer parentId) {
        return this.pageRepository.exists(slug, companyId, parentId);
    }

    @Override
    public Page findRootPageBySlug(String slug, int companyId) {
        return this.pageRepository.findRootPageBySlug(slug, companyId);
    }

    @Override
    public Page findChildPageBySlug(String parentSlug, String childSlug, int companyId) {
        Assert.notNull(parentSlug);
        Assert.notNull(childSlug);
        return this.pageRepository.findChildPageBySlug(parentSlug, childSlug, companyId);
    }
}