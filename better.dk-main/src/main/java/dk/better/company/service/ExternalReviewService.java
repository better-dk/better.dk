package dk.better.company.service;

import dk.better.api.model.widget.SocialRating;
import dk.better.api.model.widget.WidgetSimple;
import dk.better.application.dto.ExternalReviewDTO;
import dk.better.company.entity.Company;
import dk.better.company.entity.ExternalReview;
import dk.better.company.repository.CompanyRepository;
import dk.better.company.repository.ExternalReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vitalii
 */
@Service
public class ExternalReviewService {


    @Autowired
    private ExternalReviewRepository externalReviewRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public List<ExternalReview> findByCompanyId(int companyId) {
        return externalReviewRepository.findByCompanyId(companyId);
    }

    public WidgetSimple findOverallInfo(int companyId) {
        return externalReviewRepository.findOverallInfo(companyId);
    }

    public List<SocialRating> findSocialInfo(int companyId) {
        List<SocialRating> socialRatings = externalReviewRepository.findBetterSocialInfo(companyId);
        socialRatings.addAll(externalReviewRepository.findSocialInfo(companyId));
        return socialRatings;
    }

    public List<ExternalReview> findReviewsForWidget(int companyId) {
        List<ExternalReview> reviews = externalReviewRepository.findBetterReviewsForWidget(companyId);
        reviews.addAll(externalReviewRepository.findReviewsForWidget(companyId));
        return reviews;
    }

    public List<ExternalReviewDTO> findExternalReviewsLastByCompanyId(int companyId) {
        List<ExternalReview> externalReviewsFromDB = externalReviewRepository.findExternalReviewsLastByCompanyId(companyId);
        List<Company> companiesFromDB = companyRepository
                .findByIdIn(externalReviewsFromDB
                        .stream()
                        .map(ExternalReview::getCompanyId)
                        .collect(Collectors.toList()));
        return externalReviewsFromDB
                .stream()
                .map(review ->
                        new ExternalReviewDTO(review,
                                companiesFromDB
                                        .stream()
                                        .filter(company -> company.getId() == review.getCompanyId())
                                        .findFirst()
                                        .get()))
                .collect(Collectors.toList());
    }

    public List<ExternalReviewDTO> findExternalReviewsLimited() {
        List<ExternalReview> externalReviewsFromDB = externalReviewRepository.findExternalReviewsLimited();
        List<Company> companiesFromDB = companyRepository
                .findByIdIn(externalReviewsFromDB
                        .stream()
                        .map(ExternalReview::getCompanyId)
                        .collect(Collectors.toList()));
        return externalReviewsFromDB
                .stream()
                .map(review ->
                        new ExternalReviewDTO(review,
                                companiesFromDB
                                        .stream()
                                        .filter(company -> company.getId() == review.getCompanyId())
                                        .findFirst()
                                        .get()))
                .collect(Collectors.toList());
    }

    public List<ExternalReview> findBySiteAndCompanyId(int companyId, String site) {

        return externalReviewRepository.findBySiteAndCompanyId(companyId, site);
    }
}