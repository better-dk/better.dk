package dk.better.company.service;

import dk.better.application.dto.ExternalReviewDTO;
import dk.better.application.dto.ReviewDTO;
import dk.better.company.entity.Review;
import org.springframework.ui.Model;

import java.util.List;

/**
 * @author Liubov Zavorotna
 */
public interface ControlPanelControllerService {

    Long getRatingDistributionValues(int companyId);

    void getLastReviews(int companyId, Model model);

    void getNumberForRatingFordeling(Model model);

    List<ReviewDTO> mergeReviews(List<Review> reviews, List<ExternalReviewDTO> externalReview);

    void getAverageRatingBetterByCompanyId(int companyId, Model model);

    void getAverageRatingForAllSites(int companyId, Model model);

    void getAverageRatingExternalReviewsBySiteAndCompanyId(int companyId, Model model, String site);

    void getCountOfInvitation(int companyId, Model model);

    void getAverageRatingForBetterAndExternalReviews(int companyId, Model model);
}
