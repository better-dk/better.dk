package dk.better.company.service;

import dk.better.company.entity.PredefinedSite;
import dk.better.company.repository.PredefinedSiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii
 */
@Service
public class PredefinedSiteService {

    @Autowired
    private PredefinedSiteRepository predefinedSiteRepository;

    public PredefinedSite save(PredefinedSite site) {
        return this.predefinedSiteRepository.save(site);
    }

    public List<PredefinedSite> findAll() {
        return predefinedSiteRepository.findAll();
    }

    public PredefinedSite findById(Integer id) {
        return predefinedSiteRepository.findOne(id);
    }

    public PredefinedSite findByName(String name) {
        return predefinedSiteRepository.findByName(name);
    }

}