package dk.better.company.service;

import dk.better.company.entity.Company;
import dk.better.company.entity.PredefinedSite;
import dk.better.company.entity.PredefinedSiteLink;
import dk.better.company.repository.PredefinedSiteLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii
 */
@Service
public class PredefinedSiteLinkService {

    @Autowired
    private PredefinedSiteLinkRepository predefinedSiteRepository;

    public PredefinedSiteLink save(PredefinedSiteLink site) {
        return this.predefinedSiteRepository.save(site);
    }

    public List<PredefinedSiteLink> save(List<PredefinedSiteLink> site) {
        return this.predefinedSiteRepository.save(site);
    }

    public List<PredefinedSiteLink> findAll() {
        return predefinedSiteRepository.findAll();
    }

    public List<PredefinedSiteLink> findByCompany(Company company) {
        return predefinedSiteRepository.findByCompany(company);
    }

    public PredefinedSiteLink findByCompanyAndSite(Company company, PredefinedSite site) {
        return predefinedSiteRepository.findByCompanyAndSite(company, site);
    }
}