package dk.better.company.service;

import dk.better.application.service.AbstractService;
import dk.better.application.service.FieldValueExists;
import dk.better.company.entity.Industry;
import dk.better.company.entity.Service;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface ServiceService extends AbstractService<Service>, FieldValueExists {
    List<Service> findByIndustryId(int industryId);

    List<Service> findByIndustryIds(Collection<Integer> industryIds);

    /**
     * Validates whether or not a collection of services are linked to a collection of industries.
     * Returns true only if all services are linked to one of the provided industries.
     *
     * @param services   The acknowledgements to check against the industries
     * @param industries The industries for which the services must be linked
     * @return True if all services are linked to one of the provided industries. False otherwise.
     */
    boolean areLinkedToIndustries(Collection<Service> services, Collection<Industry> industries);
}