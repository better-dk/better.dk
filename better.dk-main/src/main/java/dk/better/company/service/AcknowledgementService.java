package dk.better.company.service;

import dk.better.company.entity.Acknowledgement;
import dk.better.company.entity.Industry;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface AcknowledgementService {
    List<Acknowledgement> findByIndustryId(int var1);

    List<Acknowledgement> findByIndustryIds(Collection<Integer> var1);

    List<Acknowledgement> findAll();

    List<Acknowledgement> findAll(Sort var1);

    boolean areLinkedToIndustries(Collection<Acknowledgement> var1, Collection<Industry> var2);
}