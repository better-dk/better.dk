package dk.better.company.service;

import dk.better.application.dto.ExternalReviewDTO;
import dk.better.application.dto.ReviewDTO;
import dk.better.company.entity.ExternalReview;
import dk.better.company.entity.Review;
import dk.better.company.entity.TriggeredOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Liubov Zavorotna
 */
@Service
public class ControlPanelControllerServiceImpl implements ControlPanelControllerService {

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ExternalReviewService externalReviewService;

    @Autowired
    private TriggeredOrdersService triggeredOrdersService;

    @Override
    public Long getRatingDistributionValues(int companyId) {
        Collection<Long> ratingValues = this.reviewService.getRatingDistribution(companyId).values();
        Long ratingDistributionValues;
        if (ratingValues.isEmpty()) {
            ratingDistributionValues = 0L;
        } else {
            ratingDistributionValues = Collections.max(ratingValues);
        }
        return ratingDistributionValues;
    }

    @Override
    @Transactional(readOnly = true)
    public void getLastReviews(int companyId, Model model) {
        List<ExternalReviewDTO> externalReview = externalReviewService.findExternalReviewsLastByCompanyId(companyId);
        List<Review> reviewBetter = reviewService.findLastReviews(companyId);
        List<ReviewDTO> reviewDTOS = mergeReviews(reviewBetter, externalReview);
        List<ReviewDTO> lastReviews = reviewDTOS
                .stream()
                .limit(5)
                .collect(Collectors.toList());
        lastReviews.forEach(System.out::println);
        model.addAttribute("lastReviews", lastReviews);
    }

    @Override
    public void getNumberForRatingFordeling(Model model) {
        Map<Integer, Integer> mapOfNumberForRatingFordeling = new TreeMap<>((key, value) ->
                Integer.compare(value, key));
        mapOfNumberForRatingFordeling.put(1, 1);
        mapOfNumberForRatingFordeling.put(2, 2);
        mapOfNumberForRatingFordeling.put(3, 3);
        mapOfNumberForRatingFordeling.put(4, 4);
        mapOfNumberForRatingFordeling.put(5, 5);
        model.addAttribute("mapOfNumber", mapOfNumberForRatingFordeling);
    }

    @Override
    public List<ReviewDTO> mergeReviews(List<Review> reviews, List<ExternalReviewDTO> externalReview) {
        List<ReviewDTO> finalReviews = reviews.stream().map((ReviewDTO::new)).collect(Collectors.toList());
        finalReviews.addAll(externalReview.stream().map(ReviewDTO::new).collect(Collectors.toList()));
        return finalReviews;
    }

    @Override
    public void getAverageRatingBetterByCompanyId(int companyId, Model model) {
        OptionalDouble better = this.reviewService.findByCompanyId(companyId)
                .stream()
                .mapToDouble(Review::getRating)
                .average();
        model.addAttribute("better", better.isPresent() ? new BigDecimal(better.getAsDouble()).setScale(1, RoundingMode.UP).doubleValue() : 0);
    }

    @Override
    public void getAverageRatingForAllSites(int companyId, Model model) {
        getAverageRatingExternalReviewsBySiteAndCompanyId(companyId, model, "google");
        getAverageRatingExternalReviewsBySiteAndCompanyId(companyId, model, "yelp");
        getAverageRatingExternalReviewsBySiteAndCompanyId(companyId, model, "tripadvisor");
        getAverageRatingExternalReviewsBySiteAndCompanyId(companyId, model, "facebook");
        getAverageRatingBetterByCompanyId(companyId, model);
    }

    @Override
    public void getAverageRatingExternalReviewsBySiteAndCompanyId(int companyId, Model model, String site) {
        OptionalDouble externalReviews = this.externalReviewService.findBySiteAndCompanyId(companyId, site)
                .stream()
                .mapToDouble(ExternalReview::getRating)
                .average();
        model.addAttribute(site, externalReviews.isPresent() ? new BigDecimal(externalReviews.getAsDouble()).setScale(1, RoundingMode.UP).doubleValue() : 0);
    }

    @Override
    public void getCountOfInvitation(int companyId, Model model) {
        List<TriggeredOrders> triggeredOrdersFromDB = triggeredOrdersService.findByCompanyId(companyId);
        model.addAttribute("invitation", triggeredOrdersFromDB.size());
    }

    @Override
    public void getAverageRatingForBetterAndExternalReviews(int companyId, Model model) {
        List<Integer> ratingReviews = reviewService.findByCompanyId(companyId)
                .stream()
                .map(Review::getRating)
                .collect(Collectors.toList());
        List<Integer> ratingExternalReviews = externalReviewService.findByCompanyId(companyId)
                .stream()
                .map(ExternalReview::getRating)
                .collect(Collectors.toList());
        ratingReviews.addAll(ratingExternalReviews);

        OptionalDouble average = ratingReviews.stream().mapToDouble(value -> value).average();
        model.addAttribute("average", average.isPresent() ? new BigDecimal(average.getAsDouble()).setScale(1, RoundingMode.UP).doubleValue() : 0);
    }
}
