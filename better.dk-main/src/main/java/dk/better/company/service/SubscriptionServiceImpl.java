package dk.better.company.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import dk.better.company.entity.Subscription;
import dk.better.company.util.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class SubscriptionServiceImpl implements SubscriptionService {
    @Autowired
    private DynamoDBMapper dynamoMapper;


    @Override
    public void subscribe(int companyId, ProductType productType, Date startDate, Date endDate) {
        Assert.notNull(productType);
        Assert.notNull(startDate);
        Assert.notNull(endDate);

        if (endDate.getTime() <= startDate.getTime()) {
            throw new RuntimeException("End date cannot be before the start date!");
        }

        if (this.isSubscribed(companyId, productType, startDate)) {
            throw new RuntimeException(String.format("This company (ID: %d) is already subscribed to this product (ID: %d)!", companyId, productType.getValue()));
        }

        Subscription subscription = new Subscription();
        subscription.setCompanyId(companyId);
        subscription.setProductId(productType.getValue());
        subscription.setStartDate(startDate);
        subscription.setEndDate(endDate);

        this.dynamoMapper.save(subscription);
    }

    @Override
    public boolean isSubscribed(int companyId, ProductType productType) {
        return this.isSubscribed(companyId, productType, new Date());
    }

    @Override
    public boolean isSubscribed(int companyId, ProductType productType, Date startDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        Condition rangeKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ.toString())
                .withAttributeValueList(new AttributeValue().withN(String.valueOf(productType.getValue())));

        Map<String, Condition> filters = new HashMap<>();
        filters.put("StartDate", new Condition()
                .withComparisonOperator(ComparisonOperator.LE.toString())
                .withAttributeValueList(new AttributeValue().withS(dateFormatter.format(startDate))));
        filters.put("EndDate", new Condition()
                .withComparisonOperator(ComparisonOperator.GE.toString())
                .withAttributeValueList(new AttributeValue().withS(dateFormatter.format(startDate))));

        Subscription subscriptionKey = new Subscription();
        subscriptionKey.setCompanyId(companyId);

        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression<>()
                .withIndexName("CompanyID-ProductID-index")
                .withHashKeyValues(subscriptionKey)
                .withRangeKeyCondition("ProductID", rangeKeyCondition)
                .withQueryFilter(filters)
                .withConsistentRead(false);

        List<Subscription> subscriptions = this.dynamoMapper.query(Subscription.class, queryExpression);
        return (subscriptions.size() > 0);
    }

    @Override
    public List<Subscription> getSubscriptions(int companyId, boolean includeInactive) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        Subscription subscriptionKey = new Subscription();
        subscriptionKey.setCompanyId(companyId);
        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression<>()
                .withIndexName("CompanyID-StartDate-index")
                .withHashKeyValues(subscriptionKey)
                .withConsistentRead(false)
                .withScanIndexForward(false); // Reverse order (ORDER BY StartDate DESC)

        // Only fetch active subscriptions
        if (!includeInactive) {
            Date now = new Date();

            // Range key condition
            Map<String, Condition> rangeKeyConditions = new HashMap<>(1);
            rangeKeyConditions.put("StartDate", new Condition()
                    .withComparisonOperator(ComparisonOperator.LE.toString())
                    .withAttributeValueList(new AttributeValue().withS(dateFormatter.format(now))));

            queryExpression.withRangeKeyConditions(rangeKeyConditions);

            // Filters
            Map<String, Condition> filters = new HashMap<>(1);

            filters.put("EndDate", new Condition()
                    .withComparisonOperator(ComparisonOperator.GE.toString())
                    .withAttributeValueList(new AttributeValue().withS(dateFormatter.format(now))));

            queryExpression.withQueryFilter(filters);
        }

        return this.dynamoMapper.query(Subscription.class, queryExpression);
    }
}