package dk.better.company.service;

import dk.better.account.service.AccountService;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.company.entity.NpsScore;
import dk.better.company.repository.NpsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;

/**
 * @author vitalii.
 */

@Service
public class NpsServiceImpl implements NpsService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private NpsRepository npsRepository;


    @Transactional
    public void addScore(int companyId, int score, String ipAddress) {
        this.addScore(companyId, score, ipAddress, null);
    }

    @Transactional
    public void addScore(int companyId, int score, String ipAddress, Integer accountId) {
        Assert.notNull(ipAddress);
        if (score >= 0 && score <= 10) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(5, -7);
            Date since = calendar.getTime();
            boolean hasScoredCompanyWithin;
            if (accountId != null) {
                hasScoredCompanyWithin = npsRepository.hasScoredSince(companyId, since, accountId);
            } else {
                hasScoredCompanyWithin = npsRepository.hasScoredSince(companyId, since, ipAddress);
            }

            if (!hasScoredCompanyWithin) {
                NpsScore nps = new NpsScore();
                nps.setScore(score);
                nps.setCreated(new Date());
                nps.setIpAddress(ipAddress);
                if (accountId != null) {
                    nps.setAccount(accountService.getReference(accountId));
                }

                nps.setCompany(companyService.getReference(companyId));
                this.npsRepository.save(nps);
            }

        } else {
            throw new InvalidArgumentException(String.format("Invalid score provided: %d!", score));
        }
    }
}