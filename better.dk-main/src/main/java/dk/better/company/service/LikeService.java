package dk.better.company.service;

import dk.better.company.entity.Like;

/**
 * @author Bo Andersen
 */
public interface LikeService {
    int countByCompanyIdAndAccountId(int companyId, int accountId);

    Like save(Like like);
}