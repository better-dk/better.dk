package dk.better.company.service;

import dk.better.company.entity.Industry;
import dk.better.company.repository.IndustryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Service
public class IndustryServiceImpl implements IndustryService {
    @Autowired
    private IndustryRepository industryRepository;


    @Override
    public List<Industry> findAll() {
        return this.industryRepository.findAll();
    }

    @Override
    public List<Industry> findAll(Sort sort) {
        return this.industryRepository.findAll(sort);
    }

    @Override
    public Industry find(int industryId) {
        return this.industryRepository.findOne(industryId);
    }

    @Override
    public Page<Industry> search(String query, Pageable pageable) {
        Assert.notNull(query);
        return this.industryRepository.findByNameContaining(query.toLowerCase(), pageable);
    }

    @Override
    public Industry save(Industry industry) {
        return this.industryRepository.save(industry);
    }

    @Override
    public Industry saveAndFlush(Industry industry) {
        return this.industryRepository.saveAndFlush(industry);
    }

    @Override
    @Transactional
    public void add(Collection<Industry> industries) {
        this.industryRepository.save(industries);
        this.industryRepository.flush();
    }
}