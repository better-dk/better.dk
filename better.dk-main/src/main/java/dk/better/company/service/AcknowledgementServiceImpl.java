package dk.better.company.service;

import dk.better.company.entity.Acknowledgement;
import dk.better.company.entity.Industry;
import dk.better.company.repository.AcknowledgementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Service
public class AcknowledgementServiceImpl implements AcknowledgementService {
    @Autowired
    private AcknowledgementRepository acknowledgementRepository;

    @Override
    public List<Acknowledgement> findByIndustryIds(Collection<Integer> industryIds) {
        return this.acknowledgementRepository.findByIndustryIds(industryIds);
    }

    @Override
    public List<Acknowledgement> findAll() {
        return this.acknowledgementRepository.findAll();
    }

    @Override
    public List<Acknowledgement> findAll(Sort sort) {
        return this.acknowledgementRepository.findAll(sort);
    }

    @Override
    public List<Acknowledgement> findByIndustryId(int industryId) {
        return this.acknowledgementRepository.findByIndustryId(industryId);
    }

    @Override
    public boolean areLinkedToIndustries(Collection<Acknowledgement> acknowledgements, Collection<Industry> industries) {
        return this.acknowledgementRepository.areLinkedToIndustries(acknowledgements, industries);
    }
}