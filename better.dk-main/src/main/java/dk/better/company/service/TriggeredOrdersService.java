package dk.better.company.service;

import dk.better.company.dto.TriggeredOrderDto;
import dk.better.company.entity.TriggeredOrders;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author Liubov Zavorotna
 */
public interface TriggeredOrdersService {

    List<TriggeredOrders> findByCompanyId(int companyId);

    Page<TriggeredOrders> findByCompanyIdPage(int companyId, int page);

    void insertTriggeredOrder(int companyId,
                              String customerName,
                              String customerEmail,
                              String transactionId);

    void save(TriggeredOrders orders);

    void postToSegmentIO(TriggeredOrderDto dto);

    void postToCustomerIO(TriggeredOrderDto dto);

    void trackWithSegmentIO(TriggeredOrders order);

    void trackWithCustomerIO(TriggeredOrders order);
}
