package dk.better.company.service;

import dk.better.company.entity.Like;
import dk.better.company.repository.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Bo Andersen
 */
@Service
public class LikeServiceImpl implements LikeService {
    @Autowired
    private LikeRepository likeRepository;


    @Override
    public int countByCompanyIdAndAccountId(int companyId, int accountId) {
        return this.likeRepository.countByCompanyIdAndAccountId(companyId, accountId);
    }

    @Override
    public Like save(Like like) {
        return this.likeRepository.save(like);
    }
}