package dk.better.company.service;

import com.github.segmentio.models.Props;
import com.github.segmentio.models.Traits;
import dk.better.account.entity.Account;
import dk.better.account.entity.Role;
import dk.better.account.service.AccountService;
import dk.better.account.util.AccountUtils;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.DateUtils;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.*;
import dk.better.company.repository.ReviewCommentRepository;
import dk.better.company.repository.ReviewRepository;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.ReviewSite;
import dk.better.company.util.ReviewType;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class ReviewServiceImpl implements ReviewService {
    private static final String EVENT_REVIEW_SUBMITTED = "User Made Review";
    private static final String EVENT_REVIEW_COMMENT_COMPANY = "Company Commented On Review";
    private static final String EVENT_REQUEST_REVIEW_DELETION = "Requested Review Deletion";

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ReviewCommentRepository reviewCommentRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CustomerService customerService;


    @Override
    public Review getReference(Object identifier) {
        return this.reviewRepository.getEntityManager().getReference(Review.class, identifier);
    }

    @Override
    public Object getReference(Class type, Object identifier) {
        return this.reviewRepository.getEntityManager().getReference(type, identifier);
    }

    @Override
    public Review find(int id) {
        return this.reviewRepository.findOne(id);
    }

    @Override
    public Page<Review> findByCompanyId(int companyId, Pageable pageable) {
        return this.reviewRepository.findByCompanyId(companyId, pageable);
    }

    @Override
    public List<Review> findByCompanyId(int companyId) {
        return this.reviewRepository.findByCompanyId(companyId);
    }

    @Override
    public List<Review> findLastReviews(int companyId) {
        return this.reviewRepository.findLastAddedReviews(companyId);
    }


    public List<Review> findAllByCompanyId(int companyId) {
        return this.reviewRepository.findAllByCompanyId(companyId);
    }

    @Override
    public Page<Review> findByCompanyId(int companyId, Pageable pageable, ReviewType reviewType) {
        switch (reviewType) {
            case POSITIVE:
                return this.reviewRepository.findByCompanyIdWithPositiveRating(companyId, pageable);
            case NEGATIVE:
                return this.reviewRepository.findByCompanyIdWithNegativeRating(companyId, pageable);
            default:
                return null; // In case more values are added to the enum in the future
        }
    }

    @Override
    @Transactional
    public Review create(int companyId, int rating, String title, String text) throws UniqueConstraintViolationException {
        return this.create(companyId, rating, title, text, null, null, null);
    }

    @Override
    @Transactional
    public Review create(int companyId, int rating, String title, String text, String name, String email) throws UniqueConstraintViolationException {
        return this.create(companyId, rating, title, text, name, email, null);
    }

    @Override
    @Transactional
    public Review create(int companyId, int rating, String title, String text, String name, String email, String phoneNumber) throws UniqueConstraintViolationException {
        Assert.isTrue(rating >= 1 && rating <= 5);
        boolean isNewUser = false;
        Review review = new Review();

        if (AuthUtils.isAuthenticated()) {
            review.setAccount(this.accountService.findByEmail(AuthUtils.getPrincipal().getEmail()));
        } else {
            if (email != null && email.length() > 0) {
                Account existing = this.accountService.findByEmail(email);

                if (existing != null) {
                    // Only use existing account if it does not have a password and Facebook ID (helps prevent abuse)
                    if (existing.getPassword() != null || existing.getFacebookProfileId() != null) {
                        throw new UniqueConstraintViolationException("An account already exists with this e-mail address. Log in or use a different e-mail address");
                    }

                    review.setAccount(existing);
                } else {
                    Account account = new Account();
                    account.setUuid(AccountService.generateUUID());
                    account.add((Role) this.getReference(Role.class, AccountService.ROLE_USER_ID)); // Add ROLE_USER as default
                    account.setEmail(email);
                    account.setEnabled(false);

                    if (name != null) {
                        Map<String, String> nameParts = AccountUtils.getNameParts(name);
                        account.setFirstName(nameParts.get(AccountUtils.FIRST_NAME));
                        account.setMiddleName(nameParts.get(AccountUtils.MIDDLE_NAME));
                        account.setLastName(nameParts.get(AccountUtils.LAST_NAME));
                    }

                    review.setAccount(account);
                    isNewUser = true;
                }
            }
        }

        // Link to existing customer
        if (phoneNumber != null) {
            Customer customer = this.customerService.find(companyId, phoneNumber);
            review.setCustomer((customer != null ? customer : null));
        }

        review.setUuid(GeneralUtils.getRandomUUID());
        review.setTitle(title);
        review.setText(text);
        review.setRating(rating);
        review.setCompany(this.reviewRepository.getEntityManager().getReference(Company.class, companyId));

        Review saved = this.save(review);
        this.companyService.incrementReviewCount(companyId);
        this.companyService.updateReviewRating(companyId);

        // We fetch the company after refreshing the review count and rating such that we refresh the search index
        // with the updated data (because the object in the entity manager is not synchronized after running JPQL queries)
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("A company with ID: %d could not be found!", companyId));
        }

        this.companyService.refreshSearchIndex(company);

        Traits traits = new Traits();
        traits.put("reviewerPhoneNumber", phoneNumber);
        traits.put("review", new Traits()
                .put("id", saved.getId())
                .put("uuid", saved.getUuid())
                .put("rating", saved.getRating())
                .put("title", saved.getTitle())
                .put("text", saved.getText()));

        traits.put("company", new Traits()
                .put("id", company.getId())
                .put("name", company.getName())
                .put("profile_url", company.getProfileUrl())
                .put("is_claimed", company.getAccountCompanies().size() > 0)
                .put("email", company.getEmail())
                .put("phone_number", company.getPhoneNumber())
                .put("call_tracking_number", company.getCallTrackingNumber())
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries()))
                .put("affiliate", (company.getBookingUrl() != null ? "hungry" : null)));

        this.analyticsService.track((review.getAccount() != null ? review.getAccount().getUuid() : AnalyticsService.ANONYMOUS_USER_ID), EVENT_REVIEW_SUBMITTED, traits);

        // Add new user to group
        if (isNewUser) {
            Account account = review.getAccount();
            String analyticsFirstName = ((account.getMiddleName() != null) ? account.getFirstName() + " " + account.getMiddleName() : account.getFirstName());

            this.analyticsService.identify(account.getUuid(), new Traits()
                    .put("firstName", analyticsFirstName)
                    .put("lastName", account.getLastName())
                    .put("email", account.getEmail())
                    .put("createdAt", account.getCreated().getTime()));

            Traits groupTraits = new Traits();
            traits.put("name", "Partial Users");
            this.analyticsService.group(account.getUuid(), String.valueOf(AnalyticsService.GROUP_PARTIAL_USER_ID), groupTraits);
        }

        return saved;
    }

    @Override
    public Review save(Review review) {
        return this.reviewRepository.save(review);
    }

    @Override
    public Review saveAndFlush(Review review) {
        return this.reviewRepository.saveAndFlush(review);
    }

    @Override
    public Page<Review> find(Pageable pageable) {
        return this.reviewRepository.find(pageable);
    }

    @Override
    public Map<Long, Long> getRatingDistribution(int companyId) {
        return this.reviewRepository.getRatingDistribution(companyId);
    }

    @Override
    public List<ReviewComment> getComments(int reviewId) {
        return this.reviewCommentRepository.getComments(reviewId);
    }

    @Override
    @Transactional
    public ReviewComment addCommentAsCompany(int reviewId, String text) {
        Assert.notNull(text);
        Account principal = AuthUtils.getPrincipal();
        Review review = this.reviewRepository.findOne(reviewId);

        if (review == null) {
            throw new InvalidArgumentException(String.format("Review not found (ID :%d)!", reviewId));
        }

        if (!CompanyUtils.isCompanyOwner(principal, review.getCompany().getId())) {
            throw new AccessDeniedException(String.format("The user (ID: %d) does not have permissions to comment on this review for company (ID: %d)!", principal.getId(), review.getCompany().getId()));
        }

        if (review.getComments().size() > 0) {
            throw new RuntimeException(String.format("Only one comment may be added per review (ID: %d)", reviewId));
        }

        Company company = review.getCompany();
        ReviewComment comment = new ReviewComment();
        comment.setText(text);
        comment.setCreated(new Date());
        comment.setReview(review);
        comment.setAccount(principal);
        comment.setCompany(company);

        ReviewComment saved = this.reviewCommentRepository.save(comment);

        // Analytics
        Account reviewer = review.getAccount();
        Props props = new Props();
        props.put("review", new Props()
                .put("id", review.getId())
                .put("uuid", review.getUuid()));

        props.put("comment", new Props()
                .put("text", text));

        props.put("company", new Props()
                .put("id", company.getId())
                .put("name", company.getName())
                .put("city", company.getCity())
                .put("postalCode", company.getPostalCode())
                .put("profileUrl", company.getProfileUrl())
                .put("email", company.getEmail())
                .put("phoneNumber", company.getPhoneNumber())
                .put("callTrackingNumber", company.getCallTrackingNumber())
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));

        props.put("reviewer", new Props()
                .put("firstName", reviewer.getFirstName())
                .put("lastName", reviewer.getLastName())
                .put("email", reviewer.getEmail()));

        this.analyticsService.track(principal.getUuid(), EVENT_REVIEW_COMMENT_COMPANY, props);
        return saved;
    }

    @Override
    public boolean hasAccessToReview(int reviewId, int accountId) {
        return this.reviewRepository.hasAccessToReview(reviewId, accountId);
    }

    @Override
    public Review findByUuid(String uuid) {
        Assert.notNull(uuid);
        return this.reviewRepository.findByUuid(uuid);
    }

    @Override
    public Review findByUuid(int companyId, String uuid) {
        Assert.notNull(uuid);
        return this.reviewRepository.findByUuid(companyId, uuid);
    }

    @Override
    @Transactional
    public void requestDeletion(String reviewUuid, String reason) {
        Assert.notNull(reason);
        Review review = this.findByUuid(reviewUuid);
        Account principal = AuthUtils.getPrincipal();

        if (review == null) {
            throw new ResourceNotFoundException(String.format("Review not found (UUID: %s)!", reviewUuid));
        }

        if (!CompanyUtils.isCompanyOwner(principal, review.getCompany().getId())) {
            throw new AccessDeniedException(String.format("The user (ID: %d) does not have access to this review (ID: %d)", principal.getId(), review.getId()));
        }

        Account reviewer = review.getAccount();
        Props props = new Props();
        props.put("reason", reason);
        props.put("review", new Props()
                .put("id", review.getId())
                .put("uuid", review.getUuid())
                .put("rating", review.getRating())
                .put("title", review.getTitle())
                .put("text", review.getText())
                .put("reviewer", new Props()
                        .put("id", reviewer.getId())
                        .put("uuid", reviewer.getUuid())
                        .put("email", reviewer.getEmail())
                        .put("firstName", reviewer.getFirstName())
                        .put("middleName", reviewer.getMiddleName())
                        .put("lastName", reviewer.getLastName())
                        .put("fullName", AccountUtils.getFullName(reviewer.getFirstName(), reviewer.getMiddleName(), reviewer.getLastName()))
                )
        );
        this.analyticsService.track(principal.getUuid(), EVENT_REQUEST_REVIEW_DELETION, props);
    }

    @Override
    @Transactional
    public Integer inviteToReview(int companyId, Collection<String> emails, boolean addToCustomerList, CustomerService.Type type) {
        Assert.notNull(emails);

        if (emails.size() > 500) {
            throw new RuntimeException("Maximum 500 customers may be invited at a time!");
        }

        Account principal = AuthUtils.getPrincipal();
        Company company = this.companyService.find(companyId);
        Integer importedCount = null;

        if (company == null) {
            throw new ResourceNotFoundException(String.format("Company not found (ID: %d)", companyId));
        }

        if (!CompanyUtils.isCompanyOwner(principal, companyId)) {
            throw new AccessDeniedException(String.format("This account (ID: %d) is not allowed to access this company (ID: %d)", principal.getId(), companyId));
        }

        if (addToCustomerList) {
            importedCount = this.customerService.importEmails(companyId, emails, type);
        }

        Props props = new Props();
        props.put("emails", emails);
        props.put("hasAddedToCustomerList", addToCustomerList);
        props.put("company", new Props()
                .put("id", companyId)
                .put("name", company.getName())
                .put("city", company.getCity())
                .put("postalCode", company.getPostalCode())
                .put("profileUrl", company.getProfileUrl())
                .put("email", company.getEmail())
                .put("phoneNumber", company.getPhoneNumber())
                .put("callTrackingNumber", company.getCallTrackingNumber())
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));

        this.analyticsService.track(principal.getUuid(), ReviewService.EVENT_REVIEW_INVITE, props);
        return importedCount;
    }

    @Override
    public String getReviewRedirectUrl(CompanyMeta meta, String userAgent) {
        if (meta == null) {
            return null;
        }

        UserAgent agent = UserAgent.parseUserAgentString(userAgent);
        Map<ReviewSite, Double> weights = new HashMap<>();
        weights.put(ReviewSite.BETTERDK, 5.0);

        if (meta.getGooglePlusUrl() != null && agent.getOperatingSystem() != OperatingSystem.IOS) {
            weights.put(ReviewSite.GOOGLE_PLUS, 3.0);
        }

        if (meta.getFacebookUrl() != null) weights.put(ReviewSite.FACEBOOK, 2.0);
        if (meta.getYelpUrl() != null) weights.put(ReviewSite.YELP, 1.0);
        if (meta.getKrakdkUrl() != null) weights.put(ReviewSite.KRAKDK, 1.0);
        if (meta.getBedoemmelsedkUrl() != null) weights.put(ReviewSite.BEDOEMMELSEDK, 1.0);
        if (meta.getOmdoemmedkUrl() != null) weights.put(ReviewSite.OMDOEMMEDK, 1.0);

        Double totalWeight = 0.0;

        // Construct priorities map (from where we will
        Map<Double, ReviewSite> priorities = new HashMap<>(weights.size());

        for (Map.Entry<ReviewSite, Double> entry : weights.entrySet()) {
            priorities.put(totalWeight + entry.getValue(), entry.getKey());
            totalWeight += entry.getValue();
        }

        Random r = new Random();
        double minimum = 0;
        double maximum = totalWeight;
        double randomValue = (minimum + (maximum - minimum) * r.nextDouble());
        ReviewSite chosen = ReviewSite.BETTERDK;

        for (Map.Entry<Double, ReviewSite> entry : priorities.entrySet()) {
            if (randomValue < entry.getKey()) {
                chosen = entry.getValue();
                break;
            }
        }

        switch (chosen) {
            case BETTERDK:
                return null;
            case GOOGLE_PLUS:
                return meta.getGooglePlusUrl() + "?review=1";
            case FACEBOOK:
                return meta.getFacebookUrl();
            case YELP:
                return meta.getYelpUrl();
            case KRAKDK:
                return meta.getKrakdkUrl();
            case BEDOEMMELSEDK:
                return meta.getBedoemmelsedkUrl();
            case OMDOEMMEDK:
                return meta.getOmdoemmedkUrl();
            default:
                return null;
        }
    }

    @Override
    @Transactional
    public void sendEmailToReviewedCompany(int reviewId) {
        Review review = this.reviewRepository.findOne(reviewId);

        if (review == null) {
            throw new ResourceNotFoundException(String.format("Review not found (ID %d)!", reviewId));
        }

        // Trigger send e-mail event
        Account reviewer = review.getAccount();
        Company company = review.getCompany();
        String companyEmail = company.getEmail();

        if (companyEmail == null || companyEmail.isEmpty()) {
            throw new RuntimeException(String.format("The reviewed company does not have an e-mail address (company ID: %d)", company.getId()));
        }

        Props props = new Props();
        props.put("review", new Props()
                .put("uuid", review.getUuid())
                .put("title", review.getTitle())
                .put("rating", review.getRating())
                .put("text", review.getText()));

        props.put("company", new Props()
                .put("id", company.getId())
                .put("name", company.getName())
                .put("profileUrl", company.getProfileUrl())
                .put("city", company.getCity())
                .put("postalCode", company.getPostalCode())
                .put("phoneNumber", company.getPhoneNumber())
                .put("callTrackingNumber", company.getCallTrackingNumber())
                .put("email", companyEmail)
                .put("accessToken", company.getAccessToken())
                .put("checksum", CompanyUtils.generateChecksum(company.getId()))
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries()))
                .put("isClaimed", (company.getAccountCompanies().size() > 0))
                .put("foundingDate", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

        if (reviewer != null) {
            props.put("reviewer", new Props()
                    .put("email", reviewer.getEmail())
                    .put("firstName", reviewer.getFirstName())
                    .put("middleName", reviewer.getMiddleName())
                    .put("lastName", reviewer.getLastName())
                    .put("fullName", AccountUtils.getFullName(reviewer.getFirstName(), reviewer.getMiddleName(), reviewer.getLastName())));
        }

        this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Send Company Reviewed E-mail", props);
    }
}