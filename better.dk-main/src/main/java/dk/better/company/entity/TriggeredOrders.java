package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Liubov Zavorotna
 */
@Entity
@Table(name = "triggered_orders")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggeredOrders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "company_id")
    private int companyId;

    @Column(name = "customer_name", nullable = true)
    private String customerName;

    @Column(name = "customer_email", nullable = true)
    private String customerEmail;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "invite_date", insertable = false)
    private Date inviteDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getInviteDate() {
        return inviteDate;
    }

    public void setInviteDate(Date inviteDate) {
        this.inviteDate = inviteDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    @Override
    public String toString() {
        return "TriggeredOrders{" +
                "id=" + id +
                ", companyId=" + companyId +
                ", customerName='" + customerName + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", inviteDate=" + inviteDate +
                '}';
    }
}
