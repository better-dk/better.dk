package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * @author Liubov Zavorotna
 */
@Entity
@Table(name = "send_delay")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendDelay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "company_id")
    private int companyId;

    @Column(name = "hours", nullable = true)
    private int hours;

    @Override
    public String toString() {
        return "SendDelay{" +
                "id=" + id +
                ", companyId=" + companyId +
                ", hours=" + hours +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
}
