package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "keyword")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Keyword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Length(min = 3, max = 50)
    @Column(length = 50, nullable = false)
    private String name;

    @Column(nullable = false)
    private Date created = new Date();


    public Keyword() {
    }

    public Keyword(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}