package dk.better.company.entity;

import dk.better.account.entity.Account;

import javax.persistence.*;
import java.util.Date;

/**
 * @author vitalii.
 */
@Entity
@Table(name = "nps_score")
public class NpsScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column(nullable = false)
    private int score;
    @Column(nullable = false)
    private Date created;
    @Column(name = "ip_address",
            length = 50,
            nullable = false)
    private String ipAddress;
    @ManyToOne(fetch = FetchType.LAZY,
            targetEntity = Company.class,
            optional = false)
    private Company company;
    @ManyToOne(fetch = FetchType.LAZY,
            targetEntity = Account.class,
            optional = true)
    private Account account;

    public NpsScore() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Account getAccount() {
        return this.account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
