package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.Account;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "review")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(length = 36, nullable = false)
    private String uuid;

    @Range(min = 1, max = 5)
    @Column(nullable = false)
    private int rating;

    @NotEmpty
    @Length(min = 5, max = 50)
    @Column(nullable = true)
    private String title;

    @NotEmpty
    @Length(min = 5, max = 5000)
    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private Date created = new Date();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class, optional = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class, optional = false)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class, optional = true)
    private Customer customer;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ReviewComment.class, mappedBy = "review")
    private Set<ReviewComment> comments;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, targetEntity = Vote.class, mappedBy = "review")
    private Set<Vote> votes = new HashSet<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Vote> getVotes() {
        return votes;
    }

    public void setVotes(Set<Vote> votes) {
        this.votes = votes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<ReviewComment> getComments() {
        return comments;
    }

    public void setComments(Set<ReviewComment> comments) {
        this.comments = comments;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}