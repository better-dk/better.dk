package dk.better.company.entity.identity;

import dk.better.company.entity.Customer;
import dk.better.company.entity.TextMessage;

import java.io.Serializable;

/**
 * @author Bo Andersen
 */
public class TextMessageCustomerPK implements Serializable {
    private TextMessage textMessage;
    private Customer customer;


    public TextMessageCustomerPK() {
    }

    public TextMessageCustomerPK(TextMessage textMessage, Customer customer) {
        this.textMessage = textMessage;
        this.customer = customer;
    }

    public TextMessage getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(TextMessage textMessage) {
        this.textMessage = textMessage;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int hashCode() {
        int result = ((this.textMessage != null) ? this.textMessage.hashCode() : 0);
        result = 31 * result + (this.customer != null ? this.customer.hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || !(obj instanceof TextMessageCustomerPK)) {
            return false;
        }

        TextMessageCustomerPK textMessageCustomerPK = (TextMessageCustomerPK) obj;

        return ((textMessageCustomerPK.getTextMessage().getId() == this.textMessage.getId())
                && textMessageCustomerPK.getCustomer().getId() == this.customer.getId());
    }
}