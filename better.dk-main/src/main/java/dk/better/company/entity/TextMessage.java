package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.Account;
import dk.better.company.service.TextMessageService;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "text_message")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false, length = 160)
    private String text;

    @Column(nullable = false)
    private Date created = new Date();

    @Column(nullable = false)
    private int status = TextMessageService.STATUS_PENDING_REVIEW;

    @Column(name = "number_of_recipients", nullable = false)
    private int numberOfRecipients;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class, optional = false)
    @JoinColumn(name = "sender_company_id")
    private Company senderCompany;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class, optional = false)
    @JoinColumn(name = "sender_account_id")
    private Account senderAccount;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "textMessage", cascade = CascadeType.PERSIST)
    private Set<TextMessageCustomer> textMessageCustomers;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getNumberOfRecipients() {
        return numberOfRecipients;
    }

    public void setNumberOfRecipients(int numberOfRecipients) {
        this.numberOfRecipients = numberOfRecipients;
    }

    public Company getSenderCompany() {
        return senderCompany;
    }

    public void setSenderCompany(Company senderCompany) {
        this.senderCompany = senderCompany;
    }

    public Account getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(Account senderAccount) {
        this.senderAccount = senderAccount;
    }

    public Set<TextMessageCustomer> getTextMessageCustomers() {
        return textMessageCustomers;
    }

    public void setTextMessageCustomers(Set<TextMessageCustomer> textMessageCustomers) {
        this.textMessageCustomers = textMessageCustomers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}