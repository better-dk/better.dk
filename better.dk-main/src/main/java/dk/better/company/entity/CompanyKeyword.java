package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.company.entity.identity.CompanyKeywordPK;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "company_keyword")
@IdClass(CompanyKeywordPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyKeyword implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class)
    @JoinColumn(name = "company_id")
    private Company company;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Keyword.class)
    @JoinColumn(name = "keyword_id")
    private Keyword keyword;

    @Column(name = "is_active", nullable = true)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isActive = false;

    @Column(nullable = false)
    private Date created = new Date();


    public CompanyKeyword() {
    }

    public CompanyKeyword(Company company, Keyword keyword) {
        this.company = company;
        this.keyword = keyword;
    }

    public CompanyKeyword(Company company, Keyword keyword, boolean isActive) {
        this.company = company;
        this.keyword = keyword;
        this.isActive = isActive;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Keyword getKeyword() {
        return keyword;
    }

    public void setKeyword(Keyword keyword) {
        this.keyword = keyword;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}