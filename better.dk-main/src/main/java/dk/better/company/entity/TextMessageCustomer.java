package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.company.entity.identity.TextMessageCustomerPK;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "text_message_customer")
@IdClass(TextMessageCustomerPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextMessageCustomer implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = TextMessage.class)
    @JoinColumn(name = "text_message_id")
    private TextMessage textMessage;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(length = 36, nullable = false, unique = true)
    private String uuid;

    @Column(name = "status_update", nullable = true)
    private Date statusUpdate = null;

    @Column(nullable = true)
    private Integer status = null;


    public TextMessageCustomer() {
    }

    public TextMessageCustomer(TextMessage textMessage, Customer customer, String uuid) {
        this.textMessage = textMessage;
        this.customer = customer;
        this.uuid = uuid;
    }

    public TextMessage getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(TextMessage textMessage) {
        this.textMessage = textMessage;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date isTimeSent() {
        return statusUpdate;
    }

    public void setStatusUpdate(Date isSent) {
        this.statusUpdate = isSent;
    }

    public Date getStatusUpdate() {
        return statusUpdate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}