package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author vitalii.
 */
@Entity
@Table(name = "external_reviews")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalReview implements Serializable {

    private static final long serialVersionUID = -7049957706738879274L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private int companyId;

    @Column
    private String author;

    @Column
    private int rating;

    @Column
    private String review;

    @Column
    private String link;

    @Column
    private String site;

    @Column
    private String publishDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public ExternalReview(String author, int rating, String review, String site, String publishDate) {
        this.author = author;
        this.rating = rating;
        this.review = review;
        this.site = site;
        this.publishDate = publishDate;
    }

    public ExternalReview() {
    }
}
