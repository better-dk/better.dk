package dk.better.company.entity;

import dk.better.company.entity.converter.PageTypeConverter;
import dk.better.company.util.PageType;
import org.hibernate.annotations.FilterDef;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@FilterDef(name = "rootPages", defaultCondition = "parent is null")
public class Page {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false, length = 50)
    private String name;

    @Column(nullable = false, length = 25)
    private String slug;

    @Column(nullable = false, length = 50)
    private String title;

    @Column(nullable = true)
    private String content;

    @Column(nullable = true)
    private String data;

    @Column(name = "page_type", nullable = false)
    @Convert(converter = PageTypeConverter.class)
    private PageType pageType;

    @Column(nullable = false)
    private Date created = new Date();

    @Column(name = "last_modified", nullable = false)
    private Date lastModified = new Date();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id")
    private Page parent;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Page.class, mappedBy = "parent")
    private Set<Page> children = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, targetEntity = PageRevision.class, cascade = CascadeType.PERSIST, mappedBy = "page")
    private Set<PageRevision> revisions = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class, optional = false)
    private Company company;


    public void add(PageRevision revision) {
        this.revisions.add(revision);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Page getParent() {
        return parent;
    }

    public void setParent(Page parent) {
        this.parent = parent;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<Page> getChildren() {
        return children;
    }

    public void setChildren(Set<Page> children) {
        this.children = children;
    }

    public Set<PageRevision> getRevisions() {
        return revisions;
    }

    public void setRevisions(Set<PageRevision> revisions) {
        this.revisions = revisions;
    }

    public PageType getPageType() {
        return pageType;
    }

    public void setPageType(PageType pageType) {
        this.pageType = pageType;
    }
}