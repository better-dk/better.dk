package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "industry", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"})
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Industry implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @NotEmpty
    @Length(min = 3, max = 100)
    @Pattern(regexp = "^[a-zA-ZæøåÆØÅéÉ\\-\\.\\s,/:()']+$", message = "{industry.invalid.format}")
    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 250, nullable = false, name = "quote_link")
    private String quoteLink;

    @JsonIgnore
    @ManyToMany(mappedBy = "industries")
    private Set<Service> services = new HashSet<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "industries")
    private Set<Acknowledgement> acknowledgements = new HashSet<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public Set<Acknowledgement> getAcknowledgements() {
        return acknowledgements;
    }

    public void setAcknowledgements(Set<Acknowledgement> acknowledgements) {
        this.acknowledgements = acknowledgements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Industry)) return false;

        Industry industry = (Industry) o;

        if (id != industry.id) return false;

        return true;
    }

    public String getQuoteLink() {
        return quoteLink;
    }

    public void setQuoteLink(String quoteLink) {
        this.quoteLink = quoteLink;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
