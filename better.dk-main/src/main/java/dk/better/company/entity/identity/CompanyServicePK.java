package dk.better.company.entity.identity;

import dk.better.company.entity.Company;
import dk.better.company.entity.Service;

import java.io.Serializable;

/**
 * @author Bo Andersen
 */
public class CompanyServicePK implements Serializable {
    private Company company;
    private Service service;

    public CompanyServicePK() {
    }

    public CompanyServicePK(Company company, Service service) {
        this.company = company;
        this.service = service;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public int hashCode() {
        int result = ((this.company != null) ? this.company.hashCode() : 0);
        result = 31 * result + (this.service != null ? this.service.hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || !(obj instanceof CompanyServicePK)) {
            return false;
        }

        CompanyServicePK companyServicePK = (CompanyServicePK) obj;

        return ((companyServicePK.getCompany().getId() == this.company.getId())
                && companyServicePK.getService().getId() == this.service.getId());
    }
}