package dk.better.company.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

/**
 * @author vitalii.
 */
@Entity
@DynamoDBTable(
        tableName = "Integrations"
)
public class Integration {
    @Id
    @DynamoDBHashKey(
            attributeName = "CompanyID"
    )
    private int companyId;
    @DynamoDBRangeKey(
            attributeName = "IntegrationID"
    )
    @Transient
    private int integrationId;
    @DynamoDBAttribute(
            attributeName = "Created"
    )
    @Transient
    private Date created;
    @DynamoDBAttribute(
            attributeName = "LastModified"
    )
    @Transient
    private Date lastModified;
    @DynamoDBAttribute(
            attributeName = "IsActive"
    )
    @Transient
    private boolean isActive;
    @DynamoDBAttribute(
            attributeName = "MetaData"
    )
    @Transient
    private Map<String, Object> metaData;
    @DynamoDBIgnore
    private Company company;

    public Integration() {
    }

    public int getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getIntegrationId() {
        return this.integrationId;
    }

    public void setIntegrationId(int integrationId) {
        this.integrationId = integrationId;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return this.lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Map<String, Object> getMetaData() {
        return this.metaData;
    }

    public void setMetaData(Map<String, Object> metaData) {
        this.metaData = metaData;
    }

    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
