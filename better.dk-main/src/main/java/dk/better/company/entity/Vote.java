package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.Account;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "vote")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false)
    private Date created = new Date();

    @Column(name = "is_upvote", nullable = false)
    private boolean isUpvote;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class, optional = false)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Review.class, optional = false)
    private Review review;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean isUpvote() {
        return isUpvote;
    }

    public void setUpvote(boolean isUpvote) {
        this.isUpvote = isUpvote;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
}