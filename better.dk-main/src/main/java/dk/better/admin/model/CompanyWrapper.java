package dk.better.admin.model;

import dk.better.company.entity.Acknowledgement;
import dk.better.company.entity.Company;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Bo Andersen
 */
public class CompanyWrapper {
    @Valid
    private Company company;

    private List<Acknowledgement> acknowledgements;


    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Acknowledgement> getAcknowledgements() {
        return acknowledgements;
    }

    public void setAcknowledgements(List<Acknowledgement> acknowledgements) {
        this.acknowledgements = acknowledgements;
    }
}