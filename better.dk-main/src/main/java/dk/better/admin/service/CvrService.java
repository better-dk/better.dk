package dk.better.admin.service;

import dk.better.company.entity.Company;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface CvrService {
    List<Company> getCompanies(Resource companiesCSV, Resource industryMappingCSV) throws IOException;

    int importCompanies(Resource companiesCSV, Resource industryMappingCSV) throws IOException;
}