package dk.better.admin.service;

import com.amazonaws.services.logs.model.LogGroup;
import com.amazonaws.services.logs.model.OutputLogEvent;

import java.util.List;

/**
 * @author Bo Andersen
 */
public interface LogService {
    List<OutputLogEvent> getLogs(String logGroupName);

    List<LogGroup> getLogGroups();
}