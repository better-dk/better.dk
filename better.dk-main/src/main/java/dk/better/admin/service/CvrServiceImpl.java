package dk.better.admin.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Industry;
import dk.better.company.service.CompanyService;
import dk.better.company.service.IndustryService;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.*;
import javax.validation.metadata.ConstraintDescriptor;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class CvrServiceImpl implements CvrService {
    @Autowired
    private IndustryService industryService;

    @Autowired
    private CompanyService companyService;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${application.domain.name}")
    private String domainName;

    private Map<Integer, Integer> industryMapping = new HashMap<>(); // K = CVR industry ID, V = Local industry ID
    private Map<Integer, Industry> industries = new HashMap<>(); // K = Industry ID


    @Override
    @Transactional
    public int importCompanies(Resource companiesCSV, Resource industryMappingCSV) throws IOException {
        List<Company> companies = this.getCompanies(companiesCSV, industryMappingCSV);

        if (!companies.isEmpty()) {
            this.companyService.add(companies);
            this.companyService.initializeProfileUrls("https", this.domainName);
        }

        return companies.size();
    }

    @Override
    public List<Company> getCompanies(Resource companiesCSV, Resource industryMappingCSV) throws IOException {
        if (!companiesCSV.isReadable() || !industryMappingCSV.isReadable()) {
            throw new InvalidArgumentException("Cannot read companies CSV or industry mapping CSV!");
        }

        this.loadIndustryMapping(industryMappingCSV);
        this.loadIndustries();

        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        CsvMapper mapper = new CsvMapper();
        MappingIterator<Map<String, String>> mappingIterator = mapper.reader(Map.class).with(schema).readValues(companiesCSV.getFile());
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        List<Map<String, String>> rows = mappingIterator.readAll();
        List<Company> companies = new ArrayList<>(rows.size());

        for (Map<String, String> row : rows) {
            Company c = this.getCompanyFromRow(row);

            if (c != null) {
                Set<ConstraintViolation<Company>> errors = validator.validate(c);

                if (errors.isEmpty()) {
                    companies.add(c);
                } else {
                    ConstraintViolation<Company> firstError = errors.iterator().next();

                    // If the only error is an invalid e-mail address, then clear the e-mail, add the company and move on
                    if (errors.size() == 1) {
                        ConstraintDescriptor constraintDescriptor = firstError.getConstraintDescriptor();
                        Annotation annotation = constraintDescriptor.getAnnotation();

                        if (annotation.annotationType().isAssignableFrom(Email.class)) {
                            c.setEmail(null);
                            companies.add(c);
                            continue;
                        }
                    }

                    String message = "One or more errors occurred while validating company (%s) (First error: %s. Class: %s. Field: %s. Value: %s)";
                    message = String.format(message, c.getName(), firstError.getMessage(), firstError.getLeafBean(), firstError.getPropertyPath().toString(), firstError.getInvalidValue());
                    throw new ConstraintViolationException(message, errors);
                }
            }
        }

        // Clear mappings to save memory and to avoid "Detached entity passed to persist" -exception
        this.industryMapping.clear();
        this.industries.clear();

        return companies;
    }

    private Company getCompanyFromRow(Map<String, String> row) {
        StringEmptyOperation seo = (String s) -> {
            return ((s == null || s.isEmpty()) ? null : s);
        };

        // Declare needed variables
        String companyName = row.get("navn_tekst").trim().replaceAll(" +", " ");
        String streetName = row.get("beliggenhedsadresse_vejnavn");

        if (row.get("beliggenhedsadresse_postnr").equals("") || streetName.equals("") || (companyName.equals("") || companyName.length() > 150)) {
            return null;
        }

        int postalCode = Integer.parseInt(row.get("beliggenhedsadresse_postnr"));
        String cityName = row.get("beliggenhedsadresse_bynavn");
        String phoneNumber = seo.nullifyIfEmpty(row.get("telefonnummer_kontaktoplysning"));
        String houseNumberTo = row.get("beliggenhedsadresse_husnummerTil");
        String houseLetterTo = row.get("beliggenhedsadresse_bogstavTil");
        String addressFloor = row.get("beliggenhedsadresse_etage");
        String addressDoor = row.get("beliggenhedsadresse_sidedoer");
        String industry1 = row.get("bibranche1_kode");
        String industry2 = row.get("bibranche2_kode");
        String industry3 = row.get("bibranche3_kode");

        // Begin mapping
        Company company = new Company();
        company.setAccessToken(GeneralUtils.getRandomUUID());
        company.setName(seo.nullifyIfEmpty(companyName));
        company.setPhoneNumber(((phoneNumber != null && (phoneNumber.length() < 8 || phoneNumber.length() > 11)) ? null : phoneNumber));
        company.setCiNumber(Long.parseLong(row.get("virksomhed_cvrnr")));
        company.setPostBox(seo.nullifyIfEmpty(row.get("beliggenhedsadresse_postboks")));
        company.setCoName(seo.nullifyIfEmpty(row.get("beliggenhedsadresse_coNavn")));
        company.setAdditionalAddressText(seo.nullifyIfEmpty(row.get("beliggenhedsadresse_adresseFritekst")));
        company.setPnr(Long.valueOf(row.get("pnr")));
        company.setPostalCode(postalCode);
        company.setPostalDistrict(row.get("beliggenhedsadresse_postdistrikt"));
        company.setMunicipal(row.get("beliggenhedsadresse_kommune_tekst"));
        company.setStreetName(streetName);
        String streetNumber = row.get("beliggenhedsadresse_husnummerFra");
        streetNumber += (!houseNumberTo.isEmpty() ? "-" + houseNumberTo : "");

        if (!cityName.equals("")) {
            company.setCity(cityName);
        } else {
            company.setCity(row.get("beliggenhedsadresse_postdistrikt"));
        }

        if (!houseLetterTo.isEmpty()) {
            streetNumber += " ";
        }

        streetNumber += row.get("beliggenhedsadresse_bogstavFra");
        streetNumber += (!houseLetterTo.isEmpty() ? "-" + houseLetterTo : "");

        if (!addressFloor.isEmpty()) {
            streetNumber += " " + addressFloor;
            streetNumber += (!addressDoor.isEmpty() ? ", " + addressDoor : "");
        }

        if (streetName.isEmpty() || streetNumber.isEmpty()) {
            return null;
        }

        company.setStreetNumber(streetNumber);
        company.setEmail(seo.nullifyIfEmpty(row.get("email_kontaktoplysning")));

        // Founding date
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            company.setFoundingDate(df.parse(row.get("livsforloeb_startdato")));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // Add main industry
        String tempMainIndustryId = row.get("hovedbranche_kode");
        Industry mainIndustry;
        int mainIndustryLocalId;

        if (!tempMainIndustryId.equals("")) {
            int mainIndustryId = Integer.valueOf(tempMainIndustryId);

            // Look up industry in mapping
            if (this.industryMapping.containsKey(mainIndustryId)) {
                mainIndustryLocalId = this.industryMapping.get(mainIndustryId);
            } else {
                mainIndustryLocalId = IndustryService.UNKNOWN_INDUSTRY_ID;
            }
        } else {
            mainIndustryLocalId = IndustryService.EMPTY_INDUSTRY_ID;
        }

        /*if (!this.industries.containsKey(mainIndustryLocalId)) {
            throw new RuntimeException(String.format("Local industry ID not found (ID: %d)", mainIndustryLocalId));
        }*/

        //mainIndustry = this.industries.get(mainIndustryLocalId);
        mainIndustry = this.entityManager.getReference(Industry.class, mainIndustryLocalId);
        company.addIndustry(mainIndustry);

        // Add more industries if applicable
        IndustryOperation io = (String industryId) -> {
            if (industryId != null && !industryId.isEmpty()) {
                int parsedIndustryId = Integer.valueOf(industryId);
                int localIndustryId;

                if (parsedIndustryId != IndustryService.UNDISCLOSED_INDUSTRY_ID) {
                    if (this.industryMapping.containsKey(parsedIndustryId)) {
                        localIndustryId = this.industryMapping.get(parsedIndustryId);
                    } else {
                        localIndustryId = IndustryService.UNKNOWN_INDUSTRY_ID; // improve: Handle if this occurs more than once?
                    }

                    if (this.industries.containsKey(localIndustryId)) {
                        //company.addIndustry(this.industries.get(localIndustryId));
                        company.addIndustry(this.entityManager.getReference(Industry.class, localIndustryId));
                    }
                }
            }
        };

        io.addIfNotEmpty(industry1);
        io.addIfNotEmpty(industry2);
        io.addIfNotEmpty(industry3);

        return company;
    }

    private void loadIndustryMapping(Resource mappingCSV) throws IOException {
        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
        MappingIterator<String[]> mappingIterator = mapper.reader(String[].class).readValues(mappingCSV.getFile());
        List<String[]> rows = mappingIterator.readAll();

        for (String[] row : rows) {
            this.industryMapping.put(Integer.parseInt(row[0]), Integer.parseInt(row[1]));
        }
    }

    private void loadIndustries() {
        List<Industry> industries = this.industryService.findAll();

        for (Industry industry : industries) {
            this.industries.put(industry.getId(), industry);
        }
    }

    interface IndustryOperation {
        void addIfNotEmpty(String industryId);
    }

    interface StringEmptyOperation {
        String nullifyIfEmpty(String s);
    }
}