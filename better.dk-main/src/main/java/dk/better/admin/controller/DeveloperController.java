package dk.better.admin.controller;

import dk.better.application.exception.ResourceNotFoundException;
import dk.better.company.entity.Company;
import dk.better.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DeveloperController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private TextEncryptor textEncryptor;

    @RequestMapping("/admin/developer/company/generate-api-key")
    public String getCompanyApiKey(@RequestParam(
            value = "companyId",
            required = false
    ) Integer companyId, Model model) {
        if (companyId != null) {
            Company company = companyService.find(companyId);
            if (company == null) {
                throw new ResourceNotFoundException(String.format("Company not found (ID: %d)!", companyId));
            }

            model.addAttribute("apiKey", textEncryptor.encrypt(company.getAccessToken()));
        }

        return "admin-developer-company-api-key";
    }
}
