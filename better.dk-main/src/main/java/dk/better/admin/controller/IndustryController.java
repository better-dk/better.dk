package dk.better.admin.controller;

import dk.better.admin.dto.AddIndustriesDTO;
import dk.better.company.entity.Industry;
import dk.better.company.service.IndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Controller("adminIndustryController")
@RequestMapping("/admin/industry")
public class IndustryController {
    @Autowired
    private IndustryService industryService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model, @RequestParam(value = "success", required = false) String success) {
        model.addAttribute("dto", new AddIndustriesDTO());
        model.addAttribute("success", (success != null));
        return "admin-add-industries";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String processAdd(@Valid @ModelAttribute("dto") AddIndustriesDTO dto, BindingResult bindingResult, Locale locale) {
        if (bindingResult.hasErrors()) {
            return "admin-add-industries";
        }

        String[] industryNames = dto.getIndustries().split("\\r\\n");
        Collection<Industry> industries = new ArrayList<>(industryNames.length);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        for (String name : industryNames) {
            Industry industry = new Industry();
            industry.setName(name);
            Set<ConstraintViolation<Industry>> errors = validator.validate(industry);

            if (!errors.isEmpty()) {
                for (ConstraintViolation violation : errors) {
                    // improve: if message contains {}, then look those messages up and replace
                    Object[] arguments = {name, violation.getMessage()};
                    String message = this.messageSource.getMessage("industries.constraint.violation", arguments, locale);
                    bindingResult.rejectValue("industries", null, message);
                }

                return "admin-add-industries";
            }

            industries.add(industry);
        }

        this.industryService.add(industries);
        return "redirect:/admin/industry/add?success";
    }
}