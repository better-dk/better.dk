package dk.better.booking.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddBookingDTO {
    @NotNull
    private Long created;

    @NotNull
    private Long appointmentTime;

    @NotNull
    @Valid
    private BookingCustomerDTO customer;

    @NotNull
    @Valid
    private BookingCompanyDTO company;


    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Long appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public BookingCustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(BookingCustomerDTO customer) {
        this.customer = customer;
    }

    public BookingCompanyDTO getCompany() {
        return company;
    }

    public void setCompany(BookingCompanyDTO company) {
        this.company = company;
    }
}