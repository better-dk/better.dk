package dk.better.booking.repository;

import dk.better.booking.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bo Andersen
 */
public interface BookingRepository extends JpaRepository<Booking, Integer> {
}