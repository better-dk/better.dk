package dk.better.booking.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
public enum BookingType {
    TREATMENT(1);

    private int value;
    private static Map<Integer, BookingType> map = new HashMap<>();

    private BookingType(int value) {
        this.value = value;
    }

    static {
        for (BookingType bookingType : BookingType.values()) {
            map.put(bookingType.value, bookingType);
        }
    }

    public static BookingType valueOf(int bookingType) {
        return map.get(bookingType);
    }

    public int getValue() {
        return value;
    }
}
