package dk.better.booking.entity.converter;

import dk.better.booking.util.BookingType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Bo Andersen
 */
@Converter
public class BookingTypeConverter implements AttributeConverter<BookingType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(BookingType bookingType) {
        return bookingType.getValue();
    }

    @Override
    public BookingType convertToEntityAttribute(Integer bookingType) {
        return BookingType.valueOf(bookingType);
    }
}