package dk.better.booking.service;

import com.github.segmentio.models.Props;
import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.account.util.AccountUtils;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.EventType;
import dk.better.application.util.GeneralUtils;
import dk.better.booking.dto.AddBookingDTO;
import dk.better.booking.dto.BookingCustomerDTO;
import dk.better.booking.entity.Booking;
import dk.better.booking.repository.BookingRepository;
import dk.better.booking.util.BookingType;
import dk.better.company.entity.Company;
import dk.better.company.entity.Customer;
import dk.better.company.service.CompanyService;
import dk.better.company.service.CustomerService;
import dk.better.company.util.CompanyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@Service
public class BookingServiceImpl implements BookingService {

    private static Logger logger = LogManager.getLogger((Class) BookingService.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private TextEncryptor textEncryptor;

    @Override
    @Transactional
    public void add(AddBookingDTO dto, BookingType type, String apiKey) {
        Assert.notNull((Object) apiKey);
        BookingCustomerDTO customerDTO = dto.getCustomer();
        Account account = null;
        Company company = this.companyService.findByAccessToken(this.textEncryptor.decrypt(apiKey));
        if (company == null) {
            logger.error(String.format("Could not match company (API key: %s, decrypted API key: %s)!", apiKey, this.textEncryptor.decrypt(apiKey)));
            throw new AccessDeniedException("Invalid API key!");
        }
        if (!(customerDTO.getEmail() == null || "".equals(customerDTO.getEmail()) || (account = this.accountService.findByEmail(customerDTO.getEmail())) == null || customerDTO.getPhoneNumber() == null || "".equals(customerDTO.getPhoneNumber()) || account.getPhoneNumber() != null || "".equals(account.getPhoneNumber()))) {
            account.setPhoneNumber(customerDTO.getPhoneNumber());
        }
        Booking booking = new Booking();
        booking.setCreated(new Date(dto.getCreated() * 1000));
        booking.setAppointmentTime(new Date(dto.getAppointmentTime() * 1000));
        booking.setCompany(company);
        booking.setAccount(account);
        booking.setType(type);
        booking.setCustomerEmail(GeneralUtils.nullifyIfEmpty((String) customerDTO.getEmail()));
        booking.setCustomerPhoneNumber(GeneralUtils.nullifyIfEmpty((String) customerDTO.getPhoneNumber()));
        if (customerDTO.getName() != null) {
            Map parts = AccountUtils.getNameParts((String) customerDTO.getName());
            booking.setCustomerFirstName((String) parts.get("first_name"));
            booking.setCustomerMiddleName((String) parts.get("middle_name"));
            booking.setCustomerLastName((String) parts.get("last_name"));
        }
        Customer customer = null;
        if (!GeneralUtils.isNullOrEmpty((String) customerDTO.getEmail()) || !GeneralUtils.isNullOrEmpty((String) customerDTO.getPhoneNumber())) {
            customer = this.customerService.findByEmailOrPhoneNumber(company.getId(), customerDTO.getEmail(), customerDTO.getPhoneNumber());
        }
        if (customer == null) {
            customer = new Customer();
            customer.setUuid(GeneralUtils.getRandomUUID());
            customer.setCreated(new Date());
            customer.setEmail(GeneralUtils.nullifyIfEmpty((String) customerDTO.getEmail()));
            customer.setPhoneNumber(GeneralUtils.nullifyIfEmpty((String) customerDTO.getPhoneNumber()));
            customer.setCompany(company);
            customer.setType(CustomerService.Type.CUSTOMER.getValue());
            if (customerDTO.getName() != null) {
                Map nameParts = AccountUtils.getNameParts((String) customerDTO.getName());
                customer.setFirstName(GeneralUtils.nullifyIfEmpty((String) ((String) nameParts.get("first_name"))));
                customer.setMiddleName(GeneralUtils.nullifyIfEmpty(((String) nameParts.get("middle_name"))));
                customer.setLastName(GeneralUtils.nullifyIfEmpty((String) ((String) nameParts.get("last_name"))));
            }
        } else {
            if (GeneralUtils.isNullOrEmpty(customer.getPhoneNumber()) && !GeneralUtils.isNullOrEmpty((String) customerDTO.getPhoneNumber())) {
                customer.setPhoneNumber(customerDTO.getPhoneNumber());
            }
            if (GeneralUtils.isNullOrEmpty(customer.getEmail()) && !GeneralUtils.isNullOrEmpty((String) customerDTO.getEmail())) {
                customer.setEmail(customerDTO.getEmail());
            }
        }
        booking.setCustomer(customer);
        this.bookingRepository.save(booking);
        String userId = account != null ? account.getUuid() : "Anonymous User";
        Props props = new Props();
        props.put("booking", new Props().put("id", booking.getId()).put("type", type.getValue()).put("appointmentTime", booking.getAppointmentTime().getTime() / 1000)).put("company", new Props().put("id", company.getId()).put("name", company.getName()).put("city", company.getCity()).put("postalCode", company.getPostalCode()).put("profileUrl", company.getProfileUrl()).put("email", (Object) company.getEmail()).put("phoneNumber", company.getPhoneNumber()).put("callTrackingNumber", company.getCallTrackingNumber()).put("accessToken", company.getAccessToken()).put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));
        props.put("customer", new Props().put("uuid", customer.getUuid()).put("name", customerDTO.getName()).put("email", customer.getEmail()).put("phoneNumber", customer.getPhoneNumber()));
        this.analyticsService.track(userId, EventType.BOOKING_ADDED.getValue(), props);
    }
}

