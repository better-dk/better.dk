package dk.better.account.controller;

import dk.better.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@Controller
public class AccountController {
    @Autowired
    private AccountService accountService;


    @RequestMapping("/login")
    public String login(HttpServletRequest request, Model model) {
        String queryString = request.getQueryString();
        boolean success = (queryString != null && queryString.contains("success"));

        model.addAttribute("title", "Log ind - Better.dk");
        model.addAttribute("error", (!success && queryString != null && queryString.contains("error")));
        model.addAttribute("success", success);

        return "login";
    }

    @RequestMapping("/goodbye")
    public String loggedOut(Model model) {
        model.addAttribute("title", "Log ud - Better.dk");
        return "logged-out";
    }

    @RequestMapping("/account/create")
    public String create(@RequestParam(value = "email", required = false, defaultValue = "") String email, Model model) {
        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", "Opret ny bruger på Better.dk");

        model.addAttribute("metas", metas);
        model.addAttribute("title", "Opret Bruger - Better.dk");
        model.addAttribute("email", email);

        return "create-new-account";
    }

    @RequestMapping("/account/confirm/{email}/{token}")
    public String confirm(@PathVariable("email") String email, @PathVariable("token") String token, Model model) {
        Map<String, String> metas = new HashMap<>(1);
        metas.put("robots", "noindex");

        model.addAttribute("metas", metas);
        model.addAttribute("title", "Aktivér bruger - Better.dk");
        model.addAttribute("error", !this.accountService.activate(email, token));

        return "confirm-new-account";
    }
}