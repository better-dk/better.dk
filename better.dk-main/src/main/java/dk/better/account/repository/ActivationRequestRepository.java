package dk.better.account.repository;

import dk.better.account.entity.ActivationRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface ActivationRequestRepository extends JpaRepository<ActivationRequest, Integer>, JpaSpecificationExecutor<ActivationRequest> {
    @Query("select ar from ActivationRequest ar inner join fetch ar.account a where ar.token = :token and a.email = :email")
    Slice<ActivationRequest> findRequestByEmailAndToken(Pageable pageable, @Param("email") String email, @Param("token") String token);
}