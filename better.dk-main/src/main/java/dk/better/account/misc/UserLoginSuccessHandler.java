package dk.better.account.misc;

import com.github.segmentio.AnalyticsClient;
import com.github.segmentio.models.Context;
import com.github.segmentio.models.Options;
import dk.better.account.entity.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Bo Andersen
 */
@Component
public class UserLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static Logger logger = LogManager.getLogger(UserLoginSuccessHandler.class);
    private static final String EVENT_LOGGED_IN = "Logged In (E-mail)";

    @Autowired
    private AnalyticsClient analyticsClient; // Here we use the client directly to avoid problems with accessing the request object within AnalyticsService


    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        try {
            if (authentication != null) {
                Context context = new Context();
                context.put("ip", httpServletRequest.getRemoteAddr());
                Options options = new Options();
                options.setContext(context);

                Account account = (Account) authentication.getPrincipal();
                this.analyticsClient.track(account.getUuid(), EVENT_LOGGED_IN, null, options);
            }
        } catch (Exception e) {
            logger.error("Error triggering logged in event", e);
        }

        super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
    }
}