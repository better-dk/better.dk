package dk.better.account.service;

import dk.better.account.entity.Account;
import dk.better.account.util.AccountUtils;
import dk.better.application.service.AbstractService;
import dk.better.application.util.GeneralUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.social.connect.ConnectionSignUp;

/**
 * @author Bo Andersen
 */
public interface AccountService extends AbstractService<Account>, ConnectionSignUp, UserDetailsService {
    static final String ROLE_USER = "ROLE_USER";
    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
    static final String ROLE_COMPANY_OWNER = "ROLE_COMPANY_OWNER";
    static final String ROLE_DEVELOPER = "ROLE_DEVELOPER";

    static final int ROLE_USER_ID = 1;
    static final int BETTER_USER_ID = 99999999;
    static final int GROUP_USER_ID = 1;

    enum ProfilePictureSize {
        SQUARE, SMALL, NORMAL, LARGE
    }

    static long generateChecksum(int accountId) {
        return AccountUtils.generateChecksum(accountId);
    }

    static String getProfilePictureUrl(long facebookProfileId) {
        return getProfilePictureUrl(facebookProfileId, ProfilePictureSize.SQUARE);
    }

    static String getProfilePictureUrl(long facebookProfileId, ProfilePictureSize profilePictureSize) {
        String size;

        switch (profilePictureSize) {
            case SQUARE:
                size = "square";
                break;
            case SMALL:
                size = "small";
                break;
            case NORMAL:
                size = "normal";
                break;
            case LARGE:
                size = "large";
                break;
            default:
                size = "square";
                break;
        }

        return "https://graph.facebook.com/" + facebookProfileId + "/picture?size=" + size;
    }

    static String generateUUID() {
        return GeneralUtils.getRandomUUID();
    }

    Account findByEmail(String email);

    Account findByAccessToken(String accessToken);

    Account createAndFlush(Account account);

    Account create(String name, String email, String clearTextPassword);

    Account createAndClaim(String name, String email, String clearTextPassword, int companyId);

    String encryptPassword(String clearTextPassword);

    /**
     * Activates an account
     *
     * @param email The e-mail address of the account
     * @param token The activation request token
     * @return True if successful, false if the token has expired or if the token has already been used
     */
    boolean activate(String email, String token);

    @PreAuthorize("isAuthenticated()")
    void addCompanyToFavorites(int companyId);

    Account merge(String name, String clearTextPassword, String accessToken, Long checksum);

    boolean hasAccessToReview(int reviewId, int accountId);
}