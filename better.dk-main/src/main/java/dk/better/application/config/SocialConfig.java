package dk.better.application.config;

import dk.better.account.entity.Account;
import dk.better.account.repository.AccountRepository;
import dk.better.account.service.AccountService;
import dk.better.application.security.RedirectToPreviousPageInterceptor;
import dk.better.application.service.AnalyticsService;
import dk.better.application.service.SpringSecuritySignInAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Configuration
public class SocialConfig {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private TextEncryptor textEncryptor;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private RememberMeServices rememberMeServices;

    @Value("${facebook.app.id}")
    private String facebookAppId;

    @Value("${facebook.app.secret}")
    private String facebookAppSecret;

    @Value("${facebook.app.scope}")
    private String scope;


    @Bean
    public ProviderSignInController providerSignInController(@Value("${application.http.scheme}") String httpScheme, @Value("${application.domain.name}") String domainName) {
        ProviderSignInController controller = new ProviderSignInController(this.connectionFactoryLocator(), this.usersConnectionRepository(), new SpringSecuritySignInAdapter(this.accountRepository, this.rememberMeServices));
        controller.addSignInInterceptor(new RedirectToPreviousPageInterceptor(controller, this.analyticsService, httpScheme, domainName));

        return controller;
    }

    @Bean
    public ConnectionFactoryRegistry connectionFactoryLocator() {
        ConnectionFactoryRegistry connectionFactoryRegistry = new ConnectionFactoryRegistry();
        List<ConnectionFactory<?>> connectionFactories = new ArrayList<ConnectionFactory<?>>();
        connectionFactories.add(this.facebookConnectionFactory());
        connectionFactoryRegistry.setConnectionFactories(connectionFactories);

        return connectionFactoryRegistry;
    }

    @Bean
    public FacebookConnectionFactory facebookConnectionFactory() {
        FacebookConnectionFactory connectionFactory = new FacebookConnectionFactory(this.facebookAppId, this.facebookAppSecret);
        connectionFactory.setScope(this.scope);

        return connectionFactory;
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public Facebook facebook(ConnectionRepository repository) {
        Connection<Facebook> connection = repository.findPrimaryConnection(Facebook.class);
        return (connection != null ? connection.getApi() : null);
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public ConnectionRepository connectionRepository() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            throw new IllegalStateException("Unable to get a ConnectionRepository: no user signed in");
        }

        Account account = (Account) authentication.getPrincipal();
        return this.usersConnectionRepository().createConnectionRepository(String.valueOf(account.getId()));
    }

    @Bean
    public JdbcUsersConnectionRepository usersConnectionRepository() {
        JdbcUsersConnectionRepository repository = new JdbcUsersConnectionRepository(this.dataSource, this.connectionFactoryLocator(), this.textEncryptor);
        repository.setConnectionSignUp(this.accountService);

        return repository;
    }
}