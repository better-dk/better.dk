package dk.better.application.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Bo Andersen
 */
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {
}