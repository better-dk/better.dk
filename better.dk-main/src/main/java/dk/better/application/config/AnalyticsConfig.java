package dk.better.application.config;

import com.github.segmentio.Analytics;
import com.github.segmentio.AnalyticsClient;
import io.keen.client.java.JavaKeenClientBuilder;
import io.keen.client.java.KeenClient;
import io.keen.client.java.KeenProject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Bo Andersen
 */
@Configuration
public class AnalyticsConfig {
    @Value("${analytics.segment.write-key}")
    private String segmentWriteKey;

    @Value("${analytics.keen.project-id}")
    private String keenProjectId;

    @Value("${analytics.keen.read-key}")
    private String keenReadKey;


    @Bean
    public AnalyticsClient analyticsClient() {
        Analytics.initialize(this.segmentWriteKey);
        return Analytics.getDefaultClient();
    }

    @Bean
    public KeenClient keenClient() {
        KeenClient client = new JavaKeenClientBuilder().build();
        KeenProject project = new KeenProject(this.keenProjectId, null, this.keenReadKey);
        client.setDefaultProject(project);

        return client;
    }
}