package dk.better.application.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudsearchdomain.AmazonCloudSearchDomain;
import com.amazonaws.services.cloudsearchdomain.AmazonCloudSearchDomainClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.AWSLogsClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author Bo Andersen
 */
@Configuration
public class AwsConfig {
    @Value("${aws.accessKey}")
    private String accessKey;

    @Value("${aws.secretKey}")
    private String secretKey;

    @Value("${aws.search.endpoint}")
    private String searchEndpoint;

    @Value("${aws.s3.endpoint}")
    private String s3endpoint;

    @Value("${aws.logs.endpoint}")
    private String awsLogsEndpoint;

    //@Value("${aws.cdn.endpoint}")
    //private String cdnEndpoint;

    @Bean
    public AWSCredentials awsCredentials() {
        return new BasicAWSCredentials(this.accessKey, this.secretKey);
    }

    @Bean
    public AmazonCloudSearchDomain cloudSearchDomain() {
        AmazonCloudSearchDomain searchDomainClient = new AmazonCloudSearchDomainClient(this.awsCredentials());
        searchDomainClient.setEndpoint(this.searchEndpoint);

        return searchDomainClient;
    }

    @Bean
    public AmazonS3 amazonS3Client() {
        AmazonS3 client = new AmazonS3Client(this.awsCredentials());
        client.setEndpoint(this.s3endpoint);
        return client;
    }

    @Bean
    public AWSLogs amazonLogsClient() {
        AWSLogs client = new AWSLogsClient(this.awsCredentials());
        client.setEndpoint(this.awsLogsEndpoint);
        return client;
    }

    @Bean
    @Profile({"development", "staging"})
    public DynamoDBMapper amazonDynamoMapperDevelopment() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient(this.awsCredentials());
        client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_WEST_1));
        return new DynamoDBMapper(client);
    }

    @Bean
    @Profile("production")
    public DynamoDBMapper amazonDynamoMapper() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient(this.awsCredentials());
        client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_CENTRAL_1));
        return new DynamoDBMapper(client);
    }

    /*@Bean
    public AmazonCloudFront cdnClient() {
        AmazonCloudFront client = new AmazonCloudFrontClient(this.awsCredentials());
        client.setEndpoint(this.cdnEndpoint);

        return client;
    }*/
}