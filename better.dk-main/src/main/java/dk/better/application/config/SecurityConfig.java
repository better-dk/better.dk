package dk.better.application.config;

import dk.better.account.misc.LogoutSuccessHandler;
import dk.better.account.misc.UserLoginSuccessHandler;
import dk.better.account.service.AccountService;
import dk.better.application.security.PersistentTokenBasedRememberMeServicesSubdomains;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * @author Bo Andersen
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AccountService accountService;

    @Value("${security.encryption.password}")
    private String enryptionPassword;

    @Value("${security.encryption.salt}")
    private String salt;

    @Value("${security.remember-me.key}")
    private String rememberMeKey;

    @Value("${application.domain.name}")
    private String domainName;


    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        // This bean is required for using method security annotations
        return super.authenticationManager();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // improve: block requests to /css/admin/** and other resources for others than admin
        web
                .ignoring()
                .antMatchers("/images/**", "/css/**", "/js/**", "/reviewprocess/**", "/fonts/**", "/robots.txt", "/favicon.ico");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // The "/p/**" matcher is for viewing profiles
        // Access to the ajax and api modules is permitted to all, because authentication security is handled within the packages themselves
        String[] permittedToAll = new String[]{"/", "/signin/**", "/login", "/api/**", "/search", "/s/**", "/sl/**", "/ajax/**", "/ping", "/account/create", "/account/confirm/**", "/logout", "/goodbye", "/brancher/**", "/landing/**"};

        // improve: make sure that calls to /api and /ajax handle AccessDeniedException with JSON response
        // improve: the controller advice does not work because: http://stackoverflow.com/questions/15709679/spring-security-handling-both-web-app-security-and-web-service-security/15710847
        // improve: so try to make a filter for /ajax/** and /api/**

        http
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login/process")
                .failureUrl("/login?error")
                .defaultSuccessUrl("/login?success", false)
                .successHandler(this.authenticationSuccessHandler())
                .usernameParameter("username")
                .passwordParameter("password")
                .and()
                .rememberMe()
                .rememberMeServices(this.rememberMeServices())
                .key(this.rememberMeKey)
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(this.logoutSuccessHandler)
                .invalidateHttpSession(true)
                .and()
                .authorizeRequests()
                .antMatchers(permittedToAll).permitAll()

                .antMatchers("/p/*/page/**").authenticated() // For company owners/admins
                .antMatchers("/p/**").permitAll()
                //.antMatchers("/404").permitAll() // For testing
                //.antMatchers("/500").permitAll() // For testing

                // todo: handle filter authentication within filter instead of controller actions
                // The security here is handled within the controller actions , because we want users with
                // the correct access token and checksum to be able to access these pages
                .antMatchers("/cp/company/**").permitAll()
                .antMatchers("/messages").permitAll()

                //.antMatchers("/facebook/test").authenticated()
                .antMatchers("/push/auth/**").authenticated()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().denyAll()
                .and()
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.accountService).passwordEncoder(this.passwordEncoder());
    }

    @Bean
    public RememberMeServices rememberMeServices() {
        PersistentTokenBasedRememberMeServices rememberMeServices = new PersistentTokenBasedRememberMeServicesSubdomains(this.rememberMeKey, this.accountService, this.tokenRepository(), this.domainName);
        rememberMeServices.setAlwaysRemember(true);
        rememberMeServices.setCookieName("remember-me");
        rememberMeServices.setTokenValiditySeconds(31536000); // 1 year
        return rememberMeServices;
    }

    @Bean
    public PersistentTokenRepository tokenRepository() {
        JdbcTokenRepositoryImpl repository = new JdbcTokenRepositoryImpl();
        repository.setCreateTableOnStartup(false);
        repository.setDataSource(this.dataSource);
        return repository;
    }

    @Profile("development")
    @Bean
    public TextEncryptor textEncryptorNoOp() {
        return Encryptors.noOpText();
    }

    @Profile({"production", "staging"})
    @Bean
    public TextEncryptor textEncryptor() {
        return Encryptors.queryableText(this.enryptionPassword, this.salt); // http://docs.spring.io/spring-security/site/docs/3.1.7.RELEASE/reference/crypto.html
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        SavedRequestAwareAuthenticationSuccessHandler handler = new UserLoginSuccessHandler();
        handler.setDefaultTargetUrl("/login?success");
        handler.setAlwaysUseDefaultTargetUrl(false);
        handler.setUseReferer(true);
        handler.setTargetUrlParameter("destination");

        return handler;
    }
}