package dk.better.application.service;

import dk.better.application.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author Bo Andersen
 */
@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityRepository cityRepository;

    @Override
    public Page<String> findAllCitiesWithCompaniesInIndustry(int industryId, Pageable pageable) {
        return this.cityRepository.findUniqueCityNamesWithCompaniesInIndustry(pageable, industryId);
    }
}