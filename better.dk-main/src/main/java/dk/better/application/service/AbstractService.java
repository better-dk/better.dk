package dk.better.application.service;

/**
 * @author Bo Andersen
 */
public interface AbstractService<T> {
    T find(int id);

    T save(T object);

    T saveAndFlush(T object);

    T getReference(Object identifier);

    Object getReference(Class type, Object identifier);
}