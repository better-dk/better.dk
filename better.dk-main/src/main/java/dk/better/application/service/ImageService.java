package dk.better.application.service;

import org.imgscalr.Scalr;

import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @author Bo Andersen
 */
public interface ImageService {
    BufferedImage resize(BufferedImage original, Scalr.Method quality, Scalr.Mode mode, int width, int height);

    File compressJpeg(File image, float quality);

    File convertToJpeg(File image);

    BufferedImage removeAlphaChannel(BufferedImage source);
}