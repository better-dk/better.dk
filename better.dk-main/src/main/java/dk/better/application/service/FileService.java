package dk.better.application.service;

import java.io.File;

/**
 * @author Bo Andersen
 */
public interface FileService {
    File gzip(File source);
}