package dk.better.application.service;

import com.github.segmentio.models.Options;
import com.github.segmentio.models.Props;
import com.github.segmentio.models.Traits;
import com.github.segmentio.stats.AnalyticsStatistics;

/**
 * @author Bo Andersen
 */
public interface AnalyticsService {
    static final int GROUP_PARTIAL_USER_ID = 3;
    static final String ANONYMOUS_USER_ID = "Anonymous User";

    void identify(String userId);

    void identify(String userId, Traits traits);

    void identify(String userId, Traits traits, Options options);

    void group(String userId, String groupId, Traits traits);

    void group(String userId, String groupId, Traits traits, Options options);

    void track(String userId, String event);

    void track(String userId, String event, Props properties);

    void track(String userId, String event, Props properties, Options options);

    void page(String userId, String name);

    void page(String userId, String name, Props properties);

    void page(String userId, String name, Props properties, Options options);

    void page(String userId, String name, String category, Props properties, Options options);

    void screen(String userId, String name);

    void screen(String userId, String name, Props properties);

    void screen(String userId, String name, Props properties, Options options);

    void screen(String userId, String name, String category, Props properties, Options options);

    void alias(String previousId, String userId);

    void alias(String previousId, String userId, Options options);

    void flush();

    void close();

    AnalyticsStatistics getStatistics();

    int getQueueDepth();

    String generateKeenScopedKey(int companyId);
}