package dk.better.application.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Bo Andersen
 */
public interface CityService {
    Page<String> findAllCitiesWithCompaniesInIndustry(int industryId, Pageable pageable);
}