package dk.better.application.service;

import com.pusher.rest.Pusher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Service
public class PushServiceImpl implements PushService {
    @Autowired
    private Pusher pusher;


    @Override
    public void push(String channel, String event, Object data) {
        this.pusher.trigger(channel, event, data);
    }

    @Override
    public void push(List<String> channels, String event, Object data) {
        this.pusher.trigger(channels, event, data);
    }
}