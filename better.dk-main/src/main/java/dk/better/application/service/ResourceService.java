package dk.better.application.service;

import java.io.File;
import java.io.IOException;

/**
 * @author Bo Andersen
 */
public interface ResourceService {
    void putObject(String key, File file) throws RuntimeException;

    void putObject(String bucketName, String key, File file) throws RuntimeException;

    File getObject(String key) throws IOException;

    File getObject(String bucketName, String key) throws IOException;

    //@PreAuthorize("isAuthenticated()")
    //void invalidateObjects(Collection<String> items);
}