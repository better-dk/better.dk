package dk.better.application.service;

import com.github.segmentio.AnalyticsClient;
import com.github.segmentio.models.Context;
import com.github.segmentio.models.Options;
import com.github.segmentio.models.Props;
import com.github.segmentio.models.Traits;
import com.github.segmentio.stats.AnalyticsStatistics;
import dk.better.application.util.HttpUtils;
import io.keen.client.java.KeenClient;
import io.keen.client.java.ScopedKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class AnalyticsServiceImpl implements AnalyticsService {
    @Autowired
    private AnalyticsClient client;

    @Autowired
    private KeenClient keenClient;

    @Value("${analytics.keen.master-key}")
    private String keenMasterKey;


    @Override
    public void identify(String userId) {
        this.client.identify(userId);
    }

    @Override
    public void identify(String userId, Traits traits) {
        this.client.identify(userId, traits);
    }

    @Override
    public void identify(String userId, Traits traits, Options options) {
        this.client.identify(userId, traits, options);
    }

    @Override
    public void group(String userId, String groupId, Traits traits) {
        this.client.group(userId, groupId, traits);
    }

    @Override
    public void group(String userId, String groupId, Traits traits, Options options) {
        this.client.group(userId, groupId, traits, options);
    }

    @Override
    public void track(String userId, String event) {
        Options options = new Options();
        options.setContext(this.injectGoogleAnalyticsClientId(new Context()));
        this.client.track(userId, event, null, options);
    }

    @Override
    public void track(String userId, String event, Props properties) {
        Options options = new Options();
        options.setContext(this.injectGoogleAnalyticsClientId(new Context()));
        this.client.track(userId, event, properties, options);
    }

    @Override
    public void track(String userId, String event, Props properties, Options options) {
        Assert.notNull(options);
        options.setContext(this.injectGoogleAnalyticsClientId(options.getContext()));
        this.client.track(userId, event, properties, options);
    }

    @Override
    public void page(String userId, String name) {
        Options options = new Options();
        options.setContext(this.injectGoogleAnalyticsClientId(new Context()));
        this.client.page(userId, name, null, options);
    }

    @Override
    public void page(String userId, String name, Props properties) {
        Options options = new Options();
        options.setContext(this.injectGoogleAnalyticsClientId(new Context()));
        this.client.page(userId, name, properties, options);
    }

    @Override
    public void page(String userId, String name, Props properties, Options options) {
        Assert.notNull(options);
        options.setContext(this.injectGoogleAnalyticsClientId(options.getContext()));
        this.client.page(userId, name, properties, options);
    }

    @Override
    public void page(String userId, String name, String category, Props properties, Options options) {
        Assert.notNull(options);
        options.setContext(this.injectGoogleAnalyticsClientId(options.getContext()));
        this.client.page(userId, name, category, properties, options);
    }

    @Override
    public void screen(String userId, String name) {
        this.client.screen(userId, name);
    }

    @Override
    public void screen(String userId, String name, Props properties) {
        this.client.screen(userId, name, properties);
    }

    @Override
    public void screen(String userId, String name, Props properties, Options options) {
        this.client.screen(userId, name, properties, options);
    }

    @Override
    public void screen(String userId, String name, String category, Props properties, Options options) {
        this.client.screen(userId, name, category, properties, options);
    }

    @Override
    public void alias(String previousId, String userId) {
        this.client.alias(previousId, userId);
    }

    @Override
    public void alias(String previousId, String userId, Options options) {
        this.client.alias(previousId, userId, options);
    }

    @Override
    public void flush() {
        this.client.flush();
    }

    @Override
    public void close() {
        this.client.close();
    }

    @Override
    public AnalyticsStatistics getStatistics() {
        return this.client.getStatistics();
    }

    @Override
    public int getQueueDepth() {
        return this.client.getQueueDepth();
    }

    @Override
    public String generateKeenScopedKey(int companyId) {
        Map<String, Object> filter = new HashMap<>();
        List<Map<String, Object>> filters = new ArrayList<>();
        Map<String, Object> options = new HashMap<>();

        filter.put("property_name", "company.id");
        filter.put("operator", "eq");
        filter.put("property_value", companyId);

        filters.add(filter);
        options.put("allowed_operations", Arrays.asList("read"));
        options.put("filters", filters);

        return ScopedKeys.encrypt(this.keenClient, this.keenMasterKey, options);
    }

    /**
     * Gets the Google Analytics Universal client ID from cookie, if available
     *
     * @return The client ID if available, or NULL otherwise
     */
    private String getGoogleAnalyticsClientId() {
        HttpServletRequest request = HttpUtils.getRequest();

        if (request != null) {
            Cookie[] cookies = request.getCookies();

            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("_ga")) {
                        String value = cookie.getValue();
                        String[] parts = value.split("\\.");

                        return ((parts.length >= 1) ? parts[parts.length - 2] + "." + parts[parts.length - 1] : null);
                    }
                }
            }
        }

        return null;
    }

    /**
     * Injects the Google Analytics Universal client ID into the provided context
     *
     * @param context The context object for which to add the client ID to
     * @return The context with the added client ID (if available) - otherwise the unmodified context
     */
    private Context injectGoogleAnalyticsClientId(Context context) {
        String googleAnalyticsClientId = this.getGoogleAnalyticsClientId();

        if (googleAnalyticsClientId != null) {
            context.put("Google Analytics", new Props()
                    .put("clientId", googleAnalyticsClientId));
        }

        return context;
    }
}