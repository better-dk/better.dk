package dk.better.application.service;

import java.util.List;

/**
 * @author Bo Andersen
 */
public interface PushService {
    void push(String channel, String event, Object data);

    void push(List<String> channels, String event, Object data);
}