package dk.better.application.tag;

import dk.better.application.util.GeneralUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class NewLineToBreakLine extends TagSupport {
    private static Logger logger = LogManager.getLogger(DomainNameUri.class);
    private String value;
    private String var;

    @Override
    public int doStartTag() throws JspException {
        try {
            if (this.value != null && !this.value.isEmpty()) {
                String output = GeneralUtils.nl2br(this.value);

                if (this.var != null && !this.var.isEmpty()) {
                    this.pageContext.getRequest().setAttribute(this.var, output);
                } else {
                    this.pageContext.getOut().write(output);
                }
            }
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
        }

        return super.doStartTag();
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setVar(String var) {
        this.var = var;
    }
}