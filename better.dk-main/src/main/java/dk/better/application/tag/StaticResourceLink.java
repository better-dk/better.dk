package dk.better.application.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Properties;

public class StaticResourceLink extends TagSupport {
    private static Logger logger = LogManager.getLogger(StaticResourceLink.class);
    private String baseUrl;
    private String relativePath;
    private String var;

    @Override
    public int doStartTag() throws JspException {
        try {
            String output = this.getBaseUrl() + this.relativePath;

            if (this.var != null && !this.var.isEmpty()) {
                this.pageContext.getRequest().setAttribute(this.var, output);
            } else {
                this.pageContext.getOut().write(output);
            }
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
        }

        return super.doStartTag();
    }

    private String getBaseUrl() throws IOException {
        if (this.baseUrl == null) {
            Resource resource = new ClassPathResource(System.getProperty("spring.profiles.active") + "/application.properties");
            Properties properties = PropertiesLoaderUtils.loadProperties(resource);
            this.baseUrl = properties.getProperty("static.resource.baseurl");
        }

        return this.baseUrl;
    }

    public void setRelativePath(String relativePath) {
        // Ensure that the relative path starts with a forward slash
        if (relativePath.indexOf('/') != 0) {
            relativePath = "/" + relativePath;
        }

        this.relativePath = relativePath;
    }

    public void setVar(String var) {
        this.var = var;
    }
}