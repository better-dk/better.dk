package dk.better.application.tag;

import dk.better.account.entity.Account;
import dk.better.account.util.AccountUtils;
import dk.better.application.util.AuthUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class PrincipalFullName extends TagSupport {
    private static Logger logger = LogManager.getLogger(DomainNameUri.class);
    private String var;

    @Override
    public int doStartTag() throws JspException {
        try {
            if (AuthUtils.isAuthenticated()) {
                Account account = AuthUtils.getPrincipal();
                String fullName = AccountUtils.getFullName(account.getFirstName(), account.getMiddleName(), account.getLastName());

                if (this.var != null && !this.var.isEmpty()) {
                    this.pageContext.getRequest().setAttribute(this.var, fullName);
                } else {
                    this.pageContext.getOut().write(fullName);
                }
            }
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
        }

        return super.doStartTag();
    }

    public void setVar(String var) {
        this.var = var;
    }
}