package dk.better.application.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Bo Andersen
 */
public abstract class AbstractEntityManagerRepository implements EntityManagerAware {
    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}