package dk.better.application.repository;

import dk.better.company.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface CityRepository extends JpaRepository<Company, Integer>, JpaSpecificationExecutor<Company> {
    @Query(
            value = "select c.city from Company c inner join c.industries i where i.id = :industryId group by c.city",
            countQuery = "select count(c.city) from Company c inner join c.industries i where i.id = :industryId group by c.city"
    )
    Page<String> findUniqueCityNamesWithCompaniesInIndustry(Pageable pageable, @Param("industryId") int industryId);
}