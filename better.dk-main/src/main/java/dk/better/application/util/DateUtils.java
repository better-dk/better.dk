package dk.better.application.util;

import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bo Andersen
 */
public class DateUtils {
    private DateUtils() {
    }

    /**
     * Formats a date according to the given format and returns it as a string
     *
     * @param format The format to use
     * @param date   The date to format
     * @return The formatted date as a string
     */
    public static String getFormattedDate(String format, Date date) {
        Assert.notNull(date);
        Assert.notNull(format);

        return new SimpleDateFormat(format).format(date);
    }
}