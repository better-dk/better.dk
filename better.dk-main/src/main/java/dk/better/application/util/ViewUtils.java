package dk.better.application.util;

import dk.better.account.entity.Account;
import dk.better.account.util.AccountUtils;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.ProductType;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * @author Bo Andersen
 */
public class ViewUtils {
    public static boolean contains(Collection collection, Object o) {
        return collection.contains(o);
    }

    public static String replaceAll(String s, String pattern, String replacement) {
        return s.replaceAll(pattern, replacement);
    }

    public static boolean isCompanyOwner(int companyId) {
        if (!AuthUtils.isAuthenticated()) {
            return false;
        }

        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();
        return CompanyUtils.isCompanyOwner(account, companyId);
    }

    public static boolean isAdmin() {
        return AuthUtils.isAdmin();
    }

    /**
     * Gets the static resource base URL
     *
     * @return The static resource base URL
     */
    public static String getStaticResourceUrl() throws RuntimeException {
        try {
            Resource resource = new ClassPathResource(System.getProperty("spring.profiles.active") + "/application.properties");
            Properties properties = PropertiesLoaderUtils.loadProperties(resource);
            return properties.getProperty("static.resource.baseurl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String urlEncode(String value) throws UnsupportedEncodingException {
        return ViewUtils.urlEncode(value, "UTF-8");
    }

    public static String urlEncode(String value, String encoding) throws UnsupportedEncodingException {
        return URLEncoder.encode(value, encoding);
    }

    /**
     * Decodes the passed UTF-8 String using an algorithm that's compatible with
     * JavaScript's <code>decodeURIComponent</code> function. Returns
     * <code>null</code> if the String is <code>null</code>.
     *
     * @param s The UTF-8 encoded String to be decoded
     * @return the decoded String
     */
    public static String decodeURIComponent(String s) {
        if (s == null) {
            return null;
        }

        String result;

        try {
            result = URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            result = s; // This should never occur
        }

        return result;
    }

    /**
     * Encodes the passed String as UTF-8 using an algorithm that's compatible
     * with JavaScript's <code>encodeURIComponent</code> function. Returns
     * <code>null</code> if the string is <code>null</code>.
     *
     * @param s The String to be encoded
     * @return the encoded String
     */
    public static String encodeURIComponent(String s) {
        String result;

        try {
            result = URLEncoder.encode(s, "UTF-8")
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
            result = s; // This should never occur
        }

        return result;
    }

    public static boolean matches(String expression, CharSequence sequence) {
        return Pattern.compile(expression).matcher(sequence).matches();
    }

    public static String prettyTime(Date time) {
        Assert.notNull(time);

        PrettyTime p = new PrettyTime(new Locale("da")); // todo: make locale dynamic
        return p.format(time);
    }

    public static String formatCompanyRating(Double rating) {
        return CompanyUtils.formatRating(rating);
    }

    public static String getFullName(String firstName, String middleName, String lastName) {
        return AccountUtils.getFullName(firstName, middleName, lastName);
    }

    public static String getProductName(int productId) {
        return ProductType.getName(productId);
    }
}