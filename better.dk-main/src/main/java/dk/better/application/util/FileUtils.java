package dk.better.application.util;

import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Bo Andersen
 */
public final class FileUtils {
    private FileUtils() {
    }

    // improve: document
    public static String getFileExtension(String fileName) {
        Assert.notNull(fileName);
        int lastPeriodIndex = fileName.lastIndexOf('.');

        return ((lastPeriodIndex > -1) ? fileName.substring((lastPeriodIndex + 1)) : null);
    }

    public static Path getTemporaryDirectoryPath() {
        return FileUtils.getTemporaryDirectoryPath(null);
    }

    public static Path getTemporaryDirectoryPath(String prefix) {
        try {
            Path tempDirPath = Paths.get(System.getProperty("java.io.tmpdir"));
            return Files.createTempDirectory(tempDirPath, prefix);
        } catch (IOException e) {
            throw new RuntimeException("Could not get temporary directory path", e);
        }
    }

    public static File convert(MultipartFile file) {
        try {
            File result = new File(file.getOriginalFilename());
            file.transferTo(result);
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Error converting MultipartFile to File", e);
        }
    }
}