package dk.better.application.util;

/**
 * @author Bo Andersen
 */
public enum Scheme {
    HTTP, HTTPS
}
