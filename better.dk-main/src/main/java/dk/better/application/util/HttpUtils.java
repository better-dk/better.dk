package dk.better.application.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Bo Andersen
 */
public class HttpUtils {
    private HttpUtils() {
    }

    // improve: document
    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    // improve: document
    public static boolean hasCookie(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        int i = 0;

        while (i < cookies.length) {
            if (cookies[i].getName().equals(cookieName)) {
                return true;
            }

            i++;
        }

        return false;
    }

    // improve: document
    public static String getCurrentPageUrl(HttpServletRequest request) {
        String query = request.getQueryString();
        return request.getScheme() + "://" + request.getServerName() + request.getRequestURI() + "?" + (query != null ? query : "");
    }
}