package dk.better.application.util;

/**
 * @author Bo Andersen
 */
public enum RequestStatus {
    PENDING,
    ACCEPTED,
    DECLINED;


    private RequestStatus() {
    }
}
