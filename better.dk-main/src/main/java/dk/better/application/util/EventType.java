package dk.better.application.util;

/**
 * @author Bo Andersen
 */
public enum EventType {
    REVIEW_REDIRECT("Review Redirect"),
    BOOKING_ADDED("Booking Added"),
    INTEGRATION_ACTIVATED("Integration Activated"),
    INTEGRATION_DEACTIVATED("Integration Deactivated");

    private String value;

    EventType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
