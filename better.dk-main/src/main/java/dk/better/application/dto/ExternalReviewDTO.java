package dk.better.application.dto;

import dk.better.company.entity.Company;
import dk.better.company.entity.ExternalReview;

/**
 * @author Liubov Zavorotna
 */
public class ExternalReviewDTO {

    private int id;
    private String site;
    private String review;
    private int rating;
    private String author;
    private String date;
    private String link;
    private Company company;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ExternalReviewDTO(ExternalReview externalReview, dk.better.company.entity.Company company) {
        this.id = externalReview.getCompanyId();
        this.site = externalReview.getSite();
        this.review = externalReview.getReview();
        this.rating = externalReview.getRating();
        this.author = externalReview.getAuthor();
        this.date = externalReview.getPublishDate();
        this.link = externalReview.getLink();
        this.company = company;
    }


    @Override
    public String toString() {
        return "ExternalReviewDTO{" +
                "id=" + id +
                ", site='" + site + '\'' +
                ", review='" + review + '\'' +
                ", rating=" + rating +
                ", author='" + author + '\'' +
                ", date='" + date + '\'' +
                ", link='" + link + '\'' +
                ", company=" + company +
                '}';
    }
}
