package dk.better.application.dto;

import dk.better.company.entity.Review;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Liubov Zavorotna
 */
public class ReviewDTO {
    private static final String BETTER = "better";
    private static final String DATE_PATTERN = "dd.MM.yy";
    private String site;
    private String review;
    private int rating;
    private String author;
    private String date;

    public ReviewDTO(Review review) {
        this.date = String.valueOf(new SimpleDateFormat(DATE_PATTERN).format(new Date(review.getCreated().getTime())));
        this.review = review.getText().isEmpty() ? "Ingen bedømmelsestekst." : review.getText();
        this.rating = review.getRating();
        if (review.getAccount() == null) {
            this.author = "Verificeret kunde";
        } else if (review.getAccount().getFirstName() == null) {
            this.author = "Verificeret kunde";
        } else if (review.getAccount().getLastName() == null) {
            this.author = review.getAccount().getFirstName();
        } else {
            this.author = review.getAccount().getFirstName() + " " + review.getAccount().getLastName();
        }

        this.site = BETTER;
    }

    public ReviewDTO(ExternalReviewDTO externalReview) {
        this.date = externalReview.getDate();
        this.review = externalReview.getReview().isEmpty()
                ? "Ingen bedømmelsestekst."
                : externalReview.getReview();
        this.rating = externalReview.getRating();
        this.author = externalReview.getAuthor();
        this.site = externalReview.getSite();
    }


    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ReviewDTO{" +
                "site='" + site + '\'' +
                ", review='" + review + '\'' +
                ", rating=" + rating +
                ", author='" + author + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
