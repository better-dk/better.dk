package dk.better.application.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.better.account.entity.Account;
import dk.better.application.util.AuthUtils;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private ThreadService threadService;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${application.domain.name}")
    private String domainName;


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // Make sure that the user was logged in successfully
        if (!AuthUtils.isAuthenticated()) {
            return;
        }

        // Add cookie with unread threads data
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();
        Map<Integer, Long> temp = this.threadService.getUnreadMessagesCountPerThread(account.getId());
        Map<Integer, Object> unreadThreadsForPrivateAccount = new HashMap<>(temp.size());

        for (Map.Entry<Integer, Long> entry : temp.entrySet()) {
            unreadThreadsForPrivateAccount.put(entry.getKey(), entry.getValue());
        }

        Map<String, Object> unreadThreads = new HashMap<>(2);
        unreadThreads.put("personal", unreadThreadsForPrivateAccount);

        if (!account.getAccountCompanies().isEmpty()) {
            Map<Integer, Map<Integer, Long>> unreadThreadsForCompanies = this.threadService.getUnreadMessagesCountPerThreadForClaimedCompanies(account.getId());
            unreadThreads.put("companies", unreadThreadsForCompanies);
        }

        String json = this.objectMapper.writeValueAsString(unreadThreads);
        Cookie cookie = new Cookie("unread-messages-count", URLEncoder.encode(json, "UTF-8"));
        cookie.setDomain("." + this.domainName);
        cookie.setPath("/");
        cookie.setMaxAge(-1);
        response.addCookie(cookie);
    }
}