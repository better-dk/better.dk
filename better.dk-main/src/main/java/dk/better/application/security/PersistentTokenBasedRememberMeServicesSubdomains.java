package dk.better.application.security;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @author Bo Andersen
 */
public class PersistentTokenBasedRememberMeServicesSubdomains extends PersistentTokenBasedRememberMeServices {

    private String domainName;

    public PersistentTokenBasedRememberMeServicesSubdomains(String key, UserDetailsService userDetailsService, PersistentTokenRepository tokenRepository, String domainName) {
        super(key, userDetailsService, tokenRepository);
        this.domainName = domainName;
    }

    @Override
    protected void setCookie(String[] tokens, int maxAge, HttpServletRequest request, HttpServletResponse response) {
        String contextPath = request.getContextPath();
        String cookiePath = contextPath.length() > 0 ? contextPath : "/";
        String cookieValue = encodeCookie(tokens);

        Cookie cookie = new Cookie(this.getCookieName(), cookieValue);
        cookie.setMaxAge(maxAge);
        cookie.setPath(cookiePath);
        cookie.setSecure(request.isSecure());
        cookie.setDomain("." + this.domainName);

        if (maxAge < 1) {
            cookie.setVersion(1);
        }

        // If supported, the cookie will only be available via HTTP (and not JavaScript for instance)
        Method method = ReflectionUtils.findMethod(Cookie.class, "setHttpOnly", new Class[]{Boolean.TYPE});

        if (method != null) {
            ReflectionUtils.invokeMethod(method, cookie, Boolean.TRUE);
        }

        response.addCookie(cookie);
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }
}