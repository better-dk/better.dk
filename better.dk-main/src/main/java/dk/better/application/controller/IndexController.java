package dk.better.application.controller;

import dk.better.application.dto.ExternalReviewDTO;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.service.CityService;
import dk.better.application.service.ImageService;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Industry;
import dk.better.company.entity.Review;
import dk.better.company.service.ExternalReviewService;
import dk.better.company.service.IndustryService;
import dk.better.company.service.ReviewService;
import dk.better.company.service.SubscriptionService;
import org.apache.commons.lang3.StringUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Bo Andersen
 */
@Controller("applicationIndexController")
public class IndexController {
    @Autowired
    private IndustryService industryService;

    @Autowired
    private CityService cityService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ExternalReviewService externalReviewService;

    @Autowired
    private SubscriptionService subscriptionService;

    //@Inject
    //private Facebook facebook;

    @RequestMapping("/")
    public ModelAndView home(Model model) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest(0, 10, sortOrder);
        Page<Review> reviewPage = this.reviewService.find(pageable);
        Map<String, String> metas = new HashMap<>(1);

        metas.put("description", "Find, Kontakt & Bedøm Lokale Virksomheder på Better.dk - Den Bedste Søgemaskine til Lokale Virksomheder.");

        model.addAttribute("metas", metas);
        model.addAttribute("title", "Find, Kontakt & Bedøm Lokale Virksomheder - Better.dk");
        model.addAttribute("latestReviews", reviewPage.hasContent() ? reviewPage.getContent() : null);
        java.util.List<ExternalReviewDTO> externalReviewsFromDB = externalReviewService.findExternalReviewsLimited();
        java.util.List<ExternalReviewDTO> externalReviews = externalReviewsFromDB
                .stream()
                .map((reviewObj) -> {
                    reviewObj.setReview(
                            reviewObj.getReview().contains("(Original)")
                                    ? StringUtils.substringAfter(reviewObj.getReview(), "(Original)")
                                    :reviewObj.getReview());
                    return reviewObj;
                })
                .collect(Collectors.toList());
        model.addAttribute("externalReviews", externalReviews);
        return new ModelAndView("home");
    }

    private void resizeImagesInDirectoryForGooglePlus(String pathToSourceDirectory, String pathToTargetDirectory) throws RuntimeException {
        try {
            File[] files = new File(pathToSourceDirectory).listFiles();

            if (files != null) {
                for (File file : files) {
                    if (file.getName().equals(".DS_Store") || file.isDirectory()) {
                        continue;
                    }

                    BufferedImage resized = this.imageService.resize(ImageIO.read(file), Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 250, 250);

                    // Remove alpha channel
                    BufferedImage convertedImage = new BufferedImage(554, 250, BufferedImage.TYPE_INT_RGB);
                    convertedImage.getGraphics().drawImage(resized, 152, 0, 250, 250, Color.WHITE, null);

                    File saved = new File(pathToTargetDirectory + file.getName());
                    ImageIO.write(convertedImage, "png", saved);

                    System.out.println("Resized " + file.getName());
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @RequestMapping("/facebook/test")
    public String facebookTest(Model model) {
        //if (!this.facebook.isAuthorized()) {
        //throw new AccessDeniedException("Not allowed to access Facebook!");
        //}

        //List<PlaceTag> taggedPlaces = this.facebook.userOperations().getTaggedPlaces();
        //PagedList<Page> likedPages = this.facebook.likeOperations().getPagesLiked();
        //User userProfile = this.facebook.userOperations().getUserProfile();


        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", "Facebook test");
        model.addAttribute("metas", metas);
        model.addAttribute("title", "Facebook test");
        return "home";
    }

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public ResponseEntity ping() {
        return new ResponseEntity(null, HttpStatus.OK);
    }

    @RequestMapping(value = "/mandrilltest", method = RequestMethod.POST/*, headers = { "Content-Type=application/json" }*/)
    @ResponseBody
    public void mandrillTest(@RequestParam(value = "mandrill_events", required = true) @RequestBody String json, HttpServletRequest request) {
        //logger.error("Received JSON: " + json);
        //logger.error("Mandrill IP: " + request.getRemoteAddr());
    }

    @RequestMapping("/404")
    public void test404() {
        throw new ResourceNotFoundException("Something could not be found!"); // improve: remove
    }

    @RequestMapping("/500")
    public void test500() {
        throw new RuntimeException("Something went wrong!"); // improve: remove
    }

    @RequestMapping("/access-denied")
    public String accessDenied() {
        return "access-denied";
    }

    @RequestMapping(value = {"/terms"})
    public String terms(Model model) {
        HashMap<String, String> metas = new HashMap<String, String>(1);
        metas.put("robots", "noindex");
        model.addAttribute("title", (Object) "Betingelser");
        model.addAttribute("metas", metas);
        return "terms-and-conditions";
    }

    @RequestMapping(value = {"/brancher"})
    public String industriesIndex(Model model) {
        HashMap<String, String> metas = new HashMap<String, String>(1);
        metas.put("description", "Branche oversigt p\u00e5 Better.dk.");
        model.addAttribute("metas", metas);
        model.addAttribute("industries", (Object) this.industryService.findAll(new Sort(new Sort.Order[]{new Sort.Order(Sort.Direction.ASC, "name")})));
        model.addAttribute("title", (Object) "Branche oversigt");
        return "industries-index";
    }

    @RequestMapping("/brancher/{industryId}/{industrySlug}")
    public String industryCityIndex(@PathVariable("industryId") int industryId, @PathVariable("industrySlug") String industrySlug, @RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        Industry industry = this.industryService.find(industryId);

        if (industry == null) {
            throw new ResourceNotFoundException(String.format("Industry not found (ID: %d)", industryId));
        }

        if (!GeneralUtils.getSlug(industry.getName()).equals(industrySlug)) {
            throw new InvalidArgumentException(String.format("Invalid industry slug (\"%s\")!", industrySlug));
        }

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.ASC, "city"));
        Pageable pageable = new PageRequest((page - 1), 500, sortOrder);
        Page<String> cities = this.cityService.findAllCitiesWithCompaniesInIndustry(industryId, pageable);

        Map<String, String> metas = new HashMap<>(1);
        metas.put("description", String.format("Find og kontakt lokale %s på Better.dk.", industry.getName().toLowerCase()));

        model.addAttribute("metas", metas);
        model.addAttribute("title", industry.getName());
        model.addAttribute("industryId", industryId);
        model.addAttribute("industry", industry);
        model.addAttribute("industrySlug", industrySlug);
        model.addAttribute("cities", cities.getContent());
        model.addAttribute("page", page);
        model.addAttribute("totalPages", cities.getTotalPages());

        return "industry-city-index";
    }
}