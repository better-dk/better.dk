package dk.better.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pusher.rest.Pusher;
import dk.better.account.entity.Account;
import dk.better.account.entity.AccountCompany;
import dk.better.application.util.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bo Andersen
 */
@Controller
@RequestMapping("/push")
public class PushController {
    @Autowired
    private Pusher pusher; // improve: wrap this in a service

    @Autowired
    private ObjectMapper objectMapper;


    @RequestMapping(value = "/auth/company/notifications", method = RequestMethod.GET, produces = "application/javascript; charset=utf-8")
    @ResponseBody
    public String authenticateCompanyNotifications(@RequestParam(value = "channel_name", required = true) String channelName, @RequestParam(value = "socket_id", required = true) String socketId, @RequestParam(value = "callback", required = true) String callback, HttpServletResponse response) {
        if (!channelName.matches("private-company-notifications-[0-9]+") || !AuthUtils.isAuthenticated()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return "";
        }

        // Extract company ID from channel name
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(channelName);
        matcher.find();
        int companyId = Integer.parseInt(matcher.group());

        // Validate that the user is associated with the company
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        for (AccountCompany ac : account.getAccountCompanies()) {
            if (ac.getCompany().getId() == companyId) {
                response.setStatus(HttpServletResponse.SC_OK);
                return callback + "(" + this.pusher.authenticate(socketId, channelName) + ")";
            }
        }

        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return "";
    }

    @RequestMapping(value = "/auth/user/notifications", method = RequestMethod.GET, produces = "application/javascript; charset=utf-8")
    @ResponseBody
    public String authenticateUserNotifications(@RequestParam(value = "channel_name", required = true) String channelName, @RequestParam(value = "socket_id", required = true) String socketId, @RequestParam(value = "callback", required = true) String callback, HttpServletResponse response) {
        if (!channelName.matches("private-user-notifications-[a-z0-9-]+") || !AuthUtils.isAuthenticated()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return "";
        }

        String uuid = channelName.replace("private-user-notifications-", "");
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (account.getUuid().equals(uuid)) {
            response.setStatus(HttpServletResponse.SC_OK);
            return callback + "(" + this.pusher.authenticate(socketId, channelName) + ")";
        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return "";
        }
    }
}