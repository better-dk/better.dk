package dk.better.application.model;

import java.util.LinkedHashMap;

/**
 * @author Bo Andersen
 */
public class SearchResults<T> {
    private Long numberOfHits;
    private Long start;
    private int page;
    private int totalPages;
    private LinkedHashMap<Integer, SearchResultEntry<T>> hits; // Using LinkedHashMap to preserve search result order


    public Long getNumberOfHits() {
        return numberOfHits;
    }

    public void setNumberOfHits(Long numberOfHits) {
        this.numberOfHits = numberOfHits;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public LinkedHashMap<Integer, SearchResultEntry<T>> getHits() {
        return hits;
    }

    public void setHits(LinkedHashMap<Integer, SearchResultEntry<T>> hits) {
        this.hits = hits;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}