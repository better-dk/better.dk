package dk.better.application.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Bo Andersen
 */
@JsonSerialize
public class EmptyJsonResponse {
}