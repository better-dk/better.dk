package dk.better.application.exception;

/**
 * @author Bo Andersen
 */
public class LimitExceededException extends RuntimeException {
    public LimitExceededException(String message) {
        super(message);
    }

    public LimitExceededException(Throwable cause) {
        super(cause);
    }

    public LimitExceededException(String message, Throwable cause) {
        super(message, cause);
    }
}