<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript">
    var ACCEPT_CONFIRM_TEXT = "<spring:message code="admin.company.requests-subdomains.accept-confirm-text" />";
    var ERROR_SUBDOMAIN_TAKEN = "<spring:message code="admin.company.requests-subdomains.error-subdomain-taken" />";
</script>

<div class="modal fade" id="decline-reason-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close remove-login-hash remove-login-destination" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Decline Request for <span id="decline-request-for"></span></h4>
            </div>

            <div class="modal-body">
                <p>You are about to decline this subdomain request. You may enter a reason for declining the request
                    below (optional).</p>

                <textarea name="decline-request-reason" id="decline-request-reason" class="form-control"
                          rows="5"></textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Luk</button>
                <button type="button" class="btn btn-primary" id="button-do-decline-request">Decline Request</button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <h2>Subdomain Requests</h2>
        <br/>

        <c:choose>
            <c:when test="${!empty requests}">
                <div class="row" style="border-bottom: 1px solid #777777;">
                    <div class="col-xs-2">Company</div>
                    <div class="col-xs-3">Current URL</div>
                    <div class="col-xs-2">Requested URL</div>
                    <div class="col-xs-2">Subdomain History</div>
                    <div class="col-xs-2">Requester</div>
                    <div class="col-xs-1">Actions</div>
                </div>

                <c:forEach var="request" items="${requests}">
                    <c:set var="company" value="${request.company}"/>
                    <c:set var="requester" value="${request.requester}"/>
                    <c:set var="httpRequest" value="${pageContext.request}"/>

                    <div class="row request" data-request-id="<c:out value="${request.id}" />"
                         data-company-name="<c:out value="${company.name}" />"
                         style="border-bottom: 1px solid #777777;">
                        <div class="col-xs-2">
                            <c:out value="${company.name}"/>
                            <br/>
                            CVR: <c:out value="${company.ciNumber}"/>
                            <br/>
                            <c:out value="${company.streetName} ${company.streetNumber}"/>
                            <br/>
                            <c:out value="${company.postalCode} ${company.city}"/>

                            <c:if test="${!empty company.email}">
                                <br/>
                                <a href="mailto:<c:out value="${company.email}" />"><c:out
                                        value="${company.email}"/></a>
                            </c:if>

                            <c:if test="${!empty company.phoneNumber}">
                                <br/>
                                Phone: <c:out value="${company.phoneNumber}"/>
                            </c:if>

                            <c:if test="${!empty company.callTrackingNumber}">
                                <br/>
                                Phone: <c:out value="${company.callTrackingNumber}"/>
                            </c:if>
                        </div>

                        <div class="col-xs-3">
                            <c:out value="${company.profileUrl}"/>
                        </div>

                        <div class="col-xs-2">
                            <c:out value="${httpRequest.scheme}://${request.requestedSubdomain}.${httpRequest.serverName}"/>
                        </div>

                        <div class="col-xs-2">
                            <c:set var="previousSubdomains" value="${company.getPreviousSubdomains()}"/>

                            <c:choose>
                                <c:when test="${empty previousSubdomains}">-</c:when>

                                <c:otherwise>
                                    <c:forEach var="previousSubdomain" items="${previousSubdomains}">
                                        <c:out value="${previousSubdomain.name}"/> (<fmt:formatDate
                                            value="${previousSubdomain.timeAdded}" dateStyle="FULL"/>)
                                        <br/>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </div>

                        <div class="col-xs-2">
                            <c:out value="${requester.firstName} ${requester.middleName} ${requester.lastName}"/>
                            <br/>
                            <a href="mailto:<c:out value="${requester.email}" />"><c:out
                                    value="${requester.email}"/></a>
                        </div>

                        <div class="col-xs-1">
                            <a href="#" class="accept-subdomain">Accept</a>
                            <br/>
                            <a href="#" class="decline-subdomain">Decline</a>
                        </div>
                    </div>
                </c:forEach>
            </c:when>

            <c:otherwise>
                There are currently no subdomain requests...
            </c:otherwise>
        </c:choose>
    </div>
</div>