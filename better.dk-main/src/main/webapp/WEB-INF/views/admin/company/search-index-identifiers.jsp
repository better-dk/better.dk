<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<a href="#" id="show-company-delete-json">Show Delete JSON</a>
<br/><br/>

<c:forEach var="identifier" items="${identifiers}" varStatus="loop">
    <span class="search-index-identifier"><c:out value="${identifier}"/></span>

    <c:if test="${loop.last == false}">
        ,
    </c:if>
</c:forEach>

<div class="modal fade" id="company-delete-json-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Copy JSON</h4>
            </div>

            <div class="modal-body">
                <textarea class="form-control" cols="6" rows="10" id="delete-companies-json-textarea"></textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>