<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h2>Update Search Index</h2>

<c:if test="${success == true}">
    <p><strong>Awesome, the update request was sent to the search index!</strong></p>
</c:if>

<p>Updates a set of companies within the search index. Define a range of companies (by ID) below.</p>

<form action="/admin/company/update-search-index-range" method="GET">
    <label for="firstId">First Company ID:</label> <input type="number" name="first" id="firstId"/>
    <br/>
    <label for="lastId">Last Company ID:</label> <input type="number" name="last" id="lastId"/>
    <br/>
    <input name="submit-button" type="submit" value="Submit"/>
</form>