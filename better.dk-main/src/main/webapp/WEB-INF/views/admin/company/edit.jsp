<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<c:url var="actionUrl" value="/admin/company/edit/${company.id}"/>

<div class="container">
    <form:form action="${actionUrl}" modelAttribute="company" acceptCharset="UTF-8">
        <form:hidden path="id"/>

        <%-- Name --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="name">Name:</form:label></div>
            <div class="col-xs-4"><form:input path="name"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Subdomain --%>
        <div class="row">
            <div class="col-xs-4"><label for="subdomain-name">Subdomain:</label></div>
            <div class="col-xs-4">
                <c:set var="currentSubdomainText" value=""/>

                <c:if test="${currentSubdomain != null}">
                    <c:set var="currentSubdomainText" value="${currentSubdomain.name}"/>
                </c:if>

                <input type="text" name="subdomains[].name" id="subdomain-name" value="${currentSubdomainText}"/>

                <c:if test="${!empty previousSubdomains}">
                    <ul>
                        <li><c:out value="${currentSubdomain.name}"/> (<fmt:formatDate
                                value="${currentSubdomain.timeAdded}" dateStyle="FULL"/>)
                        </li>

                        <c:forEach var="previousSubdomain" items="${previousSubdomains}">
                            <li><c:out value="${previousSubdomain.name}"/> (<fmt:formatDate
                                    value="${previousSubdomain.timeAdded}" dateStyle="FULL"/>)
                            </li>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
            <div class="col-xs-4 error-message" id="subdomain-feedback"></div>
        </div>

        <%-- E-mail address --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="email">E-mail:</form:label></div>
            <div class="col-xs-4"><form:input path="email"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Website --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="website">Website:</form:label></div>
            <div class="col-xs-4"><form:input path="website"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Street name --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="streetName">Street Name:</form:label></div>
            <div class="col-xs-4"><form:input path="streetName"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Street number --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="streetNumber">Street Number:</form:label></div>
            <div class="col-xs-4"><form:input path="streetNumber"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- C/O name --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="coName">C/O name:</form:label></div>
            <div class="col-xs-4"><form:input path="coName"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Post box --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="postBox">Post box:</form:label></div>
            <div class="col-xs-4"><form:input path="postBox"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Additional address text --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="additionalAddressText">Additional address text:</form:label></div>
            <div class="col-xs-4"><form:textarea path="additionalAddressText"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Postal code --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="postalCode">Postal code:</form:label></div>
            <div class="col-xs-4"><form:input path="postalCode"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>


        <%-- City --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="city">City:</form:label></div>
            <div class="col-xs-4"><form:input path="city"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>


        <%-- Phone number --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="phoneNumber">Phone Number:</form:label></div>
            <div class="col-xs-4"><form:input path="phoneNumber"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Call tracking number --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="callTrackingNumber">Call Tracking Number:</form:label></div>
            <div class="col-xs-4"><form:input path="callTrackingNumber"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Teaser --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="teaser">Teaser:</form:label></div>
            <div class="col-xs-4"><form:textarea path="teaser"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Description --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="description">Description:</form:label></div>
            <div class="col-xs-4"><form:textarea path="description"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- CI number --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="ciNumber">CI Number:</form:label></div>
            <div class="col-xs-4"><form:input path="ciNumber"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Booking URL --%>
        <div class="row">
            <div class="col-xs-4"><form:label path="bookingUrl">Booking URL:</form:label></div>
            <div class="col-xs-4"><form:input path="bookingUrl"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Industries --%>
        <div class="row">
            <div class="col-xs-4">Industries:</div>
            <div class="col-xs-4" style="max-height: 300px; width: 600px; overflow-y: scroll;">
                <c:forEach var="industry" items="${industries}" varStatus="loop">
                    <div class="industry">
                        <input type="checkbox"
                               <c:if test="${custom:contains(currentIndustries, industry.id)}">checked</c:if>

                               name="industries[${loop.index}].id" id="industries${loop.index}" value="${industry.id}"
                               data-industry-name="<c:out value="${industry.name}" />"/>
                        <label for="industries${loop.index}"><c:out value="${industry.name}"/></label>
                    </div>
                </c:forEach>
            </div>

            <div class="col-xs-4 error-message" id="industries-feedback"></div>

            <hr/>
        </div>

        <%-- Services --%>
        <div class="row">
            <div class="col-xs-4">Services:</div>
            <div class="col-xs-4">
                <div id="services-wrapper">
                    <c:forEach var="service" items="${services}" varStatus="loop">
                        <div class="service" data-id="<c:out value="${service.id}" />" data-index="${loop.index}">
                            <c:set var="hasService" value="${currentServices.containsKey(service.id)}"/>

                            <input type="checkbox"
                                   <c:if test="${hasService}">checked</c:if> name="services[${loop.index}].id"
                                   id="services${loop.index}" value="${service.id}"/>
                            <label for="services${loop.index}"><c:out value="${service.name}"/></label>

                                <%-- Ugly one liner to prevent spaces (cased by JSTL) within textarea --%>
                            <textarea name="description" class="service-description"
                                      <c:if test="${!hasService}">style="display: none;"</c:if>><c:if
                                    test="${hasService}"><c:out
                                    value="${currentServices[service.id]}"/></c:if></textarea>

                            <br/>
                        </div>
                    </c:forEach>
                </div>

                <div id="add-new-service">
                    <a href="#">+ Add Service</a>
                </div>
            </div>

            <div class="col-xs-4 error-message"></div>

            <hr/>
        </div>

        <input type="hidden" name="meta.id" id="meta.id" value="<c:out value="${company.meta.id}" />"/>

        <div class="row">
            <div class="col-xs-4"><form:label path="meta.googlePlusUrl">Google+ URL:</form:label></div>
            <div class="col-xs-4"><form:input path="meta.googlePlusUrl"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <div class="row">
            <div class="col-xs-4"><form:label path="meta.facebookUrl">Facebook URL:</form:label></div>
            <div class="col-xs-4"><form:input path="meta.facebookUrl"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <div class="row">
            <div class="col-xs-4"><form:label path="meta.yelpUrl">Yelp URL:</form:label></div>
            <div class="col-xs-4"><form:input path="meta.yelpUrl"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <div class="row">
            <div class="col-xs-4"><form:label path="meta.omdoemmedkUrl">Omdoemme.dk URL:</form:label></div>
            <div class="col-xs-4"><form:input path="meta.omdoemmedkUrl"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <div class="row">
            <div class="col-xs-4"><form:label path="meta.bedoemmelsedkUrl">Bedoemmelse.dk URL:</form:label></div>
            <div class="col-xs-4"><form:input path="meta.bedoemmelsedkUrl"/></div>
            <div class="col-xs-4 error-message"></div>
        </div>

        <%-- Submit --%>
        <div class="row">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <form:button id="submitChanges">Save Changes</form:button>
            </div>
            <div class="col-xs-4 error-message"></div>
        </div>
    </form:form>
</div>

<div class="modal fade in" id="add-new-service-dialog" tabindex="-1" role="dialog" aria-hidden="false"
     style="display: none;">
    <%--<div class="modal-backdrop fade in" style="height: 508px;"></div>--%>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Add New Service</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Industries</label>
                        <div class="col-sm-9" style="max-height: 200px; overflow-y: scroll;">
                            <c:forEach var="industry" items="${industries}" varStatus="loop">
                                <div class="new-service-industry-wrapper">
                                    <input type="checkbox" name="industries[${loop.index}].id"
                                           id="service-industries${loop.index}" value="${industry.id}"/>
                                    <label for="service-industries${loop.index}"><c:out
                                            value="${industry.name}"/></label>
                                </div>

                                <br/>
                            </c:forEach>
                        </div>
                        <div class="error-message"></div>
                    </div>

                    <div class="form-group">
                        <label for="service-name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input name="name" id="service-name" type="text"/>
                        </div>
                        <div class="error-message"></div>
                    </div>

                    <div class="form-group">
                        <label for="service-default-description" class="col-sm-3 control-label">Default
                            description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="6" name="defaultDescription"
                                      id="service-default-description"
                                      placeholder="Default description of the service"></textarea>
                        </div>
                        <div class="error-message"></div>
                    </div>

                    <div class="form-group">
                        <label for="service-slug" class="col-sm-3 control-label">Slug</label>
                        <div class="col-sm-9">
                            <input name="slug" id="service-slug" type="text"/>
                        </div>
                        <div class="error-message"></div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" id="new-service-reset">Reset</button>
                <button type="button" class="btn btn-primary" id="add-new-service-button">Add Service</button>
            </div>
        </div>
    </div>
</div>