<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="container">
    <form:form id="addCompanyForm" method="post" commandName="company" action="/admin/company/add-company"
               acceptCharset="UTF-8">
        <form:hidden path="id"/>

        <c:choose>
            <c:when test="${savedCompany.id!=null}">
                <div class="row">
                    <div class="col-xs-4">Company ID:</div>
                    <div class="col-xs-4">${savedCompany.id}</div>
                    <div class="col-xs-4 error-message"></div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <div class="col-xs-4" style="color: red"><form:label path="name">Name<sup>*</sup>:</form:label>
                    </div>
                    <div class="col-xs-4"><form:input path="name" type="text" id="name"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%--Subdomain--%>
                <div class="row">
                    <div class="col-xs-4"><label for="subdomainName">Subdomain:</label></div>
                    <div class="col-xs-4">
                        <c:set var="currentSubdomainText" value=""/>
                        <input type="text" name="subdomains" id="subdomainName" value="${currentSubdomainText}"/>
                    </div>
                    <div class="col-xs-4 error-message" id="subdomain-feedback"></div>
                </div>

                <%-- E-mail address --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="email">E-mail:</form:label></div>
                    <div class="col-xs-4"><form:input path="email" type="email"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Website --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="website">Website:</form:label></div>
                    <div class="col-xs-4"><form:input path="website" id="website"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Street name --%>
                <div class="row">
                    <div class="col-xs-4" style="color: red"><form:label
                            path="streetName">Street Name<sup>*</sup>:</form:label></div>
                    <div class="col-xs-4"><form:input path="streetName" id="streetName" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Street number --%>
                <div class="row">
                    <div class="col-xs-4" style="color: red"><form:label
                            path="streetNumber">Street Number<sup>*</sup>:</form:label></div>
                    <div class="col-xs-4"><form:input path="streetNumber" id="streetNumber" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- C/O name --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="coName">C/O name:</form:label></div>
                    <div class="col-xs-4"><form:input path="coName" id="coName" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Post box --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="postBox">Post box:</form:label></div>
                    <div class="col-xs-4"><form:input path="postBox" id="postBox" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Additional address text --%>
                <div class="row">
                    <div class="col-xs-4"><form:label
                            path="additionalAddressText">Additional address text:</form:label></div>
                    <div class="col-xs-4"><form:textarea path="additionalAddressText" id="additionalAddressText"
                                                         type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Postal code --%>
                <div class="row">
                    <div class="col-xs-4" style="color: red"><form:label
                            path="postalCode">Postal code<sup>*</sup>:</form:label></div>
                    <div class="col-xs-4"><form:input path="postalCode" type="text"
                                                      oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                                      this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                      id="postalCode"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Postal district --%>
                <div class="row">
                        <%--<div class="col-xs-4" style="color: red"><form:label path="postalDistrict" >Postal district<sup>*</sup>:</form:label></div>--%>
                    <div class="col-xs-4"><form:input path="postalDistrict" type="hidden" oninput=""/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- City --%>
                <div class="row">
                    <div class="col-xs-4" style="color: red"><form:label path="city">City<sup>*</sup>:</form:label>
                    </div>
                    <div class="col-xs-4"><form:input path="city" id="city" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Phone number --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="phoneNumber">Phone Number:</form:label></div>
                    <div class="col-xs-4"><form:input path="phoneNumber" type="text" id="phoneNumber"
                                                      oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Call tracking number --%>
                <div class="row">
                    <div class="col-xs-4" style="color: red"><form:label
                            path="pnr">Pnr<sup>*</sup>:</form:label></div>
                    <div class="col-xs-4"><form:input path="pnr" type="text" id="pnr" oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                                                                            this.value = this.value.replace(/(\..*)\./g, '$1');"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Teaser --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="teaser">Teaser:</form:label></div>
                    <div class="col-xs-4"><form:textarea path="teaser" id="teaser" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Description --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="description">Description:</form:label></div>
                    <div class="col-xs-4"><form:textarea path="description" id="description" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- CI number --%>
                <div class="row">
                    <div class="col-xs-4" style="border-color: red; color: red"><form:label
                            path="ciNumber">CI Number<sup>*</sup>:</form:label></div>
                    <div class="col-xs-4"><form:input path="ciNumber"
                                                      id="ciNumber" type="text"
                                                      oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%-- Booking URL --%>
                <div class="row">
                    <div class="col-xs-4"><form:label path="bookingUrl">Booking URL:</form:label></div>
                    <div class="col-xs-4"><form:input path="bookingUrl" id="bookingUrl" type="text"/></div>
                    <div class="col-xs-4 error-message"></div>
                </div>

                <%--Industries--%>
                <div class="row">
                    <div class="col-xs-4" style="border-color: red; color: red">Industries<sup>*</sup>:</div>
                    <div class="col-xs-4" style="max-height: 300px; width: 600px; overflow-y: scroll;">
                        <c:forEach var="industry" items="${industries}" varStatus="loop">
                            <div class="industry">
                                <input type="checkbox"

                                       <c:if test="${custom:contains(currentIndustries, industry.id)}">checked</c:if>
                                       name="industries" id="industries${loop.index}" value="${industry.id}"
                                       data-industry-name="<c:out value="${industry.name}" />"/>
                                <label for="industries${loop.index}"><c:out value="${industry.name}"/></label>
                            </div>
                        </c:forEach>
                    </div>

                    <div class="col-xs-4 error-message" id="industries-feedback"></div>

                    <hr/>
                </div>

                <%-- Submit --%>
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4">
                        <form:button id="submitChanges" type="submit">Save Changes</form:button>
                    </div>
                    <div class="col-xs-4 error-message"></div>
                </div>


            </c:otherwise>
        </c:choose>

    </form:form>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <script>
        $("#addCompanyForm").validate(
            {
                rules: {
                    name: {
                        required: true,
                        minlength: 1,
                        maxlength: 150
                    },
                    email: {

                        maxlength: 100,
                        email: true
                    },
                    subdomainName: {
                        minlength: 3,
                        maxlength: 50,
                    },
                    website: {
                        url: true,
                        maxlength: 100
                    },
                    streetName: {
                        required: true,
                        minlength: 2,
                        maxlength: 50
                    },
                    streetNumber: {
                        required: true,
                        maxlength: 25
                    },
                    coName: {
                        maxlength: 50
                    },
                    postBox: {
                        maxlength: 50
                    },
                    additionalAddressText: {
                        maxlength: 100
                    },
                    postalCode: {
                        required: true
                    },
                    city: {
                        required: true,
                        minlength: 2,
                        maxlength: 50
                    },
                    phoneNumber: {
                        minlength: 8,
                        maxlength: 11
                    },
                    pnr: {
                        required: true,
                        minlength: 1,
                        maxlength: 12
                    },
                    teaser: {
                        minlength: 50,
                        maxlength: 175
                    },
                    description: {
                        minlength: 100,
                        maxlength: 5000
                    },
                    ciNumber: {
                        required: true,
                        minlength: 1,
                        maxlength: 8

                    },
                    bookingUrl: {
                        url: true
                    },
                    "industries": {
                        required: true,
                        minlength: 1
                    }


                },
                submitHandler: function (form) {
                    form.submit();
                }

            }
        );
    </script>
</div>

