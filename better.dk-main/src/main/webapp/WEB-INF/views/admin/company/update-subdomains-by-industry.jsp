<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>

<style>
    .row div {
        margin-bottom: 5px;
    }

    input.new-subdomain {
        width: 300px;
        float: left;
        margin: -5px 5px 0 5px;
    }

    .new-subdomain-text {
        float: left;
    }
</style>

<script type="text/javascript">
    var URL_UPDATE_SUBDOMAIN_BASE = "<custom2:domainNameUri />/ajax/admin/company/";
    var URL_UPDATE_SUBDOMAIN_POST = "/subdomains/update";
</script>

<script type="text/javascript">
    $(function () {
        $('#choose-industry').change(function () {
            window.location = '?industryId=' + $(this).val();
        });

        $('#only-show-with-booking-url').change(function () {
            var industryId = parseInt($('#choose-industry').val());

            if ($(this).is(':checked')) {
                if (industryId > -1) {
                    window.location = '?industryId=' + industryId + '&with-booking-url=true';
                } else {
                    window.location = '?with-booking-url=true';
                }
            } else {
                if (industryId > -1) {
                    window.location = '?industryId=' + industryId;
                } else {
                    window.location = window.location.pathname;
                }
            }
        });
    });
</script>

<select id="choose-industry">
    <option value="-1">Choose industry</option>

    <c:forEach var="industry" items="${industries}">
        <option value="<c:out value="${industry.id}" />"
                <c:if test="${industryId != null && industryId == industry.id}">selected</c:if>><c:out
                value="${industry.name}"/></option>
    </c:forEach>
</select>
&nbsp;&nbsp;
<input type="checkbox"
       <c:if test="${onlyWithBookingUrl == true}">checked</c:if> id="only-show-with-booking-url"/> <label
        for="only-show-with-booking-url">Only show companies with booking URL</label>

<c:if test="${industryId != null}">
    <c:choose>
        <c:when test="${results.hasContent()}">
            <h2>Showing <span id="total-company-matches"><c:out value="${results.totalElements}"/></span> companies
                without subdomain</h2>

            <br/>

            <div class="row">
                <div class="col-xs-4"><strong>Company</strong></div>
                <div class="col-xs-2"><strong>Current Profile URL</strong></div>
                <div class="col-xs-6"><strong>New Profile URL</strong></div>
            </div>

            <c:forEach var="company" items="${results.getContent()}">
                <div class="row company" data-company-id="<c:out value="${company.id}" />">
                    <div class="col-xs-4 company-name">
                        <c:out value="${company.name}"/>
                    </div>

                    <div class="col-xs-2">
                        <a href="<c:out value="${company.profileUrl}" />" title="Show profile" target="_blank">
                            <c:out value="${company.profileUrl}"/>
                        </a>
                    </div>

                    <div class="col-xs-5">
                        <div class="new-subdomain-text">
                                ${pageContext.request.scheme}://
                        </div>

                        <input type="text" class="form-control new-subdomain" placeholder="new-subdomain"/>

                        <div class="new-subdomain-text">
                            .${pageContext.request.serverName}
                        </div>
                    </div>

                    <div class="col-xs-1">
                        <button class="btn btn-primary update-subdomain" style="position: relative; top: -5px;">Update
                        </button>
                    </div>
                </div>
            </c:forEach>

            <custom:pagination numberOfPageLinks="10" page="${page}" totalPages="${results.getTotalPages()}"
                               pageLinkPrefix="${currentPageUrl}&"/>
        </c:when>

        <c:otherwise>
            <p>There are no companies without a subdomain in this industry.</p>
        </c:otherwise>
    </c:choose>
</c:if>