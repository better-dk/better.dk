<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="actionUrl" value="/admin/company/update-search-index-lines"/>

<h1>Refresh Search Index for Company IDs</h1>

<p>One company ID per line.</p>

<c:if test="${success}">
    The request was successfully sent off to the search index!
</c:if>

<form:form action="${actionUrl}" commandName="dto" method="POST" acceptCharset="UTF-8">
    <form:textarea name="companyIds" path="companyIds" rows="5" cols="10"/>
    <br/>
    <form:errors path="companyIds"/>
    <br/>
    <form:button id="refresh-companies">Refresh Search Index</form:button>
</form:form>