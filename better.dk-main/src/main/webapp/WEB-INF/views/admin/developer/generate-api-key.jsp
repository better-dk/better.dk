<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Generate API Key</h1>

<c:if test="${!empty apiKey}">
    <p>API key: <c:out value="${apiKey}"/></p>
</c:if>

<form action="/admin/developer/company/generate-api-key" method="get">
    <label for="companyIdInput">Company ID:</label>
    <br/>
    <input type="number" name="companyId" id="companyIdInput" placeholder="Company ID, e.g. 1000002"/>
    <br/><br/>
    <input type="submit" name="submitButton" value="Generate Key"/>
</form>