<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="dateValue" class="java.util.Date"/>

<div class="container-fluid">
    <div class="container">
        <c:choose>
            <c:when test="${!empty events}">
                <div class="row">
                    <div class="col-xs-2">Time</div>
                    <div class="col-xs-10">Message</div>
                </div>

                <c:forEach var="event" items="${events}">
                    <div class="row event-wrapper">
                        <div class="col-xs-2">
                            <jsp:setProperty name="dateValue" property="time" value="${event.timestamp}"/>
                            <fmt:formatDate value="${dateValue}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </div>

                        <div class="col-xs-10 event-message"><c:out value="${event.message}"/></div>
                    </div>
                </c:forEach>
            </c:when>

            <c:otherwise>
                <p>There are no log events for this log group.</p>
            </c:otherwise>
        </c:choose>
    </div>
</div>