<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${!empty logGroups}">
        <p>Choose which log group you would like to view log events for. Note that the number of log events for each log
            stream is limited to 1000.</p>

        <c:forEach var="logGroup" items="${logGroups}">
            <a href="/admin/logs/errors/<c:out value="${logGroup.logGroupName}" />">
                <c:out value="${logGroup.logGroupName}"/>
            </a>
            <br/>
        </c:forEach>
    </c:when>

    <c:otherwise>
        No log groups are available yet...
    </c:otherwise>
</c:choose>