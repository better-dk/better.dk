<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>

<style>
    .message-row {
        display: none;
    }

    .message-row p {
        padding: 10px;
    }
</style>

<script type="text/javascript">
    $(function () {
        var database;


        function initialize() {
            window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

            if (!window.indexedDB) {
                return;
            }

            var request = window.indexedDB.open('Better', 2);

            request.onsuccess = function (event) {
                database = event.target.result;

                // Check checkboxes
                var transaction = database.transaction(['leads'], 'readonly');
                var objectStore = transaction.objectStore('leads');

                $('input.processed').each(function () {
                    var input = $(this);
                    var r = objectStore.get(parseInt(input.parents('tr').data('threadId')));
                    r.onsuccess = function (event) {
                        if (typeof event.target.result !== 'undefined' && event.target.result != null) {
                            input.prop('checked', event.target.result.processed);
                        }
                    };
                });
            };

            request.onerror = function (event) {
                alert("Could not open database!");
            };

            request.onupgradeneeded = function (event) {
                event.currentTarget.result.createObjectStore('leads');
            };
        }

        function saveProcesedState(threadId, isProcessed) {
            var transaction = database.transaction(['leads'], 'readwrite');
            var objectStore = transaction.objectStore('leads');
            var request = objectStore.put({
                processed: isProcessed
            }, threadId);

            request.onerror = function () {
                alert("Could not save object");
            };
        }

        $('.edit-link').click(function () {
            var width = 1000;
            var height = 600;
            var left = ((screen.width / 2) - (width / 2));
            var top = ((screen.height / 2) - (height / 2));

            window.open(this.href, 'Edit company', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
            return false;
        });

        $('.view-message').click(function () {
            var target = $(this).parents('tr').next('.message-row');
            var threadId = $(this).parents('tr').data('threadId');

            if (target.html() != '') {
                return; // Already loaded messages
            }

            $.ajax({
                type: 'GET',
                url: '/ajax/thread/' + threadId + '/messages?page=1',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function (result) {
                var messages = result.messages;

                $.each(messages, function () {
                    target.append('<p>' + this.text + '</p><hr />');
                });

                target.show();
            }).fail(function () {
                alert("Failed loading messages!");
            });

            return false;
        });

        $('.trigger-event').click(function () {
            var targets = $(this).parents('tr');
            $.merge(targets, targets.next()); // Merge with message row
            var threadId = $(this).parents('tr').data('threadId');

            $.ajax({
                type: 'POST',
                url: '/ajax/admin/thread/' + threadId + '/send-email-to-receiver',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function () {
                saveProcesedState(threadId, true);
                targets.fadeOut(500, function () {
                    $(this).remove();
                });
            }).fail(function () {
                alert("Failed sending e-mail!");
            });

            return false;
        });

        // Progress storage
        $('input.processed').change(function () {
            if (!window.indexedDB) {
                alert("Your browser must support indexedDB in order for you to use this feature");
                return false;
            }

            var isChecked = $(this).is(':checked');
            var threadId = parseInt($(this).parents('tr').data('threadId'));
            saveProcesedState(threadId, isChecked);
        });

        initialize();
    });
</script>

<h1>Leads (threads)</h1>

<p>Click a checkbox to mark a lead as processed or not processed.</p>

<c:choose>
    <c:when test="${threads.hasContent()}">
        <table class="table table-striped table-hover">
            <thead>
            <tr style="font-weight: bold;">
                <td>Processed</td>
                <td>Company</td>
                <td>Sender</td>
                <td>Subject</td>
                <td>Sent</td>
                <td>Phone Number</td>
                <td>E-mail</td>
                <td>Actions</td>
            </tr>
            </thead>

            <tbody>
            <c:forEach var="thread" items="${threads.getContent()}">
                <tr data-thread-id="<c:out value="${thread.id}" />">
                    <td><input type="checkbox" class="processed"/></td>
                    <td>
                        <a href="<c:out value="${thread.receiver.company.profileUrl}" />" target="_blank">
                            <c:out value="${thread.receiver.company.name}"/>
                        </a>
                    </td>
                    <td><c:out value="${thread.sender.account.firstName} ${thread.sender.account.lastName}"/></td>
                    <td><c:out value="${thread.subject}"/></td>
                    <td><fmt:formatDate value="${thread.lastActivity}" dateStyle="FULL"/></td>
                    <td><c:out value="${thread.receiver.company.phoneNumber}"/></td>
                    <td><c:out value="${thread.receiver.company.email}"/></td>
                    <td>
                        <a href="<c:url value="/admin/company/${thread.receiver.company.id}/edit" />" target="_blank"
                           class="edit-link">Edit</a> -
                        <a href="#" class="view-message">View Message</a> -
                        <a href="#" class="trigger-event">Send e-mail</a>
                    </td>
                </tr>

                <tr class="message-row"></tr>
            </c:forEach>
            </tbody>
        </table>

        <custom:pagination numberOfPageLinks="10" page="${page}" totalPages="${threads.getTotalPages()}"/>
    </c:when>

    <c:otherwise>
        <p>There are currently no leads...</p>
    </c:otherwise>
</c:choose>