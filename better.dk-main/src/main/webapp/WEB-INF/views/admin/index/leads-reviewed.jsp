<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>

<script type="text/javascript">
    $(function () {
        var database;


        function initialize() {
            window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

            if (!window.indexedDB) {
                return;
            }

            var request = window.indexedDB.open('Better', 2);

            request.onsuccess = function (event) {
                database = event.target.result;

                // Check checkboxes
                var transaction = database.transaction(['leads-reviewed'], 'readonly');
                var objectStore = transaction.objectStore('leads-reviewed');

                $('input.processed').each(function () {
                    var input = $(this);
                    var r = objectStore.get(parseInt(input.parents('tr').data('reviewId')));
                    r.onsuccess = function (event) {
                        if (typeof event.target.result !== 'undefined' && event.target.result != null) {
                            input.prop('checked', event.target.result.processed);
                        }
                    };
                });
            };

            request.onerror = function (event) {
                alert("Could not open database!");
            };

            request.onupgradeneeded = function (event) {
                event.currentTarget.result.createObjectStore('leads-reviewed');
            };
        }

        function saveProcesedState(reviewId, isProcessed) {
            var transaction = database.transaction(['leads-reviewed'], 'readwrite');
            var objectStore = transaction.objectStore('leads-reviewed');
            var request = objectStore.put({
                processed: isProcessed
            }, reviewId);

            request.onerror = function () {
                alert("Could not save object");
            };
        }

        // Progress storage
        $('input.processed').change(function () {
            if (!window.indexedDB) {
                alert("Your browser must support indexedDB in order for you to use this feature");
                return false;
            }

            var isChecked = $(this).is(':checked');
            var reviewId = parseInt($(this).parents('tr').data('reviewId'));
            saveProcesedState(reviewId, isChecked);
        });

        $('.trigger-event').click(function () {
            var target = $(this).parents('tr');
            var reviewId = $(this).parents('tr').data('reviewId');

            $.ajax({
                type: 'POST',
                url: '/ajax/admin/review/' + reviewId + '/send-email-to-reviewed-company',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function () {
                saveProcesedState(reviewId, true);
                target.fadeOut(500, function () {
                    $(this).remove();
                });
            }).fail(function () {
                alert("Failed sending e-mail!");
            });

            return false;
        });

        $('.edit-link').click(function () {
            var width = 1000;
            var height = 600;
            var left = ((screen.width / 2) - (width / 2));
            var top = ((screen.height / 2) - (height / 2));

            window.open(this.href, 'Edit company', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
            return false;
        });

        initialize();
    });
</script>

<h1>Leads (reviews)</h1>

<p>Click a checkbox to mark a lead as processed or not processed.</p>

<c:choose>
    <c:when test="${reviews.hasContent()}">
        <table class="table table-striped table-hover">
            <thead>
            <tr style="font-weight: bold;">
                <td>Processed</td>
                <td>Company</td>
                <td>Phone Number</td>
                <td>E-mail</td>
                <td>Date</td>
                <td>Rating</td>
                <td>Actions</td>
            </tr>
            </thead>

            <tbody>
            <c:forEach var="review" items="${reviews.getContent()}">
                <tr data-review-id="<c:out value="${review.id}" />">
                    <td><input type="checkbox" class="processed"/></td>
                    <td>
                        <a href="<c:out value="${review.company.profileUrl}" />/bedoemmelser/<c:out value="${review.uuid}" />"
                           target="_blank">
                            <c:out value="${review.company.name}"/>
                        </a>
                    </td>
                    <td><c:out value="${review.company.phoneNumber}"/></td>
                    <td><c:out value="${review.company.email}"/></td>
                    <td><fmt:formatDate value="${review.created}" dateStyle="FULL"/></td>
                    <td><c:out value="${review.rating}"/>/5</td>
                    <td>
                        <a href="<c:url value="/admin/company/${review.company.id}/edit" />" target="_blank"
                           class="edit-link">Edit</a> -
                        <a href="#" class="trigger-event">Send e-mail</a> -
                        <a href="<c:url value="/admin/company/${review.company.id}/subscriptions" />" target="_blank">Subscriptions</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <custom:pagination numberOfPageLinks="10" page="${page}" totalPages="${reviews.getTotalPages()}"/>
    </c:when>

    <c:otherwise>
        <p>There are currently no leads...</p>
    </c:otherwise>
</c:choose>