<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertAttribute name="js"/>

<script type="text/javascript">
    var URL_REDIRECT_REVIEW = '<c:out value="${redirectUrl}" />';
    var IS_INTERNAL_REDIRECT = <c:out value="${isInternalRedirect}" />;
    var REDIRECT_EVENT_NAME = '<c:out value="${eventName}" />';
</script>

<div class="spacer-30"></div>

<div class="container flex flex-row align-center" style="min-height: 300px;">
    <div class="flex flex-row justify-center col-xs-12" id="nps-wrapper">

        <!-- Modal -->
        <div class="modal fade" id="reviewSiteModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Please take a moment to share your experience with us on one of these
                            review sites.</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <input id="siteLinks" value="${siteLinks}">
                            <c:forEach var="site" items="${siteLinks}">

                                <c:if test="${site.status}">
                                    <div class="col-md-4 col-xs-4 col-ld-4 col-ld-4 sitereview ${site.css}">
                                        <a href="${site.link}" class="thumbnail logo">
                                            <c:if test="${site.css == 'undefined'}">
                                                <h4>${site.name}</h4>
                                                <br>
                                                <img style="height:12px">
                                            </c:if>
                                            <c:if test="${site.css != 'undefined'}">
                                                <img style="height:50px">
                                            </c:if>
                                        </a>
                                    </div>
                                </c:if>
                            </c:forEach>

                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div id="nps-completed">
            <div class="positive">
                <div class="redirect-counter"></div>
                <div class="image"></div>
                <p class="bold headline">Mange tak, din vurdering er nu gemt!</p>
                <p>Du bliver nu viderestillet, hvor du kan skrive en bedømmelse af virksomheden.</p>
            </div>

            <div class="negative">
                <div class="image"></div>
                <p class="bold headline">Mange tak, din vurdering er nu gemt!</p>
                <p><a href="<c:out value="${redirectUrl}" />" class="review-company" title="Bedøm virksomheden">Klik
                    her, hvis du også vil skrive en bedømmelse af virksomheden.</a></p>
            </div>
        </div>
    </div>
</div>

<div class="spacer-30"></div>