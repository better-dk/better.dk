<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom1" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>

<c:set var="numberOfResults" value="${searchResults.numberOfHits}"/>

<div class="container-fluid" id=search-top>
    <h1 class="light">
        Din søgning på

        <c:if test="${!empty what}">
            <span class="semi-bold"><c:out value="${what}"/></span>
        </c:if>

        <c:if test="${!empty where}">
            nær <span class="semi-bold"><c:out value="${where}"/></span>
        </c:if>

        gav

        <c:choose>
            <c:when test="${numberOfResults > 1}"> <c:out value="${numberOfResults}"/> resultater</c:when>
            <c:when test="${numberOfResults == 0}"> ingen resultater</c:when>
            <c:otherwise> 1 resultat</c:otherwise> <%-- Single result --%>
        </c:choose>
    </h1>

    <%--<a href="#" class="bold"></a>--%>
</div>

<div class="container-fluid" id="search-cover-photo"></div>

<div class="container" id="search-results-container">
    <div class="row" id="search-results-cta">
        <c:if test="${!empty what}">
            <!-- Søgeside: Top banner [async] -->
            <script type="text/javascript">
                $(function () {
                    var keywords = ['<c:out value="${what}" />'];
                    var match = BETTER.notifications.getMatchingKeyword(keywords, BETTER.notifications.TYPE_SEARCH_BANNER);

                    if (match) {
                        $('#search-results-cta').css({
                            'margin-bottom': '25px',
                            height: '200px' // Reduces the "flicker" of expanding the container
                        });
                        AdButler.ads.push({
                            handler: function (opt) {
                                AdButler.register(166948, 189405, [1170, 200], 'search-results-cta', opt);
                            }, opt: {place: 0, keywords: BETTER.normalize(match), domain: 'servedbyadbutler.com'}
                        });
                    }
                });
            </script>
        </c:if>
    </div>

    <c:if test="${!empty searchResults}">
        <c:forEach var="hit" items="${searchResults.hits}" varStatus="loop">
            <c:set var="entry" value="${hit.value.entry}"/>
            <c:set var="highlights" value="${hit.value.highlights}"/>

            <c:choose>
                <c:when test="${entry.logoName != null}">
                    <custom2:staticResourceLink relativePath="${entry.logoName}" var="logo"/>
                    <c:set var="background" value="background: #ededed 0 0 / 100% 100% url(${logo}) no-repeat;"/>
                </c:when>

                <c:otherwise>
                    <c:set var="background" value="background-color: #ededed;"/>
                </c:otherwise>
            </c:choose>

            <div class="row search-entry">
                <div class="col-xs-2 left">
                    <a class="company-logo" style=" <c:out value="${background}"/>;">
                        <span class="entry-number semi-bold"><c:out value="${loop.count}"/></span>
                    </a>
                </div>

                <div class="col-xs-7 middle">
                    <h2 class="light">
                        <a href="<c:out value="${entry.profileUrl}" />" title="<c:out value="${entry.name}" />"><c:out
                                value="${entry.name}"/></a>
                    </h2>

                    <div class="light company-address"><c:out
                            value="${entry.streetName} ${entry.streetNumber}, ${entry.postalCode} ${entry.city}"/></div>

                    <c:if test="${entry.teaser != null}">
                        <p class="light">
                            <c:choose>
                                <c:when test="${fn:contains(highlights['teaser'], '<strong>')}">
                                    <c:out value="${highlights['teaser']}" escapeXml="false"/>
                                </c:when>

                                <c:when test="${fn:contains(highlights['description'], '<strong>')}">
                                    <c:out value="${highlights['description']}" escapeXml="false"/>
                                </c:when>

                                <c:otherwise>
                                    <c:out value="${entry.teaser}" escapeXml="false"/>
                                </c:otherwise>
                            </c:choose>
                        </p>
                    </c:if>

                    <div class="search-result-actions">
                        <c:set var="done" value="false"/>
                        <c:forEach var="industry" items="${entry.industries}">
                            <c:if test="${industry.quoteLink!=null && done!='true'}">
                                <a class="btn btn-primary give-me-quote-button" value="${entry.id}"
                                   quote-link="${industry.quoteLink}"
                                   id="give-me-quote-button">Få et tilbud</a>
                                <c:set var="done" value="true"/>
                            </c:if>
                        </c:forEach>
                        <a href="<c:out value="${entry.profileUrl}" />#<spring:message code="hash.send-message" />"
                           class="btn btn-primary send-message">Send besked</a>

                        <c:if test="${!empty entry.bookingUrl}">
                            <a class="btn btn-cta book-online"
                               href="<c:out value="${entry.profileUrl}" />#<spring:message code="hash.view-profile.discount" />"
                               data-company-id="<c:out value="${entry.id}" />">Bestil online - få 10% rabat</a>
                        </c:if>
                    </div>
                </div>

                <div class="col-xs-3 right flex flex-column align-center justify-center">
                    <a href="#" title="Synes godt om virksomheden" class="like-company"
                       data-company-id="<c:out value="${entry.id}" />"></a>
                    <a href="#" title="Gem som favorit" class="favorite-company"
                       data-company-id="<c:out value="${entry.id}" />"></a>

                    <c:choose>
                        <c:when test="${entry.numberOfReviews > 0}">
                            <div class="rating"><c:out value="${custom2:formatCompanyRating(entry.rating)}"/></div>
                            <span class="light rating-description"><custom1:ratingDescription
                                    rating="${entry.rating}"/></span>
                            <div class="stars"
                                 data-stars="<fmt:formatNumber value="${(entry.rating / 2)}" maxFractionDigits="0"  />"></div>
                            <span class="light reviews-count">Baseret på <c:out
                                    value="${entry.numberOfReviews} ${entry.numberOfReviews > 1 ? 'bedømmelser' : 'bedømmelse'}"/></span>
                        </c:when>

                        <c:otherwise>
                            <div class="rating">0,0</div>
                            <a href="<c:out value="${entry.profileUrl}" />/bedoemmelser#<spring:message code="hash.create-review" />"
                               class="light">Vær den første til at bedømme virksomheden</a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </c:forEach>

        <c:if test="${numberOfResults > 10}">
            <custom1:pagination page="${searchResults.page}" totalPages="${searchResults.totalPages}"
                                numberOfPageLinks="10"/>
        </c:if>
    </c:if>
</div>