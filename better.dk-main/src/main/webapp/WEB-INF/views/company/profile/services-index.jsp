<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="top"/>
<div class="spacer-30"></div>
<tiles:insertAttribute name="actions"/>
<div class="spacer-30"></div>

<script type="text/javascript">
    var COMPANY_ID = <c:out value="${company.id}" />;
    var company = {
        id: COMPANY_ID,
        name: "<c:out value="${company.name}" />",
        profile_url: window.location.toString().split('#')[0],
        postal_code: <c:out value="${company.postalCode}" />,
        city: "<c:out value="${company.city}" />",
        industries: [
            <c:forEach var="industry" items="${company.industries}" varStatus="loop">
            "<c:out value="${industry.name}" />"

            <c:if test="${!loop.last}">, </c:if>
            </c:forEach>
        ]
    };
    var ERROR_ALREADY_LIKED_COMPANY = "<spring:message code="error.already.liked.company" />";

    var URL_LIKE_COMPANY = "<custom:domainNameUri />/api/company/<c:out value="${company.id}" />/likes";
    var URL_CLAIM_COMPANY = "<custom:domainNameUri />/ajax/company/claim/<c:out value="${company.id}" />";
    var URL_CALL_COMPANY = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/call";
    var URL_SEND_MESSAGE = "<custom:domainNameUri />/ajax/thread/add";
    var URL_UPDATE_EMAIL = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/email";
    var URL_UPDATE_WEBSITE = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/website";
    var URL_UPDATE_TEASER = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/teaser";
    var URL_UPDATE_INDUSTRIES = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/industries";
    var URL_UPDATE_KEYWORDS = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/keywords";
    var URL_REQUEST_SUBDOMAIN = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/request/subdomain";
    var URL_PROPOSE_CHANGES = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/propose-changes";
    var URL_INDUSTRY_SEARCH = "<custom:domainNameUri />/ajax/industry/search";

    var HASH_SEND_MESSAGE = "<spring:message code="hash.send-message" />";
    var HASH_SEND_EMAIL = "<spring:message code="hash.send-email" />";
    var HASH_CLAIM_COMPANY = "<spring:message code="hash.claim-company" />";
    var HASH_CLAIM_COMPANY_LOGGED_IN = "<spring:message code="hash.claim-company-logged-in" />";
    var HASH_COMPANY_CHANGE_PROPOSAL = "<spring:message code="hash.company-change-proposal" />";
    var HASH_LIKE_COMPANY = "<spring:message code="hash.like-company" />";
    var HASH_DEAL_HUNGRY = "<spring:message code="hash.hungry-deal" />";
    var HASH_DISCOUNT = "<spring:message code="hash.view-profile.discount" />";

    /*analytics.page({
        company: company
    });*/

    // todo: add keywords above
</script>

<div class="container-fluid widget-section split-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-8 left-section">
                <div class="widget tabs-container">
                    <tiles:insertAttribute name="menu-tabs"/>

                    <div class="inner-widget">
                        <c:choose>
                            <c:when test="${empty company.companyServices}">
                                <p>Denne profil har endnu ikke ingen ydelser.</p>
                            </c:when>

                            <c:otherwise>
                                <div class="col-xs-2">
                                    <tiles:insertAttribute name="services-menu"/>
                                </div>

                                <div class="col-xs-6">
                                    <p>En standard beskrivelse her</p>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="col-xs-4 right-section">
                <%-- improve: Consider if it is necessary to move this to a tile. Is this used on any other pages? --%>
                <div class="claim-suggest">
                    <c:if test="${empty company.accountCompanies}">
                        <a href="#" class="btn btn-dark-grey" id="claim-company">Er dette din virksomhed?</a>
                    </c:if>

                    <a href="#" class="btn btn-grey" id="suggest-company-edits">Foreslå ændring</a>
                </div>

                <tiles:insertAttribute name="acknowledgements"/>
            </div>
        </div>
    </div>
</div>

<tiles:insertAttribute name="claim-company-wizard"/>
<tiles:insertAttribute name="propose-company-change-dialog"/>
<tiles:insertAttribute name="call-company-dialog"/>

<c:if test="${!empty company.bookingUrl}">
    <tiles:insertAttribute name="hungry-deal-dialog"/>

    <security:authorize access="isAuthenticated()">
        <tiles:insertAttribute name="hungry-discount-dialog"/>
    </security:authorize>
</c:if>