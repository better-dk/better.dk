<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<script type="text/javascript">
    $(function () {
        $('#invite-customers').on('click', function () {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                return false;
            }

            var input = $('#customers-list');

            if (input.val() == '') {
                return false;
            }

            var customersCount = input.val().match(/.+/gi).length;
            var message = 'Er du sikker på, at du vil invitere ' + customersCount + ' ' + (customersCount > 1 ? 'kunder' : 'kunde') + '?';

            if (customersCount > 1000) {
                alert("Max 1.000 kunder kan tilføjes ad gangen.");
                return false;
            }

            if (!confirm(message)) {
                return false;
            }

            var button = $(this);
            var context = button.parents('.import');
            var image = context.find('.import-icon .icon-image');
            var shouldImportCustomers = $('#import-customers').is(':checked');
            $(button).prop('disabled', true);
            image.removeClass().addClass('loading'); // Remove all classes and add spinner

            $.ajax({
                type: 'POST',
                url: BETTER.ROOT_DOMAIN + '/ajax/cp/company/' + BETTER.CP.COMPANY_ID + '/reviews/invite',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    emails: input.val().trim().split('\n'),
                    addToCustomerList: shouldImportCustomers
                })
            }).done(function (response) {
                input.val('');
                image.removeClass().addClass('correct');
                context.find('#import-customers-wrapper').hide();
                context.find('textarea').hide();
                context.find('.title:not(.success)').remove();

                if (shouldImportCustomers && (response.count != customersCount)) {
                    var title = 'Du har inviteret <span class="import-count bold">' + customersCount + '</span> kunder. <span class="import-count bold">' + response.count + '</span> ud af <span class="attempted bold">' + customersCount + '</span> kunder blev tilføjet til din kundeliste';
                    context.find('.title.success div').html(title);
                    var html = '<p>Ovenstående tal afviger fordi <span class="bold">' + (customersCount - response.count) + '</span> e-mail adresser var inkorrekte, eller fandtes på din kundeliste i forvejen.</p>';
                    $(html).insertBefore(context.find('.title.success a'));
                }

                context.find('.title.success').show();
                context.find('.btn').hide();
            }).fail(function () {
                image.removeClass().addClass('paper-fly');
                $(button).prop('disabled', null);
                alert("Ups, der skete en fejl!");
            });

            return false;
        });

        $('#customers-list').on('focus', function () {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                return false;
            }
        });
    });
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <div class="clear spacer-30"></div>

    <h1>Invitér kunder</h1>
    <div class="description">På denne side kan du nemt invitere dine kunder til at lave en bedømmelse af din
        virksomhed.
    </div>
    <hr/>

    <div class="spacer-50"></div>

    <div class="flex flex-column align-center import">
        <div class="import-icon">
            <div class="icon-image paper-fly"></div>
        </div>

        <div class="title">Du har indtastet <span class="import-count bold">0</span> kunder</div>

        <div class="flex flex-row justify-center" id="import-customers-wrapper">
            <input type="checkbox" name="import-customers" id="import-customers" checked style="margin-right: 5px;"/>
            <label for="import-customers">Tilføj kunderne til min kundeliste</label>
        </div>

        <div class="title success">
            <div>Du har inviteret <span class="import-count bold">0</span> kunder</div>

            <a href="/cp/company/<c:out value="${company.id}" />/reviews/invite<c:out value="${urlPostfix}" />"
               class="light">Invitér flere kunder</a>
        </div>

        <div class="spacer-20"></div>
        <textarea name="customers-list" id="customers-list" class="form-control"
                  placeholder="Indtast e-mail adresser på kunder. Én e-mail pr. linje. Max 1.000 ad gangen."
                  rows="5"></textarea>
        <div class="spacer-20"></div>

        <div class="flex flex-column align-right button-wrapper">
            <button class="btn btn-primary" name="invite-customers" id="invite-customers"
                    data-pattern="Invitér {count} kunder">Invitér kunder
            </button>
        </div>
    </div>
</div>
