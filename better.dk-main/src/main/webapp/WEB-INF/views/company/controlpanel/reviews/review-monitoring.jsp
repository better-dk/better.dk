<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(function () {
        function selectText(elementId) {
            var text = document.getElementById(elementId);

            if (document.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }

        $('.link-wrapper').on('click', function () {
            selectText(this.id);
        });
    });
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <div class="clear spacer-30"></div>

    <h1>Overvågning af bedømmelser</h1>
    <div class="description">
        Nedenfor kan du indsætte URL’er til de steder du ønsker at overvåge. Som standard har vi allerede scannet de
        største bedømmelsessider for bedømmelser på din virksomhed, derfor bør du allerede kunne se nogle URL’er.
        Kontakt os hvis du ønsker at overvåge andre kilder end de nedenstående, vi udvider hele tiden porteføljen af
        sider vi scanner.
    </div>
    <hr/>

    <div>

        <br>
        <h3 style="text-align: center;">External review sites</h3>

        <table class="table">
            <thead>
            <tr>
                <th>Site</th>
                <th>Link</th>
                <%--<th>Is shown?</th>--%>
            </tr>
            </thead>

            <form:form method="post" modelAttribute="reviewLink"
                       action="/cp/company/${company.id}/reviews/monitoring" id="addSite">
                <tr>
                    <td>
                        <label for="google"> Google Place Review Url</label>
                    </td>
                    <td>
                        <input name="googlePlaceUrl" id="google" placeholder="Link" autocomplete="off"
                               class="form-control" value="${reviewLink.googlePlaceUrl}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="facebook"> Facebook Url</label>
                    </td>
                    <td>
                        <input name="facebookUrl" id="facebook" placeholder="Link" autocomplete="off"
                               class="form-control" value="${reviewLink.facebookUrl}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="yelp"> Yelp Url</label>
                    </td>
                    <td>
                        <input name="yelpUrl" id="yelp" placeholder="Link" autocomplete="off"
                               class="form-control" value="${reviewLink.yelpUrl}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="tripadvisor"> Tripadvisor Url</label>
                    </td>
                    <td>
                        <input name="tripadvisorUrl" id="tripadvisor" placeholder="Link" autocomplete="off"
                               class="form-control" value="${reviewLink.tripadvisorUrl}"/>
                    </td>
                </tr>


                <td>
                    <button type="submit" class="btn btn-success form-control glyphicon glyphicon-plus">Update</button>
                </td>
            </form:form>
        </table>

        <div id="reviews-wrapper">
            <jsp:useBean id="dateValue" class="java.util.Date"/>
            <c:forEach var="review" items="${reviews}">
                <a href="${review.link}">
                    <div class="review">
                        <div class="author flex flex-column">
                            <div class="photo">
                                <img style="width: 75px; height: auto;"
                                     src="<custom2:domainNameUri />/images/${review.site}.png"
                                     alt="Standard profil billede"/>
                            </div>
                            <span class="name"><c:out value="${review.author}"/></span>
                        </div>

                        <div class="details">
                            <jsp:setProperty name="dateValue" property="time" value="${review.id * 1000}"/>
                            <span class="date-reviewed"><c:out value="${review.publishDate}"/></span>
                            <div class="stars outlined" data-stars="<c:out value="${review.rating}" />">
                                <span style="width: ${review.rating * 18}px;"></span>
                            </div>
                            <p><custom2:lineToBreakLine value="${fn:escapeXml(review.review)}"/></p>
                        </div>
                    </div>
                </a>
            </c:forEach>
        </div>
    </div>

</div>
