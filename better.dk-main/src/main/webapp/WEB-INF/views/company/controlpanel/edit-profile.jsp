<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(function () {
        function selectText(elementId) {
            var text = document.getElementById(elementId);

            if (document.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }

        $('.link-wrapper').on('click', function () {
            selectText(this.id);
        });


    });
</script>
<div class="col-xs-11 main-container">

    <h1>Profil oplysninger</h1>

    <br/>

    <div class="row">
        <div class="col-xs-7">
        <form:form id="editProfile" method="post" modelAttribute="editCompany"
                   action="/cp/company/${editCompany.id}/edit-profile">
            <table class="table">
                <col width=40%>
                <col width=60%>
                <tr>
                    <td>
                        <label for="name"> Firmanavn</label>
                    </td>
                    <td>
                        <input name="name" id="name" type="text" autocomplete="off"
                               class="form-control" value="${editCompany.name}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="subdomain"> Profil domæne</label>
                    </td>
                    <td>

                        <input id="subdomain"
                               class="form-control"
                               value="<c:if test="${subdomain != null}">
                                <c:forEach var="subdomain" items="${subdomain}">
                                <c:if test="${subdomain.active==true}">
                                        <c:out value="${subdomain.name}"/>
                                        </c:if>
                                </c:forEach>
                                </c:if>"
                               readonly="readonly"
                               style="pointer-events: none; cursor: default;"
                        />

                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="email"> E-mail</label>
                    </td>
                    <td>
                        <input name="email" id="email" type="email" autocomplete="off"
                               class="form-control" value="${editCompany.email}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="website"> Website URL</label>
                    </td>
                    <td>
                        <input name="website" id="website" type="url" autocomplete="off"
                               class="form-control" value="${editCompany.website}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="streetName"> Vejnavn</label>
                    </td>
                    <td>
                        <input name="streetName" id="streetName" type="text" autocomplete="off"
                               class="form-control" value="${editCompany.streetName}"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="streetNumber"> Husnummer, etage, side</label>
                    </td>
                    <td>
                        <input name="streetNumber" id="streetNumber" type="text" autocomplete="off"
                               class="form-control" value="${editCompany.streetNumber}"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="postalCode"> Postnummer</label>
                    </td>
                    <td>
                        <input name="postalCode" id="postalCode" type="text" autocomplete="off"
                               class="form-control" value="${editCompany.postalCode}"
                               oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                                      this.value = this.value.replace(/(\..*)\./g, '$1');"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="city"> By</label>
                    </td>
                    <td>
                        <input name="city" id="city" type="text" autocomplete="off"
                               class="form-control" value="${editCompany.city}"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="phoneNumber"> Telefon nummer</label>
                    </td>
                    <td>
                        <input name="phoneNumber" id="phoneNumber" type="text" autocomplete="off"
                               class="form-control" value="${editCompany.phoneNumber}"
                               oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                                      this.value = this.value.replace(/(\..*)\./g, '$1');"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="teaser"> Kort beskrivelse</label>
                    </td>
                    <td>
                        <textarea name="teaser" id="teaser" type="text" autocomplete="off"
                                  style="resize: none; height: 80px;"
                                  class="form-control">${editCompany.teaser}</textarea>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="description"> Lang beskrivelse</label>
                    </td>
                    <td>
                        <textarea name="description" id="description" type="text" autocomplete="off"
                                  class="form-control"
                                  style="resize: none; height: 150px;"
                        >${editCompany.description}</textarea>
                    </td>
                </tr>


                <td>
                    <button type="submit" class="btn btn-success form-control">Save changes</button>
                </td>

            </table>
        </form:form>
        </div>

        <script>
            $("#editProfile").validate(
                {
                    rules: {
                        name: {
                            required: true,
                            minlength: 1,
                            maxlength: 150
                        },
                        email: {

                            maxlength: 100,
                            email: true
                        },

                        website: {
                            url: true,
                            maxlength: 100
                        },
                        streetName: {
                            required: true,
                            minlength: 2,
                            maxlength: 50
                        },
                        streetNumber: {
                            required: true,
                            maxlength: 25
                        },

                        postalCode: {
                            required: true
                        },
                        city: {
                            required: true,
                            minlength: 2,
                            maxlength: 50
                        },
                        phoneNumber: {
                            minlength: 8,
                            maxlength: 11
                        },

                        teaser: {
                            minlength: 50,
                            maxlength: 175
                        },
                        description: {
                            minlength: 100,
                            maxlength: 5000
                        },

                    },
                    submitHandler: function (form) {
                        form.submit();
                    }

                }
            );
        </script>
    </div>


</div>
