<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(function () {
        function selectText(elementId) {
            var text = document.getElementById(elementId);

            if (document.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }

        $('.link-wrapper').on('click', function () {
            selectText(this.id);
        });


    });
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <div class="clear spacer-30"></div>

    <h1>Widget links</h1>
    <div class="description">
        <b>Vigtig!</b> For at undgå jQuery konflikter du medtage følgende script i bunden af dit websted.
        <br>
        <code>
            &lt;script src="https://better.dk/css/widget/js/widget.js"&gt;&lt;/script&gt;
        </code>
        <br>

    </div>
    <hr/>

    <div class="spacer-50"></div>
    <br>
    Klik <b><a href="<custom2:domainNameUri />/cp/company/${company.id}/reviews/widgets"
               style="color: blue;">her</a></b> for at se sæt widgets til din virksomhed.
</div>
