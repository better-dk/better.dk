<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<jsp:setProperty name="dateValue" property="time" value="${timestampValue}"/>


<security:authorize access="isAuthenticated()" var="isLoggedIn"/>

<div class="col-xs-11 main-container">
    <section class="temporary_wrap">
        <h1 class="title1">Statistikker</h1>
        <hr class="hr1">
    </section>
    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget equalheight" data-type="widget_type_small">
                <h2 class="title2 proxi_bold mb15 al_center">Invitationer sendt</h2>
                <div class="content fz60 ubuntu" data-color="red">
                    <c:out value="${invitation}"/>
                </div>
                <p>Antal kunder du har spurgt om bedømmelse... <a
                        href="https://better.dk/cp/company/${company.id}/reviews/invite" class='td_und'>Kom
                    igang her</a></p>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget equalheight" data-type="widget_type_small">
                <h2 class="title2 proxi_bold mb15 al_center">Overall Rating</h2>
                <div class="content">
                    <p class="ubuntu al_center fz50"><c:out value="${average}"/><span class="fz30">/5</span></p>

                    <div class="stars stars-example-css basic_stars">
                        <select class="example-css overall-rating-stars" name="rating" autocomplete="off"
                                data-current-rating="<c:out value="${average}"/>">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <p>Rating i alt på tværs af bedømmelsessider</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget equalheight" data-type="widget_type_small">
                <h2 class="title2 proxi_bold mb15 al_center">Bedømmelser overvåget</h2>
                <div class="content fz60 ubuntu">
                    <c:out value="${company.numberOfReviews}"/>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget equalheight" data-type="widget_type_middle">
                <h2 class="title2 proxi_bold mb10 al_center">Rating fordeling</h2>
                <div class="rating_1">
                    <c:forEach var="mapOfNumber" items="${mapOfNumber}">
                        <c:set var="number" value="${mapOfNumber.value}"/>
                        <fmt:parseNumber var="i" type="number" value="${mapOfNumber.value}"/>
                        <div class="box-body mb9">
                            <span class="rating_1_star_count">${number}</span>
                            <select class="example-1to10 rating-five" name="rating" autocomplete="off"
                                    data-current-rating="<c:out value="${ratingDistribution.containsKey(i) ? ((ratingDistribution[i] / ratingDistributionValues) *9)+1 : 0}" />">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <span data-color="grey_light">${ratingDistribution.containsKey(i) ? ratingDistribution[i] : 0}</span>
                        </div>
                    </c:forEach>

                </div>
            </div>
        </div>

    </div>
    <hr class="hr2">
    <div class="row">
        <div class="col-lg-7 col-md-12">
            <div class="widget" data-type="widget_type_large">
                <h2 class="title2 proxi_bold mb15 al_center">Seneste bedømmelser på tværs af bedømmelsessider</h2>
                <div class="comit_wrap">
                    <c:if test="${lastReviews != null}">
                        <c:forEach var="lastReviews" items="${lastReviews}">
                            <c:set var="site" value="${lastReviews.site}"/>
                            <c:set var="rating" value="${lastReviews.rating}"/>
                            <div class="w-comit_box ${site}">
                                <div class="commit_box_header clearfix">
                                    <div class="img_box img_box_${site}">
                                        <img src="../../../images/control-panel/statistic/${lastReviews.site}-logo.svg"
                                             alt="${lastReviews.site}">
                                    </div>

                                    <span class="commit_auth commit_auth_${site}">${lastReviews.author}</span>

                                    <div class="stars stars-example-css">
                                        <select class="example-css main-rating-${site}" name="rating" autocomplete="off"
                                                data-current-rating="${lastReviews.rating}">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>

                                        </select>

                                    </div>

                                    <span class="date">${lastReviews.date}</span>
                                </div>
                                <p>${lastReviews.review}</p>
                            </div>
                        </c:forEach>

                    </c:if>

                    <div class="al_center">
                        <a href="https://better.dk/cp/company/${company.id}/reviews/monitoring" class="more"
                           style="width: 150px">Se alle bedømmelser</a>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-lg-5 col-md-12">
            <div class="widget" data-type="widget_type_middle">
                <h2 class="title2 proxi_bold mb20 al_center">Fordeling af bedømmelser på sites</h2>
                <div class="rating_2">
                    <div class="w-stars stars1 stars-example-css google clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/google.SVG" alt="google">
                        </div>
                        <select class="example-css foldering-google" name="rating" autocomplete="off"
                                data-current-rating="<c:out value="${google}"/>">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count"><c:out value="${google}"/>/5</span>

                    </div>
                    <div class="w-stars stars1 stars-example-css yelp clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/yelp.SVG" alt="yelp">
                        </div>
                        <select class="example-css foldering-yelp" name="rating" autocomplete="off"
                                data-current-rating="<c:out value="${yelp}"/>">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count"><c:out value="${yelp}"/>/5</span>
                    </div>

                    <div class="w-stars stars1 stars-example-css tripadvisor clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/tripadvisor.SVG" alt="tripadvisor">
                        </div>
                        <select class="example-css foldering-tripadvisor" name="rating" autocomplete="off"
                                data-current-rating="<c:out value="${tripadvisor}"/>">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count"><c:out value="${tripadvisor}"/>/5</span>
                    </div>

                    <div class="w-stars stars1 stars-example-css facebook clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/facebook.SVG" alt="facebook">
                        </div>
                        <select class="example-css foldering-facebook" name="rating" autocomplete="off"
                                data-current-rating="<c:out value="${facebook}"/>">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count"><c:out value="${facebook}"/>/5</span>
                    </div>

                    <div class="w-stars stars1 stars-example-css better clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/better.jpg" alt="better">
                        </div>
                        <select class="example-css foldering-better" name="rating" autocomplete="off"
                                data-current-rating="<c:out value="${better}"/>">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count"><c:out value="${better}"/>/5</span>
                    </div>

                    <div class="w-stars stars1 stars-example-css pricerunner clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/pricerunner.SVG" alt="pricerunner"
                                 data-current-rating="0">
                        </div>
                        <select class="example-css foldering-pricerunner" name="rating" autocomplete="off">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count">0.0/5</span>
                    </div>

                    <div class="w-stars stars1 stars-example-css krak clearfix">
                        <div class="img_box">
                            <img src="../../../images/control-panel/statistic/krak.SVG" alt="krak"
                                 data-current-rating="0">
                        </div>
                        <select class="example-css foldering-krak" name="rating" autocomplete="off">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>

                        <span class="count">0.0/5</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


