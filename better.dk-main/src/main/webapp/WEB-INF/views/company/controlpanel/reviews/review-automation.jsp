<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<script type="text/javascript">
    $(function () {
        var TEXT_SWITCH_ON = 'til';
        var TEXT_SWITCH_OFF = 'fra';

        $('.integration').each(function () {
            var integrationSwitch = $(this).find('.switch');
            var textElement = integrationSwitch.find('.text');

            if (integrationSwitch.hasClass('on')) {
                textElement.html(TEXT_SWITCH_ON);
            } else {
                textElement.html(TEXT_SWITCH_OFF);
            }
        });

        // Activate integration
        $('.integration .controls').on('click', '.switch.off', function () {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                return false;
            }

            var integrationSwitch = $(this);
            var integrationId = integrationSwitch.parents('.integration').data('integrationId');

            if (integrationSwitch.data('cooldown') != null) {
                alert("Vent venligst et øjeblik med at aktivere integrationen");
                return;
            }

            window.setTimeout(function () {
                integrationSwitch.removeData('cooldown');
            }, 5000);

            $.ajax({
                type: 'POST',
                url: '/ajax/cp/company/' + BETTER.CP.COMPANY_ID + '/integrations/activate',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    integration: integrationId
                })
            }).done(function () {
                integrationSwitch.removeClass('off').addClass('on').data('cooldown', true);
                integrationSwitch.find('.text').html(TEXT_SWITCH_ON);
            }).fail(function () {
                alert("Ups! Integrationen kunne desværre ikke aktiveres. Prøv venligst igen.");
            });
        });

        // Deactivate integration
        $('.integration .controls').on('click', '.switch.on', function () {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                return false;
            }

            var integrationSwitch = $(this);
            var integrationId = integrationSwitch.parents('.integration').data('integrationId');

            if (integrationSwitch.data('cooldown') != null) {
                alert("Vent venligst et øjeblik med at deaktivere integrationen");
                return;
            }

            window.setTimeout(function () {
                integrationSwitch.removeData('cooldown');
            }, 5000);

            $.ajax({
                type: 'POST',
                url: '/ajax/cp/company/' + BETTER.CP.COMPANY_ID + '/integrations/deactivate',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    integration: integrationId
                })
            }).done(function () {
                integrationSwitch.removeClass('on').addClass('off').data('cooldown', true);
                integrationSwitch.find('.text').html(TEXT_SWITCH_OFF);
            }).fail(function () {
                alert("Ups! Integrationen kunne desværre ikke deaktiveres. Prøv venligst igen.");
            });
        });
    });
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <div class="clear spacer-30"></div>

    <h1>Automatiske bedømmelser</h1>
    <div class="description">
        Her kan du slå integrationer til og fra. Ved at aktivere en integration, bliver dine kunder automatisk bedt om
        at bedømme
        din virksomhed når de har handlet hos dig. Dermed får du flere bedømmelser og styrker din online position.
    </div>
    <hr/>

    <div class="spacer-10"></div>

    <div class="integrations">
        <div class="row">
            <div class="integration hairtools" data-integration-id="1001">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1001).intValue()) && integrationStatuses[(1001).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>

            <div class="integration admind" data-integration-id="1002">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1002).intValue()) && integrationStatuses[(1002).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>

            <div class="integration onlinebooq" data-integration-id="1003">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1003).intValue()) && integrationStatuses[(1003).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="integration e-conomic" data-integration-id="1004">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1004).intValue()) && integrationStatuses[(1004).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>

            <div class="integration klikbook" data-integration-id="1005">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1005).intValue()) && integrationStatuses[(1005).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>

            <div class="integration gecko" data-integration-id="1006">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1006).intValue()) && integrationStatuses[(1006).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="integration nem-tilmeld" data-integration-id="1007">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1007).intValue()) && integrationStatuses[(1007).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>

            <div class="integration supersaas" data-integration-id="1008">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1008).intValue()) && integrationStatuses[(1008).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>

            <div class="integration dinnerbooking" data-integration-id="1009">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1009).intValue()) && integrationStatuses[(1009).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="integration cbit" data-integration-id="1010">
                <div class="logo"></div>
                <div class="controls">
                    <div class="switch <c:out value="${((integrationStatuses.containsKey((1010).intValue()) && integrationStatuses[(1010).intValue()]) ? 'on' : 'off')}" />">
                        <div class="ball"></div>
                        <div class="text"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
