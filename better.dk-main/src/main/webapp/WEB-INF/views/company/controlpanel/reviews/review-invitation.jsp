<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(function () {
        function selectText(elementId) {
            var text = document.getElementById(elementId);

            if (document.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }

        $('.link-wrapper').on('click', function () {
            selectText(this.id);
        });


    });
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <%--<div class="clear spacer-30"></div>--%>

    <%--<h1>Sendte Invitationer</h1>--%>

    <%--<hr/>--%>

    <%--<div class="spacer-20"></div>--%>
    <br>
    <c:choose>
        <c:when test="${!empty customersInvite}">
            <table class="table table-striped table-hover" style="table-layout: fixed;
    width:100%">
                <thead>
                <tr style="font-weight: bold;">
                    <td>Navn</td>
                    <td>E-mail</td>
                    <td>Dato</td>

                </tr>
                </thead>

                <tbody>
                <c:forEach var="customerInvite" items="${customersInvite}">
                    <tr style="word-wrap:break-word;">
                        <td><c:out value="${customerInvite.customerName}"/></td>
                        <td><c:out value="${customerInvite.customerEmail}"/></td>
                        <td><fmt:formatDate value="${customerInvite.inviteDate}" type="both" dateStyle="full" timeStyle="medium"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <custom:pagination page="${page}" totalPages="${totalPages}" numberOfPageLinks="10"/>
        </c:when>

        <c:otherwise>
            <p>Du har ikke nogle kunder på din liste endnu.</p>
        </c:otherwise>
    </c:choose>

</div>
