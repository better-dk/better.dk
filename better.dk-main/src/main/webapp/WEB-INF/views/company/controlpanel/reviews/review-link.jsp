<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript">
    $(function () {
        function selectText(elementId) {
            var text = document.getElementById(elementId);

            if (document.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }

        $('.link-wrapper').on('click', function () {
            selectText(this.id);
        });


    });
</script>

<script>

    $(document).ready(function () {
        $(".site_status").click(function () {

            var type = $(this).attr("datatype");
            var company = $(this).attr("datatype");
            var name = $(this).attr("resource");

            var status;
            status = !!$(this).is(':checked');

            var formData = {type: type, name: name, status: status};

            $.ajax({
                type: 'POST',
                url: BETTER.ROOT_DOMAIN + '/cp/company/' + BETTER.CP.COMPANY_ID + '/reviews/sharing/update_status',
                data: formData
            });

        });
    });
</script>

<script>
    function updateLink(field) {
        var name = field.name;

        var link = field.value;
        var formData = {link: link, name: name};

        $.ajax({
            type: 'POST',
            url: BETTER.ROOT_DOMAIN + '/cp/company/' + BETTER.CP.COMPANY_ID + '/reviews/sharing/update_link',
            data: formData
        });
    }
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <div class="clear spacer-30"></div>

    <h1>Bedømmelses link</h1>
    <div class="description">
        Når du vil forespørge bedømmelser fra dine kunder, er det eneste du skal gøre at give dem det nedenstående
        link, hvad enten det er i e-mails, på Facebook eller noget helt tredje. Så sørger vi for, at dine bedømmelser
        automatisk bliver fordelt så de skaber mest muligt værdi for din virksomhed.
    </div>
    <hr/>

    <div class="spacer-50"></div>

    <div class="flex flex-column align-center box-wrapper">
        <div class="box-icon">
            <div class="icon-image"></div>
        </div>

        <div class="title">Her er dit bedømmelses link</div>

        <div class="spacer-10"></div>
        <div class="link-wrapper" id="review-link-wrapper">
            <c:out value="${company.profileUrl}"/>/bedoem
        </div>
    </div>

    <div>

        <br>
        <h3 style="text-align: center;">External review sites</h3>

        <table class="table">
            <thead>
            <tr>
                <th>Site</th>
                <th>Link</th>
                <th>Is shown?</th>
            </tr>
            </thead>

            <c:forEach var="site" items="${siteLinks}">
                <tr>
                    <td>${site.name}</td>
                    <td><input value="${site.link}" datatype="${site.type}" name="${site.name}"
                               class="form-control" onchange="updateLink(this)"/></td>
                    <c:if test="${site.status != false}">
                        <td>
                            <input type="checkbox" name="status" checked datatype="${site.type}"
                                   resource="${site.name}" class="site_status form-control">
                        </td>
                    </c:if>
                    <c:if test="${site.status == false}">
                        <td>
                            <input type="checkbox" name="status" datatype="${site.type}" resource="${site.name}"
                                   class="site_status form-control">
                        </td>
                    </c:if>
                </tr>
            </c:forEach>

            <form:form method="post" modelAttribute="siteLink"
                       action="/cp/company/${company.id}/reviews/sharing/add" id="addSite">
                <tr>
                    <td>
                        <input list="sites" name="siteName"
                               class="form-control"
                               placeholder="Start typing site name ..."
                               required="required" autocomplete="off"/>

                        <datalist id="sites">
                            <c:forEach var="site" items="${predefinedSites}">
                                <option>${site.key}</option>
                            </c:forEach>
                        </datalist>
                    </td>

                    <td>
                        <input name="link" required="required" id="link" placeholder="Link" autocomplete="off"
                               class="form-control"/>
                    </td>

                    <td>
                        <button type="submit" class="btn btn-success form-control glyphicon glyphicon-plus">Add</button>
                    </td>
                </tr>
            </form:form>
        </table>

    </div>

</div>
