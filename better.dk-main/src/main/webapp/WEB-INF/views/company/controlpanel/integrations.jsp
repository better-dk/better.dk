<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(function () {
        function selectText(elementId) {
            var text = document.getElementById(elementId);

            if (document.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }

        $('.link-wrapper').on('click', function () {
            selectText(this.id);
        });


    });
</script>

<div class="col-xs-11 main-container has-advisor">
    <tiles:insertAttribute name="page-menu"/>
    <div class="clear spacer-30"></div>

    <h1>The script is for review invite emails</h1>
    <div class="description">
        <br>
        <code>
            &lt;script src="https://better.dk/css/review-invite/triggered-order.js"&gt;&lt;/script&gt; <br>
            &lt;script&gt; <br>
            &nbsp;GettingData.init({ <br>
            &nbsp;&nbsp;&nbsp;companyId: <c:out value="${company.id}"/>, //don't change it<br>
            &nbsp;&nbsp;&nbsp;customerName: '#customerName', //css selector on customer name<br>
            &nbsp;&nbsp;&nbsp;customerEmail: '#customerEmail', //css selector on customer email<br>
            &nbsp;&nbsp;&nbsp;transactionId: '#transactionId' //css selector on transaction ID<br>
            &nbsp;});<br>
            &lt;/script&gt; <br>
        </code>

        <br>
        <b> companyId, customerName, transactionId, sendDelay - are required.</b>
    </div>
    <hr/>
    <b>Insert send delay in hours</b><br>
    <input id="input-send-delay" type="text" value="<c:out value="${senddeley.hours}"/>" size="10px"> <span
        id="result"></span>
    <script type="text/javascript">
        var input = document.getElementById('input-send-delay');

        input.oninput = function () {
            ch = input.value.replace(/[^\d,]/g, '');
            pos = ch.indexOf(',');
            max = 504;
            if (pos !== -1) {
                if ((ch.length - pos) > 2) {
                    ch = ch.slice(0, -1);
                }
            }
            input.value = ch;
            if (input.value > max) {
                input.style.borderColor = 'red';
                input.value = 0;
                document.getElementById('result').innerHTML = 'Maximum value is 504 hours';
            } else {
                input.style.borderColor = '';
                document.getElementById('result').innerHTML = '';
                let xhr = new XMLHttpRequest();
                let apiUrl;
//                todo: change apiUrl when put on production
                apiUrl = "https://better.dk/api/send-delay";
                xhr.open("POST", apiUrl, true);
                xhr.setRequestHeader("Content-type", "application/json");
                xhr.responseType = "arraybuffer";
                let array = {"companyId": <c:out value="${company.id}"/>, "hours": input.value};
                let data = JSON.stringify(array);
                xhr.send(data);
            }


        };
    </script>
    <div class="spacer-50"></div>

</div>
<%--<div class="col-xs-11 main-container">--%>
<%--<h1>Kommer snart!</h1>--%>
<%--<img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/integrations-blurred.png"--%>
<%--alt="Integrationer"/>--%>
<%--</div>--%>