<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>

<script type="text/javascript">
    var URL_SUBMIT_REVIEW = '/ajax/landing/company/<c:out value="${company.id}" />/reviews';
    var PHONE_NUMBER = '<c:out value="${phoneNumber}" />';

    var company = {
        id: <c:out value="${company.id}" />,
        name: "<c:out value="${company.name}" />",
        profile_url: "<c:out value="${company.profileUrl}" />",
        postal_code: <c:out value="${company.postalCode}" />,
        city: "<c:out value="${company.city}" />",

        <c:if test="${!empty company.bookingUrl}">
        affiliate: "hungry",
        </c:if>

        industries: [
            <c:forEach var="industry" items="${company.industries}" varStatus="loop">
            "<c:out value="${industry.name}" />"

            <c:if test="${!loop.last}">, </c:if>
            </c:forEach>
        ]
    };

    $(function () {
        analytics.ready(function () {
            if (typeof window.$zopim !== 'undefined') {
                window.$zopim.livechat.hideAll();
            }
        });

//        analytics.page({
//            company: company,
//            phoneNumber: PHONE_NUMBER
//        });
    });
</script>

<header>
    <h1><c:out value="${company.name}"/></h1>
</header>

<div id="create-review-wrapper">
    <div class="flex flex-column justify-center align-center" id="review-stars-wrapper">
        <span class="bold" id="choose-rating-title">Vælg antal stjerner</span>

        <div class="choose-stars">
            <div class="star outlined" data-value="1"></div>
            <div class="star outlined" data-value="2"></div>
            <div class="star outlined" data-value="3"></div>
            <div class="star outlined" data-value="4"></div>
            <div class="star outlined" data-value="5"></div>
            <div class="clear"></div>
            <span class="rating-description" style="display: block;"></span>
        </div>
    </div>

    <div class="form-group">
        <textarea class="form-control" id="review-text" rows="3"
                  placeholder="Beskriv baggrunden for din bedømmelse"></textarea>
    </div>

    <c:choose>
        <c:when test="${!isLoggedIn}">
            <form class="facebook-login" method="POST" action="/signin/facebook">
                <fieldset>
                    <legend>Troværdighed (valgfrit)</legend>

                    <p>Ved at logge ind, kan du verificere din bedømmelse, som dermed har større indflydelse.</p>

                    <button type="submit" class="btn fb-login flex flex-row justify-center align-center"
                            style="width: 48%; height: 40px;">
                        <span class="icon"></span><span
                            style="font-size: 13px; padding-top: 1px;">Log ind med Facebook</span>
                    </button>

                    <button class="btn" id="review-email-button">
                        E-mail
                    </button>

                    <div class="form-group" id="review-email-wrapper" style="display: none;">
                        <input type="email" name="email" id="review-email" class="form-control"
                               placeholder="Indtast din e-mail adresse"/>
                    </div>
                </fieldset>
            </form>
        </c:when>

        <c:otherwise>
            <security:authentication property="principal" var="account"/>

            <fieldset id="logged-in-wrapper">
                <legend>Troværdighed</legend>

                <c:choose>
                    <c:when test="${account.facebookProfileId != null}">
                        <p>Du er nu logget ind som:</p>

                        <div class="thumbnail">
                                <%-- improve: Find a way to use method in AccountService (without using it directly in the view) to avoid duplicating the below URL --%>
                            <img src="https://graph.facebook.com/<c:out value="${account.facebookProfileId}" />/picture?size=square"
                                 alt="Profil billede"/>
                        </div>

                        <div class="name">
                            <p><c:out value="${account.firstName} ${account.middleName} ${account.lastName}"/></p>
                        </div>
                    </c:when>

                    <c:otherwise>
                        <p>Du er nu logget ind som <strong><c:out
                                value="${account.firstName} ${account.middleName} ${account.lastName}"/></strong></p>
                    </c:otherwise>
                </c:choose>
            </fieldset>
        </c:otherwise>
    </c:choose>

    <button class="btn" id="submit-review">Send Bedømmelse</button>
</div>

<div id="review-success">
    <p><strong>Tak!</strong> Din bedømmelse er blevet indsendt.</p>
</div>