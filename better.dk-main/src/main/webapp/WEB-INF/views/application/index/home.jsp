<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:importAttribute name="scripts"/>
<tiles:importAttribute name="stylesheets"/>
<security:authorize access="isAuthenticated()" var="isLoggedIn"/>

<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/95611e3ed7.css">
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <c:forEach var="script" items="${scripts}">
        <c:choose>
            <c:when test="${fn:startsWith(script.value, 'http')}">
                <script src="<c:out value="${script}" />"></script>
            </c:when>

            <c:otherwise>
                <script src="<custom:domainNameUri /><c:out value="${script}" />"></script>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:forEach var="stylesheet" items="${stylesheets}">
        <c:choose>
            <c:when test="${fn:startsWith(stylesheet.value, 'http')}">
                <link rel="stylesheet" type="text/css" href="<c:out value="${stylesheet}" />"/>

            </c:when>

            <c:otherwise>
                <link rel="stylesheet" type="text/css" href="<custom:domainNameUri /><c:out value="${stylesheet}" />"/>

            </c:otherwise>
        </c:choose>
    </c:forEach>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
</head>

<body>
<script type="text/javascript" src="<custom:domainNameUri />/js/application/index/bundle.js"></script>
<div id="page">
    <section id="header">
        <div id="headerBgImgHolder">
            <img id="headerBgImg" src="../images/header-bg.jpg" alt/>
        </div>
        <header id="topBar">
            <div class="container">
                <div id="topPanel">
                    <div class="logoSection">
                        <img id="headerLogo" src="../images/svg/better-dk-logo.svg" alt />
                    </div>
                    <div class="navSection">
                        <c:if test="${not isLoggedIn}">
                            <a href="/login" class="btn btn-alt btn-md">Login</a>
                            <security:authentication property="principal" var="account"/>
                        </c:if>
                        <a href="https://business.better.dk" class="btn btn-md">For virksomheder</a></div>

                </div>
            </div>
        </header>
        <div id="pageTitleSection">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="pageTitleContent">
                            <div class="someReviews">
                                <div style="top: 50px;right: 100%;margin-right:-70px;" class="someReview">
                                    <div class="reviewLogo">
                                        <img src="../images/company-logos-ico/krak.SVG" alt />
                                    </div>
                                    <div class="reviewRating">
                                        <img src="../images/stars/krak-active.svg" alt />
                                    </div>
                                    <div class="reviewText">Fremragende mad! Man får virkelig meget lækker mad for
                                        pengene.
                                    </div>
                                </div>
                                <div style="top: 90px;left: 100%;margin-left: -150px;" class="someReview someReview-sm">
                                    <div class="reviewLogo">
                                        <img src="../images/company-logos-ico/facebook.SVG" alt />
                                    </div>
                                    <div class="reviewRating">
                                        <img src="../images/stars/facebook-active.svg" alt />
                                    </div>
                                    <div class="reviewText">Super god mad, fantastisk betjening. Man bliver altid mødt
                                        med et frisk smil og højt humør!
                                    </div>
                                </div>
                                <div style="top: 100%;left: 100%;margin-top: 60px;margin-left:65px;" class="someReview">
                                    <div class="reviewLogo">
                                        <img src="../images/company-logos-ico/tripadvisor.SVG" alt />
                                    </div>
                                    <div class="reviewRating">
                                         <img src="../images/stars/tripadvisor-active.svg" alt />
                                    </div>
                                    <div class="reviewText">Kvaliteten er super og autentisk. De er super søde og maden
                                        bliver lavet i fint tempo.
                                    </div>
                                </div>
                            </div>
                            <h1 id="pageTitle">
                                Alle bedømmelser samlet ét sted</h1>
                            <h2 id="pageSubtitle">Søg, blandt mere end 1.297.039 bedømmelser og ratings</h2>
                            <form id="searchForm" action="search" method="get">
                                <fieldset><label for="searchForm__name">Søg</label>
                                    <input id="searchForm__name" type="text" name="q1"
                                           placeholder="fx efter tømrer, tandlæger, advokat" size="45"/></fieldset>
                                <fieldset><label for="searchForm__address">Nær</label>
                                    <input id="searchForm__address" type="text" name="q2"
                                           placeholder="by, adresse, post nr." size="45"/></fieldset>
                                <fieldset class="submit">
                                <span id="searchSubmitWrapper">
                                    <input id="searchSubmit" type="submit" value=""/>
                                </span></fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="reviewsSlider">
        <div class="container-fluid">
            <div class="reviewsSliderHolder">
                <div class="reviewsSlider__state">
                    <div class="reviewsSlider__stateShapeHolder">
                        <div class="reviewsSlider__stateShapeWrapper">
                            <div class="reviewsSlider__stateShape">
                            </div>
                            <div class="reviewsSlider__stateVariants">
                                    <span class="reviewsSlider__stateVariant" data-variant="krak">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/krak.SVG" alt />
                                        <span class="circles running"></span>
                                    </span>
                                <span class="reviewsSlider__stateVariant" data-variant="trustpilot">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/trustpilot.SVG" alt />
                                        <span class="circles running"></span>
                                    </span>
                                <span class="reviewsSlider__stateVariant" data-variant="google">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/google.SVG" alt />
                                            <span class="circles running"></span>
                                    </span>
                                <span class="reviewsSlider__stateVariant" data-variant="tripadvisor">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/tripadvisor.SVG" alt />
                                        <span class="circles running"></span>
                                    </span>
                                <span class="reviewsSlider__stateVariant" data-variant="yelp">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/yelp.SVG" alt />
                                        <span class="circles running_reverse"></span>
                                    </span>
                                <span class="reviewsSlider__stateVariant" data-variant="facebook">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/facebook.SVG" alt />
                                        <span class="circles running_reverse"></span>
                                    </span>
                                <span class="reviewsSlider__stateVariant" data-variant="pricerunner">
                                        <img class="reviewsSlider__stateImg" src="../images/company-logos-ico/pricerunner.SVG" alt />
                                        <span class="circles running_reverse"></span>
                                    </span>
                            </div>
                            <div class="reviewsSlider__bridge">
                                <img class="reviewsSlider__bridgePic" src="../images/svg/better-logo.svg" alt />
                                <span class="circles running"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="reviewsSlider__reviewsHolder">
                    <div class="reviewsSlider__reviewsOffset">
                        <div class="reviews owl-carousel">
                            <c:if test="${externalReviews != null}">
                                <c:forEach var="externalReview" items="${externalReviews}">
                                    <div class="reviewCol">
                                        <div data-social-type=${externalReview.site} class="review">
                                            <div>
                                                <div class="reviewHeader">
                                                    <div class="sourceLogo">
                                                        <img src="../images/company-logos-ico/${externalReview.site}.SVG" alt />
                                                    </div>
                                                    <div class="reviewRating">
                                                        <span class="reviewRating__stars">
                                                            <img src="../images/stars/${externalReview.site}.svg" alt />
                                                            </span>
                                                        <span class="reviewRating__stars reviewRating__stars_active"
                                                              style="width:${(externalReview.rating*20)}%;">
                                                            <img src="../images/stars/${externalReview.site}-active.svg" alt />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="reviewContent">
                                                    <div class="reviewText">
                                                        <c:choose>
                                                            <c:when test="${externalReview.review != ''}">
                                                                <c:out value="${fn:substring(externalReview.review, 0, 200)}"/>
                                                                <a href="<c:out value="${externalReview.link}" />"
                                                                   target="_blank"
                                                                   style="color:#16509D">...</a>
                                                            </c:when>

                                                            <c:when test="${externalReview.review = ''}">
                                                                <p>User did not make a review text
                                                                    <a href="<c:out value="${externalReview.link}" />"
                                                                       target="_blank"
                                                                       style="color:#16509D">...</a>
                                                                </p>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <p>User did not make a review text
                                                                    <a href="<c:out value="${externalReview.link}" />"
                                                                       target="_blank"
                                                                       style="color:#16509D">...</a>
                                                                </p>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="reviewDetails">
                                                    <h3 class="reviewAuthor"><c:out
                                                            value="${externalReview.author}"/></h3>
                                                    <div class="reviewCompany">bedømte <a
                                                            href="<c:out value="${externalReview.company.profileUrl}" />"><c:out
                                                            value="${externalReview.company.name}"/></a></div>
                                                    <div class="reviewDate"><c:out
                                                            value="${externalReview.date}"/></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </c:forEach>
                            </c:if>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="pageContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="a-center">Mest søgte kategorier</h3><br/></div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <ul class="categories">
                        <li><img src="../images/category_icons/Layer 1.png" class="categoryIcon"/><a
                                href="/brancher/290/malere">Maler</a></li>
                        <li><img src="../images/category_icons/Layer 2.png" class="categoryIcon"/><a
                                href="/brancher/296/bilforhandlere">Bilforhandler</a></li>
                        <li><img src="../images/category_icons/Layer 3.png" class="categoryIcon"/><a
                                href="/brancher/294/murere">Murer</a></li>
                        <li><img src="../images/category_icons/Layer 4.png" class="categoryIcon"/><a
                                href="/brancher/288/t%C3%B8mrere-og-snedkere">Tømrer</a></li>
                        <li><img src="../images/category_icons/Layer 5.png" class="categoryIcon"/><a
                                href="/brancher/285/vvs-og-blikkenslagere">VVS</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <ul class="categories">
                        <li><img src="../images/category_icons/Layer 6.png" class="categoryIcon"/><a
                                href="brancher/284/elektrikere">Elektriker</a></li>
                        <li><img src="../images/category_icons/Layer 7.png" class="categoryIcon"/><a
                                href="/brancher/601/k%C3%B8reskole">Køreskole</a></li>
                        <li><img src="../images/category_icons/Layer 8.png" class="categoryIcon"/><a
                                href="/brancher/458/restauranter">Restauranter</a></li>
                        <li><img src="../images/category_icons/Layer 9.png" class="categoryIcon"/><a
                                href="/brancher/459/pizzeriaer-grillbarer-isbarer-mv">Pizzeriaer</a></li>
                        <li><img src="../images/category_icons/Layer 10.png" class="categoryIcon"/><a href="Advokater">Advokater</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <ul class="categories">
                        <li><img src="../images/category_icons/Layer 11.png" class="categoryIcon"/><a
                                href="/brancher/299/autov%C3%A6rksteder">Autoværksteder</a></li>
                        <li><img src="../images/category_icons/Layer 12.png" class="categoryIcon"/><a
                                href="/brancher/670/bedemand">Bedemand</a></li>
                        <li><img src="../images/category_icons/Layer 13.png" class="categoryIcon"/><a
                                href="/brancher/516/ejendomsm%C3%A6glere">Ejendomsmæglere</a></li>
                        <li><img src="../images/category_icons/Layer 14.png" class="categoryIcon"/><a
                                href="/brancher/430/flytteforretninger">Flyttefirma</a></li>
                        <li><img src="../images/category_icons/Layer 15.png" class="categoryIcon"/><a
                                href="/brancher/668/fris%C3%B8r">Frisører</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <ul class="categories">
                        <li><img src="../images/category_icons/Layer 16.png" class="categoryIcon"/><a
                                href="/brancher/574/anl%C3%A6gsgartner-og-haveservice">Gartnere</a></li>
                        <li><img src="../images/category_icons/Layer 17.png" class="categoryIcon"/><a
                                href="/brancher/609/fysioterapeuter-og-ergoterapeuter">Fysioterapeut</a></li>
                        <li><img src="../images/category_icons/Layer 18.png" class="categoryIcon"/><a
                                href="/brancher/570/reng%C3%B8ring">Rengøring</a></li>
                        <li><img src="../images/category_icons/Layer 19.png" class="categoryIcon"/><a
                                href="/brancher/607/tandl%C3%A6ger">Tandlæge</a></li>
                        <li><img src="../images/category_icons/Layer 20.png" class="categoryIcon"/><a
                                href="/brancher/571/vinduespolering">Vinduespolering</a></li>
                    </ul>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="a-center">Hvad er better?</h2>
                    <p class="a-center">Better giver forbrugerne ét samlet overblik over virksomhedernes online omdømme.
                        Vi indsamler kundebedømmelser på tværs af relevante sites, for at skabe det mest troværdige og
                        komplette billede af virksomhedernes omdømme.
                        Vi hjælper virksomheder med at skabe brugergenereret indhold og feedback i form af bedømmelser,
                        for bedre at forstå kunderne, øge synligheden i søgemaskinerne og påvirke købsbeslutninger i en
                        positiv retning.
                        Bedømmelser skaber troværdighed. Vi hjælper virksomheder til aktivt at bruge kundefeedback og
                        bedømmelser til at skabe værdi og forme deres online tilstedeværelse med fokus på at øge
                        troværdigheden omkring virksomhedens brand.</p>
                </div>
            </div>
            <br/>
            <br/></div>
    </div>
    <div id="pageFooter">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="widget">
                        <h4 class="widgetTitle"><span id="footerLogo"><img
                                src="../images/footer-logo.png"/><span>Better</span></span>
                        </h4>
                        <div class="widgetContent">
                            <p>Better giver forbrugerne ét samlet overblik<br/>
                                <org> over virksomhedernes online omdømme.<br/></org>
                                <nemt>Vi indsamler kundebedømmelser på<br/></nemt>
                                <nemt> tværs af relevante sites.</nemt>
                            </p>
                            <div class="social-links">
                                <a href="https://www.facebook.com/pg/BetterBusinessDK"
                                   class="social fa fa-facebook"></a>
                                <a href="https://twitter.com/BetterBizz" class="social fa fa-twitter"></a>
                                <a href="https://www.linkedin.com/company-beta/4838651/"
                                   class="social fa fa-linkedin"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="widget">
                        <h4 class="widgetTitle">Om Better</h4>
                        <div class="widgetContent">
                            <ul>
                                <li><a href="https://business.better.dk/index.php/om-better/ ">Hvad er Better</a></li>
                                <li><a href="https://business.better.dk/index.php/om-better/ ">Support</a></li>
                                <li><a href="https://business.better.dk/index.php/om-better/ ">Presse</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="widget">
                        <h4 class="widgetTitle">For Virksomheder</h4>
                        <div class="widgetContent">
                            <ul>
                                <li><a href="https://business.better.dk/">Hvad er Better Business?</a></li>
                                <li><a href="https://business.better.dk/index.php/tilmelding-better">Tilføj virksomhed</a></li>
                                <li><a href="https://business.better.dk/">Blog</a></li>
                                <li><a href="https://business.better.dk/index.php/bliv-partner/">Partnerskab</a></li>
                                <li><a href="https://business.better.dk/index.php/bliv-partner/">API</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-2">
                    <div class="widget">
                        <h4 class="widgetTitle">Vigtige links</h4>
                        <div class="widgetContent">
                            <ul>
                                <li><a href="/login">Login</a></li>
                                <li><a href="https://better.dk/account/create">Opret bruger</a></li>
                                <li><a href="https://business.better.dk/index.php/kontakt-os/">Kontakt os</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<script type="text/javascript" src="/js/bundle.js"></script>--%>
</body>
