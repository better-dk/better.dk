<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="spacer-30"></div>

<div class="container flex flex-row justify-center align-center" style="min-height: 300px; text-align: center;">
    <div class="col-xs-12">
        <p>Tryk venligst på knappen nedenfor for at læse betingelserne for brugen af Better.dk.</p>

        <a href="//www.iubenda.com/privacy-policy/7755784" class="iubenda-white iubenda-embed" title="Privacy Policy">Privacy
            Policy</a>
        <script type="text/javascript">(function (w, d) {
            var loader = function () {
                var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
                s.src = "//cdn.iubenda.com/iubenda.js";
                tag.parentNode.insertBefore(s, tag);
            };
            if (w.addEventListener) {
                w.addEventListener("load", loader, false);
            } else if (w.attachEvent) {
                w.attachEvent("onload", loader);
            } else {
                w.onload = loader;
            }
        })(window, document);</script>
    </div>
</div>

<div class="spacer-30"></div>