<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>

<div class="spacer-30"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 style="margin-bottom: 10px;"><c:out value="${industry.name}"/></h1>

            <ul style="-webkit-column-count: 4; -moz-column-count: 4; column-count: 4;">
                <c:forEach var="city" items="${cities}">
                    <c:set var="encodedCity"
                           value="${custom2:replaceAll(custom2:encodeURIComponent(city), \"\\\s+\", \"%20\")}"/>
                    <c:set var="encodedIndustryName"
                           value="${custom2:replaceAll(custom2:encodeURIComponent(industry.name), \"\\\s+\", \"%20\")}"/>

                    <li>
                        <a href="/s/<c:out value="${encodedIndustryName}" />/<c:out value="${encodedCity}" />"
                           title="<c:out value="${industry.name} i ${city}" />"> <%-- improve: translate --%>
                            <c:out value="${city}"/>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>

<custom:pagination page="${page}" totalPages="${totalPages}" numberOfPageLinks="10"/>