<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="spacer-30"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Adgang nægtet!</h1>

            <p>Du har ikke rettigheder til at tilgå denne side.</p>
        </div>
    </div>
</div>

<div class="spacer-30"></div>