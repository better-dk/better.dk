<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="spacer-30"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Ups, der skete en fejl!</h1>

            <p>Der skete en intern fejl på serveren. Vi er blevet underrettet om problemet og arbejder på at løse det.
                Prøv venligst igen senere.</p>
        </div>
    </div>
</div>

<div class="spacer-30"></div>