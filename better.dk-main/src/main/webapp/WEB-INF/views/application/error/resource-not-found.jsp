<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="spacer-30"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Siden kunne ikke findes!</h1>

            <p>Denne side kunne desværre ikke findes. Vi beklager.</p>
        </div>
    </div>
</div>

<div class="spacer-30"></div>