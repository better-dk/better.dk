<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<script type="text/javascript">
    URL_CREATE_NEW_ACCOUNT = "<custom:domainNameUri />/ajax/account";
</script>

<div class="container" style="margin-top: 30px;">
    <div class="row">
        <div id="create-user-wrapper" class="col-xs-12 widget">
            <h1 style="text-align: center; margin-bottom: 25px;">Opret bruger</h1>

            <form class="form-horizontal" id="create-user-form" action="" method="POST"
                  style="position: relative; left: 40px;">
                <div class="form-group">
                    <label for="create-user-name" class="col-xs-3 col-xs-offset-2 control-label">Navn</label>

                    <div class="col-xs-4" style="padding-right: 0;">
                        <input name="create-user-name" id="create-user-name" class="form-control" type="text"
                               placeholder="Navn"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="create-user-email" class="col-xs-3 col-xs-offset-2 control-label">E-mail adresse</label>

                    <div class="col-xs-4" style="padding-right: 0;">
                        <input name="create-user-email" id="create-user-email" class="form-control" type="email"
                               placeholder="E-mail" value="<c:out value="${email}" />"
                               <c:if test="${fn:length(email) > 0}">disabled="disabled" </c:if> />
                    </div>
                </div>

                <div class="form-group">
                    <label for="create-user-password" class="col-xs-3 col-xs-offset-2 control-label">Adgangskode</label>

                    <div class="col-xs-4" style="padding-right: 0;">
                        <input name="create-user-password" id="create-user-password" class="form-control"
                               type="password" placeholder="Adgangskode"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="create-user-repeat-password" class="col-xs-3 col-xs-offset-2 control-label">Bekræft
                        adgangskode</label>

                    <div class="col-xs-4" style="padding-right: 0;">
                        <input name="create-user-repeat-password" id="create-user-repeat-password" class="form-control"
                               type="password" placeholder="Gentag adgangskode"/>
                    </div>
                </div>

                <div class="col-xs-7 col-xs-offset-2" style="padding: 0;">
                    <input type="submit" class="btn btn-primary" id="create-user-submit" value="Opret Bruger"
                           style="width: 100%; height: 40px; margin: 5px 0 10px 0;"/>
                </div>
            </form>
        </div>
    </div>

    <div id="create-user-success-wrapper" class="col-xs-12 widget">
        <div class="col-xs-5">
            <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/create-user-welcome.png"
                 alt=""/>
        </div>

        <div class="col-xs-7">
            <p>Du er nu logged ind</p>
        </div>
    </div>
</div>