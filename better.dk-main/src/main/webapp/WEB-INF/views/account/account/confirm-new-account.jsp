<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container" style="margin-top: 30px;">
    <div class="row">
        <div class="col-xs-12 widget">
            <h1>Aktivering af bruger</h1>

            <c:choose>
                <c:when test="${error == false}">
                    <div class="alert alert-success" role="alert">
                        Din bruger er nu aktiveret, og du kan nu <a href="/login">logge ind</a>
                    </div>
                </c:when>

                <c:otherwise>
                    <div class="alert alert-warning" role="alert">
                        <strong>Ups!</strong> Din bruger kunne desværre ikke aktiveres fordi dette link er udløbet
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>