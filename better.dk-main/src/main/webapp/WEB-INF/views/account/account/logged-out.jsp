<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="spacer-30"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <p>Du er nu blevet logget ud. Vi glæder os til at se dig igen!</p>
        </div>
    </div>
</div>

<div class="spacer-30"></div>