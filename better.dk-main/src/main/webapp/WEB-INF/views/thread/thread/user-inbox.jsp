<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:importAttribute name="user-inbox-login-dialog"/>

<script type="text/javascript">
    $(function () {
        <c:if test="${!empty threads}">
        var objDiv = document.getElementById('conversation-view');
        objDiv.scrollTop = objDiv.scrollHeight;
        </c:if>
    });
</script>

<div class="spacer-15"></div>

<div class="container-fluid user-inbox" id="inbox-wrapper">
    <div class="container">
        <c:choose>
            <%-- At least one thread available --%>
            <c:when test="${!empty threads}">
                <div class="row">
                    <div class="col-xs-4" id="conversations-wrapper" data-page="1"
                         data-has-more-threads="<c:out value="${hasMoreThreads}" />" style="padding-left: 0;">
                        <div class="inner-wrap">
                            <div class="flex flex-row align-center" id="conversations-header">
                                Indbakke&nbsp;<span id="unread-messages-count" data-count=""></span>
                            </div>

                            <div id="conversations">
                                <c:forEach var="thread" items="${threads}">
                                    <c:choose>
                                        <%-- If the sender is the current user, then the receiver must be the other participant --%>
                                        <c:when test="${thread.sender.account != null && thread.sender.account.id == accessedAccount.id}">
                                            <c:set var="otherParticipant" value="${thread.receiver}"/>
                                        </c:when>

                                        <c:otherwise>
                                            <c:set var="otherParticipant" value="${thread.sender}"/>
                                        </c:otherwise>
                                    </c:choose>

                                    <div class="conversation flex flex-row align-center"
                                         data-thread-id="<c:out value="${thread.id}" />"
                                         data-subject="<c:out value="${custom:encodeURIComponent(thread.subject)}" />">
                                        <div class="image flex flex-column align-center">
                                            <c:choose>
                                                <c:when test="${otherParticipant.company.logoName != null}">
                                                    <img src="<custom:staticResourceLink relativePath="${otherParticipant.company.logoName}" />"
                                                         alt="Virksomhedens logo"/>
                                                </c:when>

                                                <c:otherwise>
                                                    <img src="<custom:staticResourceLink relativePath="companies/logos/default/default.png" />"
                                                         alt="Virksomhedens logo"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>

                                        <div class="info-wrapper">
                                            <div class="recipient-name bold ellipsis-overflow"
                                                 title="<c:out value="${otherParticipant.company.name}" />">
                                                    <%-- Currently, users can only communicate with companies --%>
                                                <c:out value="${otherParticipant.company.name}"/>
                                            </div>

                                            <div class="last-activity"><fmt:formatDate value="${thread.lastActivity}"
                                                                                       pattern="dd-MM-yy"/></div>
                                            <div class="clear"></div>

                                            <div class="subject ellipsis-overflow"><c:out
                                                    value="${thread.subject}"/></div>
                                            <div class="most-recent-message"><c:out
                                                    value="${thread.mostRecentMessageText}"/></div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>

                            <div class="conversation flex flex-row align-center" id="conversation-template"
                                 data-thread-id="" data-subject="" style="display: none;">
                                <div class="image flex flex-column align-center">
                                    <img src="" alt="Virksomhedens logo"/>
                                </div>

                                <div class="info-wrapper">
                                    <div class="recipient-name bold ellipsis-overflow"></div>
                                    <div class="last-activity"></div>
                                    <div class="clear"></div>

                                    <div class="subject ellipsis-overflow"></div>
                                    <div class="most-recent-message"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-8" id="conversation-view" style="padding-right: 0;">
                        <div class="inner-wrap">
                            <div class="bold" id="active-thread-subject"></div>
                            <div id="conversation-view-messages"></div>

                            <div id="conversation-new-message">
                                <div style="position: relative;">
                                    <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/ajax-loader-snake.gif"
                                         alt="Sending..." id="send-message-spinner"/>
                                    <textarea name="new-message-text" id="new-message-text"
                                              placeholder="Indtast din besked..." cols="3"></textarea>
                                </div>

                                <div class="flex flex-row justify-center align-center" id="new-message-options">
                                    <label for="checkbox-send-message-return">Send besked ved tryk på Enter</label>
                                    <input type="checkbox" id="checkbox-send-message-return"/>
                                    <button class="btn btn-primary" id="send-new-message">Send Besked</button>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="message" id="message-template" style="display: none;">
                                <div class="author">
                                    <img src="" alt="Profil billede"/>
                                    <div class="clear"></div>
                                    <div class="name bold ellipsis-overflow"></div>
                                </div>

                                <div class="message-text"></div>
                                <div class="clear"></div>
                                <div class="message-date"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>

            <%-- No threads available --%>
            <c:otherwise>
                <div class="spacer-20"></div>

                <div class="row">
                    <div class="col-xs-12">
                        <p>Du har endnu ingen beskeder.</p>
                    </div>
                </div>

                <div class="spacer-20"></div>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="spacer-15"></div>

<tiles:insertAttribute name="user-inbox-login-dialog"/>