<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Title</title>
</head>
<body>

<div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-small-dark" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-small-dark" class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-small-light" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-small-light" class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-middle-dark" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-middle-dark" class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-middle-light" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-middle-light" class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-social-dark" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-social-dark" class="better-widget"></div>
    <%--<img src="<custom2:domainNameUri />/css/widget/images/screenshots/social.png" alt="Profil billede"/>--%>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-small-slider" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-small-slider" class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-large-slider" class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-large-slider" class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-large-center-slider"
            class="better-widget"&gt;&lt;/div&gt;
        </code>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-large-center-slider"
         class="better-widget"></div>
    <br>
    <div style="max-width: 550px; margin: auto">
        <code>
            &lt;div data-id=<c:out value="${company.id}"/> id="better-small-right-button"
            class="better-widget"&gt;&lt;/div&gt;
        </code>
        <br>
        <p>Right side pane</p>
    </div>
    <div data-id=
         <c:out value="${company.id}"/> id="better-small-right-button"
         class="better-widget">
    </div>
</div>
</body>
<footer>
    <script src="https://better.dk/css/widget/js/widget.js"></script>
</footer>
</html>
