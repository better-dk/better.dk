<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>
<fmt:setLocale value="da_DK" scope="session"/> <%-- improve: Make dynamic and perhaps configure this in AppConfig? --%>

<tiles:importAttribute name="scripts"/>
<tiles:importAttribute name="stylesheets"/>
<tiles:importAttribute name="trackPageView"/>
<tiles:importAttribute name="principal"/>

<%-- URLs to resources are not relative to always hit the "root" domain, even when on a subdomain (company profile) --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <link rel="shortcut icon" href="<custom:domainNameUri />/favicon.ico"/>

    <title><c:out value="${title}"/></title>

    <%-- Add scripts that should be available within views (or below) --%>
    <script type="text/javascript">
        var ERROR_NOT_AUTHENTICATED = "<spring:message code="error.not.authenticated" />";
        var URL_ROOT_DOMAIN = "<custom:domainNameUri />";
        var IS_LOGGED_IN = ${isLoggedIn};
        var PUSHER_KEY = "<c:out value="${pusherKey}" />";

        <c:if test="${isLoggedIn}">
        var OWNED_COMPANIES = [
            <security:authentication property="principal.accountCompanies" var="accountCompanies" />

            <c:forEach var="ac" items="${accountCompanies}" varStatus="loop">
            {
                id: <c:out value="${ac.company.id}" />
            }

            <c:if test="${!loop.last}">, </c:if>
            </c:forEach>
        ];
        </c:if>
    </script>

    <tiles:insertAttribute name="principal"/>

    <script src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/js/jquery.min.js"></script>
    <script src="<custom:domainNameUri />/js/pusher.min.js"></script>
    <script src="<custom:domainNameUri />/js/better.js"></script>
    <script src="<custom:domainNameUri />/js/notifications.js"></script>
    <script src="<custom:domainNameUri />/js/messaging.js"></script>
    <script src="<custom:domainNameUri />/js/main.js"></script>

    <script type="text/javascript">
        // Analytics
        !function () {
            var analytics = window.analytics = window.analytics || [];
            if (!analytics.initialize)if (analytics.invoked) window.console && console.error && console.error("Segment snippet included twice."); else {
                analytics.invoked = !0;
                analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "group", "track", "ready", "alias", "page", "once", "off", "on"];
                analytics.factory = function (t) {
                    return function () {
                        var e = Array.prototype.slice.call(arguments);
                        e.unshift(t);
                        analytics.push(e);
                        return analytics
                    }
                };
                for (var t = 0; t < analytics.methods.length; t++) {
                    var e = analytics.methods[t];
                    analytics[e] = analytics.factory(e)
                }
                analytics.load = function (t) {
                    var e = document.createElement("script");
                    e.type = "text/javascript";
                    e.async = !0;
                    e.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                    var n = document.getElementsByTagName("script")[0];
                    n.parentNode.insertBefore(e, n)
                };
                analytics.SNIPPET_VERSION = "3.0.1";
                analytics.load("<c:out value="${segmentWriteKey}" />");

                <c:if test="${trackPageView}">
//                analytics.page();
                </c:if>
            }
        }();

        <c:if test="${isLoggedIn}">
        if (document.cookie.indexOf('analytics-identified') == -1) {
            <security:authentication property="principal.uuid" var="uuid" />
            analytics.identify('<c:out value="${uuid}" />');

            BETTER.setCookie('analytics-identified', '1', null, BETTER.getRootDomain());
        }
        </c:if>

        // AdButler
        if (!window.AdButler) {
            (function () {
                var s = document.createElement("script");
                s.async = true;
                s.type = "text/javascript";
                s.src = 'https://servedbyadbutler.com/app.js';
                var n = document.getElementsByTagName("script")[0];
                n.parentNode.insertBefore(s, n);
            }());
        }
        var AdButler = AdButler || {};
        AdButler.ads = AdButler.ads || [];
    </script>

    <c:if test="${!empty metas}">
        <c:forEach var="entry" items="${metas}">
            <meta name="${entry.key}" content="<c:out value="${entry.value}" />"/>
        </c:forEach>
    </c:if>

    <c:if test="${!empty metaProperties}">
        <c:forEach var="entry" items="${metaProperties}">
            <meta property="${entry.key}" content="<c:out value="${entry.value}" />"/>
        </c:forEach>
    </c:if>

    <c:forEach var="stylesheet" items="${stylesheets}">
        <c:choose>
            <c:when test="${fn:startsWith(stylesheet.value, 'http')}">
                <link rel="stylesheet" type="text/css" href="<c:out value="${stylesheet}" />"/>
            </c:when>

            <c:otherwise>
                <link rel="stylesheet" type="text/css" href="<custom:domainNameUri /><c:out value="${stylesheet}" />"/>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</head>

<body>
<tiles:insertAttribute name="topbar"/>
<tiles:insertAttribute name="content"/>
<tiles:insertAttribute name="footer"/>

<c:forEach var="script" items="${scripts}">
    <c:choose>
        <c:when test="${fn:startsWith(script.value, 'http')}">
            <script src="<c:out value="${script}" />"></script>
        </c:when>

        <c:otherwise>
            <script src="<custom:domainNameUri /><c:out value="${script}" />"></script>
        </c:otherwise>
    </c:choose>
</c:forEach>

<tiles:insertAttribute name="login-dialog"/>
</body>
</html>