<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="js"/>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>
<fmt:setLocale value="da_DK" scope="session"/> <%-- improve: Make dynamic and perhaps configure this in AppConfig? --%>

<tiles:importAttribute name="scripts"/>
<tiles:importAttribute name="stylesheets"/>
<tiles:importAttribute name="trackPageView"/>

<script type="text/javascript">
    var URL_REDIRECT_REVIEW = '<c:out value="${redirectUrl}" />';
    var IS_INTERNAL_REDIRECT = <c:out value="${isInternalRedirect}" />;
    var REDIRECT_EVENT_NAME = '<c:out value="${eventName}" />';
</script>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


<link rel="shortcut icon" href="<custom:domainNameUri />/favicon.ico"/>
<title><c:out value="${title}"/></title>


<%-- Add scripts that should be available within views (or below) --%>
<script type="text/javascript">
    var ERROR_NOT_AUTHENTICATED = "<spring:message code="error.not.authenticated" />";
    var URL_ROOT_DOMAIN = "<custom:domainNameUri />";
    var IS_LOGGED_IN = ${isLoggedIn};
    var PUSHER_KEY = "<c:out value="${pusherKey}" />";

    <c:if test="${isLoggedIn}">
    var OWNED_COMPANIES = [
        <security:authentication property="principal.accountCompanies" var="accountCompanies" />

        <c:forEach var="ac" items="${accountCompanies}" varStatus="loop">
        {
            id: <c:out value="${ac.company.id}" />
        }

        <c:if test="${!loop.last}">, </c:if>
        </c:forEach>
    ];
    </c:if>
</script>


<script type="text/javascript">
    // Analytics
    !function () {
        var analytics = window.analytics = window.analytics || [];
        if (!analytics.initialize)if (analytics.invoked) window.console && console.error && console.error("Segment snippet included twice."); else {
            analytics.invoked = !0;
            analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "group", "track", "ready", "alias", "page", "once", "off", "on"];
            analytics.factory = function (t) {
                return function () {
                    var e = Array.prototype.slice.call(arguments);
                    e.unshift(t);
                    analytics.push(e);
                    return analytics
                }
            };
            for (var t = 0; t < analytics.methods.length; t++) {
                var e = analytics.methods[t];
                analytics[e] = analytics.factory(e)
            }
            analytics.load = function (t) {
                var e = document.createElement("script");
                e.type = "text/javascript";
                e.async = !0;
                e.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                var n = document.getElementsByTagName("script")[0];
                n.parentNode.insertBefore(e, n)
            };
            analytics.SNIPPET_VERSION = "3.0.1";
            analytics.load("<c:out value="${segmentWriteKey}" />");

            <c:if test="${trackPageView}">
//            analytics.page();
            </c:if>
        }
    }();

    <c:if test="${isLoggedIn}">
    if (document.cookie.indexOf('analytics-identified') == -1) {
        <security:authentication property="principal.uuid" var="uuid" />
        analytics.identify('<c:out value="${uuid}" />');

        BETTER.setCookie('analytics-identified', '1', null, BETTER.getRootDomain());
    }
    </c:if>

    // AdButler
    if (!window.AdButler) {
        (function () {
            var s = document.createElement("script");
            s.async = true;
            s.type = "text/javascript";
            s.src = 'https://servedbyadbutler.com/app.js';
            var n = document.getElementsByTagName("script")[0];
            n.parentNode.insertBefore(s, n);
        }());
    }
    var AdButler = AdButler || {};
    AdButler.ads = AdButler.ads || [];
</script>


<c:forEach var="stylesheet" items="${stylesheets}">
    <c:choose>
        <c:when test="${fn:startsWith(stylesheet.value, 'http')}">
            <link rel="stylesheet" type="text/css" href="<c:out value="${stylesheet}" />"/>
        </c:when>

        <c:otherwise>
            <link rel="stylesheet" type="text/css" href="<custom:domainNameUri /><c:out value="${stylesheet}" />"/>
        </c:otherwise>
    </c:choose>
</c:forEach>


<c:forEach var="script" items="${scripts}">
    <c:choose>
        <c:when test="${fn:startsWith(script.value, 'http')}">
            <script src="<c:out value="${script}" />"></script>
        </c:when>

        <c:otherwise>
            <script src="<custom:domainNameUri /><c:out value="${script}" />"></script>
        </c:otherwise>
    </c:choose>
</c:forEach>


<body id="home">


<!-- header area -->
<header class="wrapper clearfix">

    <div id="banner">
        <div id="logo"><a href="<custom:domainNameUri />">
            <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/logo.png" alt="Logo"/>
        </a>
        </div>
    </div>

</header><!-- end header -->

<section id="hero" class="clearfix">
    <div class="wrapper">
        <div class="row" style="height: 100px">
            <br>
            <div class="grid_12">
                <h2 class="bold" style="text-align: center">Hvor stor er sandssynligheden for, at du vil anbefale
                    <c:out value="${company.name}"/> til
                    en ven eller kollega?</h2>
            </div>
        </div><!-- end row -->
    </div><!-- end wrapper -->
</section><!-- end hero area -->


<!-- Modal -->
<div class="modal fade" id="reviewSiteModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vi håber du vil bruge 2 minutter på at give din ærlige feedback.
                    Din mening er vigtig for os. Du kan give din feedback på et af de nedenstående websites.</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <input id="siteLinks" value="${siteLinks}" hidden="hidden">
                    <c:forEach var="site" items="${siteLinks}">

                        <c:if test="${site.status}">
                            <div class="col-md-4 col-xs-4 col-ld-4 col-ld-4 sitereview ${site.css}">
                                <a href="${site.link}" class="thumbnail logo">
                                    <c:if test="${site.css == 'undefined'}">
                                        <h4>${site.name}</h4>
                                        <br>
                                        <img style="height:12px">
                                    </c:if>
                                    <c:if test="${site.css != 'undefined'}">
                                        <img style="height:50px">
                                    </c:if>
                                </a>
                            </div>
                        </c:if>
                    </c:forEach>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- main content area -->
<div id="main" class="wrapper">


    <!-- content area -->


    <!-- columns demo, delete it!-->
    <section id="columnsdemo" style="margin-bottom:60px; width:100%" class="clearfix">

    </section>
    <!-- end columns demo -->

</div><!-- #end div #main .wrapper -->


<!-- footer area -->
<footer>
    <div id="colophon" class="wrapper clearfix">
        <h2>Better</h2>
        <p style="max-width: 75%;">
            Vi gør det nemt at finde, kontakte og bedømme alle lokale virksomheder i Danmark. Vi vækster og skaber bedre
            lokale virksomheder i samarbejde med forbrugere og virksomhederne selv.
        </p>


    </div>

    <!--You can NOT remove this attribution statement from any page, unless you get the permission from prowebdesign.ro-->
    <div id="attribution" class="wrapper clearfix" style="color:#666; font-size:11px;">Site built with <a
            href="http://www.prowebdesign.ro/simple-responsive-template/" target="_blank"
            title="Simple Responsive Template is a free software by www.prowebdesign.ro" style="color:#777;">Simple
        Responsive Template</a></div><!--end attribution-->

</footer><!-- #end footer area -->

</body>