<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="js"/>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>
<fmt:setLocale value="da_DK" scope="session"/> <%-- improve: Make dynamic and perhaps configure this in AppConfig? --%>

<tiles:importAttribute name="scripts"/>
<tiles:importAttribute name="trackPageView"/>

<script type="text/javascript">
    var URL_REDIRECT_REVIEW = '<c:out value="${redirectUrl}" />';
    var IS_INTERNAL_REDIRECT = <c:out value="${isInternalRedirect}" />;
    var REDIRECT_EVENT_NAME = '<c:out value="${eventName}" />';
</script>

<title><c:out value="${title}"/></title>


<%-- Add scripts that should be available within views (or below) --%>
<script type="text/javascript">
    var ERROR_NOT_AUTHENTICATED = "<spring:message code="error.not.authenticated" />";
    var URL_ROOT_DOMAIN = "<custom:domainNameUri />";
    var IS_LOGGED_IN = ${isLoggedIn};
    var PUSHER_KEY = "<c:out value="${pusherKey}" />";

    <c:if test="${isLoggedIn}">
    var OWNED_COMPANIES = [
        <security:authentication property="principal.accountCompanies" var="accountCompanies" />

        <c:forEach var="ac" items="${accountCompanies}" varStatus="loop">
        {
            id: <c:out value="${ac.company.id}" />
        }

        <c:if test="${!loop.last}">, </c:if>
        </c:forEach>
    ];
    </c:if>
</script>


<script type="text/javascript">
    // Analytics
    !function () {
        var analytics = window.analytics = window.analytics || [];
        if (!analytics.initialize)if (analytics.invoked) window.console && console.error && console.error("Segment snippet included twice."); else {
            analytics.invoked = !0;
            analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "group", "track", "ready", "alias", "page", "once", "off", "on"];
            analytics.factory = function (t) {
                return function () {
                    var e = Array.prototype.slice.call(arguments);
                    e.unshift(t);
                    analytics.push(e);
                    return analytics
                }
            };
            for (var t = 0; t < analytics.methods.length; t++) {
                var e = analytics.methods[t];
                analytics[e] = analytics.factory(e)
            }
            analytics.load = function (t) {
                var e = document.createElement("script");
                e.type = "text/javascript";
                e.async = !0;
                e.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                var n = document.getElementsByTagName("script")[0];
                n.parentNode.insertBefore(e, n)
            };
            analytics.SNIPPET_VERSION = "3.0.1";
            analytics.load("<c:out value="${segmentWriteKey}" />");

            <c:if test="${trackPageView}">
//            analytics.page();
            </c:if>
        }
    }();

    <c:if test="${isLoggedIn}">
    if (document.cookie.indexOf('analytics-identified') == -1) {
        <security:authentication property="principal.uuid" var="uuid" />
        analytics.identify('<c:out value="${uuid}" />');

        BETTER.setCookie('analytics-identified', '1', null, BETTER.getRootDomain());
    }
    </c:if>

    // AdButler
    if (!window.AdButler) {
        (function () {
            var s = document.createElement("script");
            s.async = true;
            s.type = "text/javascript";
            s.src = 'https://servedbyadbutler.com/app.js';
            var n = document.getElementsByTagName("script")[0];
            n.parentNode.insertBefore(s, n);
        }());
    }
    var AdButler = AdButler || {};
    AdButler.ads = AdButler.ads || [];
</script>


<c:forEach var="script" items="${scripts}">
    <c:choose>
        <c:when test="${fn:startsWith(script.value, 'http')}">
            <script src="<c:out value="${script}" />"></script>
        </c:when>

        <c:otherwise>
            <script src="<custom:domainNameUri /><c:out value="${script}" />"></script>
        </c:otherwise>
    </c:choose>
</c:forEach>


<head>

    <meta charset="utf-8">

    <title>Better</title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Template Basic Images Start -->

    <!-- Load CSS, CSS Localstorage & WebFonts Main Function -->
    <script>!function (e) {
        "use strict";
        function t(e, t, n) {
            e.addEventListener ? e.addEventListener(t, n, !1) : e.attachEvent && e.attachEvent("on" + t, n)
        };
        function n(t, n) {
            return e.localStorage && localStorage[t + "_content"] && localStorage[t + "_file"] === n
        };
        function a(t, a) {
            if (e.localStorage && e.XMLHttpRequest) n(t, a) ? o(localStorage[t + "_content"]) : l(t, a); else {
                var s = r.createElement("link");
                s.href = a, s.id = t, s.rel = "stylesheet", s.type = "text/css", r.getElementsByTagName("head")[0].appendChild(s), r.cookie = t
            }
        }

        function l(e, t) {
            var n = new XMLHttpRequest;
            n.open("GET", t, !0), n.onreadystatechange = function () {
                4 === n.readyState && 200 === n.status && (o(n.responseText), localStorage[e + "_content"] = n.responseText, localStorage[e + "_file"] = t)
            }, n.send()
        }

        function o(e) {
            var t = r.createElement("style");
            t.setAttribute("type", "text/css"), r.getElementsByTagName("head")[0].appendChild(t), t.styleSheet ? t.styleSheet.cssText = e : t.innerHTML = e
        }

        var r = e.document;
        e.loadCSS = function (e, t, n) {
            var a, l = r.createElement("link");
            if (t) a = t; else {
                var o;
                o = r.querySelectorAll ? r.querySelectorAll("style,link[rel=stylesheet],script") : (r.body || r.getElementsByTagName("head")[0]).childNodes, a = o[o.length - 1]
            }
            var s = r.styleSheets;
            l.rel = "stylesheet", l.href = e, l.media = "only x", a.parentNode.insertBefore(l, t ? a : a.nextSibling);
            var c = function (e) {
                for (var t = l.href, n = s.length; n--;)if (s[n].href === t)return e();
                setTimeout(function () {
                    c(e)
                })
            };
            return l.onloadcssdefined = c, c(function () {
                l.media = n || "all"
            }), l
        }, e.loadLocalStorageCSS = function (l, o) {
            n(l, o) || r.cookie.indexOf(l) > -1 ? a(l, o) : t(e, "load", function () {
                    a(l, o)
                })
        }
    }(this);</script>

    <!-- Load Custom CSS Start -->
    <script>loadCSS("<custom:domainNameUri />/css/reviewprocess/header.min.css?ver=1.0.0", false, "all");</script>
    <script>loadCSS("<custom:domainNameUri />/css/reviewprocess/css/main.min.css?ver=1.0.0", false, "all");</script>
    <script>loadCSS("<custom:domainNameUri />/css/reviewprocess/css/load.css?ver=1.0.0", false, "all");</script>
    <script>loadCSS("<custom:domainNameUri />/css/reviewprocess/css/fonts.min.css?ver=1.0.0", false, "all");</script>
    <!-- Load Custom CSS End -->

    <!-- Load Custom CSS Compiled without JS Start -->
    <noscript>
        <link rel="stylesheet" href="<custom:domainNameUri />/css/reviewprocess/css/fonts.min.css">
        <link rel="stylesheet" href="<custom:domainNameUri />/css/reviewprocess/css/main.min.css">
    </noscript>
    <!-- Load Custom CSS Compiled without JS End -->

    <!-- Custom Browsers Color Start -->
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <!-- Custom Browsers Color End -->

</head>
<body id="home">

<div class="container">
    <div class="like_top_text">
        Hjælp os og hjælp andre. Du er inviteret til at bedømme:
    </div>
    <div class="like_main_logo">
        <c:if test="${company.logoName != null}">
            <img src="<custom:staticResourceLink relativePath="${company.logoName}"/>"/>
        </c:if>
    </div>
    <h1 class="like_title">
        <c:out value="${company.name}"/>
    </h1>
    <div class="like_subtitle">
        Tag venligst et øjeblik til at bedømme din oplevelse med os. <br>
        Din tilbagemelding hjælper ikke bare os, den hjælper også andre potentielle kunder.
    </div>
    <div class="like_face_container">
        <div class="row">
            <div class="like_face_item md_open f_two" data-id="modal_window">
                <div class="like_face_img item1"></div>
                <div class="like_face_text">
                    God
                </div>
            </div>
            <div class="like_face_item f_one fix md_open" data-id="modal_form_cont">
                <div class="like_face_img item2"></div>
                <div class="like_face_text">
                    Ikke dårlig
                </div>
            </div>
            <div class="like_face_item f_three md_open" data-id="modal_form_cont">
                <div class="like_face_img item3"></div>
                <div class="like_face_text">
                    Dårlig
                </div>
            </div>
        </div>
        <div class="row">
            <a href="https://better.dk/" target="_blank" class="mini_logo">
                <img src="<custom:domainNameUri />/css/reviewprocess/img/mini_logo.png" alt="">
            </a>
        </div>
    </div>
</div>
<div class="overlay f1"></div>
<div class="modal_window md-cont">
    <div class="close_modal"></div>
    <div class="modal_text">
        Tag venligst et øjeblik til at dele din oplevelse <br>
        med os på en af bedømmelses-siderne.
    </div>
    <c:forEach var="site" items="${siteLinks}">
        <c:if test="${site.status}">
            <a href="${site.link}" target="_blank" class="modal_social">
                <img style="max-width: 190px; max-height: 50px;"
                     src="<custom:domainNameUri />/css/reviewprocess/images/review_site_logo/${site.css}.png" alt="">
            </a>
        </c:if>
    </c:forEach>
</div>
<div class="modal_form_cont b2 md-cont">
    <div class="overlay f2"></div>
    <div class="modal_form">
        <div class="close_modal"></div>
        <div class="modal_text">
            Vi efterstræber 100% kundetilfredshed. Hvis det mislykkedes, <br>
            så fortæl os venligst hvordan vi kan gøre det godt igen.
        </div>
        <form method="POST" id="message-form" class="form-inline">
            <!-- END Hidden Required Fields -->
            <div class="bron_form_cont">
				<span class="field">
					<c:choose>
                        <c:when test="${isLoggedIn}">
                            <custom:principalFullName var="fullName"/>
                            <input type="text" class="form-control" id="send-email-name_first" placeholder="Dit navn"
                                   value="<c:out value="${fullName}" />" disabled/>
                        </c:when>

                        <c:otherwise>
                            <input type="text" class="form-control" id="send-email-name_first" placeholder="Dit navn"/>
                        </c:otherwise>
                    </c:choose>
				</span>
            </div>
            <div class="bron_form_cont">
				<span class="field">
					 <input type="text" class="form-control" id="send-email-subject_first" placeholder="Emne"/>
				</span>
            </div>
            <div class="bron_form_cont">
				<span class="field">
					<c:choose>
                        <c:when test="${isLoggedIn}">
                            <security:authentication property="principal.email" var="email"/>

                            <c:choose>
                                <c:when test="${!empty email}">
                                    <input type="email" class="form-control" id="send-email-email_first"
                                           placeholder="E-mail" value="<c:out value="${email}" />" disabled/>
                                </c:when>

                                <c:otherwise>
                                    <input type="email" class="form-control" id="send-email-email_first"
                                           placeholder="E-mail"/>
                                </c:otherwise>
                            </c:choose>

                        </c:when>

                        <c:otherwise>
                            <input type="email" class="form-control" id="send-email-email_first" placeholder="E-mail"/>
                        </c:otherwise>
                    </c:choose>
				</span>
            </div>
            <div class="modal_text">Besked</div>
            <div class="bron_form_cont">
				<span class="field">
					<textarea class="form-control" rows="6" id="send-email-text_first"
                              placeholder="Skriv din besked her"></textarea>
				</span>
            </div>
            <button type="button" id="submit_first" class="btn bron_btn" name="send">Send besked</button>
        </form>
        <div class="text_subform">
            Hvis du ikke ønsker at behandle dine problemer her, og <br>
            foretrækker at oprette en bedømmelse, <a href="https://better.dk/" target="_blank">klik her.</a>
        </div>
    </div>
</div>
<div class="modal_form_cont b1 md-cont">
    <div class="overlay f3"></div>
    <div class="modal_form">
        <div class="close_modal"></div>
        <div class="modal_text">
            Vi efterstræber 100% kundetilfredshed. Hvis det mislykkedes, <br>
            så fortæl os venligst hvordan vi kan gøre det godt igen.
        </div>
        <form method="POST" class="form-inline">
            <!-- Hidden Required Fields -->
            <div class="bron_form_cont">
				<span class="field">
					<c:choose>
                        <c:when test="${isLoggedIn}">
                            <custom:principalFullName var="fullName"/>
                            <input type="text" class="form-control" id="send-email-name_second" placeholder="Dit navn"
                                   value="<c:out value="${fullName}" />" disabled/>
                        </c:when>

                        <c:otherwise>
                            <input type="text" class="form-control" id="send-email-name_second" placeholder="Dit navn"/>
                        </c:otherwise>
                    </c:choose>
				</span>
            </div>
            <div class="bron_form_cont">
				<span class="field">
					 <input type="text" class="form-control" id="send-email-subject_second" placeholder="Emne"/>
				</span>
            </div>
            <div class="bron_form_cont">
				<span class="field">
					<c:choose>
                        <c:when test="${isLoggedIn}">
                            <security:authentication property="principal.email" var="email"/>

                            <c:choose>
                                <c:when test="${!empty email}">
                                    <input type="email" class="form-control" id="send-email-email_second"
                                           placeholder="E-mail" value="<c:out value="${email}" />" disabled/>
                                </c:when>

                                <c:otherwise>
                                    <input type="email" class="form-control" id="send-email-email_second"
                                           placeholder="E-mail"/>
                                </c:otherwise>
                            </c:choose>

                        </c:when>

                        <c:otherwise>
                            <input type="email" class="form-control" id="send-email-email_second" placeholder="E-mail"/>
                        </c:otherwise>
                    </c:choose>
				</span>
            </div>
            <div class="modal_text">Besked</div>
            <div class="bron_form_cont">
				<span class="field">
					<textarea class="form-control" rows="6" id="send-email-text_second"
                              placeholder="Skriv din besked her"></textarea>
				</span>
            </div>
            <button type="button" id="submit_second" class="btn bron_btn" name="send">Send besked</button>
        </form>
        <div class="text_subform">
            Hvis du ikke ønsker at behandle dine problemer her, og <br>
            foretrækker at oprette en bedømmelse, <a href="https://better.dk/" target="_blank">klik her.</a>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="<custom:domainNameUri />/css/reviewprocess/js/libs.min.js"></script>
<script>
    var URL_SEND_MESSAGE = "<custom:domainNameUri />/ajax/thread/add";

    $('#submit_first').click(function () {

        var object = {
            subject: $('#send-email-subject_first').val().trim(),
            message: $('#send-email-text_first').val().trim(),
            companyId: COMPANY_ID,
            name: $('#send-email-name_first').val().trim(),
            email: $('#send-email-email_first').val().trim()
        };

        if (object.subject.length == 0) {
            alert("Indtast venligst et emne");
            return false;
        }

        if (object.message.length == 0) {
            alert("Indtast venligst en besked");
            return false;
        }

        if (object.name.length == 0) {
            alert("Indtast venligst dit navn");
            return false;
        }

        if (object.email.length == 0) {
            alert("Indtast venligst din e-mail adresse");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: URL_SEND_MESSAGE,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object),
            success: function () {
                window.location.href = "${company.profileUrl}";
            },
            error: function () {
                window.location.href = "${company.profileUrl}";
            }
        });
    });

    $('#submit_second').click(function () {
        var object = {
            subject: $('#send-email-subject_second').val().trim(),
            message: $('#send-email-text_second').val().trim(),
            companyId: COMPANY_ID,
            name: $('#send-email-name_second').val().trim(),
            email: $('#send-email-email_second').val().trim()
        };

        if (object.subject.length == 0) {
            alert("Indtast venligst et emne");
            return false;
        }

        if (object.message.length == 0) {
            alert("Indtast venligst en besked");
            return false;
        }

        if (object.name.length == 0) {
            alert("Indtast venligst dit navn");
            return false;
        }

        if (object.email.length == 0) {
            alert("Indtast venligst din e-mail adresse");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: URL_SEND_MESSAGE,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object),
            success: function () {
                window.location.href = "${company.profileUrl}";
            },
            error: function () {
                window.location.href = "${company.profileUrl}";
            }
        });
    });


    $('.f_two').click(function () {
        analytics.track('Bedoem - Go to reviews (God)', {
            company: company
        });
        if (0 === $(".modal_social").length) {
            window.location.href = "${company.profileUrl}/bedoemmelser#bedoem";
            console.log("s");
        } else {
            var id = $(this).attr("data-id");
            $('.f1').addClass('active');
            $('body').addClass('active');
            $('.' + id).addClass('active');
        }
    });
    $('.f_one').click(function () {
        analytics.track('Bedoem - Send E-mail Dialog (Ikke dårlig)', {
            company: company
        });
        var id = $(this).attr("data-id");
        $('.f2').addClass('active');
        $('body').addClass('active');
        $('.' + id + '.b2').addClass('active');
    });
    $('.f_three').click(function () {
        analytics.track('Bedoem - Send E-mail Dialog (Dårlig)', {
            company: company
        });
        var id = $(this).attr("data-id");
        $('.f3').addClass('active');
        $('body').addClass('active');
        $('.' + id + '.b1').addClass('active');
    });
    $('.overlay,.close_modal').click(function () {
        $('.overlay').removeClass('active');
        $('.md-cont').removeClass('active');
        $('body').removeClass('active');
    });
</script>
</body>