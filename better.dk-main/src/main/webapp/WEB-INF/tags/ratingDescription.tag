<%@ attribute name="rating" required="true" type="java.lang.Double" description="A given company rating" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:choose>
    <c:when test="${rating >= 9}"><spring:message code="reviews.rating.perfect"/></c:when>
    <c:when test="${rating >= 7 && rating < 9}"><spring:message code="reviews.rating.good"/></c:when>
    <c:when test="${rating >= 5 && rating < 7}"><spring:message code="reviews.rating.average"/></c:when>
    <c:when test="${rating >= 3 && rating < 5}"><spring:message code="reviews.rating.below-average"/></c:when>
    <c:when test="${rating >= 1 && rating < 3}"><spring:message code="reviews.rating.bad"/></c:when>
</c:choose>