<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="modal fade" id="hungry-deal-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close remove-login-hash remove-login-destination" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body" style="text-align: center; border-radius: 0 0 4px 4px;">
                <div style="margin-left: auto; margin-right: auto; width: 55%;">
                    <h3 class="light" style="float: left; margin-bottom: 0; font-size: 26px;">Få <span class="bold"
                                                                                                       style="font-size: inherit;">10% rabat</span>
                        på </h3><img style="float: left; position: relative; left: 10px; top: -5px;"
                                     src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/cta/hungry_logo.png"
                                     alt="Hungry.dk logo"/>
                </div>

                <div style="clear: both;"></div>
                <p class="light" style="position: relative; top: -7px;">Når du logger ind, får du straks vist en
                    rabatkode som giver dig 10% rabat</p>

                <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook" method="POST">
                    <input type="hidden" name="hash" value=""/>

                    <button type="submit" class="fb-login" style="margin: 0 0 15px 0; width: 387px;">
                        <i class="mdi mdi-facebook-box"></i>Log ind med Facebook & få 10% rabat
                    </button>
                </form>

                <p>eller</p>

                <p style="margin-bottom: 20px;"><a
                        href="<c:out value="${company.bookingUrl}?utm_source=${custom:replaceAll(company.profileUrl, 'http[s]?://', '')}&utm_medium=referral&utm_campaign=bestilknap" />"
                        id="hungry-continue-without-discount" rel="external nofollow" target="_blank"
                        style="color: #7aa246; text-decoration: underline;">Gå direkte til Hungry.dk - uden rabat</a>
                </p>
            </div>
        </div>
    </div>
</div>