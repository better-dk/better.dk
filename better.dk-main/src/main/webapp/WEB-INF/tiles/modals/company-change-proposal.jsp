<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="modal fade" id="propose-change-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close remove-login-hash remove-login-destination" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Foreslå ændringer til <c:out value="${company.name}"/></h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea class="form-control" rows="6" id="company-change-description"
                                      placeholder="Beskriv ændringerne her"></textarea>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default remove-login-hash remove-login-destination"
                        data-dismiss="modal">Luk
                </button>
                <button type="button" class="btn btn-primary" id="submit-company-changes-proposal">Foreslå ændringer
                </button>
            </div>
        </div>
    </div>
</div>