<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>

<c:if test="${isLoggedIn}">
    <security:authentication property="principal" var="account"/>
</c:if>

<div class="modal fade" id="claim-company-wizard" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading js-title-step"></h4>
            </div>

            <div class="flex flex-column justify-center align-center modal-body">
                <div class="inner-widget modal-inner-content centered" style="width: 760px">
                    <div class="modal-inner-wrap" id="claim-modal-inner-wrap">
                        <%-- DO NOTE REMOVE: This div is required for the wizard to work, and it is used for calculating percentages for the progress bar --%>
                        <div class="navbar" style="display: none;">
                            <div class="navbar-inner">
                                <div class="container">
                                    <ul>
                                        <c:if test="${!isLoggedIn}">
                                            <li><a href="#claim-company-tab2" data-toggle="tab"></a></li>
                                        </c:if>
                                        <li><a href="#claim-company-tab1" data-toggle="tab"></a></li>
                                        <li><a href="#claim-company-tab3" data-toggle="tab"></a></li>
                                        <li><a href="#claim-company-tab4" data-toggle="tab"></a></li>

                                        <%-- Only show this tab if the profile does not have a new subdomain (i.e. not its ID) --%>
                                        <c:if test="${fn:contains(company.profileUrl, company.id)}">
                                            <li><a href="#claim-company-tab5" data-toggle="tab"></a></li>
                                        </c:if>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane claim-company-tab-ownership" id="claim-company-tab1">
                                <p class="h1 modal-subtitle semi-bold">Få flere kunder - bekræft dit ejerskab af
                                    virksomheden</p>
                                <p class="h3 company-to-claim"><c:out value="${company.name}"/> (CVR: <c:out
                                        value="${company.ciNumber}"/>)</p>
                                <p>Jeg anerkender, at jeg har bemyndigelse til at låse op for denne konto på vegne af
                                    <br/>virksomheden, og at jeg er indforstået med Betters <a href="#">servicevilkår og
                                        fortrolighedspolitik.</a></p>

                                <div style="margin-top: 20px;">
                                    <div class="icon check" style="display: inline-block;"></div>
                                    <div style="display: inline-block; font-size: 16px;">Det er 100% gratis at have en
                                        profil på Better.dk
                                    </div>
                                </div>
                            </div>

                            <c:if test="${!isLoggedIn}">
                                <div class="tab-pane claim-company-tab-login" id="claim-company-tab2">
                                    <p class="h1 modal-subtitle">Login eller opret bruger</p>

                                    <br>

                                    <div class="login-form-wrapper">
                                        <div class="row">
                                    <div id="claim-login-with-email-form-wrapper"
                                         style="float: left;margin: 0;/*! padding-left: 15px; */" class="col-xs-6">
                                        <p class="h4">Allerede bruger?</p>
                                        <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook"
                                              method="POST">
                                            <input type="hidden" name="hash"
                                                   value="<spring:message code="hash.claim-company-logged-in" />"/>

                                            <button type="submit" class="fb-login justify-center"
                                                    style="padding: 7px 31px;font-size: 11px; margin-bottom: 15px">
                                                <span class="icon" style="float: left; "></span>Log ind med Facebook
                                            </button>
                                        </form>

                                        <div style="text-align: center; margin-bottom: 15px">
                                            <span>eller</span>
                                        </div>

                                        <form class="login form-horizontal"
                                              action="<custom:domainNameUri />/login/process" method="POST">
                                            <input type="hidden" name="destination" value=""/>



                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">

                                                <div align="center">
                                                    <input name="username" id="claim-login-dialog-login-email"
                                                           class="form-control" style="width: 200px; height: 30px;"
                                                           type="email" placeholder="E-mail"/>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">

                                                <div align="center">
                                                    <input name="password" id="claim-login-dialog-login-password"
                                                           class="form-control" style="width: 200px; height: 30px;"
                                                           type="password" placeholder="Adgangskode"/>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">
                                                <div class="btn" align="center">
                                                    <input type="submit" class="btn btn-primary"
                                                           style="width: 200px; height: 34px; float: left;"
                                                           id="claim-login-with-email-submit" value="Log ind"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="claim-create-new-user-form-wrapper"
                                         style="float: right;margin: 0;padding: 0;border-left: 1px #ccc solid;border-right: 0;border-top: 0;border-bottom: 0;" class="col-xs-6">
                                        <p class="h4">Opret bruger</p>
                                        <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook"
                                              method="POST">
                                            <input type="hidden" name="hash"
                                                   value="<spring:message code="hash.claim-company-logged-in" />"/>

                                            <button type="submit" class="fb-login justify-center"
                                                    style="padding: 7px 16px;font-size: 11px; margin-bottom: 15px">
                                                <span class="icon" style="float: left"></span>Opret bruger med Facebook
                                            </button>
                                        </form>

                                        <div style="text-align: center; margin-bottom: 15px">
                                            <span>eller</span>
                                        </div>
                                        <form class="login form-horizontal" action="" method="POST">
                                            <input type="hidden" name="hash"
                                                   value="<spring:message code="hash.claim-company-logged-in" />"/>
                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">

                                                <div align="center">
                                                    <input name="create-user-name" id="create-user-name" class="form-control" type="text"
                                                           style="width: 200px; height: 30px; "
                                                           placeholder="Navn"/>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">

                                                <div align="center">
                                                    <input name="username" id="claim-create-new-user-dialog-login-email"
                                                           class="form-control" style="width: 200px; height: 30px;"
                                                           type="email" placeholder="E-mail"/>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">

                                                <div align="center">
                                                    <input name="password" id="claim-create-new-user-dialog-login-password"
                                                           class="form-control" style="width: 200px; height: 30px;"
                                                           type="password" placeholder="Adgangskode"/>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">

                                                <div align="center">
                                                    <input name="create-user-repeat-password" id="create-user-repeat-password" class="form-control"
                                                           style="width: 200px; height: 30px;"
                                                           type="password" placeholder="Gentag adgangskode"/>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 0;margin-right: 0;">
                                                <div class="btn" align="center">
                                                    <input type="button" class="btn btn-primary"
                                                           style="width: 200px; height: 34px; float: left;"
                                                           id="claim-create-new-user-with-email-submit" value="Opret Bruger"/>

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                        </div>
                                    </div>

                                </div>
                            </c:if>

                            <div class="tab-pane claim-company-tab-email" id="claim-company-tab3">
                                <p class="h1 modal-subtitle">Indtast virksomhedens e-mail adresse</p>
                                <p class="modal-description">Bekræft nedenstående e-mail adresse eller indtast en
                                    ny.</p>

                                <form class="form-horizontal" id="claim-company-email">
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <c:choose>
                                                <c:when test="${!empty company.email}">
                                                    <c:set var="companyEmail" value="${company.email}"/>
                                                </c:when>

                                                <c:when test="${isLoggedIn && !empty account.email}">
                                                    <c:set var="companyEmail" value="${account.email}"/>
                                                </c:when>

                                                <c:otherwise>
                                                    <c:set var="companyEmail" value=""/>
                                                </c:otherwise>
                                            </c:choose>

                                            <input type="text" class="form-control" id="claim-input-company-email"
                                                   name="company_email" placeholder="F.eks. din@mail.dk"
                                                   value="${companyEmail}"/>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane claim-company-tab-website" id="claim-company-tab4">
                                <p class="h1 modal-subtitle">Har virksomheden en webadresse?</p>
                                <form class="form-horizontal" id="claim-company-website">
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <c:choose>
                                                <c:when test="${!empty company.website}">
                                                    <c:set var="companyWebsite" value="${company.website}"/>
                                                </c:when>

                                                <c:otherwise>
                                                    <c:set var="companyWebsite" value=""/>
                                                </c:otherwise>
                                            </c:choose>
                                            <input type="text" class="form-control website-group"
                                                   id="claim-input-company-website" name="company_website"
                                                   placeholder="F.eks. www.min-virksomhed.dk"
                                                   value="${companyWebsite}"/>
                                        </div>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input class="checkbox-input website-group"
                                                   id="claim-input-company-no-website" name="company_nowebsite"
                                                   type="checkbox"/>
                                            <span class="checkbox-label">Virksomheden har ingen webside</span>
                                        </label>
                                    </div>
                                </form>
                            </div>

                            <%-- Only show this tab if the profile does not have a new subdomain (i.e. not its ID) --%>
                            <c:if test="${fn:contains(company.profileUrl, company.id)}">
                                <div class="tab-pane claim-company-tab-subdomain" id="claim-company-tab5">
                                    <p class="h1 modal-subtitle">Vælg virksomhedsprofilens webadresse</p>
                                    <p class="modal-description">Din Better virksomhedsprofil ligger lige nu på <strong><c:out
                                            value="${company.profileUrl}"/></strong>. Vælg noget mere passende, der gør
                                        dine kunder i stand til at finde den.</p>

                                    <form class="form-horizontal" id="claim-company-subdomain">
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3 left">
                                                <input type="text" class="form-control"
                                                       id="claim-input-company-subdomain" name="company_subdomain"
                                                       placeholder="Profilnavn"/>
                                            </div>

                                            <div class="col-sm-5 right">
                                                <span class="suffix">.better.dk</span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </c:if>

                        </div>
                    </div>
                </div>
            </div>

            <div class="progress modal-progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                     style="width: 0;"></div>
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default button-cancel" data-orientation="cancel"
                       data-dismiss="modal" name="claim-company-cancel" value="Cancel"/>
                <input type="button" class="btn btn-primary button-next" name="claim-company-next" value="Bekræft"/>
                <input type="button" class="btn btn-primary button-finish" name="claim-company-finish"
                       id="claim-company-finish" value="Complete"/>
            </div>
        </div>
    </div>
</div>