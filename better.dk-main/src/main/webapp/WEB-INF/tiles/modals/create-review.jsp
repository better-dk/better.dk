<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>

<div class="modal fade" id="create-review-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Bedøm <c:out value="${company.name}"/></h4>
            </div>

            <div class="modal-body" style="padding: 0;">
                <div class="inner-widget modal-inner-content">
                    <div class="modal-inner-wrap">
                        <%-- DO NOTE REMOVE: This div is required for the wizard to work, and it is used for calculating percentages for the progress bar --%>
                        <div class="navbar" style="display: none;">
                            <div class="navbar-inner">
                                <div class="container">
                                    <ul>
                                        <li><a href="#create-review-tab1" data-toggle="tab" data-tab-index="0"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane" id="create-review-tab1">
                                <form class="form-horizontal" id="create-review-form">
                                    <input class="rating hidden" data-max="5" data-min="1" id="rate-company-stars"
                                           type="number" data-empty-value="0"/>
                                    <div class="col-md-5 rating-word"></div>

                                    <c:if test="${!isLoggedIn}">
                                        <div class="col-md-12 form-group">
                                            <label for="create-review-name" class="col-sm-3 control-label">Dit
                                                navn</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="create-review-name"
                                                       placeholder="Dit navn"/>
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label for="create-review-email"
                                                   class="col-sm-3 control-label">E-mail</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="create-review-email"
                                                       placeholder="Din e-mail adresse"/>
                                            </div>
                                        </div>
                                    </c:if>

                                    <div class="col-md-12 form-group">
                                        <label for="create-review-title"
                                               class="col-sm-3 control-label">Overskrift</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="create-review-title"
                                                   placeholder="Bedømmelsens overskrift"/>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <label for="create-review-text" class="col-sm-3 control-label">Din
                                            bedømmelse</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" rows="6" id="create-review-text"
                                                      placeholder="Skriv din bedømmelse her"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default button-cancel" data-orientation="cancel"
                       data-dismiss="modal" name="cancel" value="Annullér"/>
                <input type="button" class="btn btn-primary button-next" name="create-new-review-next-button"
                       id="submit-new-review" value="Indsend bedømmelse"/>
            </div>
        </div>
    </div>
</div>