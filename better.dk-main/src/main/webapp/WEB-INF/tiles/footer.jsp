<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>


<div id="pageFooter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="widget">
                    <h4 class="widgetTitle"><span id="footerLogo"><img
                            src="<custom:domainNameUri />/images/footer-logo.png"/><span>Better</span></span><%--/images/footer-logo.png--%>
                    </h4>
                    <div class="widgetContent">
                        <p>Better giver forbrugerne ét samlet overblik<br/>
                            <org> over virksomhedernes online omdømme.<br/></org>
                            <nemt>Vi indsamler kundebedømmelser på<br/></nemt>
                            <nemt> tværs af relevante sites.</nemt>
                        </p>
                        <div class="social-links">
                            <a href="https://www.facebook.com/pg/BetterBusinessDK"
                               class="social fa fa-facebook"></a>
                            <a href="https://twitter.com/BetterBizz" class="social fa fa-twitter"></a>
                            <a href="https://www.linkedin.com/company-beta/4838651/"
                               class="social fa fa-linkedin"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="widget">
                    <h4 class="widgetTitle">Om Better</h4>
                    <div class="widgetContent">
                        <ul>
                            <li><a href="https://business.better.dk/index.php/om-better/ ">Hvad er Better</a></li>
                            <li><a href="https://business.better.dk/index.php/om-better/ ">Support</a></li>
                            <li><a href="https://business.better.dk/index.php/om-better/ ">Presse</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="widget">
                    <h4 class="widgetTitle">For Virksomheder</h4>
                    <div class="widgetContent">
                        <ul>
                            <li><a href="https://business.better.dk/">Hvad er Better Business?</a></li>
                            <li><a href="https://business.better.dk/index.php/tilmelding-better">Tilføj virksomhed</a></li>
                            <li><a href="https://business.better.dk/">Blog</a></li>
                            <li><a href="https://business.better.dk/index.php/bliv-partner/">Partnerskab</a></li>
                            <li><a href="https://business.better.dk/index.php/bliv-partner/">API</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-2">
                <div class="widget">
                    <h4 class="widgetTitle">Vigtige links</h4>
                    <div class="widgetContent">
                        <ul>
                            <li><a href="/login">Login</a></li>
                            <li><a href="https://better.dk/account/create">Opret bruger</a></li>
                            <li><a href="https://business.better.dk/index.php/kontakt-os/">Kontakt os</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>