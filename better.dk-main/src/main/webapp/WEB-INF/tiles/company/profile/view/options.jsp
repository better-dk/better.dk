<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="right-box" id="profile-options">
    <c:if test="${empty company.accountCompanies}">
        <div class="claim-company flex flex-row align-center">
            <div class="icon settings"></div>
            <a href="#" title="Tag ejerskab af virksomheden">Er dette din virksomhed?</a>
        </div>

        <hr class="clear"/>
    </c:if>

    <div class="suggest-changes flex flex-row align-center">
        <div class="icon edit"></div>
        <a href="#" title="Foreslå ændringer til virksomheden">Foreslå ændringer</a>
    </div>

    <hr class="clear"/>

    <div class="add-to-favorites flex flex-row align-center">
        <div class="icon star"></div>
        <a href="#" title="Tilføj virksomheden til dine favoritter">Tilføj til favoritter</a>
    </div>
</div>