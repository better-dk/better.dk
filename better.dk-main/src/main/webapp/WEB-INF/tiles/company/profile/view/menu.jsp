<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<c:set var="isCompanyOwnerOrAdmin" value="${custom:isCompanyOwner(company.id) || custom:isAdmin()}"/>

<nav class="container-fluid" id="profile-navigation">
    <div class="container">
        <ul class="custom">
            <li class="menu-item"><a href="/">Oversigt</a></li>
            <li class="menu-item"><a href="/bedoemmelser">Bedømmelser</a></li>

            <c:if test="${!empty company.pages}">
                <c:forEach var="page" items="${company.pages}">
                    <c:choose>
                        <c:when test="${!empty page.children}">
                            <li class="menu-item dropdown">
                                <a href="/<c:out value="${page.slug}" />" class="dropdown-toggle"
                                   title="<c:out value="${page.name}" />" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="true">
                                    <c:out value="${page.name}"/> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <c:forEach var="child" items="${page.children}" varStatus="loop">
                                        <li>
                                        <a href="/<c:out value="${page.slug}" />/<c:out value="${child.slug}" />"
                                           title="<c:out value="${child.name}" />">
                                            <c:out value="${child.name}"/>
                                        </a>

                                        <c:if test="${!loop.last}">
                                            <li role="separator" class="divider"></li>
                                        </c:if>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                        </c:when>

                        <c:otherwise>
                            <li class="menu-item"><a href="/<c:out value="${page.slug}" />"
                                                     title="<c:out value="${page.name}" />"><c:out
                                    value="${page.name}"/></a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:if>

            <li class="menu-item"><a href="/kontakt">Kontakt info</a></li>

            <c:if test="${isCompanyOwnerOrAdmin}">
                <li class="menu-item add-page">
                    <a href="/page/add" title="Tilføj side" id="add-page-link"><span class="plus">+</span> Tilføj
                        side</a>
                </li>
            </c:if>
        </ul>
    </div>
</nav>