<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${!empty company.companyServices}">
    <h3>Ydelser</h3>

    <c:forEach var="companyService" items="${company.companyServices}">
        <a href="<c:url value="/ydelse/${companyService.service.slug}" />">
            <c:out value="${companyService.service.name}"/>
        </a>

        <br/>
    </c:forEach>
</c:if>