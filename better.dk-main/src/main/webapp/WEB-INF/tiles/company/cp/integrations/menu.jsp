<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ul class="page-menu flex flex-row align-center">

    <li data-parent="#cp-menu-integrations">
        <a href="/cp/company/<c:out value="${company.id}" />/integrations<c:out value="${urlPostfix}" />"><span
                class="container-link"></span>Integrationer</a>
    </li>
</ul>