<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>

<c:if test="${isLoggedIn}">
    <security:authentication property="principal" var="account"/>
</c:if>

<header class="container-fluid flex flex-column justify-center" id="topbar">
    <div class="container">
        <div class="row flex flex-row align-center">
            <div class="topbar-logo" align="left" style="width: 187px; margin-left: 15px">
                <a href="<custom:domainNameUri />">
                    <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/logo.png" alt="Logo"/>
                </a>
            </div>

            <div class="topbar-company-search-form" align="right" style="width: 508px; margin-left: 68px">
                <form method="GET" action="<custom:domainNameUri />/search"
                      class="form-inline company-search-form"
                      id="topbar-search-form">
                    <div class="form-group">
                        <input type="text" name="q1" id="topbar-search-terms" class="form-control what"
                               placeholder="Fx tømrer, pizza eller virk. info" value="<c:out value="${what}" />"/>
                        <input type="text" name="q2" id="topbar-search-location" class="form-control where"
                               placeholder="I nærheden af" value="<c:out value="${where}" />"/>

                        <button type="submit" class="btn btn-primary" id="topbar-search-button">
                            <span class="icon magnify" style="margin: 0;"></span>
                        </button>
                    </div>
                </form>
            </div>

            <div class="topbar-add-company-button" align="right" style="width: 120px; margin-left: 68px">
                <form  action="https://business.better.dk/index.php/tilmelding-better"
                      class="form-inline company-add-form"
                      id="topbar-add-company-form">
                    <%--todo: go to the upload form--%>

                    <button type="submit" class="btn btn-add-primary"
                            id="topbar-add-button"  >
                            Tilføj virksomhed
                    </button>


                </form>

            </div>


            <div class="topbar-user-menu-wrapper" id="topbar-menu-wrapper" style="width: 136px; margin-left: 68px">
                <ul class="flex flex-row align-center" id="topbar-controls">
                    <li class="icon envelope" id="topbar-messages">
                        <c:choose>
                            <c:when test="${isLoggedIn}">
                                <a href="<custom:domainNameUri />/messages">
                                    <span class="container-link"></span>
                                </a>
                            </c:when>

                            <c:otherwise>
                                <a href="#">
                                    <span class="container-link"></span>
                                </a>
                            </c:otherwise>
                        </c:choose>

                        <span class="notification semi-bold" style="display: none;"></span>
                    </li>

                    <li class="icon bell" id="topbar-notifications">
                        <a href="#">
                            <span class="container-link"></span>
                        </a>

                        <span class="notification semi-bold" style="display: none;"></span>
                    </li>

                    <li>
                        <a href="#" id="topbar-account-icon" data-trigger="focus">
                            <c:choose>
                                <c:when test="${account != null && account.facebookProfileId != null}">
                                    <img src="https://graph.facebook.com/<c:out value="${account.facebookProfileId}" />/picture?size=square"
                                         alt="Profil billede"/>
                                </c:when>

                                <c:otherwise>
                                    <img src="<custom:domainNameUri />/images/default-account-photo.png"
                                         alt="Profil billede"/>
                                </c:otherwise>
                            </c:choose>

                            <span class="notification semi-bold">1</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div id="account-popover-container" style="display: none;">
                <c:choose>
                    <c:when test="${account != null}">
                        <div class="flex flex-column align-center">
                            <div class="top" style="padding-top: 20px; margin-bottom: 15px; text-align: center;">
                                <span class="semi-bold" style="font-size: 16px; display: block;"><c:out
                                        value="${account.firstName} ${account.middleName} ${account.lastName}"/></span>

                                <c:if test="${account.email != null}">
                                    <span style="display: block;"><c:out value="${account.email}"/></span>
                                </c:if>
                            </div>

                            <c:if test="${!empty account.accountCompanies}">
                                <div class="owned-companies">
                                    <c:forEach var="ac" items="${account.accountCompanies}">
                                        <div class="company" data-company-id="<c:out value="${ac.company.id}" />">
                                            <div class="inner" style="height: 100%;">
                                                <a href="<custom:domainNameUri />/cp/company/<c:out value="${ac.company.id}" />"
                                                   class="left flex flex-row align-center"
                                                   style="display: block; width: 15%; height: 100%; position: relative; /*background-color: red;*/ float: left;">
                                                    <span class="notification"
                                                          style="display: none; position: absolute; right: 0; top: -9px; left: 40px; /* IE doesn't support initial value */"></span>

                                                    <c:choose>
                                                        <c:when test="${ac.company.logoName != null}">
                                                            <img src="<custom:staticResourceLink relativePath="${ac.company.logoName}" />"
                                                                 alt="Virksomhedens logo"/>
                                                        </c:when>

                                                        <c:otherwise>
                                                            <img src="<custom:staticResourceLink relativePath="companies/logos/default/default.png" />"
                                                                 alt="Virksomhedens logo"/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>

                                                <div class="middle flex flex-column justify-center"
                                                     style="width: 55%; height: 100%; float: left;">
                                                    <a href="<c:out value="${ac.company.profileUrl}" />"
                                                       class="company-name bold ellipsis-overflow"><c:out
                                                            value="${ac.company.name}"/></a>
                                                    <a href="<c:out value="${ac.company.profileUrl}" />"
                                                       class="company-profile-link ellipsis-overflow"><c:out
                                                            value="${ac.company.profileUrl}"/></a>
                                                </div>

                                                <div class="right flex flex-row align-center"
                                                     style="-ms-justify-content: flex-end; -moz-justify-content: flex-end; -webkit-justify-content: flex-end; justify-content: flex-end; width: 30%; height: 100%; /*background-color: blue;*/ float: left;">
                                                    <a href="<custom:domainNameUri />/cp/company/<c:out value="${ac.company.id}" />"
                                                       class="btn btn-primary">Kontrolpanel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>

                            <div class="bottom" style="padding-bottom: 20px;">
                                <a href="<custom:domainNameUri />/logout"
                                   class="btn btn-logout flex flex-row justify-center">
                                    <span class="icon logout"></span>Log ud
                                </a>

                                <security:authorize access="hasRole('ROLE_ADMIN')">
                                    <a href="<custom:domainNameUri />/admin" class="btn btn-primary">Admin</a>
                                </security:authorize>
                            </div>
                        </div>
                    </c:when>

                    <c:otherwise>
                        <div class="flex flex-column align-center">
                            Du er ikke logget ind!

                            <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook"
                                  method="POST">
                                <input type="hidden" name="hash" value=""/>

                                <button class="btn flex fb-login2 flex-row justify-center align-center"
                                        style="height: 46px; padding: 11px; margin: 15px auto 0 auto;">
                                    <span><span class="icon"></span>Log ind med Facebook</span>
                                </button>
                            </form>

                            <div style="text-align: center; width: 100%;">
                                <hr style="width: 30%; display: inline-block; vertical-align: middle;">
                                <span>eller</span>
                                <hr style="width: 30%; display: inline-block; vertical-align: middle;">
                            </div>

                            <a href="<custom:domainNameUri />/login" class="btn btn-primary"
                               style="height: 46px; padding-top: 13px; margin-bottom: 8px;"
                               title="Log ind med adgangskode">Log ind med adgangskode</a>
                            <a href="<custom:domainNameUri />/login" style="font-size: 12px;">Klik her, hvis ikke du har
                                en bruger</a>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</header>





