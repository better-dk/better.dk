$(function () {
    $('#show-company-delete-json').click(function () {
        var identifiers = $('span.search-index-identifier');
        var json = '[';

        $.each(identifiers, function (index, value) {
            json += '{"type":"delete","id":' + parseInt($(value).text()) + '},';
        });

        json = json.substr(0, (json.length - 1));
        json += ']';

        $('#delete-companies-json-textarea').text(json);
        $('#company-delete-json-dialog').modal('show');

        return false;
    });

    $('#delete-companies-json-textarea').focus(function () {
        $(this).select();
    });
});