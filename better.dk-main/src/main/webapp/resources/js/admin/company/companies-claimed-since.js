$(function () {
    function getCsvStringFromTable($table) {
        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // Actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row),
                        $cols = $row.find('td');

                    return $cols.map(function (j, col) {
                        var $col = $(col),
                            text = $.trim($col.text());

                        text = text.replace(/(\r\n|\n|\r)/gm, ''); // Replace line breaks
                        text = text.replace(/ +(?= )/g, ''); // Replace multiple spaces with a single space
                        return text.replace('"', '""'); // Escape double quotes

                    }).get().join(tmpColDelim);
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';

        return csv;
    }

    $('#download-as-csv').click(function () {
        $(this).attr({
            download: $(this).data('file-name'),
            href: 'data:application/csv;charset=utf-8,' + encodeURIComponent(getCsvStringFromTable($('#companies-claimed-since-table')))
        });
    });

    $('#copy-csv-textarea').focus(function () {
        $(this).select();
    });

    $('#copy-as-csv').click(function () {
        $('#copy-csv-textarea').text(getCsvStringFromTable($('#companies-claimed-since-table')));
        $('#copy-csv-dialog').modal('show');

        return false;
    });

    $('#update-companies-claimed-since').click(function () {
        var date = $('#companies-claimed-since-date-picker').datepicker().val();
        var redirectUrl = TARGET_BASE_URL + date;
        var selectedIndustries = $('#filter-industries option:selected');

        if (selectedIndustries.length > 0) {
            redirectUrl += "?industries=";

            var values = $.map(selectedIndustries, function (option) {
                return parseInt(option.value);
            });

            redirectUrl += values.join(',');
        }

        window.location = redirectUrl;
    });

    $('#companies-claimed-since-date-picker').datepicker({
        dateFormat: 'yy-mm-dd',
        defaultDate: COMPANIES_CLAIMED_SINCE
    });
});