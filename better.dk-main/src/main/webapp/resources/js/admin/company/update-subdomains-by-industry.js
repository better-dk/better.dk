$(function () {
    function getSubdomainSuggestion(companyName) {
        var suggestion = companyName.toLowerCase();
        suggestion = suggestion.replace(/\saps/, '');
        suggestion = suggestion.replace(/\sivs/, '');
        suggestion = suggestion.replace(/\sa\/s/, '');
        suggestion = suggestion.replace(/\si\/s/, '');
        suggestion = suggestion.replace(/\sp\/s/, '');
        suggestion = suggestion.replace(/\ssmba/, '');
        suggestion = suggestion.replace(/\ss\.m\.b\.a/, '');
        suggestion = suggestion.replace(/`/g, '');
        suggestion = suggestion.replace(/´/g, '');
        suggestion = suggestion.replace(/'/g, '');
        suggestion = suggestion.replace(/,/g, '');
        suggestion = suggestion.replace(/æ/g, 'ae');
        suggestion = suggestion.replace(/ø/g, 'oe');
        suggestion = suggestion.replace(/å/g, 'aa');
        suggestion = suggestion.replace(/v\/[a-zA-Z0-9\s]*/g, '');
        suggestion = suggestion.replace(/\//g, '');
        suggestion = suggestion.replace(/[\\.]/g, '');
        suggestion = suggestion.replace(/&/g, '-');
        suggestion = suggestion.replace(/ä/g, '');
        suggestion = suggestion.replace(/ö/g, '');
        suggestion = suggestion.replace(/\s/g, '-');
        suggestion = suggestion.replace(/-+/g, '-'); // Clean up consecutive hyphens
        suggestion = suggestion.replace(/-$/g, '');
        suggestion = suggestion.replace(/^-/g, '');

        return suggestion;
    }

    $('.update-subdomain').click(function () {
        var button = $(this);
        $(button).prop('disabled', true);
        var parentDiv = $(this).parents('.company');
        var subdomain = parentDiv.find('input').val().trim();
        var companyId = parseInt(parentDiv.data('companyId'));

        $.ajax({
            type: 'POST',
            url: URL_UPDATE_SUBDOMAIN_BASE + companyId + URL_UPDATE_SUBDOMAIN_POST,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                subdomain: subdomain
            })
        }).done(function () {
            var matchesElement = $('#total-company-matches');
            var currentCompanyCount = parseInt(matchesElement.text());
            matchesElement.text(--currentCompanyCount);

            parentDiv.fadeOut(500, function () {
                $(this).remove();
            });
        }).fail(function (xhr) {
            $(button).prop('disabled', null);
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case 'error.unique.constraint.violation':
                    alert("This subdomain is already taken");
                    break;
                default:
                    alert("The subdomain could not be updated. Maybe it is invalid?");
                    break;
            }
        });

        return false;
    });

    $('div.company').each(function () {
        var companyName = $(this).find('.company-name').text().trim();
        var suggestion = getSubdomainSuggestion(companyName);
        var pattern = new RegExp(/^[a-z0-9\-]+$/);

        if (pattern.test(suggestion)) {
            $(this).find('input').val(suggestion);
        }
    });
});