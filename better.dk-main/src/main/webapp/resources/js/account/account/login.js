$(function () {
    $('#login-with-email').click(function () {
        // This is all we have to do here, because the rest of the animation is handled in main.js
        $('#login-wrapper').animate({
            height: '+=90px'
        }, 500);

        return false;
    });
});