$(function () {
    $('#create-user-submit').click(function () {
        var email = $('#create-user-email').val().trim();

        $.ajax({
            type: 'POST',
            url: URL_CREATE_NEW_ACCOUNT,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                name: $('#create-user-name').val().trim(),
                email: email,
                password: $('#create-user-password').val().trim(),
                repeatPassword: $('#create-user-repeat-password').val().trim()
            })
        }).done(function (result, textStatus, xhr) {
            if (xhr.status == 201) { // Created
                window.location.replace("/login?success");
            }
        }).fail(function (xhr) {
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case "error.invalid.arguments":
                    var message = response.entries[0].message;

                    if (typeof response.entries[0].field !== 'undefined') {
                        message = response.entries[0].field + ": " + message;
                    }

                    alert(message);
                    break;

                default:
                    break;
            }
        });

        return false;
    });
});