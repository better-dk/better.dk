$(function () {
    $('.stars').stars();

    var refreshRelativeTimes = function () {
        $('.review .review-time .review-time-relative').each(function () {
            var timestamp = parseInt($(this).attr('data-timestamp'));
            $(this).html(moment(timestamp).fromNow());
        });
    };

    refreshRelativeTimes();
    window.setInterval(refreshRelativeTimes, 30000);

    window.setInterval(function () {
        // Move last review to first position
        var reviewWidth = $('#latest-reviews .review').outerWidth();
        var lastReview = $('#latest-reviews .review:last-child');
        lastReview.css('left', '-' + reviewWidth + 'px');
        lastReview.prependTo('#latest-reviews');
        $('#latest-reviews .review').css('left', '-' + reviewWidth + 'px');

        // Move reviews to the right
        $('#latest-reviews .review').animate({
            left: '+=' + reviewWidth + 'px'
        }, 1000);
    }, 5000);

    $('#about-user-cta').on('click', function () {
        var searchForm = $('#company-search-form');
        searchForm.css({
            position: 'absolute',
            left: -searchForm.outerWidth() + 'px',
            opacity: 0
        });

        window.setTimeout(function () {
            searchForm.animate({
                left: ($(window).width() / 2) - (searchForm.outerWidth() / 2) + 'px',
                opacity: 1
            }, 750);
        }, 500);

        $('html, body').animate({
            scrollTop: $('#topbar').offset().top
        }, 1000, function () {
            $('#search-terms').focus();
        });

        return false;
    });
});