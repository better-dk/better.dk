$.fn.stars = function () {
    return $(this).each(function () {
        var numberOfStars = $(this).data('stars');

        if (numberOfStars == null || typeof numberOfStars === 'undefined') {
            return;
        }

        numberOfStars = (Math.round(parseFloat(numberOfStars) * 2) / 2); // Round to nearest half
        var starWidth = 18;

        if ($(this).hasClass('outlined')) {
            $(this).width(5 * starWidth);
        }

        var size = (Math.max(0, (Math.min(5, numberOfStars))) * starWidth); // Make sure that the value is in 0 - 5 range; multiply to get width
        var span = $('<span />').width(size);
        $(this).html(span);
    });
};