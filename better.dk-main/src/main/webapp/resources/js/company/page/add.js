$(function () {
    function updateSlug(value) {
        $('#permanent-link .relative-url').html(value).show();
    };

    function getSlug() {
        var pageName = $('#new-page-name').val();
        var selectedParent = $('#new-page-parent option:selected');
        var parentSlug = selectedParent.length > 0 ? selectedParent.data('slug') : null;
        var slug = BETTER.getSlug(pageName);

        if (parentSlug != null) {
            slug = parentSlug + '/' + slug;
        }

        return slug;
    }

    window.onbeforeunload = function (e) {
        var message = 'Er du sikker på, at du vil forlade denne side? Eventuelt indtastet data vil gå tabt!',
            e = e || window.event;

        // For IE and Firefox
        if (e) {
            e.returnValue = message;
        }

        // For Safari
        return message;
    };

    $('#new-page-name').on('keyup', function () {
        updateSlug(getSlug());
    });

    $('#new-page-content').on('keyup', function () {
        var content = $(this).val().trim();
        var wordCount = 0;

        if (content != '') {
            var split = content.split(' ');
            wordCount = split.length;
        }

        $('#new-page-form .word-count').html(wordCount);
    });

    $('#new-page-parent').change(function () {
        updateSlug(getSlug());
    });

    $('#new-page-form').on('submit', function (event) {
        var button = $('#publish-new-page');
        $(button).prop('disabled', true);

        if (!confirm('Er du sikker på, at du vil udgive denne side og gøre den offentligt synlig?')) {
            $(button).prop('disabled', null);
            event.preventDefault();
            return;
        }

        var relativeUrl = $('#permanent-link .relative-url').html();
        var selectedParent = $('#new-page-parent option:selected');

        var object = {
            name: $('#new-page-name').val(),
            slug: relativeUrl.split('/').reverse()[0], // Get value after last slash
            title: $('#new-page-title').val(),
            content: $('#new-page-content').val(),
            parentId: ((selectedParent.length > 0 && selectedParent.val() > -1) ? selectedParent.data('id') : null)
        };

        $.ajax({
            type: 'POST',
            url: URL_PUBLISH_PAGE,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object)
        }).done(function () {
            window.onbeforeunload = null; // Remove warning when leaving page
            window.location = '/' + getSlug();
        }).fail(function (xhr) {
            var response = $.parseJSON(xhr.responseText);

            // todo: handle other scenarios
            switch (response.code) {
                case 'error.invalid.arguments':
                    var errors = [];

                    for (var i = 0; i < response.entries.length; i++) {
                        errors.push({
                            field: response.entries[i].field,
                            message: response.entries[i].message
                        });
                    }

                    alert("Ups! Nogle af felterne kunne ikke valideres. Tjek venligst det indtastede data og prøv igen.");
                    break;

                default:
                    break;
            }
        }).always(function () {
            $(button).prop('disabled', null);
        });

        return false;
    });
});