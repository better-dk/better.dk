$(function () {
    $('.import textarea').on('keyup', function () {
        var element = $(this);
        var context = element.parents('.import');
        var content = element.val();
        var button = context.find('.btn');
        var template = button.data('pattern');
        var customerCount = (content != '' ? content.match(/.+/gi).length : 0);

        context.find('.import-count').html(customerCount);

        if (customerCount > 0) {
            if (customerCount == 1) {
                template = template.replace('kunder', 'kunde');
            }

            button.html(template.replace('{count}', customerCount));
        } else {
            button.html(template.replace('{count}', ''));
        }
    });
});