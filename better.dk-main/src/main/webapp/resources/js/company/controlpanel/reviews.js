$(function () {
    $('.stars').stars();

    var refreshRelativeTimes = function () {
        $('.review .review-time-relative').each(function () {
            var timestamp = parseInt($(this).attr('data-timestamp'));
            var prefix = $(this).attr('data-prefix').trim();
            $(this).html(prefix + ' ' + moment(timestamp).fromNow());
        });
    };

    refreshRelativeTimes();
    window.setInterval(refreshRelativeTimes, 30000);

    $('.view-and-reply').on('click', function () {
        var review = $(this).parents('.review');
        var reviewId = review.data('reviewId');
        var viewAndReply = review.find('.review-info');
        var deletionRequest = review.find('.request-deletion-info');

        viewAndReply.slideToggle(500);

        if (deletionRequest.is(':visible')) {
            deletionRequest.slideToggle(500);
        }

        return false;
    });

    $('.request-deletion').on('click', function () {
        if (!BETTER.IS_LOGGED_IN) {
            $('#create-account-dialog').modal('show');
            return false;
        }

        var review = $(this).parents('.review');
        var viewAndReply = review.find('.review-info');
        var deletionRequest = review.find('.request-deletion-info');

        if (viewAndReply.is(':visible')) {
            viewAndReply.slideToggle(500);
        }

        deletionRequest.slideToggle(500);
        return false;
    });

    $('.review .comment-text').on('focus', function () {
        if (!BETTER.IS_LOGGED_IN) {
            $('#create-account-dialog').modal('show');
            return false;
        }
    });

    $('.write-comment form').on('submit', function () {
        if (!BETTER.IS_LOGGED_IN) {
            $('#create-account-dialog').modal('show');
            return false;
        }

        var message = 'Denne kommentar vil blive offentligt tilgængeligt! Er du sikker på, at du vil fortsætte?';

        if (!confirm(message)) {
            return false;
        }

        var review = $(this).parents('.review');
        var viewAndReply = $(this).parents('.review-info');
        var reviewId = review.data('reviewId');
        var button = review.find('.submit-comment');
        var comment = review.find('.comment-text');
        $(button).prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: BETTER.ROOT_DOMAIN + '/ajax/review/' + reviewId + '/comments/add',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                companyId: BETTER.CP.COMPANY_ID,
                text: comment.val().trim()
            })
        }).done(function () {
            alert("Din kommentar blev tilføjet!");
            viewAndReply.slideToggle(500);
            comment.val('');
        }).fail(function () {
            $(button).prop('disabled', null);
            alert("Ups, der skete en fejl! Tjek venligst, at det indtastede indhold er korrekt.");
        });

        return false;
    });

    $('.request-deletion-info form').on('submit', function () {
        var review = $(this).parents('.review');
        var reason = review.find('.deletion-request-reason');

        if (reason.val().length < 10) {
            alert("Teksten skal være mindst 10 tegn lang");
            return false;
        }

        var message = 'Er du sikker på, at du vil anmode om, at denne bedømmelse bliver slettet?';

        if (!confirm(message)) {
            return false;
        }

        var reviewUuid = review.data('reviewUuid');
        var deletionRequest = review.find('.request-deletion-info');
        var button = review.find('.submit-deletion-request');
        $(button).prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: BETTER.ROOT_DOMAIN + '/ajax/cp/company/' + BETTER.CP.COMPANY_ID + '/reviews/' + reviewUuid + '/request-deletion',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                reason: reason.val().trim()
            })
        }).done(function () {
            alert("Din anmodning blev sendt!");
            deletionRequest.slideToggle(500);
            reason.val('');
        }).fail(function () {
            $(button).prop('disabled', null);
            alert("Ups, der skete en fejl! Tjek venligst, at det indtastede indhold er korrekt.");
        });

        return false;
    });

    $('.review .cancel-comment').on('click', function () {
        var review = $(this).parents('.review');
        var viewAndReply = $(this).parents('.review-info');

        if (review.find('.comment-text').val() == '') {
            viewAndReply.slideToggle(500);
        } else {
            var message = 'Er du sikker på, at du vil annullere kommentaren?';

            if (confirm(message)) {
                viewAndReply.slideToggle(500);
                review.find('.comment-text').val('');
            }
        }

        return false;
    });
});