$(function () {
    analytics.trackLink($('#nps-completed .negative .review-company'), 'Clicked Review Redirect Link', function (element) {
        return {
            company: company,
            isInternalRedirect: IS_INTERNAL_REDIRECT,
            redirectUrl: URL_REDIRECT_REVIEW
        };
    });

    $(window).load(function () {
        var links = $("#siteLinks").val();
        if (links == '[]') {
            analytics.track('Review Redirect', {
                company: company,
                isInternalRedirect: IS_INTERNAL_REDIRECT,
                redirectUrl: URL_REDIRECT_REVIEW
            }, null, function () {
                window.location = URL_REDIRECT_REVIEW;
            });
        } else {
            $('#reviewSiteModal').modal('show');
        }
    });
});
