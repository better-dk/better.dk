$(function () {
    var hasClaimed = false;
    var isClaimWizardInitialized = false;

    $('.stars').stars();

    // Mark active menu entry
    var currentPage = window.location.href; // this.href returns an absolute URL, even if the href is relative
    currentPage = currentPage.split('#')[0]; // Remove hash
    currentPage = currentPage.replace(window.location.search, ''); // Remove query string

    $('#profile-navigation li a').each(function () {
        if (this.href == currentPage) {
            $(this).parent().addClass('active');
        }
    });

    $('#profile-navigation .dropdown:has(li.active)').addClass('active');

    // Show map
    if ($('#address-map').length > 0) {
        var options = {
            center: new google.maps.LatLng(56.26392, 9.501785), // Denmark
            disableDefaultUI: true,
            zoom: 7,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('address-map'), options);
        var geocoder = new google.maps.Geocoder();
        var address = $('#company-address').text();

        geocoder.geocode({address: address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                map.setZoom(12);

                new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                console.log("Couldn't fetch coordinates because: " + status);
            }
        });
    }


    $('#hungry-deal-dialog .facebook-login').submit(function () {
        BETTER.setLoginHash(HASH_DEAL_HUNGRY);
    });
    /***** CREATE COMPANY *****/
    $('#topbar-add-button').click(function () {
        // TODO: add action
        analytics.track('Profile - Add Company', {
            company: company
        })
    });
    /***** SEND MESSAGE *****/
    $('#send-message-button').click(function () {
        $('#send-message-dialog').modal('show');

        analytics.track('Profile - Send Message Dialog', {
            company: company
        });

        return false;
    });

    $('#send-email-button').click(function () {
        $('#send-email-dialog').modal('show');

        analytics.track('Profile - Send E-mail Dialog', {
            company: company
        });

        return false;
    });

    $('#give-me-quote-button').click(function () {
        var link = $(this).attr("quote-link");
        analytics.track('Profile - Give me a quote', {
            company: company
        });
        window.location.href = link;
    });

    $('#button-send-email').click(function () {
        var button = $(this);
        $(button).prop('disabled', true);

        var object = {
            subject: $('#send-email-subject').val().trim(),
            message: $('#send-email-text').val().trim(),
            companyId: COMPANY_ID,
            name: $('#send-email-name').val().trim(),
            email: $('#send-email-email').val().trim()
        };

        if (object.subject.length == 0) {
            alert("Indtast venligst et emne");
            return false;
        }

        if (object.message.length == 0) {
            alert("Indtast venligst en besked");
            return false;
        }

        if (object.name.length == 0) {
            alert("Indtast venligst dit navn");
            return false;
        }

        if (object.email.length == 0) {
            alert("Indtast venligst din e-mail adresse");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: URL_SEND_MESSAGE,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object)
        }).done(function () {
            $('#send-email-dialog').modal('hide');
            $('#send-message-success-cta-dialog').modal('show');
            $('#send-email-form')[0].reset();
            window.location.hash = '';
        }).fail(function (xhr) {
            $(button).prop('disabled', null);
            var response = $.parseJSON(xhr.responseText);

            // todo: handle other scenarios
            switch (response.code) {
                case "error.invalid.arguments":
                    var errors = [];

                    for (var i = 0; i < response.entries.length; i++) {
                        errors.push({
                            field: response.entries[i].field,
                            message: response.entries[i].message
                        });
                    }

                    analytics.track('Validation Error (Create New Thread)', {
                        message: object,
                        errors: errors
                    });

                    alert("Ups! Nogle af felterne kunne ikke valideres. Tjek venligst det indtastede data og prøv igen.");
                    break;

                default:
                    break;
            }
        });

        return false;
    });

    $('#button-send-message').click(function () {
        var button = $(this);
        $(button).prop('disabled', true);

        var object = {
            subject: $('#send-message-subject').val().trim(),
            message: $('#send-message-text').val().trim(),
            companyId: COMPANY_ID,
            name: $('#send-message-name').val().trim(),
            email: $('#send-message-email').val().trim()
        };

        if (object.subject.length == 0) {
            alert("Indtast venligst et emne");
            return false;
        }

        if (object.message.length == 0) {
            alert("Indtast venligst en besked");
            return false;
        }

        if (object.name.length == 0) {
            alert("Indtast venligst dit navn");
            return false;
        }

        if (object.email.length == 0) {
            alert("Indtast venligst din e-mail adresse");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: URL_SEND_MESSAGE,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object)
        }).done(function () {
            $('#send-message-dialog').modal('hide');
            $('#send-message-success-cta-dialog').modal('show');
            $('#send-message-form')[0].reset();
            window.location.hash = '';
        }).fail(function (xhr) {
            $(button).prop('disabled', null);
            var response = $.parseJSON(xhr.responseText);

            // todo: handle other scenarios
            switch (response.code) {
                case "error.invalid.arguments":
                    var errors = [];

                    for (var i = 0; i < response.entries.length; i++) {
                        errors.push({
                            field: response.entries[i].field,
                            message: response.entries[i].message
                        });
                    }

                    analytics.track('Validation Error (Create New Thread)', {
                        message: object,
                        errors: errors
                    });

                    alert("Ups! Nogle af felterne kunne ikke valideres. Tjek venligst det indtastede data og prøv igen.");
                    break;

                default:
                    break;
            }
        });

        return false;
    });

    /***** ADD TO FAVORITES *****/
    $('#profile-options .add-to-favorites a').on('click', function () {
        CompanyFavorite.favorite();
        return false;
    });

    /***** COMPANY EDITS *****/
    $('#profile-options .suggest-changes a').click(function () {
        $('#propose-change-dialog').modal('show');

        analytics.track('Profile - Propose Changes Dialog', {
            company: company
        });

        return false;
    });

    $('#submit-company-changes-proposal').click(function () {
        $.ajax({
            type: 'POST',
            url: URL_PROPOSE_CHANGES,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                changes: $('#company-change-description').val()
            })
        }).done(function (result, textStatus, xhr) {
            if (xhr.status == 200) { // Created
                alert("Dine forslag til ændringer blev gemt!"); // improve: handle differently
                $('#propose-change-dialog').modal('hide');
                $('#company-change-description').val('');
            }
        }).fail(function () {
            alert("Ups, ændringerne kunne ikke gemmes! Tjek venligst de indtastede data (minimum 10 tegn).");
        });

        return false;
    });

    $('#claim-create-new-user-with-email-submit').click(function () {

        var email = $('#claim-create-new-user-dialog-login-email').val().trim();

        $.ajax({
            type: 'POST',
            url: BETTER.ROOT_DOMAIN+'/ajax/account',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                name: $('#create-user-name').val().trim(),
                email: email,
                password: $('#claim-create-new-user-dialog-login-password').val().trim(),
                repeatPassword: $('#create-user-repeat-password').val().trim()
            })
        }).done(function (result, textStatus, xhr) {
            if (xhr.status === 201) { // Created
                window.location.reload();
            }
        }).fail(function (xhr) {
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case "error.invalid.arguments":
                    var message = response.entries[0].message;

                    if (typeof response.entries[0].field !== 'undefined') {
                        message = response.entries[0].field + ": " + message;
                    }

                    alert(message);
                    break;

                default:
                    break;
            }
        });

        return false;
    });
    /***** CLAIM COMPANY *****/
    CompanyClaimCompany = {
        start: function () {
            $('#profile-options .claim-company a').click(function () {
                CompanyClaimCompany.modal();
                return false;
            });
        },

        /**
         * Opens the claim modal and initializes the wizard (if it has not already been initialized)
         *
         * @param step The step to go to; used for skipping steps (Optional)
         */
        modal: function (step) {
            var wizard = $('#claim-company-wizard');
            wizard.modal({
                keyboard: false,
                backdrop: 'static'
            });

            if (!isClaimWizardInitialized) {
                CompanyClaimCompany.wizard(wizard);
                isClaimWizardInitialized = true;

                if (typeof step === 'undefined') {
                    // Only trigger page view if we are not skipping steps
                    analytics.page({
                        title: 'Claim - ownership',
                        url: window.location.href.split('#')[0].replace(/\/$/, '') + '#ownership', // Remove hash and trailing slash
                        path: '/#ownership',
                        referrer: window.location.href.split('#')[0]
                    });
                } else {
                    $('#claim-company-wizard').bootstrapWizard('show', step);
                }
            }
        },
        wizard: function (wizard) {
            var footer = wizard.find('.modal-footer');
            var nextButton = wizard.find('.button-next');
            var finishButton = wizard.find('.button-finish');

            wizard.bootstrapWizard({
                nextSelector: '.button-next',
                previousSelector: '.button-previous',
                finishSelector: '.button-finish',
                onTabClick: function () {
                    return false;
                },
                onTabShow: function (tab, navigation, index) {
                    var currentStep = (index + 1);
                    var currentTabPane = wizard.find('.tab-pane.active');
                    var numberOfSteps = navigation.find('li').length;

                    // Change text on next button for first step
                    if (currentTabPane.hasClass('claim-company-tab-ownership')) {
                        nextButton.val('Bekræft');
                    } else {
                        nextButton.val('Næste');
                    }

                    // Disable next button upon Facebook login
                    if (currentTabPane.hasClass('claim-company-tab-login')) {
                        nextButton.prop('disabled', true);
                    }

                    // Show finish button on the last step
                    if (currentStep >= numberOfSteps) {
                        nextButton.hide();
                        finishButton.show().removeClass('disabled');
                    } else {
                        nextButton.show();
                        finishButton.hide();
                    }

                    // Update progress bar
                    var percentage = ((currentStep / numberOfSteps) * 100);
                    wizard.find('.progress-bar').css('width', percentage + '%');

                    // Analytics (virtual page views)
                    // NOTE: No page view is triggered for the first tab here because of a bug in the wizard plugin that triggers onTabShow twice for the first tab
                    var trackingUrlBase = window.location.href.split('#')[0].replace(/\/$/, ''); // Remove hash and trailing slash
                    var referrer = window.location.href.split('#')[0];

                    if (currentTabPane.hasClass('claim-company-tab-login'))
                    {
                        $('#claim-login-with-email-submit').click(function () {
                            $('#claim-login-with-email-form-wrapper form input[name=destination]').val(window.location.href);
                        });
                        $('#claim-company-wizard .modal-body').animate({
                            height: '+=60px'
                        }, 500);

                        $('#claim-login-with-email').click(function () {
                            var cta = $('#claim-login-with-email-cta');
                            cta.animate({
                                top: '-=200px',
                                opacity: 0
                            }, 500);

                            var emailFormWrapper = $('#claim-login-with-email-form-wrapper');
                            emailFormWrapper.css('top', (cta.position().top + 200) + 'px');

                            emailFormWrapper.animate({
                                top: '-=200px',
                                opacity: 1
                            }, 500);

                            return false;
                        });

                        analytics.page({
                            title: 'Claim - login',
                            url: trackingUrlBase + '#login',
                            path: '/#login',
                            referrer: referrer
                        });
                    } else if (currentTabPane.hasClass('claim-company-tab-email')) {
                        CompanyClaimCompany.claim();

                        analytics.page({
                            title: 'Claim - email',
                            url: trackingUrlBase + '#email',
                            path: '/#email',
                            referrer: referrer
                        });
                    } else if (currentTabPane.hasClass('claim-company-tab-website')) {
                        analytics.page({
                            title: 'Claim - website',
                            url: trackingUrlBase + '#website',
                            path: '/#website',
                            referrer: referrer
                        });
                        console.log("Not has Subdomain! before");


                            finishButton.click(function () {
                                if (CompanyClaimCompany.validate(currentTabPane)) {

                                    CompanyClaimCompany.onCompleteWebSite();
                                }
                            });

                        console.log("has Subdomain! after");

                    }

                    else if (currentTabPane.hasClass('claim-company-tab-subdomain')) {
                        analytics.page({
                            title: 'Claim - subdomain',
                            url: trackingUrlBase + '#subdomain',
                            path: '/#subdomain',
                            referrer: referrer
                        });

                        finishButton.click(function () {
                            if (CompanyClaimCompany.validate(currentTabPane)) {
                                CompanyClaimCompany.onComplete();
                            }
                        });
                    }

                    CompanyClaimCompany.liveValidate(currentTabPane);
                },
                onNext: function (tab, navigation, index) {
                    var wizard = $('#claim-company-wizard');
                    var currentTabPane = wizard.find('.tab-pane.active');
                    var isValid = CompanyClaimCompany.validate(currentTabPane);

                    if (isValid) {
                        if (currentTabPane.hasClass('claim-company-tab-email')) {
                            CompanyClaimCompany.updateEmail($('#claim-input-company-email').val());
                        } else if (currentTabPane.hasClass('claim-company-tab-website')) {
                            if ($('#claim-input-company-no-website:checked').length == 0) {
                                CompanyClaimCompany.updateWebsite($('#claim-input-company-website').val());
                            } else {
                                CompanyClaimCompany.updateWebsite(null);
                            }
                        } else if (currentTabPane.hasClass('claim-company-tab-subdomain')) {
                            CompanyClaimCompany.requestSubdomain($('#claim-input-company-subdomain').val());
                        }

                    }

                    return isValid;
                }
            });

            wizard.bootstrapWizard('show', 0);
        },
        liveValidate: function (currentTabPane) {
            // STEP 3: DOMAIN
            if (currentTabPane.hasClass('claim-company-tab-subdomain')) {
                //$('#claim-input-company-subdomain').keyup();

                // JQUERY VALIDATION
                var form = $('form#claim-company-subdomain');
                $.validator.addMethod('regex', function (value, element, regex) {
                        var re = new RegExp(regex);
                        return this.optional(element) || re.test(value);
                    },
                    'Tegnsætning samt æ, ø og å er ikke tilladt'
                );

                form.validate({
                    debug: true,
                    errorClass: 'has-error',
                    rules: {
                        company_subdomain: {
                            required: true,
                            regex: "^[a-z0-9\-]+$"
                        }
                    },
                    onkeyup: BETTER.debounce(function () {
                        var nextButton = $('#claim-company-wizard').find('.button-next');

                        if (!form.valid()) {
                            nextButton.prop('disabled', true);
                        } else {
                            nextButton.prop('disabled', null);
                            var input = $('#claim-input-company-subdomain');

                            if (input.val().length == 0) {
                                return;
                            }

                            nextButton.prop('disabled', null);
                            input.removeClass('has-error');

                            $.ajax({
                                type: 'GET',
                                dataType: 'json',
                                url: BETTER.ROOT_DOMAIN + '/api/subdomain/' + input.val()
                            }).done(function (result, textStatus, xhr) {
                                if (xhr.status == 200) {
                                    nextButton.prop('disabled', true);
                                    input.addClass('has-error');
                                }
                            }).fail(function (xhr) {
                                if (xhr.status == 404) {
                                    // The subdomain is available
                                }
                            });

                        }
                    }, 500)
                });

                // STEP: WEBSITE
            } else if (currentTabPane.hasClass('claim-company-tab-website')) {
                form = $('form#claim-company-website');
                input = currentTabPane.find('#claim-input-company-website');

                input.blur(function () {
                    if ($(this).val().length > 0) {
                        if ($(this).val().indexOf('http://') == -1 && $(this).val().indexOf('https://') == -1) {
                            $(this).val('http://' + $(this).val());
                        }
                    }
                    form.valid();
                });

                form.validate({
                    debug: true,
                    errorClass: 'has-error',
                    rules: {
                        company_website: {
                            require_from_group: [1, ".website-group"],
                            url: true
                        },
                        company_nowebsite: {
                            require_from_group: [1, ".website-group"]
                        }
                    },
                    messages: {
                        company_website: 'Indtast venligst firmaets webside eller vælg "ingen webside"',
                        company_nowebsite: 'Indtast venligst firmaets webside eller vælg "ingen webside"'
                    },
                    groups: {
                        company_websitefields: 'company_website company_nowebsite'
                    }
                });

            }

        },
        validate: function (currentTabPane) {
            var wizard = $('#claim-company-wizard');
            var isStepValid = true;
            var validator;
            var form;

            // STEP: E-MAIL
            if (currentTabPane.hasClass('claim-company-tab-email')) {
                form = $('form#claim-company-email');
                validator = form.validate({
                    debug: true,
                    errorClass: 'has-error',
                    rules: {
                        company_email: {
                            required: true,
                            email: true
                        }
                    }
                });
                isStepValid = form.valid();
            }

            // STEP: WEBSITE
            else if (currentTabPane.hasClass('claim-company-tab-website')) {
                form = $('form#claim-company-website');
                input = form.find('#claim-input-company-website');

                if (input.val().length > 0) {
                    if (input.val().indexOf('http://') == -1 && input.val().indexOf('https://') == -1) {
                        input.val('http://' + input.val());
                    }
                }

                validator = form.validate({
                    debug: true,
                    errorClass: 'has-error',
                    rules: {
                        company_website: {
                            require_from_group: [1, ".website-group"],
                            url: true
                        },
                        company_nowebsite: {
                            require_from_group: [1, ".website-group"]
                        }
                    },
                    messages: {
                        company_website: 'Indtast venligst firmaets webside eller vælg "ingen webside"',
                        company_nowebsite: 'Indtast venligst firmaets webside eller vælg "ingen webside"'
                    },
                    groups: {
                        company_websitefields: 'company_website company_nowebsite'
                    }
                });
                isStepValid = form.valid();
            }

            // STEP: SUBDOMAIN
            else if (currentTabPane.hasClass('claim-company-tab-subdomain')) {
                form = $('form#claim-company-subdomain');
                $.validator.addMethod('regex', function (value, element, regex) {
                        var re = new RegExp(regex);
                        return this.optional(element) || re.test(value);
                    },
                    'Tegnsætning samt æ, ø og å er ikke tilladt'
                );

                form.validate({
                    debug: true,
                    errorClass: 'has-error',
                    rules: {
                        company_subdomain: {
                            required: true,
                            regex: "^[a-z0-9\-]+$"
                        }
                    }
                });
                isStepValid = form.valid();
            }

            if (!isStepValid && typeof validator !== 'undefined') {
                validator.focusInvalid();
            }

            return isStepValid;
        },
        claim: function () {
            if (hasClaimed || !IS_LOGGED_IN) {
                return;
            }

            $.ajax({
                type: 'POST',
                url: URL_CLAIM_COMPANY,
                dataType: 'json',
                contentType: 'application/json'
            }).done(function (result, textStatus, xhr) {
                if (xhr.status == 200) {
                    BETTER.push.subscribeToCompanyNotifications(BETTER.OWNED_COMPANIES);
                    hasClaimed = true;
                }
            }).fail(function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                switch (response.code) {
                    case "error.not.authenticated":
                        alert(ERROR_NOT_AUTHENTICATED);
                        break;
                    default:
                        alert("Der skete desværre en fejl da der skulle tages ejerskab af virksomheden");
                        $('#claim-company-wizard').modal('hide').bootstrapWizard('show', 0);

                        break;
                }
            });
        },
        updateEmail: function (newEmail) {
            if (!hasClaimed) {
                return;
            }

            $.ajax({
                type: 'POST',
                url: URL_UPDATE_EMAIL,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    email: newEmail
                })
            }).fail(function (xhr) {
                // todo: handle this
                var response = $.parseJSON(xhr.responseText);

                switch (response.code) {
                    case "error.not.authenticated":
                        alert(ERROR_NOT_AUTHENTICATED);
                        break;
                    default:
                        break;
                }
            });
        },
        updateWebsite: function (newWebsite) {
            if (!hasClaimed) {
                return;
            }

            $.ajax({
                type: 'POST',
                url: URL_UPDATE_WEBSITE,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    website: newWebsite
                })
            }).fail(function (xhr) {
                // todo: handle this
                var response = $.parseJSON(xhr.responseText);

                switch (response.code) {
                    case "error.not.authenticated":
                        alert(ERROR_NOT_AUTHENTICATED);
                        break;
                    default:
                        break;
                }
            });
        },
        requestSubdomain: function (subdomain) {
            if (!hasClaimed) {
                return;
            }

            $.ajax({
                type: 'POST',
                url: 'https://better.dk/ajax/company/'+COMPANY_ID+'/subdomain/add',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    subdomain: subdomain
                })
            }).done(function (result, textStatus, xhr) {
                if (xhr.status === 200) { // Created
                    window.location.replace('https://better.dk/cp/company/' + COMPANY_ID + '/edit-profile');
                }
            }).fail(function (xhr) {
                // todo: handle this
                var response = $.parseJSON(xhr.responseText);

                switch (response.code) {
                    case "error.not.authenticated":
                        alert(ERROR_NOT_AUTHENTICATED);
                        break;
                    default:
                        break;
                }
            });
        },
        onComplete: function () {
            $('#claim-company-wizard').modal('hide').bootstrapWizard('show', 0);
            CompanyClaimCompany.requestSubdomain($('#claim-input-company-subdomain').val());

            // Refresh page when all AJAX requests have completed to avoid interrupting the connection(s)
            $(document).ajaxStop(function () {

                window.location.replace('https://better.dk/cp/company/' + COMPANY_ID + '/edit-profile');
            });


            analytics.page({
                title: 'Claim - complete',
                url: window.location.href.split('#')[0].replace(/\/$/, '') + '#complete', // Remove hash and trailing slash
                path: '/#complete',
                referrer: window.location.href.split('#')[0]
            });

            return false;
        },
        onCompleteWebSite: function () {
            $('#claim-company-wizard').modal('hide').bootstrapWizard('show', 0);
            window.location.replace('https://better.dk/cp/company/' + COMPANY_ID + '/edit-profile');

            // Refresh page when all AJAX requests have completed to avoid interrupting the connection(s)
            $(document).ajaxStop(function () {
                window.location.replace('https://better.dk/cp/company/' + COMPANY_ID + '/edit-profile');
                window.location.reload();
            });


            analytics.page({
                title: 'Claim - complete',
                url: window.location.href.split('#')[0].replace(/\/$/, '') + '#complete', // Remove hash and trailing slash
                path: '/#complete',
                referrer: window.location.href.split('#')[0]
            });

            return false;
        }
    };
    CompanyClaimCompany.start();


    /***** FAVORITE COMPANY *****/
    CompanyFavorite = {
        favorite: function () {
            if (IS_LOGGED_IN) {
                $.ajax({
                    type: 'POST',
                    url: BETTER.ROOT_DOMAIN + '/ajax/company/' + company.id + '/favorite',
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function () {
                    alert("Virksomheden blev tilføjet til dine favoritter!");
                }).fail(function () {
                    alert("Ups, der skete en fejl!");
                });
            } else {
                BETTER.setLoginHash(HASH_FAVORITE_COMPANY);
                BETTER.setLoginDestination('#' + HASH_FAVORITE_COMPANY, true);
                $('#login-dialog').modal('show');
            }
        }
    };

    /***** LIKE COMPANY *****/
    CompanyLikeCompany = {
        start: function () {
            $('#like-company-button').click(function () {
                CompanyLikeCompany.like();
                return false;
            });
        },
        like: function () {
            if (IS_LOGGED_IN) {
                $.ajax({
                    type: 'POST',
                    url: URL_LIKE_COMPANY,
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function (result, textStatus, xhr) {
                    if (xhr.status == 201) { // Created
                        alert("Du har syntes godt om virksomheden!");

                        var likes = $('#like-company-button .left');
                        var likesNumber = likes.data('likes');

                        likes.attr('data-likes-number', (likesNumber + 1));
                        likes.html(likesNumber + 1);

                        $('#like-company-button').attr('disabled', true);
                    }
                }).fail(function (xhr) {
                    var response = $.parseJSON(xhr.responseText);

                    switch (response.code) {
                        case "error.unique.constraint.violation":
                            alert(ERROR_ALREADY_LIKED_COMPANY);
                            break;
                        case "error.not.authenticated":
                            alert(ERROR_NOT_AUTHENTICATED);
                            break;
                        default:
                            break;
                    }
                });
            } else {
                analytics.track('Profile - Like Login Dialog', {
                    company: company
                });

                BETTER.setLoginHash(HASH_LIKE_COMPANY);
                BETTER.setLoginDestination('#' + HASH_LIKE_COMPANY, true);
                $('#login-dialog').modal('show');
            }
        }
    };
    CompanyLikeCompany.start();


    /***** INITIALIZATION *****/

    /** Mark current tab as active **/
    $('.tabs-container .tabs a').each(function () {
        if (this.href == window.location.href.split('#')[0]) { // this.href returns an absolute URL, even if the href is relative
            $(this).parent('li').addClass('active');
        }
    });

    if ($('.tabs-container .tabs li.active').length == 0) {
        // Mark products/services tab as active if the URL contains the below string
        if (window.location.href.indexOf('/ydelse/') > -1) {
            $('#profile-tab-services').addClass('active');
        }
    }

    $('#online-booking-button').click(function () {
        analytics.track('Clicked Booking Button', {
            company: company
        });

        var dialog;

        if (IS_LOGGED_IN) {
            dialog = $('#hungry-discount-dialog');
            dialog.on('shown.bs.modal', function () {
                analytics.track('Displayed Discount Dialog', {
                    company: company
                });
            });
        } else {
            dialog = $('#hungry-deal-dialog');
            dialog.on('shown.bs.modal', function () {
                analytics.track('Displayed Deal Dialog', {
                    company: company
                });
            });
        }

        dialog.modal('show');
        return false;
    });

    analytics.trackLink(document.getElementById('hungry-continue-without-discount'), 'Deal Dialog - Continued Without Discount', {
        company: company
    });

    analytics.trackLink(document.getElementById('continue-to-hungry-with-discount'), 'Discount Dialog - Continued With Discount', {
        company: company
    });

    $('#claim-input-company-email').focus(function () {
        $(this).select();
    });

    // Obscure phone number if the number is not a call tracking number
    if ($('#main-cta .phone-number:not(.call-tracking)').length > 0) {
        var phoneNumberWrapper = $('#main-cta .phone-number');
        var phoneNumber = phoneNumberWrapper.html().trim();
        phoneNumberWrapper.html(phoneNumber);
    }

    /***** CALL COMPANY *****/
    $('#call-company').click(function () {
        analytics.track('Profile - Call Company Dialog', {
            company: company
        });

        $('#call-company-dialog').modal('show');
        return false;
    });

    $('#call-company-initiate-call').click(function () {
        var fromPhoneNumber = $('#call-company-phone-number').val().trim();
        fromPhoneNumber = fromPhoneNumber.replace(/\s/g, '');

        if (fromPhoneNumber.length < 8) {
            alert("Indtast venligst et korrekt telefon nummer");
            return false;
        }

        $('#call-company-wrapper').children().fadeOut(500);

        setTimeout(function () {
            $('#call-company-success').show();
        }, 500);

        $.ajax({
            type: 'POST',
            url: URL_CALL_COMPANY,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                from: fromPhoneNumber
            })
        }).done(function () {

        }).fail(function () {
            $('#call-company-wrapper').children().fadeOut(500);

            setTimeout(function () {
                $('#call-company-failure').show();
            }, 500);
        });

        return false;
    });

    $('#call-company-dialog').on('hidden.bs.modal', function () {
        $('#call-company-wrapper').children().hide();
        $('#call-company-inputs').show();
    });

    /***** HASHES *****/
    if (typeof window.location.hash !== 'undefined') {
        var hash = window.location.hash.substr(1); // Remove #

        switch (hash) {
            case 'besked':
                $('#send-message-dialog').modal('show');
                break;

            case HASH_SEND_MESSAGE:
                $('#send-email-dialog').modal('show');
                break;

            case HASH_SEND_EMAIL:
                $('#send-email-dialog').modal('show');
                break;

            case HASH_LIKE_COMPANY:
                CompanyLikeCompany.like();
                window.location.hash = '';
                break;

            case HASH_FAVORITE_COMPANY:
                CompanyFavorite.favorite();
                break;

            case HASH_CLAIM_COMPANY:
                CompanyClaimCompany.modal();
                break;

            case HASH_CLAIM_COMPANY_LOGGED_IN:
                window.location.hash = '';
                CompanyClaimCompany.modal(1); // 1 = skip login step
                break;

            case HASH_DEAL_HUNGRY:
                window.location.hash = '';

                if (IS_LOGGED_IN) {
                    analytics.track('Displayed Discount Dialog', {
                        company: company
                    });

                    $('#hungry-discount-dialog').modal('show');
                }

                break;

            case HASH_DISCOUNT:
                var dialog;

                if (IS_LOGGED_IN) {
                    dialog = $('#hungry-discount-dialog');
                    dialog.on('shown.bs.modal', function () {
                        analytics.track('Displayed Discount Dialog', {
                            company: company
                        });
                    });
                } else {
                    dialog = $('#hungry-deal-dialog');
                    dialog.on('shown.bs.modal', function () {
                        analytics.track('Displayed Deal Dialog', {
                            company: company
                        });
                    });
                }

                dialog.modal('show');
                break;

            case HASH_COMPANY_CHANGE_PROPOSAL:
                window.location.hash = '';

                if (!IS_LOGGED_IN) {
                    analytics.track('Profile - Propose Changes Login Dialog', {
                        company: company
                    });

                    BETTER.setLoginHash(HASH_COMPANY_CHANGE_PROPOSAL);
                    $('#login-dialog').modal('show');
                } else {
                    $('#propose-change-dialog').modal('show');

                    analytics.track('Profile - Propose Changes Dialog', {
                        company: company
                    });
                }

                break;

            default:
                break;
        }
    }

    /***** Show CTAs if applicable *****/

        // Splash
    var keywords = [company.name];
    $.merge(keywords, company.industries);

    if ($('#cta-splash').length > 0 && $('#profile-options .claim-company').length > 0) { // Don't show splash if the company has been claimed
        var match = BETTER.notifications.getMatchingKeyword(keywords, BETTER.notifications.TYPE_SPLASH);

        if (match) {
            AdButler.ads.push({
                handler: function (opt) {
                    AdButler.register(166948, 189410, [600, 300], 'cta-splash-zone', opt);
                }, opt: {place: 0, keywords: BETTER.normalize(match), domain: 'servedbyadbutler.com'}
            });

            window.setTimeout(function () {
                // Only show splash if no modal is shown and no menu is expanded
                if ($('.modal.in').length == 0 && $('.menu-item[aria-expanded="true"]').length == 0) {
                    if ($('#cta-splash .modal-body:not(:empty)').length > 0) { // This should always be true at this point, but just in case...
                        $('#cta-splash').modal('show');
                    }
                }
            }, 3000);
        }
    }
});