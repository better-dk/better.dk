var BETTER = BETTER || {};

BETTER.messaging = BETTER.messaging || {};
BETTER.messaging.inbox = {};
BETTER.messaging.inbox.SEND_MESSAGE_ON_RETURN = false;
BETTER.messaging.inbox.COOKIE_SEND_MESSAGE_ENTER = 'send-message-enter';
BETTER.messaging.inbox.IS_COMPANY = ($('#inbox-wrapper.company-inbox').length > 0);

BETTER.messaging.inbox.initialize = function () {
    // If there are at least one thread, then view that thread by default
    var threads = $('.conversation');

    if (threads.length > 0) {
        var threadId = BETTER.qs('threadId');

        if (threadId != null) {
            var threadTarget = $('.conversation[data-thread-id="' + threadId + '"]');

            if (threadTarget.length > 0) {
                threadTarget.get(0).click();
            } else {
                $(threads).get(0).click();
            }
        } else {
            $(threads).get(0).click();
        }
    }

    // Set send message on enter value from cookie
    var sendMessageOnEnter = BETTER.getCookieValue(BETTER.messaging.inbox.COOKIE_SEND_MESSAGE_ENTER);

    if (sendMessageOnEnter != null) {
        BETTER.messaging.inbox.SEND_MESSAGE_ON_RETURN = true;
        $('#checkbox-send-message-return').prop('checked', true);
        $('#send-new-message').hide();
    }

    // Adapt heights based on screen height
    var resize = function () {
        var margin = ($('#inbox-wrapper.company-inbox').length > 0 ? 40 : 30);
        var targetHeight = ($(window).height() - ($('#topbar').outerHeight() + margin));
        $('#conversations-wrapper').css('height', targetHeight + 'px');
        $('#conversation-view').css('height', targetHeight + 'px');
        $('#conversation-view-messages').css('height', (targetHeight - $('#active-thread-subject').outerHeight() - $('#conversation-new-message').outerHeight()) + 'px');
        BETTER.messaging.inbox.scrollToBottom();
    };
    resize();
    $(window).resize(resize);

    // New thread event
    BETTER.messaging.addListener(BETTER.messaging.EVENT_NEW_THREAD, function (data) {
        if (data.companyId != null) {
            if ($('#inbox-wrapper.company-inbox[data-company-id="' + data.companyId + '"]').length == 0) {
                return;
            }
        }

        var senderName;
        var imageUrl;

        if (data.message.sender.company != null) {
            var company = data.message.sender.company;
            senderName = company.name;
            imageUrl = company.logoUrl;
        } else {
            var senderAccount = data.message.sender.account;
            senderName = senderAccount.firstName;
            senderName += (senderAccount.middleName != null ? ' ' + senderAccount.middleName : '');
            senderName += (senderAccount.lastName != null ? ' ' + senderAccount.lastName : '');
            imageUrl = senderAccount.profilePicture;
        }

        var template = $('#conversation-template');
        template.removeAttr('id');
        template.addClass('unread');
        template.attr('data-thread-id', data.threadId);
        template.attr('data-subject', encodeURIComponent(data.subject));
        template.find('.subject').html(data.subject);
        template.find('.image img').attr('src', imageUrl);
        template.find('.last-activity').html(moment(parseInt(data.message.created)).format('DD-MM-YY'));
        template.find('.recipient-name').html(senderName).attr('title', senderName);
        template.find('.most-recent-message').html(data.message.text);
        template.show();
        $('#conversations').prepend(template);
    });

    // Mark unread threads
    var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);

    if (cookieValue != null) {
        var companyId = $('#inbox-wrapper.company-inbox').data('companyId');
        var threadIds = BETTER.messaging.getUnreadThreadIds(JSON.parse(decodeURIComponent(cookieValue)), companyId);

        if (threadIds.length > 0) {
            BETTER.messaging.inbox.displayThreadsAsUnread(threadIds);
        }
    }

    // Refresh relative times
    window.setInterval(this.refreshRelativeTimes, 15000);
};

BETTER.messaging.inbox.getActiveThreadId = function () {
    var collection = $('.conversation.active');
    return ((collection.length > 0) ? parseInt(collection.data('threadId')) : null);
};

BETTER.messaging.inbox.markAsRead = function (threadId) {
    var url = BETTER.ROOT_DOMAIN + '/ajax/thread/' + threadId + '/mark/read';
    var accessToken = BETTER.qs('token');
    var checksum = BETTER.qs('checksum');
    var parameters = {};

    if (BETTER.messaging.inbox.IS_COMPANY) {
        parameters.companyId = $('#inbox-wrapper.company-inbox').data('companyId');
    }

    if (accessToken != null && checksum != null) {
        parameters.token = accessToken;
        parameters.checksum = checksum;
    }

    $.post(url + '?' + $.param(parameters));
    $('#conversations-wrapper .conversation[data-thread-id="' + threadId + '"]').removeClass('unread');
    var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);

    if (cookieValue != null) {
        var unreadMessagesPerThread = JSON.parse(decodeURIComponent(cookieValue));
        delete unreadMessagesPerThread.personal[threadId];

        // Remove thread from companies unread threads
        if (typeof unreadMessagesPerThread.companies !== 'undefined') {
            var keys = Object.keys(unreadMessagesPerThread.companies);

            for (var i = 0; i < keys.length; i++) {
                var companyId = keys[i];
                delete unreadMessagesPerThread.companies[companyId][threadId];
            }
        }

        BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(unreadMessagesPerThread)), null, BETTER.getRootDomain());
    }

    BETTER.messaging.refreshUnreadThreadsNotification();
};

BETTER.messaging.inbox.displayThreadsAsUnread = function (threadIds) {
    if (!threadIds instanceof Array) {
        return;
    }

    for (var i = 0; i < threadIds.length; i++) {
        $('.conversation[data-thread-id="' + threadIds[i] + '"]').addClass('unread');
    }
};

BETTER.messaging.inbox.scrollToBottom = function () {
    var container = document.getElementById('conversation-view-messages');

    if (container != null) {
        container.scrollTop = container.scrollHeight;
    }
};

BETTER.messaging.inbox.refreshConversation = function (threadId, mostRecentMessageText, lastActivityUnixTimestamp) {
    var thread = $('#conversations .conversation[data-thread-id="' + threadId + '"]');

    if (thread.length == 0) {
        return;
    }

    thread.find('.info-wrapper .most-recent-message').html(mostRecentMessageText);
    thread.find('.info-wrapper .last-activity').html(moment(parseInt(lastActivityUnixTimestamp) * 1000).format('DD-MM-YY'));
};

// messages = sorted ascending
BETTER.messaging.inbox.displayMessages = function (messages, keepScrollPosition) {
    var target = $('#conversation-view-messages');
    var groups = [];
    var previousMessage = null;
    var targetOffset = 0;
    var hasMessages = (target.find('.group').length > 0);

    if (keepScrollPosition === true && hasMessages) {
        // Calculate the position of the first message relative to the parent, so that we can set the parent's scrollTop
        // property when scrolling, and thereby keep the scroll position even after having emptied the message container
        var firstGroup = target.find(':first');
        targetOffset = (Math.abs((firstGroup[0].offsetTop - target[0].scrollHeight)) + firstGroup.find(':first').outerHeight());
    }

    var isSelf = function (isCompany, message) {
        return (isCompany && message.sender.company != null) || (!isCompany && message.sender.company == null);
    };

    // First loop through the messages and group them together where appropriate
    for (var i = 0; i < messages.length; i++) {
        if (groups.length == 0 || (previousMessage == null || (messages[i].sender.id != previousMessage.sender.id))) {
            groups.push({
                isSelf: isSelf(this.IS_COMPANY, messages[i]),
                messages: [messages[i]]
            });
        } else {
            groups[groups.length - 1].messages.push(messages[i]);
        }

        previousMessage = messages[i];
    }

    // Display groups
    target.empty();
    var template = $('#message-template').clone();
    $(template).filter(':first').removeAttr('id');
    var currentTemplate, group, message, markup;

    for (var j = 0; j < groups.length; j++) {
        group = groups[j];
        markup = '<div class="group' + (group.isSelf ? ' self' : '') + '">';

        for (var k = 0; k < group.messages.length; k++) {
            message = group.messages[k];
            currentTemplate = $(template).clone();
            var created = new Date(message.created);
            $(currentTemplate).find('.message-text').html(message.text).attr('title', created.toLocaleString());
            $(currentTemplate).removeAttr('style');

            // Only show author information for the first message in the group
            if (k == 0) {
                $(currentTemplate).find('.author img').attr('src', (message.sender.company != null ? message.sender.company.logoUrl : message.sender.account.profilePicture));
                $(currentTemplate).find('.author .name').html(message.sender.account.firstName);
            } else {
                $(currentTemplate).find('.author').empty();
            }

            // Only show message date for the last message in the group
            if (k == (group.messages.length - 1)) {
                $(currentTemplate).find('.message-date').html(moment(message.created).fromNow()).attr('title', created.toLocaleString()).attr('data-timestamp', message.created);
            } else {
                $(currentTemplate).find('.message-date').remove();
            }

            markup += $(currentTemplate)[0].outerHTML;
        }

        target.append(markup + '</div>');
    }

    if (keepScrollPosition === true && hasMessages) {
        // Set the scroll position to where it was before emptying and adding new messages
        // We use the target's scrollHeight property again to account for the container's new height
        target[0].scrollTop = (target[0].scrollHeight - targetOffset);
    }
};

BETTER.messaging.inbox.handleIncomingUserMessage = function (threadId, message) {
    if (BETTER.messaging.inbox.IS_COMPANY) {
        return;
    }

    var activeThreadId = this.getActiveThreadId();

    if (typeof activeThreadId != null) {
        if (activeThreadId == threadId) {
            this.displayMessages(BETTER.messaging.getThread(threadId).messages);
            this.scrollToBottom();
            this.markAsRead(threadId);
        } else {
            this.moveThreadToTop(threadId);
            this.markAsUnread(threadId);
        }

        this.refreshConversation(threadId, message.text, (message.created / 1000));
        BETTER.messaging.refreshUnreadThreadsNotification();
    }
};

BETTER.messaging.inbox.handleIncomingCompanyMessage = function (threadId, message) {
    if (!BETTER.messaging.inbox.IS_COMPANY) {
        return;
    }

    var activeThreadId = this.getActiveThreadId();

    if (activeThreadId != null) {
        if (activeThreadId == threadId) {
            this.markAsRead(threadId);
            this.displayMessages(BETTER.messaging.getThread(threadId).messages);
            this.scrollToBottom();
        } else {
            this.moveThreadToTop(threadId);
            this.markAsUnread(threadId);
        }

        this.refreshConversation(threadId, message.text, (message.created / 1000));
        BETTER.messaging.refreshUnreadThreadsNotification();
    }
};

BETTER.messaging.inbox.moveThreadToTop = function (threadId) {
    var conversation = $('#conversations-wrapper .conversation[data-thread-id="' + threadId + '"]');
    conversation.prependTo('#conversations');
};

BETTER.messaging.inbox.markAsUnread = function (threadId) {
    var conversation = $('#conversations-wrapper .conversation[data-thread-id="' + threadId + '"]');
    conversation.addClass('unread');
};

BETTER.messaging.inbox.refreshRelativeTimes = function () {
    $('#conversation-view-messages .message .message-date').each(function () {
        var timestamp = parseInt($(this).attr('data-timestamp'));
        $(this).html(moment(timestamp).fromNow());
    });
};

$(function () {
    $('#conversations-wrapper').on('click', '.conversation', function () {
        $('.conversation').removeClass('active'); // Remove active classes from previously active thread
        $(this).addClass('active');
        var threadId = parseInt($(this).data('threadId'));
        var currentThread = BETTER.messaging.getThread(threadId);
        $('#active-thread-subject').html(decodeURIComponent($(this).data('subject')));

        // The thread has already been loaded; display loaded messages
        if (currentThread != null && currentThread.messages != null) {
            BETTER.messaging.inbox.displayMessages(currentThread.messages);
            BETTER.messaging.inbox.scrollToBottom();
        } else {
            // Fetch and display messages
            BETTER.messaging.getMessages(threadId, 1, function (messages, hasMoreMessages) {
                $('.conversation.active').attr('data-has-more-messages', hasMoreMessages);
                BETTER.messaging.inbox.displayMessages(messages);
                BETTER.messaging.inbox.scrollToBottom();
            });
        }

        BETTER.messaging.inbox.markAsRead(threadId);
    });

    function sendNewMessage(isCompany) {
        var threadId = BETTER.messaging.inbox.getActiveThreadId();

        if (typeof threadId != null) {
            var messageText = $('#new-message-text').val().trim();

            if (messageText.length < 1) {
                return false;
            }

            var spinner = $('#send-message-spinner');
            var successCallback = function () {
                var activeThreadId = BETTER.messaging.inbox.getActiveThreadId();
                BETTER.messaging.inbox.displayMessages(BETTER.messaging.getThread(activeThreadId).messages);
                BETTER.messaging.inbox.scrollToBottom();
                BETTER.messaging.inbox.moveThreadToTop(activeThreadId);
                BETTER.messaging.inbox.refreshConversation(activeThreadId, messageText, (Date.now() / 1000));
            };

            var failureCallback = function () {
                alert("Beskeden kunne desværre ikke sendes");
            };

            var alwaysCallback = function () {
                spinner.fadeOut(250);
            };

            spinner.fadeIn(250);
            BETTER.messaging.sendNewMessage(threadId, messageText, isCompany, successCallback, failureCallback, alwaysCallback());
        } else {
            return false;
        }
    }

    var keysPressed = {13: false, 16: false, 17: false}; // 13 = Enter, 16 = shift, 17 = ctrl

    $('#new-message-text').keydown(function (event) {
        if (!BETTER.messaging.inbox.SEND_MESSAGE_ON_RETURN) {
            return;
        }

        if (event.keyCode in keysPressed) {
            keysPressed[event.keyCode] = true;
        }
    }).keyup(function (event) {
        if (!BETTER.messaging.inbox.SEND_MESSAGE_ON_RETURN) {
            return;
        }

        // Only enter pressed (send message) and not ctrl or shift (for new line)
        if (keysPressed[13] && !(keysPressed[16] || keysPressed[17])) {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                event.preventDefault();
                return false;
            }

            if (sendNewMessage(BETTER.messaging.inbox.IS_COMPANY) !== false) {
                $(this).val('');
            }

            event.preventDefault();
        }

        if (!keysPressed[13]) { // Enter not pressed
            for (var key in keysPressed) {
                keysPressed[key] = false;
            }
        } else {
            if (event.keyCode in keysPressed) {
                keysPressed[event.keyCode] = false;
            }
        }
    });

    $('#send-new-message').click(function () {
        if (!BETTER.IS_LOGGED_IN) {
            return false;
        }

        if (sendNewMessage(BETTER.messaging.inbox.IS_COMPANY) !== false) {
            $('#new-message-text').val('');
        }

        return false;
    });

    $('#conversations-wrapper').scroll(function () {
        var list = $(this);
        var scrollTop = $(this).scrollTop();
        var scrollHeight = $(this).get(0).scrollHeight;
        var outerHeight = $(this).outerHeight();

        if (scrollTop == (scrollHeight - outerHeight)) {
            if (JSON.parse(list.attr('data-has-more-threads')) == false) { // We use JSON.parse here because the attribute is a string
                return;
            }

            var pageToFetch = (parseInt(list.data('page')) + 1);
            var companyId = null;

            var successHandler = function (data) {
                list.attr('data-page', pageToFetch);
                list.attr('data-has-more-threads', data.hasNext);

                // Add threads to DOM
                $.each(data.threads, function (index, thread) {
                    var otherParticipant;

                    if (BETTER.messaging.inbox.IS_COMPANY) {
                        otherParticipant = thread.sender;
                    } else {
                        otherParticipant = thread.receiver;
                    }

                    var otherParticipantName;

                    if (otherParticipant.company != null) {
                        otherParticipantName = otherParticipant.company.name;
                    } else {
                        otherParticipantName = otherParticipant.account.firstName;
                        otherParticipantName += (otherParticipant.account.middleName != null ? ' ' + otherParticipant.account.middleName : '');
                        otherParticipantName += (otherParticipant.account.lastName != null ? ' ' + otherParticipant.account.lastName : '');
                    }

                    var template = $('#conversation-template').clone();
                    template.removeAttr('id');
                    template.attr('data-thread-id', thread.id);
                    template.attr('data-subject', encodeURIComponent(thread.subject));
                    template.find('.image img').attr('src', (BETTER.messaging.inbox.IS_COMPANY ? otherParticipant.account.profilePicture : otherParticipant.company.logoUrl));
                    template.find('.subject').html(thread.subject);
                    template.find('.most-recent-message').html(thread.mostRecentMessageText);
                    template.find('.recipient-name').html(otherParticipantName);
                    template.find('.last-activity').html(moment(parseInt(thread.lastActivity)).format('DD-MM-YY'));
                    template.show();
                    list.find('#conversations').append($(template));
                });

                // Mark unread threads in case any of the threads we just added are unread
                var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);

                if (cookieValue != null) {
                    var companyId = $('#inbox-wrapper.company-inbox').data('companyId');
                    var threadIds = BETTER.messaging.getUnreadThreadIds(JSON.parse(decodeURIComponent(cookieValue)), companyId);

                    if (threadIds.length > 0) {
                        BETTER.messaging.inbox.displayThreadsAsUnread(threadIds);
                    }
                }
            };

            BETTER.messaging.getThreads(pageToFetch, companyId, successHandler);
        }
    });

    $('#conversation-view-messages').scroll(function () {
        var activeThreadId = BETTER.messaging.inbox.getActiveThreadId();
        var dataWrapper = $('#conversations-wrapper .conversation[data-thread-id="' + activeThreadId + '"]');
        var previousScroll = dataWrapper.data('scroll');
        previousScroll = (typeof previousScroll !== 'undefined' ? previousScroll : {});

        if (previousScroll != null && previousScroll.y > 0 && $(this).scrollTop() == 0) { // previousScroll.y ensures that the scroll was in the upwards direction
            var hasMoreMessages = JSON.parse($('.conversation.active').attr('data-has-more-messages')); // We use JSON.parse here because the attribute is a string

            if (typeof activeThreadId != null && hasMoreMessages != false) {
                var currentThread = BETTER.messaging.getThread(activeThreadId);
                var pageToFetch = 1;

                if (currentThread != null) {
                    pageToFetch = (currentThread.currentPage + 1);
                }

                // Fetch and display messages
                BETTER.messaging.getMessages(activeThreadId, pageToFetch, function (messages, hasMore) {
                    BETTER.messaging.inbox.displayMessages(BETTER.messaging.getThread(activeThreadId).messages, true);
                    $('.conversation[data-thread-id="' + activeThreadId + '"]').attr('data-has-more-messages', hasMore);
                });
            }
        }

        previousScroll.x = $(this).scrollLeft();
        previousScroll.y = $(this).scrollTop();
        dataWrapper.data('scroll', previousScroll);
    });

    $('#checkbox-send-message-return').change(function () {
        if ($(this).is(':checked')) {
            $('#send-new-message').fadeOut(500);
            BETTER.messaging.inbox.SEND_MESSAGE_ON_RETURN = true;
            BETTER.setCookie(BETTER.messaging.inbox.COOKIE_SEND_MESSAGE_ENTER, 1, 365, BETTER.getRootDomain());
        } else {
            $('#send-new-message').fadeIn(500);
            BETTER.messaging.inbox.SEND_MESSAGE_ON_RETURN = false;
            BETTER.deleteCookie(BETTER.messaging.inbox.COOKIE_SEND_MESSAGE_ENTER, '/', BETTER.getRootDomain());
        }
    });

    BETTER.messaging.inbox.initialize();
});