$(function () {
    $('.list-group-item.active').removeClass('active');

    var currentPage = window.location.href; // this.href returns an absolute URL, even if the href is relative
    currentPage = currentPage.split('#')[0]; // Remove hash
    currentPage = currentPage.replace(window.location.search, ''); // Remove query string

    $('.list-group-item').each(function () {
        if (this.href == currentPage) {
            $(this).addClass('active');
        }
    });
});