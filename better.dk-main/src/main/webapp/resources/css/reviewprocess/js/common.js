$(function () {

    //Форма отправки 2.0 //
    $(function () {
        $("[name=send]").click(function () {
            $(":input.error").removeClass('error');
            $(".allert").remove();

            var error;
            var btn = $(this);
            var ref = btn.closest('form').find('[required]');
            var msg = btn.closest('form').find('input');
            var send_btn = btn.closest('form').find('[name=send]');
            var send_options = btn.closest('form').find('[name=campaign_token]');

            $(ref).each(function () {
                if ($(this).val() == '') {
                    var errorfield = $(this);
                    $(this).addClass('error').parent('.field').append('<div class="allert"><span>This field is reqiured</span><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>');
                    error = 1;
                    $(":input.error:first").focus();
                    return;
                } else {
                    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                    if ($(this).attr("type") == 'email') {
                        if (!pattern.test($(this).val())) {
                            $("[name=email]").val('');
                            $(this).addClass('error').parent('.field').append('<div class="allert"><span>This field is not e-mail</span><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>');
                            error = 1;
                            $(":input.error:first").focus();
                        }
                    }
                    var patterntel = /^()[0-9]{9,18}/i;
                    if ($(this).attr("type") == 'tel') {
                        if (!patterntel.test($(this).val())) {
                            $("[name=phone]").val('');
                            $(this).addClass('error').parent('.field').append('<div class="allert"><span>Укажите коректный номер телефона</span><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>');
                            error = 1;
                            $(":input.error:first").focus();
                        }
                    }
                }
            });
            if (!(error == 1)) {
                $(send_btn).each(function () {
                    $(this).attr('disabled', true);
                });
                $(send_options).each(function () {
                    if ($(this).val() == '') {
                        $.ajax({
                            type: 'POST',
                            url: 'mail.php',
                            data: msg,
                            success: function () {
                                $('form').trigger("reset");
                                setTimeout(function () {
                                    $("[name=send]").removeAttr("disabled");
                                }, 1000);
                                // Настройки модального окна после удачной отправки
                                $('div.md-show').removeClass('md-show');
                                $("#modal_callback_ok").addClass('md-show');
                                setTimeout(function () {
                                    $("#modal_callback_ok").removeClass('md-show');
                                }, 2000);
                            },
                            error: function (xhr, str) {
                                alert('Возникла ошибка: ' + xhr.responseCode);
                            }
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: 'mail.php',
                            data: msg,
                            success: $.ajax({
                                type: 'POST',
                                url: 'https://app.getresponse.com/add_subscriber.html',
                                data: msg,
                                statusCode: {
                                    0: function () {
                                        $('form').trigger("reset");
                                        setTimeout(function () {
                                            $("[name=send]").removeAttr("disabled");
                                        }, 1000);
                                        // Настройки модального окна после удачной отправки
                                        $('div.md-show').removeClass('md-show');
                                        $("#modal_callback_ok").addClass('md-show');
                                        yaCounter39760580.reachGoal('zay');
                                        setTimeout(function () {
                                            $("#modal_callback_ok").removeClass('md-show');
                                        }, 2000);
                                    }
                                }
                            }),
                            error: function (xhr, str) {
                                alert('Возникла ошибка: ' + xhr.responseCode);
                            }
                        });
                    }
                });
            }
            return false;
        })
    });
});