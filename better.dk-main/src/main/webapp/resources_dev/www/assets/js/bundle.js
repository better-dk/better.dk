document.addEventListener('DOMContentLoaded',function(){
    var reviews = $('.reviews');
    var img = reviews.closest('.reviewsSlider').find('.reviewsSlider__stateImg');
    var owl = reviews.owlCarousel({
        loop:true,
        autoplay: true,
        autoplayTimeout: 3000,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        },
        onInitialized: function(e){
            alignReviewsSize(e.relatedTarget.$element);
        },
        onResized: function(e){
            (function (el){
                var maxHeight = 0;
                var items = el.find('.review');
                items.css({
                    height: 'auto'
                });
                setTimeout(function(){
                    alignReviewsSize(el);
                },0)
            })(e.relatedTarget.$element);       
        }
    });
    owl.on('changed.owl.carousel', function(event) {
        var offset, middle, total, active, activeItem, activeReview, activeSocial,imgSrc;
        if ( event.item.index >= event.item.count ){
            offset = event.item.index - event.item.count;
        }
        else{
            offset = event.item.index;
        }
        middle = Math.ceil(event.item.count / 2);
        total = middle + offset;
        if ( total > event.item.count ){
            active = total - event.item.count;
        }
        else{
            active = total;
        }
        activeItem = event.relatedTarget._items[active-1];
        activeReview = $('.review',activeItem);
        activeSocial = activeReview.data('social-type');
        imgSrc = '/pic/reviewsSlider__state_' + activeSocial + '.png';
        img.attr('src',imgSrc);
    });
},false);

function alignReviewsSize (el){
    var maxHeight = 0;
    var items = el.find('.review');
    items.each(function(){
        if ( this.offsetHeight > maxHeight ){
            maxHeight = this.offsetHeight;
        }
    });
    items.css({
        height: maxHeight + 'px'
    }); 
}