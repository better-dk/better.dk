var gulp = require('gulp');
var jade = require('gulp-jade');
var concat = require('gulp-concat');
var htmlbeautify = require('gulp-html-beautify');

// gulp.task('html',function(){
// 	return gulp.src('src/*.jade')
// 			.pipe(jade())
// 			.pipe(htmlbeautify())
// 			.pipe(gulp.dest('www'));
// });

gulp.task('css',function(){
	return gulp.src(['node_modules/reset-css/reset.css','node_modules/bootstrap-grid/dist/grid.css','src/css/font-awesome.css','src/css/main.css'])
			.pipe(concat('bundle.css'))
			.pipe(gulp.dest('../resources/css'));
})

gulp.task('js',function(){
	return gulp.src(['src/js/*.js'])
			.pipe(concat('bundle.js'))
			.pipe(gulp.dest('../resources/js/application/index'));
});

gulp.task('default',[/*'html',*/'css','js']);