var path = require('path');
var express = require('express');
var app = express();

app.use(express.static('www/assets'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, './www', 'index.html'));
});

app.listen(9999, function () {});