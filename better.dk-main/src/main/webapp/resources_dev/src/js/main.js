/*document.addEventListener('DOMContentLoaded',function(){
    reviewsSliders();
});*/
$(window).on("load",function (){
    reviewsSliders();
});

function reviewsSliders(){
    var i, el, slider;
    var els = document.getElementsByClassName('reviewsSlider');
    for ( i = 0; i < els.length; i++ ){
        el = els[i];
        slider = new ReviewsSlider(el);
    }
}
function ReviewsSlider(el){
    var i;
    var self = this;
    this.el = el;
    this.sliderEl = el.querySelector('.reviews');
    this.stateVariants = el.querySelectorAll('.reviewsSlider__stateVariant');
    this.items = null;
    this.stateVariantsArr = [];
    for (i=0;i<this.stateVariants.length;i++){
        this.stateVariantsArr.push(this.stateVariants[i]);
    }
    this.bridgeEl = el.querySelector(".reviewsSlider__bridge");
    this.slider = null;
    this.attachSlider = function (){
        this.slider = $(this.sliderEl).owlCarousel({
            loop:true,
            autoplay: true,
            autoplayTimeout: 3000,
            margin:10,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            },
            onInitialized: function(e){
                self.alignReviewsSize(e.relatedTarget.$element);
                self.bridgeEl.classList.add("active");
            },
            onResized: function(e){
                (function (el){
                    var maxHeight = 0;
                    var items = el.find('.review');
                    items.css({
                        height: 'auto'
                    });
                    setTimeout(function(){
                        self.alignReviewsSize(el);
                    },0)
                })(e.relatedTarget.$element);  
            },
            onChanged: function (event){
                var items = self.sliderEl.querySelectorAll('.owl-item');
                var offset, activeItem, activeReview, activeSocial, imgSrc, i, curr, another, j;
                offset = Math.floor(event.page.size/2);
                if ( !event.item.index ){
                    activeItem = items[offset];
                }
                else{
                    activeItem = items[event.item.index + offset];
                }
                if ( !activeItem ){
                    return;
                }
                activeReview = $('.review',activeItem);
                activeSocial = activeReview.data('social-type');
                for (i=0;i<self.stateVariantsArr.length;i++){
                    curr = self.stateVariantsArr[i];
                    if (curr.dataset.variant == activeSocial){
                        curr.classList.add("active");
                        another = self.stateVariantsArr.slice();
                        another.splice(i,1);
                        for (j=0;j<another.length;j++){
                            currAnother = another[j];
                            if (currAnother.classList.contains("active")){
                                currAnother.classList.remove("active");
                                break;
                            }
                        }
                        break;
                    }
                }
                // debugger
            }        
        });
    },
    this.alignReviewsSize = function (){
        var maxHeight = 0;
        var items = $(this.sliderEl).find('.review');
        items.each(function(){
            if ( this.offsetHeight > maxHeight ){
                maxHeight = this.clientHeight;
            }
        });
        items.css({
            height: maxHeight + 'px'
        });        
    }
    this.attachSlider();
}