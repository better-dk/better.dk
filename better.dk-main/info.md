test id: 1508999

#   Data base configuration:
*   /META-INF/context.xml

#   All application properties:
*   /src/main/resources/{/development/staging/production}

#   Run with development profile:
*   JVM command line options: -Dspring.profiles.active="development"


#   Port forwarding for correct work on local machine:
*   sudo iptables -t nat -A OUTPUT -o lo -p tcp --dport 80 -j REDIRECT --to-port 8080

#   Star rating page:
*   /WEB-INF/views/company/profile/review-landing.jsp
*   http://localhost:8080/p/{companyId}/bedoem

#   Review services list page:
*   /WEB-INF/views/company/controlpanel/reviews/index.jsp
*   http://localhost:8080/cp/company/{companyId}/reviews



#   Needs revert:
*   application/util/GeneralUtils.java - uncomment lines from 29 to 33;


CREATE TABLE public.review_sites (
  id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('review_sites_id_seq'::regclass),
  name CHARACTER VARYING(255) NOT NULL,
  css CHARACTER VARYING(100)
);
CREATE UNIQUE INDEX review_sites_id_uindex ON review_sites USING BTREE (id);


CREATE TABLE public.predefined_sites_link (
  id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('predefined_sites_link_id_seq'::regclass),
  company_id INTEGER,
  site_id INTEGER,
  link CHARACTER VARYING(255),
  active BOOLEAN DEFAULT false
);
CREATE UNIQUE INDEX predefined_sites_link_id_uindex ON predefined_sites_link USING BTREE (id);


CREATE TABLE public.user_defined_sites_link (
  id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('user_defined_sites_link_id_seq'::regclass),
  company_id INTEGER,
  name CHARACTER VARYING(255),
  link CHARACTER VARYING(255),
  active BOOLEAN,
  css CHARACTER VARYING(100) NOT NULL DEFAULT 'undefined'::character varying
);
CREATE UNIQUE INDEX user_defined_sites_link_id_uindex ON user_defined_sites_link USING BTREE (id);


testId 1094907


